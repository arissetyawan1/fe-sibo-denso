export const appConfig = {
  apiUrl: process.env.REACT_APP_API_URL || "https://eos-backend-stag.k3s.bangun-kreatif.com/api/v1",
  imageUrl: process.env.REACT_APP_IMAGE_URL || "https://etapasbar-file.app.bangun-kreatif.com"
};
