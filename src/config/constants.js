export const constants = {
    ROLES: {
        ADMIN: "ef8c22d0-65f2-4585-9d65-11ccd0c0e392",
        DRIVER: "5144acf7-4895-4b6d-9a29-15f252fd400d",
        CO_DRIVER: "d5e1a7f1-245d-4f9e-8ab1-2c1f02453dc2",
        BOD: "bdbfdfed-bd8f-442a-ab02-d5d026a5272a",
        DIRUT: "e1c4a403-04ac-487f-9f9f-cdc062d4e666",
        MARKETING: "a076d385-32b9-45d4-b09a-290bbedd839f",
        HEAD: "3c2661db-6db2-4a91-ae95-4b813a978b13",
        STAFF: "f3de4b1d-cafb-42b0-bf48-12952257b3f2",
        OPERASIONAL: "6f34321a-3e5b-4cce-a119-fb97d03587c6",
        ADMIN_POOL: "2d36401a-6b19-4dc8-9a41-1a6c3790294a",
        STAFF_ADMIN: "4d29cc58-e832-4ad8-85f5-a57665be6d92",
        HOSPITAL: "069d6207-9b78-46fe-a6ac-8022f2d0c7e4",
    },
    DOCUMENT_TYPE: {
        STNK: "bb173593-5fa0-4d9a-afb7-aa032dbea328",
        KIR: "9698590b-3d7a-401c-a2fa-806497f8924f",
        RECOMMEND: "08f57a8e-350d-4b4a-943c-a6030b230d65",
        DIRJEN: "ca81660f-54d6-4afd-9df7-6089ffacc522",
    },
    COST_TYPES: {
        FUEL: "dbeae696-4a33-4a63-9db3-6a369f40d69d",
        FOOD: "a7d34596-8b42-4a89-9a18-55776cd4fd46",
        PARKING: "98c18d7f-f923-4cb8-bbe6-11d14dd7850d",
        TOLL: "4742ac32-5e76-4e84-be31-b1753800b20c",
    },
    UOM: {
        WB: "65f28135-2a5a-4a1f-8d37-b3dc269d96d2",
        KG: "a6b68629-48cb-4476-8785-0a6f2f6e3c81",
    },
};
