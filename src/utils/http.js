import superagent from "superagent";
import { appConfig } from "../config/app";
import { store } from "./useStores";
import { action, observable } from "mobx";
type HttpOption = {
  raw?: boolean,
};

export class http {
  token = "";
  baseUrl = "";
  config = {
    apiUrl: "",
    imageUrl: "",
  };
  @observable dataResponUpload = "";

  constructor(context, config = appConfig) {
    this.context = context;
    this.baseUrl = config.apiUrl;
    this.token = config;
    this.config = config;
  }

  setToken(token) {
    this.token = token;
  }

  checkStatus(result) {
    if (!result.ok) {
      return result.json().then((data) => {
        return Promise.reject(data);
      });
    }
    return result;
  }

  raw(url, options = (HttpOption = {})) {
    let headers = new Headers();
    console.log(url, options, "raw http request");
    return fetch(url, { headers: {} })
      .then((response) => {
        if (options.raw) {
          return response;
        }

        return response.json();
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  }

  async checkToken() {
    const { token } = this;
    if (!token) {
      this.token = this.context.token;
    }
    // if (!token) {
    //   this.token = await get("token");
    // }

    return this.token;
  }
  get = (url, opts = {}) => {
    let apiUrl = opts?.apiUrl ? opts.apiUrl + url : appConfig.apiUrl + url;
    let q = superagent.get(apiUrl);
    if (store.token) {
      q = q.set({
        Authorization: "Bearer " + store.token,
      });
    }
    return q;
  };
  // post = (url, data, options: HttpOption = {}) => {
  //     let apiUrl = options?.apiUrl ? options.apiUrl + url : appConfig.apiUrl + url;
  //     let headers = new Headers();
  //     if (store.token) {
  //         headers.append("Authorization", 'Bearer ' + store.token);
  //         headers.append("Content-Type", "application/json");
  //     }
  //     return fetch(this.baseUrl + url, Object.assign({
  //         method: 'POST',
  //         headers,
  //         body: JSON.stringify(data)
  //     }, options))
  //         .then(this.checkStatus)
  //         .then(response => {
  //             if (options.raw) {
  //                 return response;
  //             }

  //             return response.json()
  //         })
  //         .catch(err => {
  //             console.log(err, "pusiiing");
  //             throw err;
  //         })
  // }
  async post(url, data, options: HttpOption = {}) {
    await this.checkToken();
    // let headers = new Headers();
    // headers.append("Authorization", ` Bearer ${this.token}`);
    // headers.append("Content-Type", "application/json");
    // console.log("called this bruh", this.token);
    // return fetch(
    //   this.baseUrl + url,
    //   Object.assign(
    //     {
    //       method: "POST",
    //       headers,
    //       body: JSON.stringify(data),
    //     },
    //     options
    //   )
    // )
    //   .then(this.checkStatus)
    //   .then((response) => {
    //     if (options.raw) {
    //       return response;
    //     }

    //     return response.json();
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     throw err;
    //   });
    let apiUrl = options?.apiUrl ? options.apiUrl + url : appConfig.apiUrl + url;
    let q = superagent.post(apiUrl).send(data);
    if (store.token) {
      q = q.set({
        Authorization: "Bearer " + store.token,
      });
    }
    return q;
  }
  put = (url, data, opts) => {
    let apiUrl = opts?.apiUrl ? opts.apiUrl + url : appConfig.apiUrl + url;
    let q = superagent.put(apiUrl).send(data);
    if (store.token) {
      q = q.set({
        Authorization: "Bearer " + store.token,
      });
    }
    return q;
  };
  del = (url, opts) => {
    let apiUrl = opts?.apiUrl ? opts.apiUrl + url : appConfig.apiUrl + url;
    let q = superagent.del(apiUrl);
    if (store.token) {
      q = q.set({
        Authorization: "Bearer " + store.token,
      });
    }
    return q;
  };
  upload = (file, options: HttpOption = {}) => {
    const request = superagent
      .post(appConfig.apiUrl + "/v1/files")
      .attach("file", file);

    return request;
  };
  uploadAntd = (args) => {
    const file = args.file;
    const request = http.upload(file);
    request
      .on("progress", (event) => {
        args.onProgress(event);
      })
      .then((it) => {
        args.onSuccess(it);
      })
      .catch((err) => {
        args.onError(err);
      });

    return request;
  };
  @action
  uploadAntdOperasional = (args) => {
    const file = args.file;
    const request = superagent
      .post(appConfig.apiUrl + "/v1/files")
      .attach("file", file)
      .then((res) => {
        this.dataResponUpload = res.body.message;
        return res;
      });

    return request;
  };
}
