import { action, observable } from 'mobx';
import * as firebase from "firebase/app";
import Config from "../config";
import { http } from "../utils/http";
import * as qs from "querystring";
import { BaseStore } from './base_store';

export class UserStore extends BaseStore {
    constructor(props) {
        super(props)
        this.http = new http();
        this.url = '/user-service/api/v1/users'
    }

    @observable user = {
        name: null,
        email: null,
        isRegistered: false,
        isAnonymous: true
    };
    
    baseUrl = Config.isUsingFirebaseAsDatabase ? 'users/' : '';
    @observable data = [];
    @observable maxLength = 0;
    @observable dataProfil = [];
    @observable filterData = [];
    @observable query = {
        page :1,
        per_page:10
    };
    
    @action
    getAll() {
      return this.http.get(this.url + '?'+ qs.stringify(this.query)).then((res) => {
        this.data = res.body.data;
        this.maxLength = res.body.max;
        this.isLoading = false;
      });
    }
    @action
    initUser = async () => {
        this.data = [];
        return firebase.firestore().collection(this.baseUrl).get().then((querySnapshot) => {
            const data = [];
            querySnapshot.forEach((doc) => {
                console.log(`${doc.id} => ${doc.data()}`);
                data.push({ id: doc.id, data: doc.data() });
            });
            this.data = data
        });
    };

    @action 
    getUserID(id) {
        this.isLoading = true
        return this.http
            .get("/user-service/api/v1/users/" + id)
            .then((res) => {
                this.isLoading = false
                this.dataProfil = res.body.message
                return res;
            })
            .catch((err) => {
                this.isLoading = false;
                throw err;
            });
    }

    @action
    initFirebase() {
        this.user = JSON.parse(localStorage.getItem('user_profile')) || undefined;

        if (!this.user) {
            localStorage.setItem('user_profile', JSON.stringify(this.user));
        }

        firebase.auth().onAuthStateChanged(function (user) {
            console.log({ user }, "firebase.auth().onAuthStateChanged start");
            if (user) {
                // User is signed in.
                let displayName = user.displayName;
                let email = user.email;
                let emailVerified = user.emailVerified;
                let photoURL = user.photoURL;
                let uid = user.uid;
                let phoneNumber = user.phoneNumber;
                let providerData = user.providerData;
                user.getIdToken().then(function (accessToken) {
                    console.log({ accessToken }, "user.getIdToken() start");
                    // document.getElementById('sign-in-status').textContent = 'Signed in';
                    // document.getElementById('sign-in').textContent = 'Sign out';
                    // document.getElementById('account-details').textContent = JSON.stringify({
                    //     displayName: displayName,
                    //     email: email,
                    //     emailVerified: emailVerified,
                    //     phoneNumber: phoneNumber,
                    //     photoURL: photoURL,
                    //     uid: uid,
                    //     accessToken: accessToken,
                    //     providerData: providerData
                    // }, null, '  ');
                });
            } else {
                // User is signed out.
                console.log('User is signed out');
                // document.getElementById('sign-in-status').textContent = 'Signed out';
                // document.getElementById('sign-in').textContent = 'Sign in';
                // document.getElementById('account-details').textContent = 'null';
            }
        }, function (error) {
            console.log(error);
        });
    };

    // @action
    // async getAll() {
    //     const response = await this.http.get('/user-service/api/v1/users');
    //     this.data = response.body.data;
    //     this.filterData = response.body.data;
    // }

    @action
    createData = (data) => {
        this.url = "/user-service/api/v1/users";
        console.log(data, 'its buying')
        return this.create(data)
            .finally(() => {
                this.getAll();
            });
        // const response = await this.http.post('/api/v1/users').send(data);

    }
    @action
    update(id, data) {
      console.log(id, 'id update')
      console.log(data, 'data update')
      return this.http
        .put("/user-service/api/v1/users/" + id).send(data)
        .then((res) => {
          this.isLoading = false;
          this.getAll();
          return res;
        })
        .catch((err) => {
          this.isLoading = false;
          throw err;
        });
    }
    // @action
    // async update(id, data) {
    //     const response = await this.http.put('/user-service/api/v1/users/' + id).send(data);
    //     await this.getAll();
    // }
    @action
    checkUser(data) {
      return this.http
        .post("/user-service/api/v1/users/check", data )
        .then((res) => {
          this.isLoading = false;
          this.check = res.message
          return res;
        })
        .catch((err) => {
          this.isLoading = false;
          throw err;
        });
    }

    
    @action
    async delete(id) {
        const response = await this.http.del('/user-service/api/v1/users/' + id);
        await this.getAll();
    }
}
