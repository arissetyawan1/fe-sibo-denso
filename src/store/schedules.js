import {action, observable} from 'mobx'
import {BaseStore} from "./base_store";

export class ScheduleStore extends BaseStore {
    constructor(props) {
        super(props);
        this.url = "/schedules";
    }
}