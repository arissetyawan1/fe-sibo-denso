import { action, observable } from "mobx";

export class DivisionStore {
  @observable listData = [];
  constructor(context) {
    this.context = context;
    this.url = "/division";
    this.http = context.http;
  }

  @action
  getAll() {
    return this.http
      .get(this.url)
      .then((response) => {
        console.log(response.body.results, " = response");
        this.listData = response.body.results || [];
      })
      .catch((err) => {
        console.log(err, "ini pesan error");
        throw err;
      });
  }
}
