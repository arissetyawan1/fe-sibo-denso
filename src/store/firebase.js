import {action, observable} from 'mobx';
import * as firebase from "firebase/app";
import "firebase/analytics";
import "firebase/auth";
import "firebase/firestore";
import "firebase/database";
import "firebase/functions";
import "firebase/performance";


export class FirebaseStore {

    baseUrl = '';
    firebaseInstance = {};
    firebaseAnalytic = {};
    firebaseFirestore = {};
    firebaseConfig = {
        apiKey: "AIzaSyC2KWKlWhN-JuralTcq64C0ahTNJYKrRQ4",
        authDomain: "cariparkir-dashboard.firebaseapp.com",
        databaseURL: "https://cariparkir-dashboard.firebaseio.com",
        projectId: "cariparkir-dashboard",
        storageBucket: "cariparkir-dashboard.appspot.com",
        messagingSenderId: "229041193404",
        appId: "1:229041193404:web:2835d682fcea217464ebb9",
        measurementId: "G-VZ2Z3F9SBH"
    };

    @observable data = [];
    @observable performance = {};
    @observable user = {
        name: null,
        email: null,
        isRegistered: false,
        isAnonymous: true
    };

    constructor(context) {
        this.context = context;
        this.firebaseInstance = firebase.initializeApp(this.firebaseConfig);
        this.firebaseAnalytic = firebase.analytics();
        this.firebaseFirestore = firebase.firestore();
        this.firebaseFirestore.enablePersistence()
            .catch(function(err) {
                if (err.code == 'failed-precondition') {
                    // Multiple tabs open, persistence can only be enabled
                    // in one tab at a a time.
                    // ...
                } else if (err.code == 'unimplemented') {
                    // The current browser does not support all of the
                    // features required to enable persistence
                    // ...
                }
            });
// Subsequent queries will use persistence, if it was enabled successfully

        this.performance = firebase.performance();
    }

    @action
    initFirebase() {
        this.user =  JSON.parse(localStorage.getItem('user_profile')) || undefined;
        if (!this.user) {
            localStorage.setItem('user_profile', JSON.stringify(this.user));
        }

        firebase.auth().onAuthStateChanged(function(user) {
            console.log({user}, "firebase.auth().onAuthStateChanged start");
            if (user) {
                // User is signed in.
                let displayName = user.displayName;
                let email = user.email;
                let emailVerified = user.emailVerified;
                let photoURL = user.photoURL;
                let uid = user.uid;
                let phoneNumber = user.phoneNumber;
                let providerData = user.providerData;
                user.getIdToken().then(function(accessToken) {
                    console.log({accessToken}, "user.getIdToken() start");
                    // document.getElementById('sign-in-status').textContent = 'Signed in';
                    // document.getElementById('sign-in').textContent = 'Sign out';
                    // document.getElementById('account-details').textContent = JSON.stringify({
                    //     displayName: displayName,
                    //     email: email,
                    //     emailVerified: emailVerified,
                    //     phoneNumber: phoneNumber,
                    //     photoURL: photoURL,
                    //     uid: uid,
                    //     accessToken: accessToken,
                    //     providerData: providerData
                    // }, null, '  ');
                });
            } else {
                // User is signed out.
                console.log('User is signed out');
                // document.getElementById('sign-in-status').textContent = 'Signed out';
                // document.getElementById('sign-in').textContent = 'Sign in';
                // document.getElementById('account-details').textContent = 'null';
            }
        }, function(error) {
            console.log(error);
        });
    };

    // @action
    // getAll() {
    //     return fetch('https://api-staging.cariparkir.co.id' + this.baseUrl + '?page=1page=1')
    //         .then(res => res.json())
    //         .then(res => {
    //             this.data = res.results;
    //             return res;
    //         });
    // }
}
