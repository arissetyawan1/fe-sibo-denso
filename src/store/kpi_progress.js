import {BaseStore} from "./base_store";
import {action, observable} from "mobx";

export class KPIProgressStore extends BaseStore {
    constructor(props) {
        super(props);
        this.url = "/kpi-service/api/v1/kpi_progress";
    }
}
