import {BaseStore} from "./base_store";
import {action, observable} from "mobx";
import { queryStringBuilder } from "../utils";
import * as qs from "querystring";

export class LogStore extends BaseStore {
    @observable data = [];
    @observable filterData = [];
    @observable pageSizeLog = 10;
    @observable page = 1;
    @observable query = {
        page: 1
    };
    @observable maxPageLog = 0;
    @observable isLoading = false;

    constructor(props) {
        super(props);
        this.url = "/log-service/api/v1/log";
    }

    @action
    createData = (data) => {
        return this.create(data)
            .finally(() => {
                this.getAll();
            });
    }

    @action 
    getAllLog = () => {
        this.isLoading = true
        
        let data = this.http.get(this.url + '?'+ qs.stringify(this.query)).then(res => {
            this.isLoading = false
            this.data = res.body.data
            // this.pageSizeLog = res.body.size
            this.maxPageLog = res.body.max
        });

        return data;
    }
}
