import {action, observable} from 'mobx'
import moment from "moment";

export class CommisionStore {

    baseUrl = '/commision-service/api/v5/commisions';

    @observable data = [];
    @observable query = {
        page: 1
    };

    // @action
    // getAll() {
    //     return fetch('https://api-staging.cariparkir.co.id' + this.baseUrl + '?page=1-308')
    //         .then(res => res.json())
    //         .then(res => {
    //             this.data = res.results;
    //             return res;
    //         });
    // }

    @action
    getAll() {
        console.log({data: this.data}, "dataaa");

        const mockData = {
            status: 200,
            message: 'OK',
            elements: "1",
            pages: "1-1",
            results: [{
                id: "4e12d266-e6e6-41ba-b126-c83038d5a09f",
                name: "2W Komisi",
                applied_payments: ["GOPAY", "OVO"],
                applied_partners: ["parkiran astra", "altira business park"],
                incentive: {
                    incentive_type: 'percentage',
                    incentive_value: 3,
                },
                status: "active",
                date_start: moment(),
                date_end: "",
                date_create: moment(),
            }]
        };

        return new Promise((resolve) => {
            this.data = mockData.results;
            return resolve(mockData);
        });

        // return fetch('https://api-staging.cariparkir.co.id' + this.baseUrl + '?page=1-308')
        //     .then(res => res.json())
        //     .then(res => {
        //         this.data = res.results;
        //         return res;
        //     });
    }
}
