import { action, observable, computed } from "mobx";
import { http } from "../utils/http";
import { Authentication } from "./authentication";
import { DivisionStore } from "./division";
import { KPIMasterStore } from "./kpi_master";
import { RoleStore } from "./role";
import { UserStore } from "./user";
import { OrganizationStore } from "./organization";
import { DepartmentStore } from "./department";
import { StaffStore } from "./staff";
import { EventsStore } from "./events";
import { KPIProgressStore } from "./kpi_progress";
import { KeymetricStore } from "./keymetric";
import { OperationalDataStore } from "./OperationalData";
import { LogStore } from "./log";
import { TripSchedule } from "./trip_schedule";

export class AppState {
  @observable token = "";

  http = new http(this);
  auth = new Authentication(this);
  division = new DivisionStore(this);
  department = new DepartmentStore(this);
  staff = new StaffStore(this);
  kpiMaster = new KPIMasterStore(this);
  kpiProgress = new KPIProgressStore(this);
  role = new RoleStore(this);
  user = new UserStore(this);
  organization = new OrganizationStore(this);
  events = new EventsStore(this);
  keymetric = new KeymetricStore(this);
  operationalData = new OperationalDataStore(this);
  log = new LogStore(this);
  

  @action
  setToken(token) {
    this.http.setToken(token);
    this.token = token;
  }

  @computed
  get userData() {
    const token = localStorage.getItem("id_token");
    if (!token) {
      return {
        id: "",
        roles: "staff",
        username: "",
        email: "",
      };
    }

    let tokenData = JSON.parse(atob(token.split(".")[1]));

    return tokenData;
  }
}
