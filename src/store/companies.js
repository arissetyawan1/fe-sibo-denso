import { action, observable } from "mobx";
import { BaseStore } from "./base_store";

export class CompaniesStore {
  @observable listData = [];
  constructor(context) {
    this.context = context;
    this.http = context.http;
  }

  @action
  getAll() {
    return this.http
      .get("/company")
      .then((res) => {
        console.log(res.body.results, "ini res");
        this.listData = res.body.results || [];
      })
      .catch((e) => {
        console.log(e, "error gan");
        throw e;
      });
  }
}
