import {BaseStore} from "./base_store";
import {action, observable} from "mobx";

export class StaffStore extends BaseStore {
    @observable departId = '6d3cdf75-95e2-42f3-b7af-0aac68fd39fa';
    constructor(props) {
        super(props);
        // this.url = "/masterdata-service/api/v1/departments";
    }

    @action url() {
        return `/masterdata-service/api/v1/departments/${this.departId}/staffs`;
    }

    getStaffByDepartment(id) {
        this.isLoading = true;
        
        // const q = queryStringBuilder(query);
        // console.log(this.url, "huaaaaa")
        return this.http.get(`/masterdata-service/api/v1/departments/${id}/staffs`)
            .then(
                (res) =>{
                    this.dataStaff = res.body.data
                }, (error) =>{
                    this.dataStaff =[]
                }
            )
            .finally(res => this.isLoading = false);
    }

}
