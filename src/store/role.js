import { action, observable } from 'mobx';
import Config from "../config";
import * as firebase from "firebase";
import { http } from "../utils/http";

export class RoleStore {
    constructor() {
        this.http = new http();
    }

    baseUrl = Config.isUsingFirebaseAsDatabase ? '/auth-service/api/v1/role/' : '';
    @observable data = [];

    @action
    initRole = async () => {
        this.data = [];
        return firebase.firestore().collection(this.baseUrl).get().then((querySnapshot) => {
            const data = [];
            querySnapshot.forEach((doc) => {
                console.log(`${doc.id} => ${doc.data()}`);
                data.push({ id: doc.id, data: doc.data() });
            });
            this.data = data;
        });
    }

    @action
    async getAll() {
        // const response = await this.http.get('/auth-service/api/v1/roles');
        // this.data = response.body.data;
        // return this.data
    }

    @action
    async getMyRole() {
        // const url = "/auth-service/api/v1/roles";
        // const data = await this.http.get(url);
        // console.log(data,'bruh123123')
    }
}
