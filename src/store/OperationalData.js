import {BaseStore} from "./base_store";
import {action, observable} from "mobx";

export class OperationalDataStore extends BaseStore {

    @observable latest = [];

    constructor(props) {
        super(props);
        this.url = "/operational-service/api/v1";
    }

    @action
    getLatest() {
        this.isLoading = true;
        return this.http.get(this.url + "/latest")
            .then(res => {
                this.latest = res.body.data;
                return res;
            })
            .finally(() => this.isLoading = false);
    }
    
    @action
    async delete(id) {
        const response = await this.http.del(this.url + '/' + id);
        await this.getAll();
    }
}
