import {action, computed, observable} from 'mobx'
const TAG = "[store][transaction_draft]";

const log = (...args) => {
  console.log(TAG, ...args);
};

const dataDraft = {
  id:'',
  date:'',
  bill_no:'',
  bill_type:'',
  bill_periode:'',
  bill_status:'',
  partner_name:'',
};

const defaultStatus = {
  "status": ""
};


export class TransactionDraftStore {

  @observable formData = {};
  @observable draft = [];


  @observable isFormModalShowed = false;
  @observable isLoading = false;



  @computed
  @observable formMode = 'create';
  constructor(ctx) {
    this.ctx = ctx;
  }

  @action
  showFormModal() {
    this.isFormModalShowed = true;
  }

  @action
  hideFormModal() {
    this.isFormModalShowed = false;
  }

  @action
  onModalOk() {
    this.hideFormModal();
  }


  @action
  showCreateDraftModal() {
    this.formMode = 'create';
    this.showFormModal();
  }

  @action
  async deleteDraft() {
    const id = this.formData.id;
    this.hideFormModal()
  }



}
