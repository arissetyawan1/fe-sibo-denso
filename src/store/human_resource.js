import { BaseStore } from "./base_store";
import {observable, action, computed} from "mobx";
import {queryStringBuilder} from "../index";

export class HumanResourceStore {
  @observable listData = [];
  @observable detailData = [];
  @observable currentPage = 1;
  @observable dataPerPage = 10;

  constructor(context) {
    this.content = context;
    this.http = context.http;
    this.reqQuery = {
      sort : JSON.stringify(['-employee.created_at'])
    }
  }

  @action
  getData() {
    const q = queryStringBuilder(Object.assign({
      page: this.currentPage,
      per_page: this.dataPerPage,
    },this.reqQuery));
    return this.http
      .get(`/employee?${q}`)
      .then((res) => {
        console.log(res.body.results.data, "ini res");
        this.listData = res.body.results.data || [];
      })
      .catch((e) => {
        console.log(e, "error gan");
        throw e;
      });
  }

  @action
  getDetail(id) {
    return this.http
      .get(`/employee/id/${id}`)
      .then((res) => {
        console.log(res.body.results, "ini res");
        this.detailData = res.body.results || [];
        return res;
      })
      .catch((e) => {
        console.log(e, "error gan");
        throw e;
      });
  }

  @action
  addData(data, id) {
    return this.http
      .post("/employee", data)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        console.log(e, "error gan");
        throw e;
      });
  }

  @action
  delData(id) {
    return this.http
      .del(`/employee/${id}`)
      .then((res) => {
        return res;
      })
      .catch((e) => {
        console.log(e, "error gan");
        throw e;
      });
  }

  @action
  editData(id, data) {
    return this.http
      .put(`/employee/${id}`, data)
      .then((res) => {
        this.getData();
        return res;
      })
      .catch((e) => {
        console.log(e, "error gan");
        throw e;
      });
  }

  @computed
  get getFilteredData() {
    return this.listData && this.listData.length ? this.listData : []
  }

  @action
  getFiltered(query = null) {
    let queryString, data;
    if (query) {
       data = {
        sort: JSON.stringify(query.sort)
      }
      if (query.filter) {
        data.filter = JSON.stringify(query.filter);
      }
      queryString = queryStringBuilder(data);
    }
    return this.http.get(`/employee?${queryString}`).then(res => {
      console.log(res, 'ksksksksk')
      this.listData = res.body.results.data || [];
    }).catch(err => {
      console.log(err, 'failed get employees')
    });
  }
}
