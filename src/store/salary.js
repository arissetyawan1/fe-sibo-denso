import { action, observable } from "mobx";
import { BaseStore } from "./base_store";
import { http } from "../utils/http";

export class TotalSalary {
    constructor(context) {
        this.context = context;
        this.http = context.http;
    }

    @observable data = [];

    @action
    getAll() {
        return this.http
            .get(`/totalsalary`)
            .then((res) => {
                this.data = res.body.results.data;
                return res;
            })
            .catch((e) => {
                console.log(e, "error gan");
                throw e;
            });
    }

}
