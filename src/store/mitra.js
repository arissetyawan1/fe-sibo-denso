import { action, observable } from 'mobx'
import { http } from "../utils/http";

export class MitraStore {
    constructor() {
        this.http = new http()
    }
    baseUrl = '/user-service/api/v5/admin/usermanagement/list-mitra';

    @observable data = [];

    @observable query = {
        page: 1
    };

    @action
    async getAll() {
        const response = await this.http.get('/api/v1/partner?pageSize=1000');
        this.data = response.body.data;
    }
}
