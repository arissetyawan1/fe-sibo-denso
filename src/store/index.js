import { action, observable, computed } from "mobx";

import { TransactionStore } from "./transaction";
import { http } from "../utils/http";
import { MitraStore } from "./mitra";
import { EventsStore } from "./events";
import { TransactionDraftStore } from "./transaction_draft";
import { UserStore } from "./user";
import { FirebaseStore } from "./firebase";
import { RoleStore } from "./role";
import { Authentication } from "./authentication";
import { UiStore } from "./ui";
import { CommisionStore } from "./commision";
import { NewsStore } from "./news";
import {DashboardStore} from "./dashboard";
import { KeymetricStore } from "./keymetric";
import { DivisionStore } from "./division";
import { PositionStore } from "./position";
import { OrganizationStore } from "./organization";
import { OperationalDataStore } from "./OperationalData";
import { KPIMasterStore } from "./kpi_master";
import { KPIProgressStore } from "./kpi_progress";
import { DepartmentStore } from "./department";
import { StaffStore } from "./staff";
import { VisionMission } from "./vision_mission";
import { LogStore } from "./log";
import { FormulaStore } from "./formula";
import { AdjustMenuStore } from "./adjustmenu";
import { TripSchedule } from "./trip_schedule";
import { HumanResourceStore } from "./human_resource";
import { ClientStore } from "./client.js";
import { DivisionSIBOStore } from "./divisions";
import { ContractStore } from "./contracts";
import { ScheduleStore } from "./schedules";
import { TransporterStore } from "./transporters";
import {CompaniesStore} from "./companies";
import {PermitStore} from "./permit";

export class Store {
    http = new http(this);
    authentication = new Authentication(this);
    transaction = new TransactionStore(this);
    commision = new CommisionStore(this);
    mitra = new MitraStore(this);
    news = new NewsStore(this);
    user = new UserStore(this);
    ui = new UiStore(this);
    role = new RoleStore(this);
    firebase = new FirebaseStore(this);
    draft = new TransactionDraftStore(this);
    dashboard = new DashboardStore(this);
    keymetric = new KeymetricStore(this);
    division = new DivisionStore(this);
    position = new PositionStore(this);
    events = new EventsStore(this);
    organizations = new OrganizationStore(this);
    operationalData = new OperationalDataStore(this);
    kpiMaster = new KPIMasterStore(this);
    kpiProgress = new KPIProgressStore(this);
    department = new DepartmentStore(this);
    staff = new StaffStore(this);
    visionMission = new VisionMission(this);
    log = new LogStore(this);
    formula = new FormulaStore(this);
    adjust = new AdjustMenuStore(this);
    trip_schedule = new TripSchedule(this);
    human_resource = new HumanResourceStore(this);
    clients = new ClientStore(this);
    division_sibo = new DivisionSIBOStore(this);
    contracts = new ContractStore(this);
    schedules = new ScheduleStore(this);
    companies = new CompaniesStore(this);
    permit = new PermitStore(this)


  @observable token = "";

  @computed get isLoggedIn() {
    return !!this.token;
  }

  constructor(token) {
    this.setToken(token);
  }

  @action
  setToken(token) {
    this.token = token;
    if (token) {
      localStorage.setItem("id_token", token);
    } else {
      localStorage.removeItem("id_token");
    }
  }

  @computed
  get userData() {
    const token = localStorage.getItem("id_token");
    if (!token || token === "undefined") {
      return {
        id: "",
        roles: "staff",
        username: "",
        email: "",
      };
    }

    let tokenData = JSON.parse(atob(token.split(".")[1]));

    return tokenData;
  }
}
