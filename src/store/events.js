import { BaseStore } from "./base_store";
import { appConfig } from "../config/app";
import { action, observable } from "mobx";

export class EventsStore {
  @observable data = [];

  constructor(context) {
    // super(props);
    this.context = context;
    this.http = context.http;
    // this.url = "/events-service/api/v1";
    this.url = "/events-service/api/v1/events";
  }

  @action
  getAll() {
    return this.http
      .get("/event-detail")
      .then((response) => {
        this.data = response.body.results || [];
        console.log(response, "store");
        return response;
      })
      .catch((e) => {
        console.log(e, " Pesan error");
        throw e;
      });
  }

  @action
  insertEvent(data) {
    return this.http
      .post("/event-detail", data)
      .then((response) => {
        return response;
      })
      .catch((e) => {
        console.log(e, " pesan error");
        throw e;
      });
  }
  // eventsData(data) {
  //     return this.http.post(this.url,data)
  //         .then(res=>{
  //             console.log('ini');
  //             return res;
  //         })
  //         .catch(err=>{
  //             console.log('err');
  //             throw err;
  //         })
  // }

  // getEventsData = () => {
  //     return this.http.get("/events-service/api/v1/events");
  // }

  // @action
  // createData = (data) => {
  //     return this.create(data)
  //         .finally(() => {
  //             this.getAll();
  //         });
  // }
  // @action
  // async update(id, data) {
  //     const response = await this.http.put(this.url+ '/' + id).send(data);
  //     await this.getAll();
  // }
  // @action
  // async delete(id) {
  //     const response = await this.http.del(this.url + '/' + id);
  //     await this.getAll();
  // }
  // getEventsData() {
  //     return this.http.get(this.url)
  //         .then(res => {
  //             this.data = res.data
  //         }).catch(err => {
  //             throw err;
  //         })
  // }
}
