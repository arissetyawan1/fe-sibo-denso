import {BaseStore} from "./base_store";
import {action, observable} from "mobx";

export class DepartmentStore extends BaseStore {
    @observable divisionId = '6d3cdf75-95e2-42f3-b7af-0aac68fd39fa';
    constructor(props) {
        super(props);
        this.url = "/masterdata-service/api/v1/divisions";
    }

    @action url() {
        return `/masterdata-service/api/v1/divisions/${this.divisionId}/departments`;
    }

    getDepartmentByDivision(id) {
        this.isLoading = true;
        
        // const q = queryStringBuilder(query);

        return this.http.get(`/masterdata-service/api/v1/divisions/${this.divisionId}/departments`)
            .then(
                (res) =>{
                    this.dataDepart = res.body.data
                }, (error) =>{
                    this.dataDepart =[]
                }
            )
            .finally(res => this.isLoading = false);
    }

}
