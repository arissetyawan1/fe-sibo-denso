// import { BaseStore } from "./base_store";
import { observable, action } from "mobx";
export class PositionStore {
  @observable listData = [];
  constructor(context) {
    this.context = context;
    this.http = context.http;
    this.url = "/position";
  }
  @action
  getAll() {
    return this.http
      .get(this.url)
      .then((response) => {
        console.log(response.body.results, " data positions");
        this.listData = response.body.results || [];
      })
      .catch((error) => {
        console.log(error, " error messages");
        throw error;
      });
  }

  //   async create(data) {
  //     return this.http
  //       .post(this.url, data)
  //       .then((res) => {
  //         this.data = res;
  //       })
  //       .catch((e) => {
  //         throw e;
  //       });
  //   }
}
