import {action, computed, observable} from 'mobx'
import {appConfig} from "../config/app";

export class NewsStore {
    baseUrl = '/internal-cms-service/api/v5';

    @observable data = [];
    @observable query = {
        page: 1
    };

    constructor(context) {
        this.context = context;
    }

    @computed
    get calculatedData() {
        return this.data.map((itemOrig, index) => {
            const item = JSON.parse(JSON.stringify(itemOrig));
            // this.getDetail(item.id);
            return item;
        });
    }

    @action
    getAll() {
        return fetch(appConfig.newsApiUrl + this.baseUrl + '/info?page=1&orderBy=createdDate&direction=desc&type=N')
            .then(res => res.json())
            .then(res => {
                this.data = res.results;
                res.results.map(item => this.getDetail(item.id));
                return res;
            });
    }

    @action
    async getDetail(id) {
        return fetch(appConfig.newsApiUrl + this.baseUrl + '/info/' + id)
            .then(res => res.json())
            .then(res => {
                const index = this.data.findIndex(news => news.id === id);
                if (index >= 0) {
                    this.data[index].detail = res.results;
                    this.data[index].detail.url = appConfig.newsApiUrl + this.baseUrl + '/info/' + id;
                }
                return res;
            });
    }

}
