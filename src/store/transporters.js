import { action, observable } from 'mobx'
import { BaseStore } from "./base_store";

export class TransporterStore extends BaseStore {
    constructor(props) {
        super(props);
        this.url = "/company/list";
    }
}
