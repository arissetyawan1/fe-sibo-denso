import { BaseStore } from "./base_store";
import { action, observable } from "mobx";

export class AdjustMenuStore extends BaseStore {
    @observable data = [];
    @observable isLoading = false;

    constructor(props) {
        super(props);
        this.url = "/adjust-service/api/v1/adjust-menu";
    }

    @action
    getAllAdjust = () => {
        this.isLoading = true
        let data = this.http.get(this.url).then(res => {
            this.isLoading = false
            this.data = res.body.data
        });
        
        return data;
    }

    @action
    async updateDate(id, data) {
        const response = await this.http.put(this.url + '/' + id).send(data);
        await this.getAllAdjust();
    }
}