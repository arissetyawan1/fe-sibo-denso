import { BaseStore } from "./base_store";
import { action, observable } from "mobx";

export class FormulaStore extends BaseStore {
  @observable data = [];
  @observable filterData = [];

  constructor(props) {
    super(props);
    this.url = "/formulas-service/api/v1/formula";
  }

  @action
  getAllFormula() {
    this.isLoading = true;
    return this.http.get("/formulas-service/api/v1/formula").then((res) => {
      this.data = res.body;
      this.isLoading = false;
    });
  }

  @action
  getFormula() {
    return this.http.get("/formulas-service/api/v1/formula");
  }

  @action
  getFormulaMonth() {
    return this.http
      .get("/formulas-service/api/v1/formula?type=month")
      .then((res) => {
        this.filterData = res.body;
      });
  }

  @action
  updateFormula(id, data) {
    this.isLoading = true;
    return this.http
      .put(this.url + "/" + id)
      .send(data)
      .then((res) => {
        this.isLoading = false;
        this.getAll();
        return res;
      })
      .catch((err) => {
        this.isLoading = false;
        throw err;
      });
  }

  @action
  getFormulaGrade() {
    return this.http
      .get("/formulas-service/api/v1/formula?type=grade")
      .then((res) => {
        this.filterData = res.body;
      });
  }

  // @action
  // createData = (data) => {
  //     return this.create(data)
  //         .finally(() => {
  //             this.getAll();
  //         });
  // }
}
