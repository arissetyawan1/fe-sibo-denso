import { BaseStore } from "./base_store";

export class DivisionSIBOStore extends BaseStore {
    constructor(props) {
        super(props);
        this.url = "/divisions";
    }
}
