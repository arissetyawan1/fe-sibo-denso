import {BaseStore} from "./base_store";
import {action, observable} from "mobx";

export class VisionMission extends BaseStore {
    @observable vision = [];
    @observable mission = [];
    @observable culture = [];
    @observable filterData = [];
    
    constructor(props) {
        super(props);
        this.url = "/vision-mission/api/v1/";
    }
  
    async delete(id) {
        const response = await this.http.del(this.url + id);
        await this.getAll();
    }
    @action
    update(id, data) {
      console.log(id, 'id update')
      console.log(data, 'data update')
      return this.http
        .put(this.url + id).send(data)
        .then((res) => {
          this.isLoading = false;
          this.getAll();
          return res;
        })
        .catch((err) => {
          this.isLoading = false;
          throw err;
        });
    }
}
