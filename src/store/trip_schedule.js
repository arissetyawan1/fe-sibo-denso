import { BaseStore } from "./base_store";
import { action, observable } from "mobx";
import { add, format } from "date-fns";
import * as uuid from "uuid";
export class TripSchedule extends BaseStore {
  constructor(props) {
    super(props);
    this.url = "/schedules";
  }
}
