import { action, computed, observable } from "mobx";
import { http } from "../utils/http";
import { BaseStore } from "./base_store";

export class Authentication extends BaseStore {
  @observable data = [];

  @computed
  get userData() {
    if (!this.context.token) {
      return {
        user_id: "",
        role: "",
      };
    }
    return JSON.parse(atob(this.context.token.split(".")[1]));
  }

  constructor(context) {
    super(context);
    this.context = context;
    this.http = context.http;
    this.url = "/authentication";
  }

  @action
            login(data) {
    return this.http
      .post("/login", data)
      .then((res) => {
        this.context.http.setToken(res.body.results.accessToken);
        this.context.setToken(res.body.results.accessToken);
        localStorage.setItem("id_token", res.body.results.accessToken);
        return res;
      })
      .catch((err) => {
        console.log(err);
        // console.log(appConfig)
        throw err;
      });
  }

  @action
  logout() {
    this.context.setToken();
  }
}
