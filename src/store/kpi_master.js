import { BaseStore } from "./base_store";
import { action, observable } from "mobx";
import { queryStringBuilder } from "../utils";
import { appConfig } from "../config/app";

export class KPIMasterStore extends BaseStore {
  @observable corporates = [];
  @observable corporateKPI = [];
  @observable divisions = [];
  @observable departments = [];
  @observable staffs = [];
  @observable staffsUser = [];
  @observable filterData = [];
  @observable query = {}

  @observable queryEntryDivisi={
    page :1,
    per_page :1000
  }

  constructor(props) {
    super(props);
    this.url = "/masterdata-service/api/v1/kpi_masters";
  }

  @action
  getExportCorporate(option = {}) {
    this.isLoading = true;
    const q = queryStringBuilder({
      ...option
    })
    return this.http.get(
      this.url + '/corporates/export?' + q
    ).then((res) => {
      console.log(res, "response export");
      window.open(appConfig.apiUrl + res.body.path);
    }).finally((res) => { this.isLoading = false });

  }

  @action
  getCorporate(option = {}) {
    this.isLoading = true;

    const q = queryStringBuilder({
      // page: this.currentPage,
      // limit: this.dataPerPage,
      // ...this.query,
      ...option,
    });

    return this.http
      .get(this.url + "/corporates?" + q)
      .then((res) => {
        this.corporates = res.body.data;
        return res;
      })
      .finally((res) => (this.isLoading = false));
  }

  @action
  getCorporateKPI(option = {}) {
    this.isLoading = true;

    const q = queryStringBuilder({
      // page: this.currentPage,
      // limit: this.dataPerPage,
      // ...this.query,
      ...option,
    });

    return this.http
      .get(this.url + "/corporates?" + q)
      .then((res) => {
        this.corporateKPI = res.body.data;
        return res;
      })
      .finally((res) => (this.isLoading = false));
  }

  @action
  getDivision(query) {
    this.isLoading = true;

    const q = queryStringBuilder(query);
    const paging = queryStringBuilder(query);

    return this.http
      .get(this.url + "/divisions?&per_page=1000&" + q )
      .then((res) => {
        this.divisions = res.body.data;
        this.max = res.body.max;
        return res;
      })
      .finally((res) => (this.isLoading = false));
  }

  @action
  getExportDivisions(option = {}) {
    this.isLoading = true;
    const q = queryStringBuilder({
      ...option
    })
    return this.http.get(
      this.url + '/divisions/export?&per_page=1000&' + q
    ).then((res) => {
      console.log(res, "response export");
      window.open(appConfig.apiUrl + res.body.path);
    }).finally((res) => { this.isLoading = false });

  }

  @action
  getDepartments(query) {
    this.isLoading = true;

    const q = queryStringBuilder(query);

    return this.http
      .get(this.url + "/departments?&per_page=1000&" + q)
      .then((res) => {
        this.departments = res.body.data;
        return res;
      })
      .finally((res) => (this.isLoading = false));
  }

  @action
  getExportDepartment(option = {}) {
    this.isLoading = true;
    const q = queryStringBuilder({
      ...option
    })
    return this.http.get(
      this.url + '/departments/export?&per_page=1000&' + q
    ).then((res) => {
      console.log(res, "response export");
      window.open(appConfig.apiUrl + res.body.path);
    }).finally((res) => { this.isLoading = false });

  }

  @action
  getStaffs(query) {
    this.isLoading = true;

    const q = queryStringBuilder(query);

    return this.http
      .get(this.url + "/staffs?&per_page=1000&" + q)
      .then((res) => {
        this.staffs = res.body.data;
        return res;
      })
      .finally((res) => (this.isLoading = false));
  }
  @action
  getExportStaff(option = {}) {
    this.isLoading = true;
    const q = queryStringBuilder({
      ...option
    })
    return this.http.get(
      this.url + '/staffs/export?&per_page=1000&' + q
    ).then((res) => {
      console.log(res, "response export");
      window.open(appConfig.apiUrl + res.body.path);
    }).finally((res) => { this.isLoading = false });

  }
  @action
  getStaffsByUser(query) {
    this.isLoading = true;

    const q = queryStringBuilder(query);

    return this.http
      .get(this.url + "/kpi_user?" + q)
      .then((res) => {
        this.staffsUser = res.body.data;
        return res;
      })
      .finally((res) => (this.isLoading = false));
  }
  @action
  createData = (data) => {
    return this.create(data).finally(() => {
      this.getAll();
    });
  };
  @action
  async update(id, data) {
    const response = await this.http.put(this.url + "/" + id).send(data);
    await this.getAll();
  }
  @action 
  async updateGol13(id, data) {
    const response = await this.http.put(this.url + "/kpi13/" + id).send(data);
    await this.getAll();
  }
  @action
  async updateStatus(data) {
    const response = await this.http
      .put(this.url + "/submit/status")
      .send(data);
    await this.getAll();
  }
  @action
  async delete(id) {
    const response = await this.http.del(this.url + "/" + id);
    await this.getAll();
  }
}
