import { action, observable } from "mobx";
import { http } from "../utils/http";

export class DashboardStore {
  constructor(context) {
    this.context = context;
    this.http = context.http;
  }
  @observable detailData = [];
  @observable data = {
    income: 0,
    paidIncome: 0,
    grossVolume: 0,
  };

  @observable dataPerPeriod = {
    income: 0,
    paidIncome: 0,
    grossVolume: 0,
  };

  @observable period = "today";
  @observable salary = 0;
  @observable employee = 0;
  @observable threeMonthData = [];
  @observable oneMonthData = [];

  @action
  async getAll() {
    // const response = await http.get('/api/v1/dashboard');
    // this.data = response.body;
  }

  @action
  async getDashboardByPeriod() {
    // const response = await http.get('/api/v1/dashboard?period=' + this.period);
    // this.dataPerPeriod = response.body;
  }

  @action
  async setPeriod(period) {
    this.period = period;
    await this.getDashboardByPeriod();
  }
  @action
  getAllEmployee() {
    return this.http
      .get(`/dashboard/employee`)
      .then((res) => {
        this.employee = res.body.results.countAll || 0;
      })
      .catch((e) => {
        console.log(e, "error ganm");
        throw e;
      });
  }

  @action
  getAllSalary() {
    return this.http
      .get(`/dashboard/salary`)
      .then((res) => {
        this.salary = res.body.results.countSalary || 0;
      })
      .catch((e) => {
        console.log(e, "error ganm");
        throw e;
      });
  }

  @action
  getOneMonthDuration() {
    return this.http
      .get(`/dashboard/one-month`)
      .then((res) => {
        this.oneMonthData = res.body.results || 0;
      })
      .catch((e) => {
        console.log(e, "error ganm");
        throw e;
      });
  }

  @action
  getThreeMonthDuration() {
    return this.http
      .get(`/dashboard/three-month`)
      .then((res) => {
        this.threeMonthData = res.body.results || 0;
      })
      .catch((e) => {
        console.log(e, "error ganm");
        throw e;
      });
  }
}
