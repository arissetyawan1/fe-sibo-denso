import { action, observable } from "mobx";

export class PermitStore {
  @observable detailData = [];
  @observable listData = [];

  constructor(context) {
    this.context = context;
    this.http = context.http;
  }

  @action
  getAll() {
    return this.http
      .get("/permit")
      .then((res) => {
        this.listData = res.body.results || [];
        console.log(res, "coba liat");
        return res;
      })
      .catch((e) => {
        throw e;
      });
  }

  @action
  getDetailById(id) {
    return this.http
      .get(`/permit/id/${id}`)
      .then((res) => {
        console.log(res.body.results, " => get by id");
        this.detailData = res.body.results || [];
        return res;
      })
      .catch((err) => {
        console.log(err, "error");
        throw err;
      });
  }

  @action
  createData(data) {
    return this.http
      .post("/permit", data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error, "pesan error");
        throw error;
      });
  }
  @action
  updateData(data, id) {
    return this.http
      .put(`/permit /${id}`, data)
      .then((response) => {
        this.getAll();
        return response;
      })
      .catch((error) => {
        console.log(error, " pesan error");
        throw error;
      });
  }
  @action
  deleteContract(id) {
    return this.http
      .del(`/permit/${id}`)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        console.log(err, "error check");
        throw err;
      });
  }

  @action
  getAllEmployee() {
    return this.http
      .get(`/employee/all`)
      .then((res) => {
        this.listData = res.body.results || [];
        return res;
      })
      .catch((e) => {
        console.log(e, "error ganm");
        throw e;
      });
  }
}
