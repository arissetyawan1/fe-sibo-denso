import { action, observable, computed } from "mobx";
import { queryStringBuilder } from "../index";
import { BaseStore } from "./base_store";

export class ContractStore {
  constructor(context) {
    this.context = context;
    this.http = context.http;
    this.reqQuery = {
      sort: JSON.stringify(["-contract.created_at"]),
    };
  }

  @observable data = [];
  @observable detailData = [];
  // paging observable
  @observable currentPage = 1;
  @observable dataPerPage = 10;

  @action
  getAll(query = {}) {
    let filter = "";
    if (query.month) {
      filter = `month=${query.month}`;
    }
    return this.http
      .get(`/contract?${filter}`)
      .then({ ...query })
      .then((res) => {
        console.log(res.body.results, "ini res");
        this.data = res.body.results || [];
      })
      .catch((e) => {
        console.log(e, "error gan");
        throw e;
      });
  }
  @action
  getDetailById(id) {
    return this.http
      .get(`/contract/id/${id}`)
      .then((res) => {
        console.log(res.body.results, " => get by id");
        this.detailData = res.body.results || [];
        return res;
      })
      .catch((err) => {
        console.log(err, "error");
        throw err;
      });
  }
  @action
  createData(data) {
    return this.http
      .post("/contract", data)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        console.log(error, "pesan error");
        throw error;
      });
  }
  @action
  updateData(data, id) {
    return this.http
      .put(`/contract/${id}`, data)
      .then((response) => {
        this.getAll();
        return response;
      })
      .catch((error) => {
        console.log(error, " pesan error");
        throw error;
      });
  }
  @action
  deleteContract(id) {
    return this.http
      .del(`/contract/${id}`)
      .then((response) => {
        return response;
      })
      .catch((err) => {
        console.log(err, "error check");
        throw err;
      });
  }

  // filter feature
  @computed
  get getFilteredData() {
    return this.data && this.data.length ? this.data : [];
  }
  @action
  getFiltered(query = null) {
    let queryString, data;
    if (query) {
      data = {
        sort: JSON.stringify(query.sort),
      };
      if (query.filter) {
        data.filter = JSON.stringify(query.filter);
      }
      queryString = queryStringBuilder(data);
    }
    return this.http
      .get(`/contract?${queryString}`)
      .then((res) => {
        console.log(res, "ksksksksk");
        this.data = res.body.results || [];
      })
      .catch((err) => {
        console.log(err, "failed get employees");
      });
  }
}
