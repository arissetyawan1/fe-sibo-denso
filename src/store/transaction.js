import { action, computed, observable } from 'mobx'
import { http } from "../utils/http";
import * as qs from "querystring";

export class TransactionStore {
    baseUrl = '/api/v1/transactions';

    @observable data = [];
    @observable query = {
        page: 1,
        limit: 100
    };

    constructor(context) {
        this.context = context;
        this.http = new http()
    }

    @computed
    get calculatedData() {
        return this.data.map((itemOrig, index) => {
            const item = JSON.parse(JSON.stringify(itemOrig));

            item.commision = 0;
            item.commisiontax = 0;

            let parkingName = item.parking_name.toLowerCase();
            let dataFound = this.context.commision.data.find((item) => {
                return item.applied_partners.find((item) => item.toLowerCase() === parkingName);
            })

            // console.log(parkingName, dataFound, "dataFound");

            if (dataFound) {
                item.commision = item.total * dataFound.incentive.incentive_value / 100;
                item.commisiontax = item.commision * 3 / 100;
            };

            return item;
        })
    }

    @action
    getAll() {
        return this.http.get(this.baseUrl + '?' + qs.stringify(this.query))
            .then(res => {
                console.log({ res }, 'TransactionStore -> ')
                this.data = res.body.results;
                return res;
            });
    }
}
