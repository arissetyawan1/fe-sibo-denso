import { BaseStore } from "./base_store";
import { action, observable } from "mobx";

export class KeymetricStore extends BaseStore {
  @observable data = [];
  @observable dataLatest = '';
  @observable traffic = [];
  @observable allKeyMetrics = [];
  @observable latestKeymetrics = [];

  constructor(props) {
    super(props);
    this.url = "/keymetric-service/api/v1/keymetrics";
  }

  @action
  createRaw(data) {
    this.url = "/keymetric-service/api/v1/keymetrics/raw";
    console.log(data, "isi data apa?");
    return this.create(data).finally(() => {
      this.url = "/keymetric-service/api/v1/keymetrics";
    });
  } 
  @action
  getAllKM() {
    // return this.http.get("/keymetric-service/api/v1/keymetrics/rawAll").then(
    //   (res) => {
    //     this.allKeyMetrics = res.body;
    //   },
    //   (error) => {
    //     this.allKeyMetrics = [];
    //   }
    // );
  }
  @action
  getKMLatest() {
    // return this.http
    //   .get("/keymetric-service/api/v1/keymetrics/latest")
    //   .then((res) => {
    //     this.dataLatest = res.body.data;

    //     return res.body.data
    //   })
    //   .catch((err) => {
    //     console.log("error on get data ", err)
    //   });
    }
  @action
  getAllKMS() {
    // this.http
    //   .get(
    //     "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_daily_revenue"
    //   )
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get(
    //     "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_daily_traffic"
    //   )
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get("/keymetric-service/api/v1/keymetrics/raw?type=keymetric_avkt")
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get("/keymetric-service/api/v1/keymetrics/raw?type=keymetric_fre")
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get("/keymetric-service/api/v1/keymetrics/raw?type=keymetric_accident")
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get(
    //     "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_traffic_classification"
    //   )
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get(
    //     "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_financial_summary"
    //   )
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get("/keymetric-service/api/v1/keymetrics/raw?type=keymetric_cashflow")
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
    // this.http
    //   .get(
    //     "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_balance_sheet"
    //   )
    //   .then((res) => {
    //     this.latestKeymetrics = [...this.latestKeymetrics, res.body];
    //   });
  }

  @action
  async getDailyRevenue(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_daily_revenue&year=" + year
    // );
  }

  @action
  getDailyTraffic(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_daily_traffic&year=" + year
    // );
  }

  @action
  getAVKT(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_avkt&year=" + year
    // );
  }

  @action
  getFRE(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_fre&year=" + year
    // );
  }

  @action
  getAccident(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_accident&year=" + year
    // );
  }

  @action
  getClassification(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_traffic_classification&year=" + year
    // );
  }

  @action
  getFinancialSummary(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_financial_summary&year=" + year
    // );
  }

  @action
  getCashflow(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_cashflow&year=" + year
    // );
  }

  @action
  getBalanceSheet(year) {
    // return this.http.get(
    //   "/keymetric-service/api/v1/keymetrics/raw?type=keymetric_balance_sheet&year=" + year
    // );
  }
}
