import { action, observable } from "mobx";
import { BaseStore } from "./base_store";
import { http } from "../utils/http";

export class ClientStore extends BaseStore {

  constructor(props) {
    super(props);
    this.url = "/hospitals";
  }
}
