import { BaseStore } from "./base_store";
import { action, observable } from "mobx";
import {queryStringBuilder} from "../utils";

export class OrganizationStore extends BaseStore {
    @observable isLoading = false;
    constructor(props) {
        super(props);
        this.url = "/masterdata-service/api/v1/organization";
    }

    @observable filterData = []
    @observable dataDepart =[]
    @observable dataStaff =[]
    @observable  dataPositions =[]
    @observable  organizationAll =[]

    @action
    getChilds(id) {
        this.isLoading = true;
        
        // const q = queryStringBuilder(query);

        return this.http.get(`${this.url}/${id}/childs`)
            .then(
                (res) =>{
                    this.dataDepart = res.body.data
                }, (error) =>{
                    this.dataDepart =[]
                }
            )
            .finally(res => this.isLoading = false);
    }
    @action
    // getChildsStaff(id) {
    //     this.isLoading = true;
        
    //     // const q = queryStringBuilder(query);

    //     return this.http.get(`${this.url}/${id}/childs`)
    //         .then(
    //             (res) =>{
    //                 this.dataStaff = res.body.data
    //             }, (error) =>{
    //                 this.dataStaff =[]
    //             }
    //         )
    //         .finally(res => this.isLoading = false);
    // }
    // @action
    getChildsDepart(id) {
        this.isLoading = true;
        
        // const q = queryStringBuilder(query);

        return this.http.get(`${this.url}/${id}/childs`)
        .then(
            (res) =>{
                this.dataPositions = res.body.data
            }, (error) =>{
                this.dataPositions =[]
            }
        )
        .finally(res => this.isLoading = false);
          
    }

    @action
    update(id, data) {
      console.log(id, 'id update')
      console.log(data, 'data update')
      return this.http
        .put(this.url+'/' + id).send(data)
        .then((res) => {
          this.isLoading = false;
          this.getAll();
          return res;
        })
        .catch((err) => {
          this.isLoading = false;
          throw err;
        });
    }
    @action
    async delete(id) {
        console.log(id, "idnya")
        const response = await this.http.del("/masterdata-service/api/v1/organization/" + id);
        await this.getAll();
    }

    @action
    getAllOrganization() {
        this.isLoading = true;
        
        // const q = queryStringBuilder(query);

        return this.http.get(`${this.url}/all-organization?per_page=1000`)
            .then(
                (res) =>{
                    this.organizationAll = res.body.data
                }, (error) =>{
                    this.organizationAll =[]
                }
            )
            .finally(res => this.isLoading = false);
    }
}
