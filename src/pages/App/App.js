import React, { useState, useEffect } from "react";
import { AppRoutes } from "../../routes/app";
import {
  Layout,
  Menu,
  Avatar,
  Input,
  Button,
  Typography,
  Divider,
  message,
  Popover,
  Card,
  PageHeader,
  Breadcrumb,
  Drawer,
  Radio,
  Space,
  Image,
} from "antd";
import { createUseStyles } from "react-jss";
import "bootstrap/dist/css/bootstrap.min.css";
import {
  Link,
  useHistory,
  useLocation,
  useParams,
  useRouteMatch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import logoI from "../../assets/images/unnamed.png";
import parkirLogo from "../../assets/images/parkir.png";
import parkirLogoFull from "../../assets/images/astra.png";
import densoLogo from "../../assets/images/denso-logo.png";
import MediaQuery, { useMediaQuery } from "react-responsive";
import { UserOutlined } from "@ant-design/icons";
import {
  ReconciliationOutlined,
  SettingOutlined,
  HomeOutlined,
  BarChartOutlined,
  MailOutlined,
  FileProtectOutlined,
  DollarOutlined,
  PaperClipOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import { observer } from "mobx-react-lite";
import { keys } from "mobx";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
import { useStore } from "../../utils/useStores";
import { MenuPartner } from "./Menu-Partner";
import { MenuSuperAdministrator } from "./Menu-SuperAdministrator";
import { MenuAdmin } from "./Menu-Admin";
import { MenuStaff } from "./Menu-Staff";
import { BreadcrumbComponent } from "../../component/Breadcrumb";
import MenuOutlined from "@ant-design/icons/lib/icons/MenuOutlined";
import { AddBillModal } from "../Report/Bill/Modal/Add-Bill";
import { AddInvoiceModal } from "../Report/Invoice/Modal/Add-Invoice";

const { Search } = Input;
const { Text, Paragraph } = Typography;
const { Header, Content, Sider } = Layout;

const { SubMenu } = Menu;
const rootSubmenuKeys = ["sub1", "sub2", "sub4"];

export const App = observer((props) => {
  let history = useHistory();
  const store = useStore();
  const [user, setUser] = useState({ email: "" });
  const [password, setPassword] = useState({ pass: "" });
  let location = useLocation();
  const xxl = useMediaQuery({ minDeviceWidth: 1600 });
  const xl = useMediaQuery({ minWidth: 1024 });
  const mediaQuery = {
    isBigScreen: useMediaQuery({ query: "(min-device-width: 1824px)" }),
    isDesktop: useMediaQuery({ minWidth: 1024 }),
    isTablet: useMediaQuery({ minWidth: 768, maxWidth: 1023 }),
    isMobile: useMediaQuery({ maxWidth: 767 }),
    isNotMobile: useMediaQuery({ minWidth: 768 }),
  };

  const useStyles = createUseStyles({
    logo: {
      // height: 30,
      width: 200,
      // paddingLeft: 20,
      marginRight: 20,
    },
    logoFull: {
      //height: 32,
      // background: '#ffc822',
      borderRadius: 4,
      padding: 2,
      width: mediaQuery.isDesktop ? 145 : null,
      position: "relative",
      marginTop: mediaQuery.isDesktop ? "-10.5px" : null,
    },
    hideSidebarButton: `SettingOutlined 
        font-size: 18px;
    padding: 0 24px;
    cursor: pointer;
     color: #fff;
    transition: color 0.3s;
    `,
    hideSidebarButtonHovered: { "&:hover": `color: #fff;` },
    sideFixed: {
      overflow: "auto",
      height: "100vh",
      position: "fixed",
      left: 0,
      zIndex: 99,
    },
    containFixed: {
      overflow: "auto",
      height: "100vh",
      position: "fixed",
      top: 48,
      width: "100vw",
    },
  });

  const buttonProfile = (
    <Button
      size={"default"}
      type={store.ui.mediaQuery.isDesktop ? "" : "link"}
      style={{ marginRight: store.ui.mediaQuery.isDesktop ? 20 : 10 }}
      icon={
        store.ui.mediaQuery.isDesktop ? (
          <UserOutlined style={{ fontSize: "13px" }} />
        ) : (
          <UserOutlined style={{ fontSize: "13px", color: "#5b5b5b" }} />
        )
      }
    />
  );
  const buttonQuickAction = (
    <Button
      onClick={(e) => e.preventDefault()}
      type={"primary"}
      size={"default"}
      style={{ marginRight: 10 }}
      icon={
        store.ui.mediaQuery.isDesktop ? (
          <PlusOutlined style={{ fontSize: "13px" }} />
        ) : (
          <PlusOutlined style={{ fontSize: "13px", color: "#5b5b5b" }} />
        )
      }
    />
  );
  const onClick = ({ key }) => {
    message.info(`Click on item ${key}`);
  };

  const [collapsed, setCollapsed] = useState(false);
  const [visible, setVisible] = useState(false);
  const [drawerIsShown, setDrawerIsShown] = useState(false);
  const [clicked, setClicked] = useState(false);
  const [clickedQuickAction, setClickedQuickAction] = useState(false);
  const [setKeys, setSetKeys] = useState(["dashboard"]);
  const [openSubKeys, setOpenSubKeys] = useState([""]);
  const [openKeys, setOpenKeys] = useState(["sub4"]);
  useEffect(() => {
    store.ui.setMediaQuery(mediaQuery);

    async function init() {
      await store.authentication.login();
      await store.user.initUser();
    }

    store.role.getMyRole().then((res) => {
      console.log(store.userData, "this is user data");
    });
  });
  const classes = useStyles();
  const profile = {
    name: "Erwin Yudha Pratama",
    role: 0,
  };
  const logo = require("../../assets/images/denso-logo.png");
  const profilePopover = (
    <Popover
      className={store.ui.mediaQuery.isDesktop ? "shadow" : null}
      autoAdjustOverflow={true}
      placement="bottomRight"
      content={
        <Menu
          type={"line"}
          inlineIndent={0}
          theme="light"
          style={{
            backgroundColor: "transparent",
            borderRightWidth: 0,
          }}
          mode="inline"
        >
          <Menu.Item>
            <Link to="/app/profile">
              <span>Profile</span>
            </Link>
          </Menu.Item>
          <Menu.Item
            onClick={() => {
              store.authentication.logout();
              return history.push("/login");
            }}
          >
            <span>Sign out</span>
          </Menu.Item>
        </Menu>
      }
      title={
        <Text>
          {store.userData.email}{" "}
          <Paragraph style={{ fontWeight: 400 }} type={"secondary-dark"}>
            {store.userData.role}
          </Paragraph>
        </Text>
      }
      trigger="click"
      visible={clicked}
      onVisibleChange={() => setClicked(!clicked)}
    >
      {buttonProfile}
    </Popover>
  );

  const opened = (openKeys) => {
    const latestOpenKey = openKeys[0];
    if (rootSubmenuKeys.indexOf(openKeys) === -1) {
      setOpenKeys(openKeys);
    } else {
      setOpenKeys(openKeys + ":" + latestOpenKey ? [latestOpenKey] : []);
    }
  };

  console.log({ history }, "App Start");
  store.ui.pushRoute(history.location.pathname);
  const hide = () => {
    setVisible(false);
  };
  const handleVisibleChange = (visible) => {
    setVisible(visible);
  };

  const menuAction = (
    <Menu
      inlineIndent={0}
      theme="light"
      style={{
        backgroundColor: "transparent",
        borderRightWidth: 0,
        color: "#196bb1",
      }}
      onClick={hide}
      mode="inline"
    >
      <Menu.Item disabled={true}>Cash in</Menu.Item>
      <Menu.Item>
        <AddInvoiceModal>Invoice</AddInvoiceModal>
      </Menu.Item>
      <Menu.Divider style={{ paddingTop: 0, marginBottom: 12 }} />
      <Menu.Item disabled={true}>Cash out</Menu.Item>
      <Menu.Item>
        <AddBillModal>Bill</AddBillModal>
      </Menu.Item>
      <Menu.Item>Deposit</Menu.Item>
    </Menu>
  );

  return (
    <Layout
      theme={"light"}
      className={"transparent"}
      hasSider={store.ui.mediaQuery.isDesktop}
      style={{
        paddingLeft: mediaQuery.isBigScreen ? "calc(50vw - 695px)" : "0",
        display: "flex",
        // minWidth: 1024,
        width: "100%",
        height: "100%",
        position: "relative",
      }}
    >
      {store.ui.mediaQuery.isDesktop && (
        <Sider
          className={"transparent"}
          width={240}
          style={{
            overflowX: "hidden",
            bottom: 0,
            justifyContent: "flex-start",
            paddingTop: 20,
            paddingLeft: 20,
            position: "fixed",
            top: 0,
            zIndex: 10,
          }}
        >
          <div className={classes.logo}>
            <Image src={densoLogo} width={180} preview={false} />
          </div>
          {/* {store.userData.role !== "super_admin" && store.userData.role !== "BOD" && <MenuStaff />}
          {store.userData.role === "super_admin" && <MenuSuperAdministrator />}
          {store.userData.role === "BOD" && <MenuAdmin />} */}
          <MenuSuperAdministrator />
        </Sider>
      )}

      {store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet ? (
        <Drawer
          title={<Image src={densoLogo} width={150} preview={false} />}
          placement={"left"}
          closable={false}
          width={"80%"}
          onClose={() => store.ui.toggleLeftDrawerIsShown()}
          visible={store.ui.leftDrawerIsShown}
          key={"dashboard-drawer"}
          bodyStyle={{
            padding: 0,
            // paddingTop: 30,
          }}
        >
          <div
            style={{
              marginLeft: 0,
              paddingLeft: 0,
            }}
          >
            {/*{store.userData.role !== "super_admin" &&*/}
            {/*  store.userData.role !== "BOD" && <MenuStaff />}*/}
            {store.userData.role === "SuperAdmin" && <MenuSuperAdministrator />}
            {store.userData.role === "BOD" && <MenuAdmin />}
          </div>
        </Drawer>
      ) : (
        false
      )}

      <Layout
        className={"transparent"}
        style={{
          position: "relative",
          display: "block",
          paddingLeft: store.ui.mediaQuery.isDesktop ? 240 : 0,
          height: "100vh",
        }}
      >
        <Header
          theme={"light"}
          className={
            store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
              ? "shadow"
              : null
          }
          style={{
            height: store.ui.mediaQuery.isDesktop ? 54 : 65,
            // width: 'calc(100vw - 200px)',
            width: "100%",
            position: store.ui.mediaQuery.isDesktop ? "fixed" : "fixed",
            zIndex: 99,
            paddingLeft: store.ui.mediaQuery.isDesktop ? 14 : 0,
            paddingRight: store.ui.mediaQuery.isDesktop ? 14 : 0,
            backgroundColor:
              store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
                ? "#fff"
                : "transparent",
            maxWidth: store.ui.mediaQuery.isDesktop ? 1024 : 768,
            // minWidth: store.ui.mediaQuery.isDesktop ? 768 : 0,
            top:
              store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
                ? 0
                : 12,
            /**/
          }}
        >
          {store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet ? (
            <div
              className={
                store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
                  ? "shadow"
                  : null
              }
              style={{
                top: 0,
                left: 0,
                height: "100%",
                // paddingTop: 8,
                position: "relative",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "stretch",
                flexDirection: "column",
                // boxShadow: '0 7px 14px 0 rgba(60, 66, 87, 0.05), 0 3px 6px 0 rgba(0, 0, 0, 0.05)'
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                  height: "100%",
                  width: "100%",
                  paddingLeft: store.ui.mediaQuery.isMobile ? 5 : 20,
                  paddingRight: store.ui.mediaQuery.isMobile ? 0 : 20,
                  paddingBottom: 4,
                  marginTop: -5,
                }}
              >
                <Button
                  type="link"
                  icon={
                    <MenuOutlined
                      style={{
                        fontSize: "12px",
                        color: "#5b5b5b",
                      }}
                    />
                  }
                  onClick={() => store.ui.toggleLeftDrawerIsShown()}
                />
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column",
                    justifyContent: "flex-start",
                    alignItems: "stretch",
                    padding: 0,
                    margin: 0,
                    paddingTop: 5,
                  }}
                >
                  {/* <img
                    className={[classes.logoFull]}
                    style={{
                      maxWidth: store.ui.mediaQuery.isMobile ? 75 : 150,
                    }}
                    src={parkirLogoFull}
                  /> */}
                  <Paragraph
                    style={{
                      margin: 10,
                      padding: 0,
                      lineHeight: "18px",
                      fontSize: 8,
                      fontWeight: 600,
                      color: "#333",
                      textAlign: "center",
                    }}
                  >
                    {store.authentication.userData.username || "Apps"}
                  </Paragraph>
                </div>
                {/* {profilePopover} */}
              </div>
              <div
                style={{
                  borderTopWidth: 0.5,
                  borderTopColor: "#e3e3e3",
                  borderTopStyle: "solid",
                }}
              />
            </div>
          ) : (
            false
          )}

          {store.ui.mediaQuery.isDesktop && (
            <div
              className={["transparent"].join(" ")}
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
                alignItems: "center",
                height: "100%",
                maxWidth: 1024,
                minWidth: 768,
                width: "calc(90vw - 220px)",
              }}
            >
              <div
                style={{
                  flexDirection: "row",
                  display: "flex",
                  justifyContent: "flex-start",
                  alignItems: "center",
                  width: "100%",
                }}
              >
                {/*<BreadcrumbComponent*/}
                {/*    style={{*/}
                {/*        fontSize: 11,*/}
                {/*        fontWeight: 600,*/}
                {/*    }}*/}
                {/*    params={store.ui.params}*/}
                {/*    baseUrl={store.ui.baseUrl}*/}
                {/*    data={store.ui.routes}*/}
                {/*/>*/}
              </div>
              {/* {profilePopover} */}
            </div>
          )}
        </Header>
        <Layout
          className={["transparent"].join(" ")}
          tabIndex={"-1"}
          style={{
            zIndex: 0,
            display: "flex",
            // overflowX: store.ui.mediaQuery.isMobile ? 'auto' : "hidden",
            // paddingLeft: 8,
            // paddingRight: 8,
            width: "calc(80vw - 220px)",
            height: "100%",
          }}
        >
          <Content
            className={["transparent"].join(" ")}
            style={{
              // maxHeight: 'calc(100vh - 98px)',
              // paddingBottom: 48,
              padding: 0,
              margin: 0,
              position: "relative",
              marginTop: store.ui.mediaQuery.isDesktop ? 78 : 72,
              // overflow: 'auto',
              overflowX: store.ui.mediaQuery.isMobile ? "auto" : null,
              // minWidth: 768,
              maxWidth: 1024,
              // minWidth: 768,
              width: store.ui.mediaQuery.isDesktop
                ? "calc(95vw - 210px)"
                : "calc(100vw)",
              zIndex: 22,
              // height: `calc(100vh - ${store.ui.mediaQuery.isDesktop ? 78 : 71}px)`,
              height: `calc(100vh - ${
                store.ui.mediaQuery.isDesktop ? 78 : 71
              }px)`,
              // paddingLeft: 8,
              // paddingRight: 8
            }}
          >
            {store.ui.mediaQuery.isDesktop && (
              <PageHeader
                style={{
                  padding: 8,
                  paddingLeft: 18,
                  marginBottom: 0,
                  backgroundColor: "transparent",
                }}
                // className={'card-page-header'}
                // ghost={false}
                // onBack={() => window.history.back()}
                // title={<span style={{fontSize: 28}}>Today {store.authentication.userData.role}</span>}
                // breadcrumb={store.ui.getRoute}
                // subTitle="This is a subtitle"
                title={""}
              >
                {/*<BreadcrumbComponent*/}
                {/*    style={{*/}
                {/*        paddingTop: 6,*/}
                {/*        paddingBottom: 6,*/}
                {/*        marginLeft: -5,*/}
                {/*        fontSize: 10,*/}
                {/*        color: "#b1b1b1",*/}
                {/*        fontWeight: 600,*/}
                {/*        paddingLeft: 20,*/}
                {/*        paddingRight: 20,*/}
                {/*        backgroundColor: "#e6f9ff",*/}
                {/*        whiteSpace: "nowrap",*/}
                {/*        overflow: "auto",*/}
                {/*    }}*/}
                {/*    params={store.ui.params}*/}
                {/*    baseUrl={store.ui.baseUrl}*/}
                {/*    data={store.ui.routes}*/}
                {/*/>*/}
              </PageHeader>
            )}
            <AppRoutes />
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
});
