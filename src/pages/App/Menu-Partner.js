import React, {useState, useEffect} from "react";
import {Layout, Menu, Avatar, Input, Button, Typography, Divider, message, Popover, Card} from 'antd';
import {createUseStyles} from "react-jss";
import {Link, useHistory, useRouteMatch, BrowserRouter as Router, Route} from "react-router-dom";
import {LINKS} from "../../routes";
import {
    HomeOutlined,
    BarChartOutlined,
    FileProtectOutlined,
    PaperClipOutlined,
    TeamOutlined,
} from '@ant-design/icons';
import {observer} from 'mobx-react-lite';
import {useStore} from "../../utils/useStores";

const {Search} = Input;
const {Text, Paragraph} = Typography;
const {Header, Content, Sider} = Layout;
const useStyles = createUseStyles({
    logo: {
        paddingLeft: 20,
        marginBottom: 16
    },
});
const {SubMenu} = Menu;
const rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];

export const MenuPartner = observer((props) => {
    let history = useHistory();
    const store = useStore();
    useEffect(() => {

    }, []);

    const [setKeys, setSetKeys] = useState(["dashboard"]);
    const [openKeys, setOpenKeys] = useState(['sub4']);

    const opened = openKeys => {
        const latestOpenKey = openKeys[0];
        if (rootSubmenuKeys.indexOf(openKeys) === -1) {
            setOpenKeys(openKeys)
        } else {
            setOpenKeys(openKeys + ':' + latestOpenKey ? [latestOpenKey] : [])
        }
    };
    return (<Menu
        defaultOpenKeys={['sub4']}
        theme="light"
        style={{
            backgroundColor: 'transparent',
            borderRightWidth: 0,
            fontWeight: 400,
            paddingLeft: 0
        }}
        openKeys={openKeys}
        onClick={({keyPath, item}) => {
            console.log(item, keyPath);
            store.ui.toggleLeftDrawerIsShown();
            if (keyPath === "dashboard") {
                history.push(LINKS.DASHBOARD);
            };
        }}
        mode="inline"
        selectedKeys={setKeys}
        onSelect={({setKeys, item, selectedKeys}) =>
            setSetKeys(selectedKeys)}
        onOpenChange={opened}
        overflowedIndicator={0}
        forceSubMenuRender={true}>
        <Menu.Item key="dashboard">
            <Link to={location => ({...location, pathname: "/app"})}>
                <HomeOutlined/>
                <span>Excecutive Summary</span>
            </Link>
        </Menu.Item>
        <Menu.Item key="2">
            <Link to="/app/tasks">
                <PaperClipOutlined/>
                <span>Task</span>
            </Link>
        </Menu.Item>
        <Divider orientation="left">
            <p style={{color: '#dbdbdb', fontSize: 10, marginTop: 8, marginLeft: store.ui.mediaQuery.isDesktop ? 8 : 0}}>ACTIVITIES</p>
        </Divider>
        {/*<Menu.Divider orientation="right" style={{background: '#dbdbdb', marginTop: 15, marginBottom: 15}}>Test</Menu.Divider>*/}
        <Menu.Item key="5">
            <Link to="/app/reports">
                <BarChartOutlined/>
                <span>Reports</span>
            </Link>
        </Menu.Item>
    </Menu>)
});
