import React, { useState, useEffect } from "react";
import {
  Layout,
  Menu,
  Avatar,
  Input,
  Button,
  Typography,
  Divider,
  message,
  Popover,
  Card,
} from "antd";
import { createUseStyles } from "react-jss";
import {
  Link,
  useHistory,
  useRouteMatch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import {
  HomeOutlined,
  AreaChartOutlined,
  BarChartOutlined,
  FileProtectOutlined,
  PaperClipOutlined,
  UploadOutlined,
  FileOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import SettingOutlined from "@ant-design/icons/lib/icons/SettingOutlined";
import * as _ from "lodash";

const { Search } = Input;
const { Text, Paragraph } = Typography;
const { Header, Content, Sider } = Layout;
const useStyles = createUseStyles({
  logo: {
    paddingLeft: 20,
    marginBottom: 16,
  },
});
const { SubMenu } = Menu;
const rootSubmenuKeys = ["sub1", "sub2", "sub4"];
const keyMetrics = [
  {
    name: "Executive Summary",
    link: "/app/reports/key-metrics/executive-summary",
    icon: "",
  },
  {
    name: "Daily Revenue",
    link: "/app/reports/key-metrics/daily-revenue",
    icon: "",
  },
  {
    name: "Daily Traffic Volume",
    link: "/app/reports/key-metrics/daily-traffic-volume",
    icon: "",
  },
  {
    name: "Average Vehicle KM",
    link: "/app/reports/key-metrics/average-vehicle-km",
    icon: "",
  },
  {
    name: "Full Route Equivalent",
    link: "/app/reports/key-metrics/full-route-equivalente",
    icon: "",
  },
  {
    name: "Accident",
    link: "/app/reports/key-metrics/accident",
    icon: "",
  },
  {
    name: "Traffic Classification",
    link: "/app/reports/key-metrics/traffic-classification",
    icon: "",
  },
  {
    name: "Financial Summary",
    link: "/app/reports/key-metrics/financial-summary",
    icon: "",
  },
  {
    name: "Cashflow",
    link: "/app/reports/key-metrics/cashflow",
    icon: "",
  },
  {
    name: "Balance Sheet",
    link: "/app/reports/key-metrics/balance-sheet",
    icon: "",
  },
];

export const MenuStaff = observer((props) => {
  let history = useHistory();
  const store = useStore();
  useEffect(() => { loadData(); }, []);

  async function loadData() {
    await store.user.getUserID(store.userData.id);
  }

  const [setKeys, setSetKeys] = useState(["dashboard"]);
  const [openKeys, setOpenKeys] = useState([]);

  const opened = (openKeys) => {
    const latestOpenKey = openKeys[0];
    if (rootSubmenuKeys.indexOf(openKeys) === -1) {
      setOpenKeys(openKeys);
    } else {
      setOpenKeys(openKeys + ":" + latestOpenKey ? [latestOpenKey] : []);
    }
  };

  const dataDivision = store.userData.role === "head_division" ? _.get(store.user.dataProfil, 'position_user.position.id') : _.get(store.user.dataProfil, 'position_user.position.organization.organization.id');

  return (
    <Menu
      defaultOpenKeys={["sub4"]}
      theme="light"
      style={{
        backgroundColor: "transparent",
        borderRightWidth: 0,
        fontWeight: 400,
        paddingLeft: 0,
      }}
      openKeys={openKeys}
      onClick={({ keyPath, item }) => {
        console.log(item, keyPath);
        store.ui.toggleLeftDrawerIsShown();
        if (keyPath == "dashboard") {
          history.push(LINKS.DASHBOARD);
        }
      }}
      mode="inline"
      selectedKeys={setKeys}
      onSelect={({ setKeys, item, selectedKeys }) => setSetKeys(selectedKeys)}
      onOpenChange={opened}
      overflowedIndicator={0}
      forceSubMenuRender={true}
    >
      {console.log(dataDivision, "ini userData")}
      <Menu.Item key="dashboard">
        <Link to={LINKS.DASHBOARD}>
          <HomeOutlined />
          <span>Excecutive Summary</span>
        </Link>
      </Menu.Item>
      <Menu.Item key="keymetics">
        <Link to={LINKS["KEY METRICS"]}>
          <BarChartOutlined />
          <span>Key Metrics</span>
        </Link>
      </Menu.Item>
      <Menu.Item key="7">
         <Link to={LINKS.KPI}>
            <AreaChartOutlined />
             <span>KPI</span>
         </Link>
      </Menu.Item>
      <Menu.Item key="15">
        <Link to={LINKS["OPERATIONAL DATA"] +"/" +dataDivision}>
          <FileOutlined />
          <span>Operational Data</span>
        </Link>
      </Menu.Item> 
      <Menu.Item key="calendar">
        <Link to={LINKS.CALENDAR}>
          <CalendarOutlined />
          <span>Calendar</span>
        </Link>
      </Menu.Item>
      </Menu>
  );
});