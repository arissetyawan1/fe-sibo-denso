import React, { useState, useEffect } from "react";
import {
  Layout,
  Menu,
  Avatar,
  Input,
  Modal,
  Button,
  Typography,
  Divider,
  message,
  Popover,
  Card,
  Image,
} from "antd";
import { createUseStyles } from "react-jss";
import {
  Link,
  useHistory,
  useRouteMatch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import {
  HomeOutlined,
  LogoutOutlined,
  UserOutlined,
  AuditOutlined,
  CalendarOutlined,
} from "@ant-design/icons";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import densoLogo from "../../assets/images/denso-logo.png";
import MediaQuery, { useMediaQuery } from "react-responsive";
import SettingOutlined from "@ant-design/icons/lib/icons/SettingOutlined";

const { Search } = Input;
const { Text, Paragraph } = Typography;
const { Header, Content, Sider } = Layout;
const useStyles = createUseStyles({
  logo: {
    paddingLeft: 20,
    marginBottom: 16,
  },
});
const { SubMenu } = Menu;
const rootSubmenuKeys = ["sub1", "sub2", "sub4"];

export const MenuSuperAdministrator = observer((props) => {
  let history = useHistory();
  const store = useStore();
  useEffect(() => {}, []);
  const [setKeys, setSetKeys] = useState(["dashboard"]);
  const [openKeys, setOpenKeys] = useState([]);
  const [collapse, setCollapse] = useState(false);
  const mediaQuery = {
    isBigScreen: useMediaQuery({ query: "(min-device-width: 1824px)" }),
    isDesktop: useMediaQuery({ minWidth: 1024 }),
    isTablet: useMediaQuery({ minWidth: 768, maxWidth: 1023 }),
    isMobile: useMediaQuery({ maxWidth: 767 }),
    isNotMobile: useMediaQuery({ minWidth: 768 }),
  };

  const opened = (openKeys) => {
    const latestOpenKey = openKeys[0];
    if (rootSubmenuKeys.indexOf(openKeys) === -1) {
      setOpenKeys(openKeys);
    } else {
      setOpenKeys(openKeys + ":" + latestOpenKey ? [latestOpenKey] : []);
    }
  };

  // modal logout confirmation
  const confirmLogout = () => {
    Modal.confirm({
      title: "Log out from dashboard?",
      onOk: () => {
        store.authentication.logout();
        return history.push("/login");
      },
      onCancel: () => {},
    });
  };

  return (
    <Menu
      defaultOpenKeys={["sub4"]}
      theme="light"
      style={{
        backgroundColor: "transpaent",
        borderRightWidth: 0,
        fontWeight: 400,
      }}
      className={"menuStyle"}
      openKeys={openKeys}
      onClick={({ keyPath, item }) => {
        console.log(item, keyPath);
        store.ui.toggleLeftDrawerIsShown();
        if (keyPath == "dashboard") {
          history.push(LINKS.DASHBOARD);
        }
      }}
      mode="inline"
      selectedKeys={setKeys}
      onSelect={({ setKeys, item, selectedKeys }) => setSetKeys(selectedKeys)}
      onOpenChange={opened}
      overflowedIndicator={0}
      forceSubMenuRender={true}
    >
      <Menu.Item key="dashboard" className={"menuStyle"}>
        <Link to={LINKS.DASHBOARD}>
          <HomeOutlined />
          <span className={"menuStyle"}>Dashboard</span>
        </Link>
      </Menu.Item>
      <Menu.Item key="EMPLOYEE">
        <Link to={LINKS.EMPLOYEE}>
          <UserOutlined />
          <span>Employee</span>
        </Link>
      </Menu.Item>
      <Menu.Item key="contract">
        <Link to={LINKS.CONTRACT}>
          <AuditOutlined />
          <span>Contracts</span>
        </Link>
      </Menu.Item>
      <Menu.Item key="event">
        <Link to={LINKS.EVENT}>
          <CalendarOutlined />
          <span>Event</span>
        </Link>
      </Menu.Item>
      <Divider />
      <Menu.Item onClick={() => confirmLogout()}>
        <LogoutOutlined />
        <span>Sign out</span>
      </Menu.Item>
    </Menu>
  );
});

{
  /* <Menu.Item key="permit">
  <Link to={LINKS.PERMIT}>
    <UserOutlined />
    <span>Permit</span>
  </Link>
</Menu.Item> */
}
// const keyMetrics = [
//   {
//     name: "Executive Summary",
//     link: "/app/reports/key-metrics/executive-summary",
//     icon: "",
//   },
//   {
//     name: "Daily Revenue",
//     link: "/app/reports/key-metrics/daily-revenue",
//     icon: "",
//   },
//   {
//     name: "Daily Traffic Volume",
//     link: "/app/reports/key-metrics/daily-traffic-volume",
//     icon: "",
//   },
//   {
//     name: "Average Vehicle KM",
//     link: "/app/reports/key-metrics/average-vehicle-km",
//     icon: "",
//   },
//   {
//     name: "Full Route Equivalent",
//     link: "/app/reports/key-metrics/full-route-equivalente",
//     icon: "",
//   },
//   {
//     name: "Accident",
//     link: "/app/reports/key-metrics/accident",
//     icon: "",
//   },
//   {
//     name: "Traffic Classification",
//     link: "/app/reports/key-metrics/traffic-classification",
//     icon: "",
//   },
//   {
//     name: "Financial Summary",
//     link: "/app/reports/key-metrics/financial-summary",
//     icon: "",
//   },
//   {
//     name: "Cashflow",
//     link: "/app/reports/key-metrics/cashflow",
//     icon: "",
//   },
//   {
//     name: "Balance Sheet",
//     link: "/app/reports/key-metrics/balance-sheet",
//     icon: "",
//   },
// ];
