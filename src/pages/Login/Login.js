import React, { useState, createRef, useEffect } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import {
  Form,
  Input,
  Button,
  Checkbox,
  Row,
  Col,
  Card,
  Typography,
  Avatar,
  message,
} from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import densoLogo from "../../assets/images/denso-logo.png";
import lmsLogo from "../../assets/images/astra-infra-1.png";
import { createUseStyles } from "react-jss";
import { LINKS } from "../../routes";
import { Link, useHistory } from "react-router-dom";
import * as firebase from "firebase/app";

const useStyles = createUseStyles({
  logo: {
    // height: 30,
    paddingLeft: 20,
    marginBottom: 16,
  },
  logoFull: {
    height: 80,
    width: 300,
    marginBottom: 8,
  },
});
const { Text, Paragraph, Title } = Typography;
const { Meta } = Card;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 8 },
};
const tailLayout = {
  wrapperCol: { offset: 6, span: 8 },
};

export const Login = observer(() => {
  const [isLogin, setIsLogin] = useState(false);
  const [loading, setLoading] = useState(false);
  const [email, handleEmail] = useState("");
  const [password, handlePassword] = useState("");
  const [remember, handleRemember] = useState(false);
  const store = useStore();

  let history = useHistory();

  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    enterLoading(values)
      .then((res) => {
        console.log(res, "awasaa");
      })
      .catch((error) => {
        console.log({ error }, "awasaa error");
      });
  };

  const classes = useStyles();

  const enterLoading = async (props) => {
    setLoading(true);
    const { email, password } = props;
    console.log({ email, password }, "loginUsingEmail started");

    try {
      await store.authentication.login({
        username: email,
        password: password,
      });
      setLoading(false);
      return history.push("/app/dashboard");
    } catch (err) {
      message.error(err.message);
      console.log({ err }, "loading failed error");
      setLoading(false);
    }
  };

  return (
    <div
      style={{
        width: "100vw",
        display: "flex",
        justifyContent: "center",
      }}
    >
      <Row
        type="flex"
        style={{
          display: "flex",
          justifyContent: "flex-start",
          marginTop: "5vh",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <div>
          <Col>
            <Card
              style={{ width: 320, textAlign: "center" }}
              headStyle={{ fontSize: 13, fontWeight: 200 }}
              className={"shadow"}
              bordered={true}
              cover={<img alt="Logo Denso" src={densoLogo} height="10" />}
              hoverable
            >
              <Meta description="Sign in to your account" />
              <Form
                layout={"vertical"}
                name="normal_login"
                className="login-form"
                initialValues={{ email, password, remember }}
                onFinish={onFinish}
              >
                <Form.Item
                  label="Email"
                  name="email"
                  size={"large"}
                  rules={[
                    {
                      required: false,
                      message: "Please input your Username!",
                    },
                  ]}
                >
                  <Input
                    prefix={<UserOutlined className="site-form-item-icon" />}
                    type="text"
                    placeholder="Email"
                  />
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: 0,
                  }}
                  label="Password"
                  name="password"
                  size={"large"}
                  rules={[
                    {
                      required: false,
                      message: "Please input your Password!",
                    },
                  ]}
                >
                  <Input.Password
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                  />
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: 20,
                    textAlign: "left",
                  }}
                >
                  <Form.Item name="remember" valuePropName="checked" noStyle>
                    <Checkbox
                      value={remember}
                      onChange={(val) => handleRemember(val)}
                    >
                      Remember me
                    </Checkbox>
                  </Form.Item>
                </Form.Item>

                <Form.Item
                  style={{
                    marginBottom: 0,
                    width: "100%",
                  }}
                >
                  <Button
                    type="primary"
                    block
                    loading={loading}
                    htmlType="submit"
                    size={"large"}
                    onSubmit={enterLoading}
                    // onClick={enterLoading}
                    className="login-form-button"
                  >
                    Sign In
                  </Button>
                </Form.Item>
              </Form>
            </Card>
          </Col>
        </div>
      </Row>
    </div>
  );
});

{
  /* Permission Form
                <Form.Item
                  style={{
                    marginTop: 0,
                    float: "right",
                    position: "inline-block",
                    padding: 0,
                    border: 1,
                    fontWeight: "750",
                  }}
                  name="permit-form"
                  size={"small"}
                >
                  <a className="login-form-forgot" href={LINKS.FORM_PERMIT}>
                    Permit Form
                  </a>
                </Form.Item> */
}
