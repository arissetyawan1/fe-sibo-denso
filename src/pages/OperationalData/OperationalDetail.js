
import React, { useEffect, Fragment, useState } from 'react';
import 'antd/dist/antd.css';
import { Table, Input, Button, Icon, Typography, Modal, message, Upload, Row, Col } from 'antd';
import { UploadOutlined } from "@ant-design/icons";
import Highlighter from 'react-highlight-words';
import { inject, observer } from "mobx-react";
import moment from 'moment';
import { useParams } from "react-router-dom"
import { useStore } from "../../utils/useStores"
import { appConfig } from "../../config/app"
import * as download from "downloadjs";
import FileViewer from "react-file-viewer";
import './operational.css';
import * as _ from "lodash";
import { http } from "../../utils/http";

const operationalDataDetail = observer(props => {
    const store = useStore()
    const params = useParams();
    const confirm = Modal.info;
    const [state, setState] = useState({
        searchText: '',
        searchedColumn: '',
    })

    function showConfirm(record) {
        viewer = true
        fileee =  appConfig.apiUrl + '/v1/files/' + record.location
        type = fileee.split(".").pop().toLowerCase();
        confirm({
          title: 'Detail View File',
            width:store.ui.mediaQuery.isTablet ? '90%' :800,
          
          content: <FileViewer fileType={String(type)} filePath={String(fileee)} />,
          okText:"Close",
        });
      }

    useEffect(() => {
       loadData()
    }, [])

    async function loadData () {
        store.operationalData.query.division_id = params.id;
        store.operationalData.getAll();
    }



    let searchInput
    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.select());
            }
        },
        render: text =>
            state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                    searchWords={[state.searchText]}
                    autoEscape
                    textToHighlight={text}
                />
            ) : (
                    text
                ),
    });

    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };

    const handleReset = clearFilters => {
        clearFilters();
        setState({ searchText: '' });
    };

    const deleteUpload = (idFile, idUser, idLogin) => {
        if((store.userData.role === "super_admin") || (idUser === idLogin )) {
            operationalData.delete(idFile)
            .then(res => {
                message.success('action success!')
                loadData()
            })
            .catch(err => {
                message.error(err.message)
            })
        }else {
            message.error('File tidak dapat dihapus, anda bukan pemilik file!')
        }
    }

    const operationalData = store.operationalData;

    const columns = [
        {
            title: 'File',
            dataIndex: 'file',
            key: 'file',
            ...getColumnSearchProps('file'),
        },
        {
            title: 'Date',
            dataIndex: 'created_at',
            key: 'created_at',
            ...getColumnSearchProps('file'),
            render: text => moment(text).format('HH:MM, DD MMMM YYYY'),
        },
        {
            title: 'Division',
            dataIndex: 'division',
            key: 'division_id',
            render: (text, record) => (
                record.division.name
            )
            // ...getColumnSearchProps('file'),
        },
        {
            title: 'Uploader',
            dataIndex: 'user',
            key: 'user',
            render: (text) => {
                let user = _.get(text, 'full_name');
                return user ? user : "Superadmin";
            }
        },
        {
            title: 'Action',
            dataIndex: '',
            key: '',
            render: (_, record) => {
                return (
                    // <p>test</p>
                    <span>
                        <a
                            href="javascript:void(0)"
                            onClick={() => downloadFile(record)}
                            style={{
                                marginRight: 10,
                            }}
                        >
                            <Typography.Text type={"danger"}>
                                Download
                            </Typography.Text>
                        </a>
                        <a
                            href="javascript:void(0)"
                            onClick={() => showConfirm(record)}
                            style={{
                                marginRight: 8,
                            }}
                        >
                            
                        <Typography.Text>
                            View
                            </Typography.Text>
                        </a>
                        {
                            store.userData.role !== "BOD" &&
                        <a 
                        href="javascript:void(0)"
                        onClick={() => deleteUpload(record.id, record.user_id, store.userData.id)}>
                        <Typography.Text>
                            Delete
                        </Typography.Text>  
                        </a>
                        }
                    </span>
                )
            },
        },
    ];
    let user = store.userData.id
    let role = store.userData.role
    const downloadFile = (record) => {
        const log = {
            user_id : user,
            event_name: "Download Operational Data",
            data: {
                location:{
                    pathname:"/app/operational-data/",
                    search: "",
                    hash: "",
                    key: role,
                },
                action:"DOWNLOAD",
            },
        }
        store.log.createData(log)
        console.log(appConfig.apiUrl + '/v1/files/' + record.location, "DIO")
        var x = new XMLHttpRequest();
        // x.open("GET", '/v1/files/' + record.location, true);
        x.open("GET", appConfig.apiUrl + "/v1/files/" + record.location, true);
        x.responseType = "blob";
        x.onload = function (e) { download(e.target.response, record.file, "application/octet-stream"); };
        x.send();
        // } 
    };
    
    let viewer = false;
    let fileee ="";
    let type ="";

    const viewFile = async (record) => {
        viewer = await true
        fileee = await appConfig.apiUrl + '/v1/files/' + record.location
        type = fileee.split(".").pop();
        console.log(viewer, "yak masuk view")
        console.log(String(fileee), "yak masuk file")
        console.log(type, "yak masuk type")
        return ( <Modal visible={viewer}>
            <p>INII</p>
            {/* <FileViewer fileType={String(type)} filePath={String(fileee)} />  */}
        </Modal> )
    };

    return (
        <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet ?'10px':''}}>
            <Row>
                <Col lg={3} offset={ store.ui.mediaQuery.isMobile?15: store.ui.mediaQuery.isTablet?20:21}>
                {store.userData.role !== "BOD" && (
                    <Upload name="logo" customRequest={(event) => {
                        const log = {
                            user_id : user,
                            event_name: "Upload Operational Data",
                            data: {
                                location:{
                                    pathname:"/app/operational-data/input-data-operational",
                                    search: "",
                                    hash: "",
                                    key: role,
                                },
                                action:"PUSH",
                            },
                        }
                        store.http.uploadAntdOperasional(event).then(res => {
                            store.log.createData(log);
                            store.operationalData.create({
                                file: event.file.name,
                                division_id:params.id,
                                location: res.body.path,
                                user_id : store.userData.id
                            }).then(res => {
                                loadData();
                                message.success("Data Uploaded!");
                            }).catch(err => {
                                console.log("error, " , err)
                            }); 
                        }).catch(err => {
                            message.error("Error on uploading data, ", err)
                        })
                    }} action="#" showUploadList={false} >
                        <Button style={{marginBottom:30}}>
                            <UploadOutlined /> Upload Data
                        </Button>
                    </Upload>
                    )}
                </Col>
            </Row>

            <Table scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }} columns={columns} dataSource={operationalData.data} />
        </div>
    );

})

export default operationalDataDetail;
