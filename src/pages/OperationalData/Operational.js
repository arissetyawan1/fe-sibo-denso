import React, { Component, useState, useEffect, Fragment } from 'react';
import { Col, Row, Button, Typography, PageHeader, Card } from 'antd';
import { UploadOutlined, BoldOutlined, AlignCenterOutlined } from "@ant-design/icons";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch
} from "react-router-dom";
import { useStore } from "../../utils/useStores";
import { inject, observer } from "mobx-react";
import { LINKS } from '../../routes';
import * as _ from "lodash";

let operationalUser
let divisionId
const OperationalData = observer(props => {
    const store = useStore()
    const divisi = store.division.data
    const getId = toString(store.userData.id)
    const { Text } = Typography;

    useEffect(() => {
        
        loadData()
    }, [])

    async function loadData () {
        await store.division.getAll();
        await store.user.getAll();
        store.user.filterData = _.filter(store.user.data, i => i.id === store.userData.id);
        
        // operationalUser =  _.filter(store.division.data, i => i.id === divisionId);
    }

    {
        const positionCheck = store.user.filterData.map(r => {
            return r.position_user
        })
        if(positionCheck[0] !== null){
            let divisionId = store.user.filterData.map(d => {
                return d.position_user.position.organization.organization.id
            })
            operationalUser = _.filter(divisi, i => i.id === divisionId[0]);
        }
        
        return (
            <div className="site-card-wrapper"  style={{padding: store.ui.mediaQuery.isTablet?'10px':''}}>
                <PageHeader
                    className={'card-page-header'}
                    title={"Operational Data"}
                    subTitle=""
                    extra={ store.userData.role !== "BOD" &&
                        <Link to={LINKS["OPERATIONAL DATA UPLOAD"]}>
                        <Button color="secondary">
                            <UploadOutlined /> Upload Data
                        </Button>
                        </Link> } />
                    <Row gutter={[8, 8]} justify={store.ui.mediaQuery.isMobile? 'center':''}>
                        {/* {( store.division.data || [])  */}
                        {(positionCheck[0] == null ? store.division.data : operationalUser)
                            .map(d =>
                                <Col lg={6} md={6} sm={21} xs={21} style={{marginBottom:10}}>
    
                                    <Link to={LINKS["OPERATIONAL DATA"] + '/' + d.id}>
                                        <Card bordered={true} style={{paddingTop:20,textAlign: 'center', paddingBottom:20}}>
                                            <Text strong>{d.name}</Text> 
                                        </Card>
                                    </Link>
    
                                </Col>
                            )}
                    </Row>
    
            </div >
        );
    }

})


export default OperationalData;
