import React from "react";
import {Row, Col, Button, Statistic, Card, Empty, PageHeader, Dropdown, Table, Menu} from "antd";
import {FilterOutlined, PlusOutlined} from "@ant-design/icons";

const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const gridStyle = {
  overflow: 'hidden',
  position: 'relative',
  padding: '16px 15px 8px',
  margin: '-1px -1px 0 0',
  height: 184,
  transition: 'transform .4s'
};

const optionFilter = [
  {
    key: 1,
    value: 'Enabled'
  }, {
    key: 2,
    value: 'Disabled'
  },
];

function handleMenuClick(e) {
  console.log('click', e);
}

const menu = (
  <Menu onClick={handleMenuClick} style={{background: '#fff'}}>
    {optionFilter.map((i => {
      return <Menu.Item key={i.key}>{i.value}</Menu.Item>
    }))
    }
  </Menu>
);

export const Task = () => {

  return <div className={['cariparkir-container'].join(' ')}>
    <Card bordered={false} className={'shadow'} bodyStyle={{padding: '0'}}>
      <PageHeader
        className={'card-page-header'}
        title={<Dropdown overlay={menu} trigger={['click']}>
          <Button icon={<FilterOutlined/>} size={'default'} style={{margin: 3}}>
            Filter
          </Button>
        </Dropdown>}
        subTitle=""
        extra={[
          <Button key="1">
            <PlusOutlined/> New
          </Button>
        ]}
      />
      <div style={{minHeight: 'calc(100vh - 450px)', display:'flex', justifyContent:'center', alignItems:'center'}}>
        <Empty/>
      </div>
    </Card>
  </div>
};
