import React, { useEffect} from "react";
import { Row, Col, Card, Avatar, } from "antd";
import { useStore } from "../../utils/useStores";
import { observer } from "mobx-react-lite";
import { UserOutlined } from '@ant-design/icons';
import * as _ from "lodash";
import moment from "moment";

export const Profile = observer(() => {
  const store = useStore();
  useEffect(() => {
    fetchData();
  }, []);

  let dataLogin = store.authentication.userData.id

  async function fetchData() {
    await store.user.getUserID(dataLogin);
  }

  const userData = store.userData

  function getToKnowRole () {
    if(userData.role === "head_division") {
      return <Col lg={10} className="col-detail-card">
        <Row gutter={[8, 16]} justify="center">
          <h5>Organization</h5>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Division</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "position_user.position.name") || "-"}</h6></Col>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Department</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "position_user.position.organization.name") || "-"}</h6></Col>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Position</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>Head of Division {_.get(store.user.dataProfil, "position_user.position.name") || "-"}</h6></Col>
        </Row>
      </Col>
    }else {
      return <Col lg={10} className="col-detail-card">
        <Row gutter={[8, 16]} justify="center">
          <h5>Organization</h5>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Division</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "position_user.position.organization.organization.name") || "-"}</h6></Col>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Department</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "position_user.position.organization.name") || "-"}</h6></Col>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Position</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "position_user.position.name") || "-"}</h6></Col>
        </Row>
      </Col>
    }
  }

  return <div className={["cariparkir-container"].join(" ")}>
  <Card title="Profile" className={'shadow'} bordered={false} >
    <Row justify="center">
      <Col lg={5} className="col-detail-card">
        <Avatar size={120} icon={<UserOutlined />} />
      </Col>
      <Col lg={9} className="col-detail-card">
        <Row gutter={[8, 16]} justify="center">
          <h5>Basic Information</h5>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Full Name</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "full_name") || "-"}</h6></Col>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Username</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "username") || "-"}</h6></Col>
        </Row>
        <Row gutter={[8, 16]} justify="center">
          <Col className="gutter-row" lg={7}><h6>Role</h6></Col>
          <Col lg={1}>:</Col>
          <Col lg={13}><h6>{_.get(store.user.dataProfil, "role.name") || "-"}</h6></Col>
        </Row>
      </Col>
      {getToKnowRole()}
    </Row>
  </Card>
</div>
});
