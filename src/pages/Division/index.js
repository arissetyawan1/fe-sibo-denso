import React, { useEffect, Fragment, useState } from "react";
import "antd/dist/antd.css";
import {
    Table,
    Input,
    Card,
    Button,
    Tooltip,
    message,
    Modal,
    Tree,
} from "antd";
import {
    DeleteOutlined,
    EditOutlined,
    SearchOutlined,
    PlusSquareOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import { observer } from "mobx-react";
import { useStore } from "../../utils/useStores";
import { PageHeaderComponent } from "../../component/DefaultPage/PageHeader";
import { ModalForm } from "../../component/Form/ModalForm";

export const DivisionSIBO = observer((initialData) => {
    const { division_sibo: divisionSIBOStore, ui: uiStore } = useStore();
    const [state, setState] = useState({
        searchText: "",
        searchedColumn: "",
    });
    const [selectedData, setData] = useState({});
    const [isFormVisible, setFormVisible] = useState(false);
    const [mode, setMode] = useState("add");

    let searchInput;

    useEffect(() => {
        divisionSIBOStore.getAll();
    }, []);

    const getColumnSearchProps = (dataIndex) => ({
        filterDropdown: ({
            setSelectedKeys,
            selectedKeys,
            confirm,
            clearFilters,
        }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={(node) => {
                        searchInput = node;
                    }}
                    placeholder={`Cari ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={(e) =>
                        setSelectedKeys(e.target.value ? [e.target.value] : [])
                    }
                    onPressEnter={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    style={{ width: 188, marginBottom: 8, display: "block" }}
                />
                <Button
                    type="primary"
                    onClick={() =>
                        handleSearch(selectedKeys, confirm, dataIndex)
                    }
                    icon={<SearchOutlined />}
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Cari
                </Button>
                <Button
                    onClick={() => handleReset(clearFilters)}
                    size="small"
                    style={{ width: 90 }}
                >
                    Reset
                </Button>
            </div>
        ),
        filterIcon: (filtered) => (
            <SearchOutlined
                style={{ color: filtered ? "#FBFBFB" : undefined }}
            />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                ? record[dataIndex]
                      .toString()
                      .toLowerCase()
                      .includes(value.toLowerCase())
                : "",
        onFilterDropdownVisibleChange: (visible) => {
            if (visible) {
                setTimeout(() => searchInput.select(), 100);
            }
        },
        render: (text) =>
            state.searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{ backgroundColor: "#FF9D00", padding: 0 }}
                    searchWords={[state.searchText]}
                    autoEscape
                    textToHighlight={text ? text.toString() : text}
                />
            ) : (
                text
            ),
    });
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setState({
            searchText: selectedKeys[0],
            searchedColumn: dataIndex,
        });
    };
    const handleReset = (clearFilters) => {
        clearFilters();
        setState({ searchText: "" });
    };

    const columns = [
        {
            title: "No",
            dataIndex: "no",
            render: (t, r, index) => `${index + 1}`,
            width: "5%",
        },
        {
            title: "Kode",
            dataIndex: "code",
            width: "15%",
            ...getColumnSearchProps("code"),
        },
        {
            title: "Nama",
            dataIndex: "name",
            width: "15%",
            ...getColumnSearchProps("name"),
        },
        {
            title: "Action",
            dataIndex: "",
            width: "15%",
            key: "",
            render: (text, record, index) => {
                return (
                    <span>
                        {/* <Link to={LINKS["DETAIL CONTRACT"]}>
              <Tooltip placement="top" title={"Lihat detail"}>
                <Button
                  style={{
                    background: "blue",
                    color: "white",
                    boxShadow: "none",
                  }}
                >
                  <EyeOutlined />
                </Button>
              </Tooltip>
            </Link> */}
                        <Tooltip
                            placement="top"
                            title={"Edit Kontrak"}
                            onClick={() => editData(record)}
                        >
                            <Button
                                style={{
                                    background: "gold",
                                    boxShadow: "none",
                                    color: "black",
                                }}
                            >
                                <EditOutlined />
                            </Button>
                        </Tooltip>
                        <Tooltip placement="top" title={"Hapus Data"}>
                            <Button
                                style={{
                                    background: "red",
                                    color: "white",
                                    boxShadow: "none",
                                    marginLeft: "8px",
                                }}
                                onClick={() => confirmDelete(record.id)}
                            >
                                <DeleteOutlined />
                            </Button>
                        </Tooltip>
                    </span>
                );
            },
        },
    ];

    const treeData = [
        {
            title: "Board of director (0100)",
            key: "0-0",
            children: [
                {
                    title: "Precident director (0101)",
                    key: "0-0-0",
                },
            ],
        },
        {
            title: "Marketing (0200)",
            key: "0-1",
            children: [
                {
                    title: "Head (0201)",
                    key: "0-1-0",
                },
                {
                    title: "Staff (0202)",
                    key: "0-1-1",
                },
            ],
        },
        {
            title: "Operational (0300)",
            key: "02",
            children: [
                {
                    title: "Admin pool (0301)",
                    key: "021",
                },
                {
                    title: "Staff admin (0302)",
                    key: "022",
                },
                {
                    title: "Driver (0303)",
                    key: "023",
                },
            ],
        },
    ];

    const onSelect = (selectedKeys, info) => {
        console.log("selected", selectedKeys, info);
    };

    const breadCrumb = [
        {
            link: "",
            name: "Struktur organisasi",
            styles: { fontWeight: "bold" },
        },
    ];

    const addData = () => {
        setMode("add");
        setFormVisible(true);
    };

    const editData = (record) => {
        setMode("edit");
        setData(record);
        setFormVisible(true);
    };

    const deleteData = async (id) => {
        try {
            await divisionSIBOStore.delete(id);
            await divisionSIBOStore.getAll();
            message.success("Berhasil menghapus");
        } catch (err) {
            console.log(err, "bruh123123");
            message.error("Gagal menghapus");
        }
    };

    const confirmDelete = (id) => {
        Modal.confirm({
            title: "Apakah yakin ingin menghapus?",
            content: "Data yang dihapus tidak bisa kembali",
            onOk: () => {
                return deleteData(id);
            },
            onCancel() {},
        });
    };

    const closeForm = () => {
        setFormVisible(false);
    };

    const actions = [
        <Button onClick={addData}>
            <PlusSquareOutlined /> Tambah
        </Button>,
    ];

    const formItems = [
        {
            type: "input",
            name: "code",
            label: "Kode Divisi",
        },
        {
            type: "input",
            name: "name",
            label: "Nama",
        },
    ];

    const saveData = async (data) => {
        try {
            if (mode === "add") {
                await divisionSIBOStore.create(data);
            } else {
                await divisionSIBOStore.update(data.id, data);
            }
            await divisionSIBOStore.getAll();
            return data;
        } catch (err) {
            console.log(err);
            throw err;
        }
    };

    const data = divisionSIBOStore.data
        .map((prop) => prop)
        .sort((a, b) => parseFloat(a.code) - parseFloat(b.code));

    const tableData = [];
    const treeParent = data.map((it) => {
        const newCode = it.code || "";
        if (newCode.substr(3, 4) === "0") {
            const data = {
                title: it.name,
                key: newCode.substr(0, 2),
                children: [],
            };

            tableData.push(data);
        }
    });

    const treeChildren = data.map((it) => {
        const newCode = it.code || "";
        if (newCode.substr(3, 4) > 0) {
            for (let i = 0; i < tableData.length; i++) {
                if (tableData[i].key === newCode.substr(0, 2)) {
                    const data = {
                        title: it.name,
                        key: newCode.substr(0, 2).concat(newCode.substr(3, 4)),
                    };

                    tableData[i].children.push(data);
                }
            }
        }
    });

    return (
        <div
            className="site-card-wrapper"
            style={{
                padding:
                    uiStore.mediaQuery.isMobile || uiStore.mediaQuery.isTablet
                        ? "10px"
                        : "",
                marginTop: 10,
            }}
        >
            <PageHeaderComponent
                title={"Struktur Organisasi"}
                breadCrumb={breadCrumb}
                actions={actions}
            />
            <Card>
                <div>
                    <Tree
                        showLine={true}
                        showIcon={true}
                        // defaultExpandedKeys={["0-0-0", "0-1-0", "0-2-0"]}
                        onSelect={onSelect}
                        treeData={tableData}
                    />
                </div>
            </Card>

            {/* <Table
        scroll={{ x: uiStore.mediaQuery.isMobile ? 500 : "" }}
        columns={columns}
        dataSource={tableData}
      /> */}
            <ModalForm
                title={"Struktur Organisasi"}
                defaultData={selectedData}
                visible={isFormVisible}
                onClose={closeForm}
                forms={formItems}
                mode={mode}
                onSave={(data) => saveData(data)}
            />
        </div>
    );
});
