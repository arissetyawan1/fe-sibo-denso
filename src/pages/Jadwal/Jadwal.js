import React, { useEffect } from "react";
import "antd/dist/antd.css";
import { Table, Breadcrumb, Row, Col, PageHeader } from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import { LINKS } from "../../routes";
import moment from "moment";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import "moment/locale/id";

export const Jadwal = observer((props) => {
    const {
        trip_schedule: scheduleStore,
        ui: uiStore,
        client: clientStore,
    } = useStore();
    useEffect(() => {
        loadInitialData();
    }, []);

    const loadInitialData = async () => {
        await scheduleStore.getAll();
    };

    const columns = scheduleStore.data.columns || [{}];

    // dataColumn[0].fixed = "left";
    // dataColumn[0].width = "5%";
    // dataColumn[0].title = "Penghasil / Tanggal";

    const newData = [
        {
            dataIndex: "columnName",
            fixed: "left",
            key: "columnName",
            title: "Penghasil / Tanggal",
            width: "5%",
        },
    ];

    columns.map((it, index) => {
        if (index > 0) {
            const title = moment(it.title, "DD-MM-YYYY");
            const newTitle = moment(title).format("dddd, DD-MM-YYYY");

            const data = {
                dataIndex: it.dataIndex,
                key: it.key,
                title: newTitle,
            };

            newData.push(data);
        }
    });

    const newColumns = [
        {
            title: "No",
            dataIndex: "no",
            fixed: "left",
            render: (t, r, index) => `${index + 1}`,
            align: "center",
        },
    ].concat(
        newData.map((it, index) => {
            if (index) {
                return {
                    ...it,
                    render: (text) =>
                        text && {
                            props: {
                                style: { background: "green" },
                            },
                        },
                };
            }
            return it;
        })
    );

    const { rows } = scheduleStore.data;

    const { length } = columns;
    const scrollX =
        length === 29
            ? 3250
            : length === 30
                ? 3360
                : length === 31
                    ? 3475
                    : 3575;

    //@todo
    const handleClickRow = (record, index) => ({
        onClick: async (event) => {
            console.log(record, "ini record");
            // history.push(LINKS.DETAIL_CONTRACT.replace(":id", record.id));
        },
    });

    return (
        <div
            className="site-card-wrapper"
            style={{
                padding:
                    uiStore.mediaQuery.isMobile || uiStore.mediaQuery.isTablet
                        ? "10px"
                        : "",
                marginTop: 10,
            }}
        >
            <Breadcrumb style={{ marginLeft: 12 }}>
                <Breadcrumb.Item>
                    <Link to={LINKS.DASHBOARD}>
                        <HomeOutlined />
                    </Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item style={{ fontWeight: "bold" }}>
                    Jadwal
                </Breadcrumb.Item>
            </Breadcrumb>
            <Row>
                <Col flex="auto">
                    <PageHeader
                        className={"card-page-header"}
                        title={"Jadwal"}
                        subTitle=""
                    />
                </Col>
            </Row>
            <Table
                id="table-jadwal"
                bordered
                scroll={{ x: scrollX, y: 500 }}
                columns={newColumns}
                dataSource={rows}
                pagination={{
                    pageSizeOptions: ["10", "30", "50"],
                }}
                onRow={handleClickRow}
            />
        </div>
    );
});
