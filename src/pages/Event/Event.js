import React, { Component, useEffect, useState } from "react";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
// import '@fullcalendar/core/main.css';
import interactionPlugin from "@fullcalendar/interaction";
import "@fullcalendar/daygrid/main.css";
import "antd/dist/antd.css";
import {
  Row,
  Col,
  DatePicker,
  Badge,
  Form,
  Alert,
  Input,
  Select,
  Card,
  message,
  Modal,
  Table,
  Button,
  Popconfirm,
  PageHeader,
} from "antd";
import { DeleteOutlined, PlusOutlined } from "@ant-design/icons";
import { useStore } from "../../utils/useStores";
import { observer } from "mobx-react";
import moment from "moment";
import * as _ from "lodash";

const Event = observer((initialData) => {
  const { events: eventsStore } = useStore();
  const [form] = Form.useForm();
  const store = useStore();

  const [state, setState] = useState({
    data: {},
    success: false,
    detail: false,
    title: "",
    // location: "",
    start_date: "",
    end_date: "",
    desc: "",
    id: "",
    isNewEvent: false,
  });
  const [status, setStatus] = useState("");

  useEffect(() => {
    loadInitialData();
  }, []);

  // async function fetchData() {
  //   await store.events.getAll();

  // }

  const loadInitialData = async () => {
    await eventsStore.getAll();
  };

  const toggleSuccess = () => {
    setState({
      success: !state.success,
    });
  };

  const onSubmit = async (values) => {
    try {
      await eventsStore.insertEvent(values);
      message.success("Created Event Success");
      toggleSuccess();
      await loadInitialData();
    } catch (e) {
      message.error(`Error on Add Event, ${e.message}`);
      message.error(e.message);
    }
  };
  // const user = store.userData.id;
  // const role = store.userData.role;
  // function onSubmit(e) {
  //   const event = {
  //     title: e.title,
  //     start_date: e.start_date || null,
  //     end_date: e.end_date || null,
  //     // location: e.location,
  //     desc: e.desc,
  //     status: e.status,
  //     event_type: e.event_type,
  //   };
  //   const event2 = {
  //     title: e.title,
  //     start_date: e.start_date || null,
  //     end_date: e.end_date || null,
  //     // location: e.location,
  //     desc: e.desc,
  //     status: "accepted",
  //     event_type: e.event_type,
  //   };
  //   const log = {
  //     user_id: user,
  //     event_name: "Submit Event Calendar",
  //     data: {
  //       location: {
  //         pathname: "/app/calendar",
  //         search: "",
  //         hash: "",
  //         key: role,
  //       },
  //       action: "PUSH",
  //     },
  //   };

  //   if (e.isNewEvent) {
  //     store.events
  //       .update(e.isNewEvent, event)
  //       .then((res) => {
  //         message.success("Event Saved Successfully!");
  //         setState({
  //           success: false,
  //         });
  //       })
  //       .catch((err) => {
  //         message.error(`Error on editing Event, ${err.message}`);
  //         message.error(err.message);
  //       });
  //   } else {
  //     if (store.userData.role === "super_admin") {
  //       store.log.createData(log);
  //       store.events
  //         .createData(event2)
  //         .then((res) => {
  //           message.success("New Event Added!");
  //           toggleSuccess();
  //           fetchData();
  //         })
  //         .catch((err) => {
  //           message.error(`Error on Add Event, ${err.message}`);
  //           message.error(err.message);
  //         });
  //     } else {
  //       store.log.createData(log);
  //       store.events
  //         .createData(event)
  //         .then((res) => {
  //           message.success("Event Created, Waiting for Admin Approval");
  //           toggleSuccess();
  //           fetchData();
  //         })
  //         .catch((err) => {
  //           message.error(`Error on Add Event`);
  //           message.error(err.message);
  //         });
  //     }
  //   }
  // }

  const CalendarData = store.events.data.map((e) => {
    // console.log(e, "here");
    return {
      title: e.name,
      start: e.start_date,
      end: e.end_date,
      id: e.id,
      location: e.location,
      desc: e.desc,
      status: e.status,
      event_type: e.event_type,
      color: "orange",
    };
  });

  // const CalendarDataAccepted = _.filter(
  //   store.events.data,
  //   (i) => i.status === "accepted"
  // ).map((e) => {
  //   if (e.event_type === "Company Event") {
  //     return {
  //       title: e.title,
  //       start: e.start_date,
  //       end: e.end_date,
  //       id: e.id,
  //       location: e.location,
  //       desc: e.desc,
  //       status: e.status,
  //       event_type: e.event_type,
  //       color: "#ffd400",
  //     };
  //   } else if (e.event_type === "BOD Schedule") {
  //     return {
  //       title: e.title,
  //       start: e.start_date,
  //       end: e.end_date,
  //       id: e.id,
  //       location: e.location,
  //       desc: e.desc,
  //       status: e.status,
  //       event_type: e.event_type,
  //       color: "dodgerblue",
  //     };
  //   } else {
  //     return {
  //       title: e.title,
  //       start: e.start_date,
  //       end: e.end_date,
  //       id: e.id,
  //       location: e.location,
  //       desc: e.desc,
  //       status: e.status,
  //       event_type: e.event_type,
  //       color: "#ed1f24",
  //     };
  //   }
  // });

  // let modif = (value) => {
  //   let modified = _.filter(CalendarData, (event) => {
  //     return event.id == value.event.id;
  //   });
  //   const data = modified[0];
  //   // console.log(data, "Ini Data")
  // };

  const setDate = (event) => {
    // console.log(event, "ini event")
    let date = new Date(event.date);
    let fixDate = moment(date);
    setState({
      success: true,
    });
    form.setFieldsValue({
      start_date: fixDate || null,
    });
  };

  // const setEditMode = (value) => {
  //   setState((prevState) => ({
  //     ...prevState,
  //     success: true,
  //     detail: false,
  //   }));
  //   let data = value;
  //   // console.log(data, "ini data")
  //   form.setFieldsValue({
  //     start_date: data.start_date || null,
  //     title: data.title || null,
  //     event_type: data.event_type,
  //     end_date: data.end_date || null,
  //     desc: data.desc || null,
  //     location: data.location || null,
  //     isNewEvent: data.isNewEvent || null,
  //   });
  // };

  const handlerClickedEvent = (values) => {
    alert("Di klik");
  };
  const setDetail = (value) => {
    let modified = _.filter(store.events.data, (event) => {
      return event.id == value.event.id;
    });
    const data = modified[0];
    setStatus(data.status);
    setState((prevState) => ({
      ...prevState,
      detail: true,
    }));

    let dateStart = new Date(data.start_date);
    let dateEnd = new Date(data.end_date);
    let fixDateStart = moment(dateStart).format("Do MMMM YYYY");
    let fixDateEnd = moment(dateEnd).format("Do MMMM YYYY");
    // start.setDate(change.getDate())
    // console.log(data, "ini data")
    // console.log(fixDateStart, "ini start")
    form.setFieldsValue({
      start_date: fixDateStart || null,
      event_type: data.event_type,
      name: data.name || null,
      end_date: fixDateEnd || null,
      desc: data.description || null,
      location: data.location || null,
      // isNewEvent: data.id || null,
      // status: "accepted" || null,
    });
    // console.log(state.status, "hua")
  };

  // const deleteEvent = (id) => {
  //   setState((prevState) => ({
  //     ...prevState,
  //     detail: false,
  //   }));
  //   // console.log(id, "Anda punya id?")
  //   store.events
  //     .delete(id)
  //     .then((res) => {
  //       message.success("action success!");
  //       fetchData();
  //     })
  //     .catch((err) => {
  //       message.error(err.message);
  //     });
  // };

  {
    return (
      <div
        style={{
          padding:
            store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
              ? "10px"
              : "",
        }}
      >
        <Card>
          <PageHeader
            style={{
              padding: store.ui.mediaQuery.isMobile ? "16px 0px 20px" : "",
            }}
            className={"card-page-header"}
            title={
              <div>
                <h2
                  style={{
                    fontSize: store.ui.mediaQuery.isMobile ? "1rem" : "",
                  }}
                >
                  Calendar Event
                </h2>
              </div>
            }
            subTitle=""
            extra={
              <Button color="secondary" onClick={toggleSuccess}>
                <PlusOutlined /> Add New Event
              </Button>
            }
          />
          {/* {Modal2()} */}
          {renderModal()}
          {modalView()}
          {/* {console.log(store.events.data, "here data")} */}
          <FullCalendar
            header={{
              left: "prev, next, today",
              center: "title",
              right: "title",
            }}
            editable={true}
            weekends={true}
            eventClick={(info) => setDetail(info)}
            dateClick={(info) => setDate(info)}
            events={CalendarData}
            defaultView="dayGridMonth"
            plugins={[dayGridPlugin, interactionPlugin]}
          />
        </Card>
      </div>
    );
  }

  function renderModal() {
    return (
      <Modal
        title="Calendar Event"
        closable={true}
        okText="Save"
        cancelText="Cancel"
        destroyOnClose={true}
        onCancel={() => {
          form.validateFields().then((values) => {
            form.resetFields();
          });
          toggleSuccess();
        }}
        onOk={() => {
          form
            .validateFields()
            .then((values) => {
              form.resetFields();
              onSubmit(values);
              form.setFieldsValue({});
            })
            .catch((info) => {
              console.log("Validate Failed:", info);
            });
        }}
        // destroyOnClose={true}
        visible={state.success}
      >
        <Form
          layout="vertical"
          form={form}
          className={"custom-form"}
          name="form_in_modal"
          initialValues={initialData}
        >
          <Form.Item
            name="name"
            label="Event Type"
            rules={[{ required: true }]}
          >
            <Input placeholder="Input Event" />
          </Form.Item>

          {/* <Form.Item name="title" label="Title" rules={[{ required: true }]}>
            <Input />
          </Form.Item> */}

          <Row className="flex" justify="space-between">
            <Col>
              <Form.Item
                name="start_date"
                label="Start Date"
                rules={[{ required: true }]}
              >
                <DatePicker showTime />
              </Form.Item>
            </Col>
            <Col>
              <Form.Item
                name="end_date"
                label="End Date"
                rules={[{ required: true }]}
              >
                <DatePicker showTime />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item name="description" label="Notes">
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
  function modalView() {
    return (
      <Modal
        title="Detail Calendar Event"
        closable={true}
        okText="Save"
        cancelText="Cancel"
        destroyOnClose={true}
        visible={state.detail}
      >
        <Form
          layout="vertical"
          form={form}
          className={"custom-form"}
          name="form_in_modal"
          initialValues={initialData}
        >
          <Form.Item name="name" label="Title">
            <Input disabled={true} />
          </Form.Item>
          <Row className="flex" justify="space-between">
            <Col>
              <Form.Item name="start_date" label="Start Date">
                <Input disabled={true} width={300} />
              </Form.Item>
            </Col>
            <Col>
              <Form.Item name="end_date" label="End Date">
                <Input disabled={true} width={300} />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item name="desc" label="Notes">
            <Input disabled={true} />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
});

export default Event;
