import React, {Component, useEffect,useState} from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
//import '@fullcalendar/core/main.css';
import interactionPlugin from "@fullcalendar/interaction";
import '@fullcalendar/daygrid/main.css';
import 'antd/dist/antd.css';
import { Row, Col, DatePicker, Badge, Form, Alert, Input, Select, Card, message, Modal, Table, Button, Popconfirm, PageHeader } from 'antd';
import {DeleteOutlined, PlusOutlined} from "@ant-design/icons";
import {useStore} from '../../utils/useStores';
import {observer} from "mobx-react";
import moment from "moment";
import * as _ from 'lodash';

let status
const Calendar = observer((initialData) => {
    
    const store = useStore();
    const [form] = Form.useForm();

    const [state, setState] = useState({
        data: {},
        success : false,
        detail : false,
        title: '',
        location: '',
        start_date: '',
        end_date: '',
        desc: '',
        id: '',
        isNewEvent: false
    })
    const[status, setStatus] = useState('')

    useEffect(() => {
        fetchData()
    }, []);
    
    async function fetchData() {
        await store.events.getAll();
		await store.user.getAll();
    }

    const toggleSuccess = () =>{
        setState({
            success: !state.success
        });
    }
    const user = store.userData.id
    const role = store.userData.role
    function onSubmit(e) {
        const event = {
            title: e.title,
            start_date: e.start_date || null,
            end_date: e.end_date || null,
            location:e.location,
            desc: e.desc,
            status: e.status,
            event_type: e.event_type,
        };
        const event2 = {
            title: e.title,
            start_date: e.start_date || null,
            end_date: e.end_date || null,
            location:e.location,
            desc: e.desc,
            status: "accepted",
            event_type: e.event_type,
        };
        const log = {
            user_id : user,
            event_name: "Submit Event Calendar",
            data: {
                location:{
                    pathname:"/app/calendar",
                    search: "",
                    hash: "",
                    key: role,
                },
                action:"PUSH",
            },
        }

        if(e.isNewEvent) {
            store.events.update(e.isNewEvent, event)
            .then(res => {
                message.success('Event Saved Successfully!');
                setState({
                    success:false
                })
            })
            .catch(err => {
                message.error(`Error on editing Event, ${err.message}`);
                message.error(err.message);
            });
        } else {
            if(store.userData.role === "super_admin"){
                store.log.createData(log)
                store.events.createData(event2)
                .then(res => {
                    message.success('New Event Added!');
                    toggleSuccess();
                    fetchData();
                })
                .catch(err => {
                    message.error(`Error on Add Event, ${err.message}`);
                    message.error(err.message);
                });
            }else{
                store.log.createData(log)
                store.events.createData(event)
                .then(res => {
                    message.success('Event Created, Waiting for Admin Approval');
                    toggleSuccess();
                    fetchData();
                })
                .catch(err => {
                    message.error(`Error on Add Event`);
                    message.error(err.message);
                });
            }
        }
    }
    
    const CalendarData = store.events.data.map(e => { 
        if(e.event_type === "Company Event" && e.status === "accepted") {
            return {
                title: e.title,
                start : e.start_date,
                end : e.end_date,
                id : e.id,
                location : e.location,
                desc: e.desc,
                status: e.status,
                event_type: e.event_type,
                color:"#ffd400",
            } 
        }else if(e.event_type === "BOD Schedule" && e.status === "accepted"){
            return {
                title: e.title,
                start : e.start_date,
                end : e.end_date,
                id : e.id,
                location : e.location,
                desc: e.desc,
                status: e.status,
                event_type: e.event_type,
                color:"dodgerblue",
            } 
        }else if(e.event_type === "Holiday" && e.status === "accepted") {
            return {
                title: e.title,
                start : e.start_date,
                end : e.end_date,
                id : e.id,
                location : e.location,
                desc: e.desc,
                status: e.status,
                event_type: e.event_type,
                color:"#ed1f24",
            } 
        }else{
            return {
                title: e.title,
                start : e.start_date,
                end : e.end_date,
                id : e.id,
                location : e.location,
                desc: e.desc,
                status: e.status,
                event_type: e.event_type,
                color:"darkgray",
            } 
        }
    });

    const CalendarDataAccepted = _.filter(store.events.data, i => i.status === "accepted" ).map(e => { 
        if(e.event_type === "Company Event") {
            return {
                title: e.title,
                start : e.start_date,
                end : e.end_date,
                id : e.id,
                location : e.location,
                desc: e.desc,
                status: e.status,
                event_type: e.event_type,
                color:"#ffd400",
            } 
        }else if(e.event_type === "BOD Schedule"){
            return {
                title: e.title,
                start : e.start_date,
                end : e.end_date,
                id : e.id,
                location : e.location,
                desc: e.desc,
                status: e.status,
                event_type: e.event_type,
                color:"dodgerblue",
            } 
        }else {
            return {
                title: e.title,
                start : e.start_date,
                end : e.end_date,
                id : e.id,
                location : e.location,
                desc: e.desc,
                status: e.status,
                event_type: e.event_type,
                color:"#ed1f24",
            }
        }
    });

    let modif = (value) => {
        let modified = _.filter(CalendarData, event =>{
            return event.id == value.event.id
        })
        const data = modified[0];
        // console.log(data, "Ini Data")
        }

    const setDate = (event) => { 
        // console.log(event, "ini event")
        let date = new Date(event.date);
        let fixDate = moment(date);
        setState({
            success : true,
        })
        form.setFieldsValue({
            start_date : fixDate || null,
        })
    }

    const setEditMode = (value) => {
        setState(prevState => ({
            ...prevState,
            success : true,
            detail : false,
        }));
        let data = value;
        // console.log(data, "ini data")
        form.setFieldsValue({
            start_date : data.start_date  || null,
            title : data.title || null,
            event_type : data.event_type,
            end_date : data.end_date || null,
            desc : data.desc || null,
            location: data.location || null,
            isNewEvent: data.isNewEvent || null,
        })
    }
    const setDetail = (value) => {
        let modified = _.filter(CalendarData, event =>{
            return event.id == value.event.id
        })
        const data = modified[0];
        setStatus(data.status)
        setState(prevState => ({
            ...prevState,
            detail : true
        }));
       
        let dateStart = new Date(data.start)
        let dateEnd = new Date(data.end)
        let fixDateStart = moment(dateStart)
        let fixDateEnd = moment(dateEnd)
        // start.setDate(change.getDate())
        // console.log(data, "ini data")
        // console.log(fixDateStart, "ini start")
        form.setFieldsValue({
            start_date : fixDateStart  || null,
            event_type : data.event_type,
            title : data.title || null,
            end_date : fixDateEnd || null,
            desc : data.desc || null,
            location: data.location || null,
            isNewEvent: data.id || null,
            status: "accepted" || null,
        })
        // console.log(state.status, "hua")
    }
    const deleteEvent = (id) => {
        setState(prevState => ({
            ...prevState,
            detail : false,
        }));
        // console.log(id, "Anda punya id?")
        store.events.delete(id)
        .then(res => {
            message.success('action success!')
            fetchData()
        })
        .catch(err => {
            message.error(err.message)
        })
    }

    {
        return  <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <Card>
                <PageHeader
                style={{padding :store.ui.mediaQuery.isMobile ? '16px 0px 20px' : ''}}
                className={'card-page-header'}
                title={<div><h2 style={{fontSize: store.ui.mediaQuery.isMobile?'1rem':'' }} >Calendar Event</h2>  </div>}
                subTitle=""
                extra={
                    store.userData.role !== "BOD" && [
                    <Button color="secondary" onClick={toggleSuccess}>
                        <PlusOutlined /> Add New Event
                    </Button>]}
            />
            {/* {Modal2()} */}
            {renderModal()}
            {modalView()}
            <FullCalendar
            header={{left:'prev, next, today', center:'title', right:'title'}}
            editable={true}
            weekends={true}
            eventClick={(info) => setDetail(info)}
            dateClick={(info) => store.userData.role !== "BOD" && setDate(info)}
            events={CalendarData}
            defaultView="dayGridMonth" plugins={[ dayGridPlugin, interactionPlugin ]} 
            /> 
            </Card>
            </div>
    } 

    function renderModal() {
        return <Modal title="Calendar Event"
        closable={true}
		okText="Save"
        cancelText="Cancel"
        destroyOnClose={true}
        onCancel={() => { 
            form
            .validateFields()
            .then(values => {
                form.resetFields();
            })
            toggleSuccess();
        }}
        onOk={ () => { 
            form
            .validateFields()
            .then(values => {
                form.resetFields();
                onSubmit(values);
                form.setFieldsValue({});
            })
            .catch(info => {
                console.log('Validate Failed:', info);
            });
        }}
        destroyOnClose={true}
        visible={state.success}
    >
        <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
            <Form.Item name="event_type" label="Event Type" rules={[{ required: true}]}>
                <Select placeholder="Select Event Type">
                    {['Company Event',
                      'BOD Schedule',
                      'Holiday',].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                </Select>
            </Form.Item>
            
            <Form.Item name="title" label="Title" rules={[{ required: true }]}>
                <Input/>
            </Form.Item>

            <Row className="flex" justify="space-between">
                <Col>
                    <Form.Item name="start_date" label="Start Date" rules={[{ required: true }]}>
                        <DatePicker showTime />
                    </Form.Item>
                </Col>
                <Col>
                    <Form.Item name="end_date" label="End Date" rules={[{ required: true }]}>
                        <DatePicker showTime />
                    </Form.Item>
                </Col>
            </Row>

            <Form.Item name="isNewEvent" hidden={true}>
                <Input />
            </Form.Item>
            <Form.Item name="status" hidden={true}>
                <Input  disabled={true} />
            </Form.Item>

            <Form.Item name="location" label="Location" rules={[{ required: true }]}>
                <Input />
            </Form.Item>

            <Form.Item name="desc" label="Notes">
                <Input />
            </Form.Item>
        </Form>
    </Modal>
    }
    function modalView() {
        return <Modal title=" Detail Calendar Event"
        closable={true}
        okText="Edit"
        cancelText="Cancel"
        destroyOnClose={true}
        onCancel={() => { 
            form
            .validateFields()
            .then(values => {
                form.resetFields();
            })
            setState({
                detail : false,
            })

        }}
        onOk={ () => { 
            if (store.userData.role !== "super_admin") {
                setState({detail:false}) 
                message.error("Only Super Admin Allowed to Edit Event!");
            }
            else {
                form
                .validateFields()
                .then(values => {
                    form.resetFields();
                    setEditMode(values);
                    form.setFieldsValue({});
                })
                .catch(info => {
                    console.log('Validate Failed:', info);
                });
            }
        }}
        destroyOnClose={true}
        visible={state.detail}>

        <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
        {store.userData.role === "super_admin" &&
            <Form.Item style={{marginBottom:"-2%"}}>
                  {status === null ? <div>
                    <Button style={{float:"right", marginLeft:7}} onClick={() => {
                        form.validateFields()
                        .then(values => {
                            form.resetFields();
                            onSubmit(values);
                            form.setFieldsValue({});
                        })
                    }}>Accept Event</Button>
                    <Button style={{float:"right", marginLeft:7}} onClick={() => {
                        form.validateFields().then(values=> {
                        form.resetFields();
                        deleteEvent(values.isNewEvent)})}}>Reject Event</Button>
                </div>
            : null}
            <Popconfirm
                title="Are you sure delete this event?"
                onCancel=""
                onConfirm={ () => {
                    form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        deleteEvent(values.isNewEvent);
                    })
                } }
                okText="Yes"
                cancelText="No"
                >
                <Button style={{float:"right"}}> Delete </Button>
            </Popconfirm> 
            </Form.Item> }
            <Form.Item name="event_type" label="Event Type">
                <Select placeholder="Select Event Type" disabled={true}>
                    {['Company Event',
                      'BOD Schedule',
                      'Holiday',].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                </Select>
            </Form.Item> 
            <Form.Item name="title" label="Title" >
                <Input disabled={true} />
            </Form.Item>
            <Row className="flex" justify="space-between">
                <Col>
                    <Form.Item name="start_date" label="Start Date" >
                        <DatePicker  disabled={true} showTime />
                    </Form.Item>
                </Col>
                <Col>
                    <Form.Item name="end_date" label="End Date" >
                        <DatePicker  disabled={true} showTime />
                    </Form.Item>
                </Col>
            </Row>
            <Form.Item name="isNewEvent" hidden={true}>
                <Input  disabled={true} />
            </Form.Item>
            <Form.Item name="status" hidden={true}>
                <Input  disabled={true} />
            </Form.Item>
            <Form.Item name="location" label="Location" >
                <Input  disabled={true}/>
            </Form.Item>
            <Form.Item name="desc" label="Notes">
                <Input  disabled={true}/>
            </Form.Item >
        </Form>
    </Modal>
    }

} );

export default Calendar;