import {inject, observer} from "mobx-react";
import * as React from "react";
import {AppointmentForm} from "@devexpress/dx-react-scheduler-material-ui";
import IconButton from "@material-ui/core/IconButton";
import Close from "@material-ui/icons/Close";
import Create from "@material-ui/icons/Create";
import TextField from "@material-ui/core/TextField";
import CalendarToday from "@material-ui/icons/CalendarToday";
import {KeyboardDateTimePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import MomentUtils from "@date-io/moment";
import LocationOn from "@material-ui/icons/LocationOn";
import Notes from "@material-ui/icons/Notes";
import Button from "@material-ui/core/Button";
import {Grid} from "@material-ui/core"
import {useStore} from "../../utils/useStores";
import {useState} from "react";
import {appointments} from './appointments';

export const AppointmentFormContainerBasic = observer((props) => {
const store = useStore();
const [state, setState] = useState({
		appointmentChanges: {},
		titleError: '',
		selectedDate: new Date(),
		form: {
			title: '',
			location: '',
			desc: '',
			start_date: null,
			end_date: null,
		}
	})


const getAppointmentData = () => {
	const {appointmentData} = props
	return appointmentData;
}
const getAppointmentChanges = () => {
	const {appointmentChanges} = props
	return appointmentChanges
}

	function changeAppointment({field, changes}) {
		const nextChanges = {
			...getAppointmentChanges(),
			[field]: changes,
		};

		setState({
			appointmentChanges: nextChanges,
		});

	}

	function commitAppointment(type) {
		console.log('commitAppointment type', type);
		const {commitChanges} = props;
		const appointment = {
			...getAppointmentData(),
			...getAppointmentChanges(),
		};
		if (type === 'deleted') {
			commitChanges({[type]: appointment.id});
		} else if (type === 'changed') {
			commitChanges({[type]: {[appointment.id]: appointment}});
		} else {
			commitChanges({[type]: appointment});
		}
		setState({appointmentChanges: {}});
	}

	function onSubmit() {
		const {title, location, start_date, end_date, desc} = state;
		const {store} = props;

		const data = {
			title: title,
			location: location,
			desc: desc,
			start_date: start_date || null,
			end_date: end_date || null
		};

		store.events.eventsData(data).then(res => {
			return res
		}).catch(err => {
			return err
		})
	}

	const change = (key, value) => {
		setState({form: {...state.form,[key]: value}});
	};

	const validate = () => {
		let isError = false;
		const errors = {
			titleError: "",
		}
		if (state.form.title === '') {
			isError = true;
			errors.emailError = "Requires valid email";
		}

	}

	const handleDateChange = (date) => {
		// console.log({date}, 'AppointmentFormContainerBasic -> handleDateChange')
		setState({
			form: {
				...state.form,
				start_date: date
			}
		})
	};

	const handleChangeData = (date) => {
		// console.log({date}, 'AppointmentFormContainerBasic -> handleChangeData')
		setState({
			form: {
				...state.form,
				end_date: date
			}
		})
	};

	const {
		classes,
		visible,
		visibleChange,
		appointmentData,
		cancelAppointment,
		target,
		onHide,
	} = props;

	const {appointmentChanges, title, location, desc, start_date, end_date, date} = state;

	const displayAppointmentData = {
		...appointmentData,
		...appointmentChanges,
		...title,
		...location,
		...desc,
		...start_date,
		...end_date
	};

	const isNewAppointment = appointments.id === undefined;
	const applyChanges = isNewAppointment
		? () => commitAppointment('added')
		: () => commitAppointment('changed');

	const textEditorProps = field => ({
		variant: 'outlined',
		onChange: ({target: change}) => changeAppointment({
			field: [field], changes: change.value,
		}),
		value: displayAppointmentData[field] || '',
		label: field[0].toUpperCase() + field.slice(1),
		className: classes.textField,
	});

	const pickerEditorProps = field => ({
		className: classes.picker,
		// keyboard: true,
		ampm: false,
		value: displayAppointmentData[field],
		onChange: date => {
			setState({
				form: {
					...state.form,
					[field]: date
				}
			})
		},
		inputVariant: 'outlined',
		format: 'DD/MM/YYYY HH:mm',
		disablePast: true,
		onError: () => null,
	});

	const cancelChanges = () => {
		setState({
			appointmentChanges: {},
		});
		visibleChange();
		cancelAppointment();
	};

	return (
		<AppointmentForm.Overlay
			visible={visible}
			target={target}
			fullSize
			onHide={onHide}
		>
			<div>
				<div className={classes.header}>
					<IconButton
						className={classes.closeButton}
						onClick={cancelChanges}
					>
						<Close color="action"/>
					</IconButton>
				</div>
				<div container lg={12} className={classes.content}>
					<div className={classes.wrapper}>
						<Create className={classes.icon} color="action"/>
						<TextField
							{...textEditorProps('title')}
							onChange={(e) => change('title', e.target.value)}
							value={state.form.title}
						/>
					</div>
					<Grid container lg={12}>
						{/*<CalendarToday className={classes.icon} color="action"/>*/}
						<MuiPickersUtilsProvider utils={MomentUtils}>
							<Grid lg={6}>
								<KeyboardDateTimePicker
									label="Start Date"
									{...pickerEditorProps('startDate')}
									onChange={date => handleDateChange(date)}
									value={state.form.start_date}
								/>
							</Grid>
							<Grid lg={6}>
								<KeyboardDateTimePicker
									label="End Date"
									{...pickerEditorProps('endDate')}
									onChange={date => handleChangeData(date)}
									value={state.form.end_date}
								/>
							</Grid>
						</MuiPickersUtilsProvider>
					</Grid>
					<div className={classes.wrapper}>
						<LocationOn className={classes.icon} color="action"/>
						<TextField
							{...textEditorProps('location')}
							onChange={(e) => change('location', e.target.value)}
							value={state.form.location}
						/>
					</div>
					<div className={classes.wrapper}>
						<Notes className={classes.icon} color="action"/>
						<TextField
							{...textEditorProps('notes')}
							multiline
							onChange={(e) => change('desc', e.target.value)}
							value={state.form.desc}
							rows="6"
						/>
					</div>
				</div>
				<div className={classes.buttonGroup}>
					{!isNewAppointment && (
						<Button
							variant="outlined"
							color="secondary"
							className={classes.button}
							onClick={() => {
								visibleChange();
								commitAppointment('deleted');
							}}
						>
							Delete
						</Button>
					)}
					<Button
						variant="outlined"
						color="primary"
						className={classes.button}
						onClick={() => {
							onSubmit();
							visibleChange();
							applyChanges();
						}}
					>
						{isNewAppointment ? 'Create' : 'Save'}
					</Button>
				</div>
			</div>
		</AppointmentForm.Overlay>
	);


});
