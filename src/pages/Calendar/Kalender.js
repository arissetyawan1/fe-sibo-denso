import React, {Components, useEffect,useState} from 'react';
import 'antd/dist/antd.css';
import { Calendar, Row, Col, DatePicker, Badge, Form, Alert, Input, Empty, Select, Card, message, Modal, Table, Button, PageHeader } from 'antd';
import {useStore} from '../../utils/useStores';
import {FilterOutlined, PlusOutlined, DeleteOutlined, ExclamationCircleOutlined, EditOutlined } from "@ant-design/icons";
import {observer} from "mobx-react";
import {appointments} from './appointments';
import moment from "moment";
import * as _ from 'lodash';

const Kalendar = observer((initialData) => {
    // const {RangePicker} = DatePicker;
    const store = useStore();
    const [form] = Form.useForm();

    const [state, setState] = useState({
        data: appointments,
        success : false,
        title: '',
        location: '',
        start_date: new Date(),
        end_date: new Date(),
        desc: '',
        isNewEvent: false,
    })

    
    useEffect(() => {
        fetchData()
    }, []);
    
    async function fetchData() {
        await store.events.getEventsData();
    }

    const toggleSuccess = () =>{
        setState({
            success: !state.success
        });
    }

    const onSubmit = (e) => {
        const data = {
            title: e.title,
            location:e.location,
            desc: e.desc,
            start_date: e.start_date || null,
            end_date: e.end_date || null
        };
        
        if(e.isNewEvent) {
            store.events.update(e.isNewEvent, data)
            .then(res => {
                message.success('Event Saved Successfully!');
                toggleSuccess();
                fetchData();
            })
            .catch(err => {
                message.error(`Error on editing Event, ${err.message}`);
                message.error(err.message);
            });
        } else {
        store.events.create(data)
        .then(res => {
            message.success('New Event Added!');
            toggleSuccess();
            fetchData();
        })
        .catch(err => {
            message.error(`Error on creating Event, ${err.message}`);
            message.error(err.message);
        });
    }
}
function dateCellRender() {
    const listData = store.events.data;
    console.log(listData, "ini event?")
    // return (
    //   <ul className="events">
    //     {listData.map(item => (
    //       <li key={item.content}>
    //         <Badge status={item.type} text={item.content} />
    //       </li>
    //     ))}
    //   </ul>
    // );
  }
const setEditMode = (value) => {
    setState(prevState => ({
        ...prevState,
        success:true
    }))

    form.setFieldsValue({
        isNewEvent: value.id,
        success: true,
        title: value.title,
        location: value.location,
        desc: value.desc,
        start_date: value.start_date || null,
        end_date: value.end_date || null
    })
}

{
    return <div>
        <Card className={'shadow'}>
        <Card bordered={false} bodyStyle={{ padding: 0 }}>
            <PageHeader
                className={'card-page-header'}
                title={<div> <h2> Calendar Event </h2> </div>}
                subTitle=""
                extra={[
                    <Button color="secondary" onClick={toggleSuccess}>
                        <PlusOutlined /> Add New Event
                    </Button>]}
            />
            {console.log(state.success, "INI STATE")}
            {renderModal()}
            <Calendar dateCellRender={dateCellRender} onSelect={setEditMode}/>
        </Card>
        </Card>
    </div>
}

    function renderModal() {
        return <Modal title="Calendar Event"
		okText="Save"
        cancelText="Cancel"
        onCancel={() => { 
            form.setFieldsValue({});
            toggleSuccess();
        }}
        onOk={ () => { 
            form
            .validateFields()
            .then(values => {
                form.resetFields();
                onSubmit(values);
                form.setFieldsValue({});
            })
            .catch(info => {
                console.log('Validate Failed:', info);
            });
        }}
        destroyOnClose={true}
        visible={state.success}
    >
        <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
            <Form.Item name="title" label="Title" rules={[{ required: true }]}>
                <Input />
            </Form.Item>

            <Row className="flex" justify="space-between">
                <Col>
                    <Form.Item name="start_date" label="Start Date" rules={[{ required: true }]}>
                        <DatePicker showTime />
                    </Form.Item>
                </Col>
                <Col>
                    <Form.Item name="end_date" label="End Date" rules={[{ required: true }]}>
                        <DatePicker showTime />
                    </Form.Item>
                </Col>
            </Row>

            <Form.Item name="isNewEvent" hidden={true}>
                <Input />
            </Form.Item>

            <Form.Item name="location" label="Location" rules={[{ required: true }]}>
                <Input />
            </Form.Item>

            <Form.Item name="desc" label="Notes">
                <Input />
            </Form.Item>

        </Form>
    </Modal>
    }

})

export default Kalendar;