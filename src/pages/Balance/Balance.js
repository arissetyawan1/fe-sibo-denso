import React, {useEffect} from "react";
import {useStore} from "../../utils/useStores";
import {Table, Button, Tabs, Card} from 'antd'
import {observer} from 'mobx-react-lite';
import {BalanceRoute} from './BalanceRoute'
import {Link, useHistory} from 'react-router-dom'
import {LINKS} from "../../routes";

export const Balance = observer(() => {
  const history = useHistory();
  return (
    <div>
      {/*<Tabs defaultActiveKey="2w" onChange={(key) => {*/}
      {/*  if(key === '2w') {*/}
      {/*    history.push(LINKS.SERVICE + '/2w')*/}
      {/*  }*/}
      {/*  if(key === '4w') {*/}
      {/*    history.push(LINKS.SERVICE + '/4w')*/}
      {/*  }*/}
      {/*  if(key === 'cari_service') {*/}
      {/*    history.push(LINKS.SERVICE + '/cari_service')*/}
      {/*  }*/}
      {/*  // if(key)*/}
      {/*}}>*/}
      {/*  <Tabs.TabPane tab="2W" key="2w">*/}
      {/*  </Tabs.TabPane>*/}
      {/*  <Tabs.TabPane tab="4W" key="4w">*/}
      {/*  </Tabs.TabPane>*/}
      {/*  <Tabs.TabPane tab="Cari Service" key="cari_service">*/}
      {/*  </Tabs.TabPane>*/}
      {/*</Tabs>*/}
      {/*<hr/>*/}
      <BalanceRoute/>
    </div>
  )
});
