import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../routes";
import {BalanceTransaction} from "./Transaction/Transaction";
import {BalanceAccount} from "./Account/Account";
import {BalanceInvoices} from "./Invoice/Invoice";
import {AnimatedSwitch} from "react-router-transition";
import {useStore} from "../../utils/useStores";
import {observer} from "mobx-react-lite";

export const BalanceRoute = observer(() => {
  const store = useStore();
    return (
      <AnimatedSwitch
        atEnter={{opacity: 0}}
        atLeave={{opacity: 0}}
        atActive={{opacity: 1}}
        className="switch-wrapper-app">
            <Route path={LINKS.BALANCE + "/transactions"}>
                <BalanceTransaction/>
            </Route>
          <Route path={LINKS.BALANCE + "/accounts"}>
            <BalanceAccount/>
          </Route>
          <Route path={LINKS.BALANCE + "/invoices"}>
            <BalanceInvoices/>
          </Route>
      </AnimatedSwitch>
    )
});
