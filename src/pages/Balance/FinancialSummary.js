import React, { Component, useState, useEffect } from 'react';
import Highlighter from 'react-highlight-words';
import 'antd/dist/antd.css';
import { Table, Input, Button, Icon } from 'antd';
import {Card, CardBody, CardColumns, CardHeader, CardTitle, Col, Row} from 'reactstrap';
import {Doughnut, Bar} from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import {inject, observer} from "mobx-react";
import * as _ from "lodash";
import { HotTable } from '@handsontable/react';
import * as canvasDatagrid from 'canvas-datagrid';
import {useStore} from "../../../utils/useStores";
const labelMapping = {
    depreciation: 'Depreciation & Amortisation',
    EBITDAMargin: 'EBITDA Margin',
    EBITDAConstruction: 'EBITDA ex. Construction',
    netProfit: 'Net Profit (Loss)',
    tradingProfit: 'Trading Profit Astra Portion',
};

const doughnut = {
    labels: [
        'Red',
        'Green',
        'Yellow',
    ],
    datasets: [
        {
            data: [300, 50, 100],
            backgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56',
            ],
            hoverBackgroundColor: [
                '#FF6384',
                '#36A2EB',
                '#FFCE56',
            ],
        }],
};

const data = [
    {
        key: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
    },
    {
        key: '2',
        name: 'Joe Black',
        age: 42,
        address: 'London No. 1 Lake Park',
    },
    {
        key: '3',
        name: 'Jim Green',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
    },
    {
        key: '4',
        name: 'Jim Red',
        age: 32,
        address: 'London No. 2 Lake Park',
    },
];

const options = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips
    },
    maintainAspectRatio: false
}

const FinancialSummary = () => {

const [searchText, setSearchText] = useState('');
const [isLoaded, setIsLoaded] = useState(false);
const [report, setReport] = useState({});
const store = useStore();
const handleSearch = (selectedKeys, confirm) => {
        confirm();
        // this.setState({ searchText: selectedKeys[0] });
        setSearchText(selectedKeys[0]);
    };
const handleReset = clearFilters => {
        clearFilters();
        // this.setState({ searchText: '' });
        setSearchText('')
    };
const searchInput = null;
const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.select());
            }
        },
        render: text => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        ),
    });

    useEffect(()=>{
        // store.keymetric.getFinancialSummary()
        //     .then(res => {
        //         setReport(res);
        //         setIsLoaded(true)
                // const grid = canvasDatagrid({
                //     parentNode: document.getElementById('canvas-grid-container'),
                //     data: res.data.rawExcel,
                //     style: {
                //         height: 100,
                //         width: 100
                //     },
                //     allowColumnReordering: false,
                //     allowRowReordering: false,
                //     autoResizeColumns: true,
                //     allowSorting: false
                // });
                //
                // // grid.style.height=100;
                // // grid.style.width=100;
                //
                // grid.allowColumnReordering = false;
                // grid.allowRowReordering = false;
                // grid.autoResizeColumns = true;
                // grid.allowSorting = true;
                // grid.setColumnWidth(2, 5);
            // });
    }, [])
 

  

    const getLabel = (k) => labelMapping[k] ? labelMapping[k] : _.startCase(k);

    const dynamicColors = function() {
        const r = Math.floor(Math.random() * 255);
        const g = Math.floor(Math.random() * 255);
        const b = Math.floor(Math.random() * 255);
        return "rgb(" + r + "," + g + "," + b + ")";
    };

    const generateMTD = (keys = [])=>{

        let datasetLabels = Object.keys(_.get(report, 'data.tollRevenue.mtd') || {});
        let labels = Object.keys(report.data || {})
            .filter(k => ['year', 'month'].indexOf(k) < 0)
            .filter(k => keys.length > 0 ? keys.indexOf(k) >= 0 : true);

        return {
            labels: labels.map(k => getLabel(k)),
            datasets: datasetLabels.map(k => {
                return {
                    label: getLabel(k),
                    // barPercentage: 0.5,
                    barThickness: 20,
                    // maxBarThickness: 8,
                    // minBarLength: 2,
                    backgroundColor: dynamicColors(),
                    data: labels.map(l => _.get(report, `data.${l}.mtd.${k}`))
                }
            }),
            options: {

            }
        }
    }
    
    const generateYTD = (keys = []) => {

        const datasetLabels = Object.keys(_.get(report, 'data.tollRevenue.ytd') || {});
        const labels = Object.keys(report.data || {})
            .filter(k => ['year', 'month'].indexOf(k) < 0)
            .filter(k => keys.length > 0 ? keys.indexOf(k) >= 0 : true);

        return {
            labels: labels.map(k => getLabel(k)),
            datasets: datasetLabels.map(k => {
                return {
                    label: getLabel(k),
                    // barPercentage: 0.5,
                    barThickness: 20,
                    // maxBarThickness: 8,
                    // minBarLength: 2,
                    backgroundColor: dynamicColors(),
                    data: labels.map(l => _.get(report, `data.${l}.ytd.${k}`))
                }
            }),
            options: {

            }
        }
    }

    const generateFY = (keys = []) =>{

        let datasetLabels = Object.keys(_.get(report, 'data.tollRevenue.fy') || {});
        let labels = Object.keys(report.data || {})
            .filter(k => ['year', 'month'].indexOf(k) < 0)
            .filter(k => keys.length > 0 ? keys.indexOf(k) >= 0 : true);

        return {
            labels: labels.map(k => getLabel(k)),
            datasets: datasetLabels.map(k => {
                return {
                    label: getLabel(k),
                    // barPercentage: 0.5,
                    barThickness: 20,
                    // maxBarThickness: 8,
                    // minBarLength: 2,
                    backgroundColor: dynamicColors(),
                    data: labels.map(l => _.get(report, `data.${l}.fy.${k}`))
                }
            }),
            options: {

            }
        }
    }

    const generateCard = (props) => {

        return (
            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">
                        {props.title}
                        <span
                            style={{
                                marginLeft: 10
                            }}
                            className={"text-muted"}>{parseFloat(props.diff).toFixed(2)}%</span>
                    </CardTitle>
                    <div className="large" style={{
                        fontSize: '3em'
                    }}>
                        {props.mainNumber} <span style={{fontSize: 32}} className={"text-muted"}> / {props.secondNumber}</span>
                    </div>
                </CardHeader>
                <CardBody>

                </CardBody>
            </Card>
        )
    }
    const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                width: '30%',
                ...getColumnSearchProps('name'),
            },
            {
                title: 'Age',
                dataIndex: 'age',
                key: 'age',
                width: '20%',
                ...getColumnSearchProps('age'),
            },
            {
                title: 'Address',
                dataIndex: 'address',
                key: 'address',
                ...getColumnSearchProps('address'),
            },
        ];

        const month = _.get(report, 'data.month');
        const year = _.get(report, 'data.year');

        const revenueKeys = [
            'tollRevenue',
            'constructionRevenue',
            'totalRevenue'
        ];

        const expenseKeys = [
            'constructionCost',
            'overlayProvision',
            'directExpense',
            'contributionMargin',
            'CM',
            'indirectExpense',
            'depreciation',
            'operatingProfit',
            'EBITDAMargin',
            'EBITDAConstruction',
        ];

        const otherIncomeExpenseKeys = [
            'netInterest',
            'others',
            'profitBeforeTax',
            'deferredTaxBenefit',
            'netProfit',
            'tradingProfit',
        ];

        const columns2 = Object.keys(_.get(report, 'data.rawExcel[0]', {}))
            .map(k => {
                return {
                    title: k.length > 1 ? _.startCase(k) : '',
                    dataIndex: k,
                    key: k,
                    // ...this.getColumnSearchProps('address'),
                }
            });
        

    return (<div className={'page-section ' + report.id}>

            <div id={"canvas-grid-container"}></div>

            <Table
                columns={columns2}
                pagination={false}
                scroll={{
                    x: 2000,
                    y: 500
                }}
                dataSource={_.get(report, 'data.rawExcel', [])} />
            {/*<HotTable data={_.get(report, 'data.rawExcel', [])} width="600" height="300" />*/}
            <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Revenue
                </div>
            </div>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(revenueKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(revenueKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(revenueKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Expense
                </div>
            </div>
            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Expense MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(expenseKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Expense YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(expenseKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Expense FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(expenseKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Other Income (Expense)
                </div>
            </div>
            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Other Income (Expense) MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(otherIncomeExpenseKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Other Income (Expense) YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(otherIncomeExpenseKeys)}/>
                    </div>
                </CardBody>
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Other Income (Expense) FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(otherIncomeExpenseKeys)}/>
                    </div>
                </CardBody>
            </Card>
        </div>)
    const renderCards =()=> {

        const month = _.get(report, 'data.month');
        const year = _.get(report, 'data.year');

        return (
            <React.Fragment>
                <div className={'page-separator'}>
                    <div className={'page-separator__text'}>
                        Revenue MTD {month} {year}
                    </div>
                </div>
                <Row>
                    <Col lg={4}>
                        {generateCard({
                            title: 'Toll Revenue',
                            mainNumber: _.get(report, `data.tollRevenue.mtd.${month}'${year}`),
                            secondNumber: _.get(report, `data.tollRevenue.mtd.OL3`),
                            diff: (_.get(report, `data.tollRevenue.mtd.actVsOL`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Construction Revenue',
                            mainNumber: _.get(report, `data.constructionRevenue.mtd.${month}'${year}`),
                            secondNumber: _.get(report, `data.constructionRevenue.mtd.OL3`),
                            diff: (_.get(report, `data.constructionRevenue.mtd.actVsOL`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Total Revenue',
                            mainNumber: _.get(report, `data.totalRevenue.mtd.${month}'${year}`),
                            secondNumber: _.get(report, `data.totalRevenue.mtd.OL3`),
                            diff: (_.get(report, `data.totalRevenue.mtd.actVsOL`)),
                        })}
                    </Col>
                </Row>

                <div className={'page-separator'}>
                    <div className={'page-separator__text'}>
                        Revenue YTD {month} {year}
                    </div>
                </div>
                <Row>
                    <Col lg={4}>
                        {generateCard({
                            title: 'Toll Revenue',
                            mainNumber: _.get(report, `data.tollRevenue.ytd.${month}'${year}`),
                            secondNumber: _.get(report, `data.tollRevenue.ytd.OL3`),
                            thirdNumber: _.get(report, `data.tollRevenue.ytd.OL3`),
                            diff: (_.get(report, `data.tollRevenue.ytd.actVsOL`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Construction Revenue',
                            mainNumber: _.get(report, `data.constructionRevenue.ytd.${month}'${year}`),
                            secondNumber: _.get(report, `data.constructionRevenue.ytd.OL3`),
                            diff: (_.get(report, `data.constructionRevenue.ytd.actVsOL`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Total Revenue',
                            mainNumber: _.get(report, `data.totalRevenue.ytd.${month}'${year}`),
                            secondNumber: _.get(report, `data.totalRevenue.ytd.OL3`),
                            diff: (_.get(report, `data.totalRevenue.ytd.actVsOL`)),
                        })}
                    </Col>
                </Row>

                <div className={'page-separator'}>
                    <div className={'page-separator__text'}>
                        Revenue YTD VS Last Year
                    </div>
                </div>
                <Row>
                    <Col lg={4}>
                        {generateCard({
                            title: 'Toll Revenue',
                            mainNumber: _.get(report, `data.tollRevenue.ytd.${month}'${year}`),
                            secondNumber: _.get(report, `data.tollRevenue.ytd.${month}'${year-1}`),
                            diff: (_.get(report, `data.tollRevenue.ytd.actVsOL2`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Construction Revenue',
                            mainNumber: _.get(report, `data.constructionRevenue.ytd.${month}'${year}`),
                            secondNumber: _.get(report, `data.constructionRevenue.ytd.${month}'${year-1}`),
                            diff: (_.get(report, `data.constructionRevenue.ytd.actVsOL2`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Total Revenue',
                            mainNumber: _.get(report, `data.totalRevenue.ytd.${month}'${year}`),
                            secondNumber: _.get(report, `data.totalRevenue.ytd.${month}'${year-1}`),
                            diff: (_.get(report, `data.totalRevenue.ytd.actVsOL2`)),
                        })}
                    </Col>
                </Row>

                <div className={'page-separator'}>
                    <div className={'page-separator__text'}>
                        Revenue Financial Year
                    </div>
                </div>
                <Row>
                    <Col lg={4}>
                        {generateCard({
                            title: 'Toll Revenue',
                            mainNumber: _.get(report, `data.tollRevenue.fy.OL3`),
                            secondNumber: _.get(report, `data.tollRevenue.fy.Aud'${year-1}`),
                            diff: 0,
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Construction Revenue',
                            mainNumber: _.get(report, `data.constructionRevenue.fy.OL3`),
                            secondNumber: _.get(report, `data.constructionRevenue.fy.Aud'${year-1}`),
                            diff: 0,
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Total Revenue',
                            mainNumber: _.get(report, `data.totalRevenue.fy.OL3`),
                            secondNumber: _.get(report, `data.totalRevenue.fy.Aud'${year-1}`),
                            diff: 0,
                        })}
                    </Col>
                </Row>

                {/*END REVENUE*/}

                <div className={'page-separator'}>
                    <div className={'page-separator__text'}>
                        Expenses MTD {month} {year}
                    </div>
                </div>
                <Row>
                    <Col lg={4}>
                        {generateCard({
                            title: 'Toll Revenue',
                            mainNumber: _.get(report, `data.constructionCost.mtd.${month}'${year}`),
                            secondNumber: _.get(report, `data.constructionCost.mtd.OL3`),
                            diff: (_.get(report, `data.constructionCost.mtd.actVsOL`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Construction Revenue',
                            mainNumber: _.get(report, `data.overlayProvision.mtd.${month}'${year}`),
                            secondNumber: _.get(report, `data.overlayProvision.mtd.OL3`),
                            diff: (_.get(report, `data.overlayProvision.mtd.actVsOL`)),
                        })}
                    </Col>

                    <Col lg={4}>
                        {generateCard({
                            title: 'Total Revenue',
                            mainNumber: _.get(report, `data.directExpense.mtd.${month}'${year}`),
                            secondNumber: _.get(report, `data.directExpense.mtd.OL3`),
                            diff: (_.get(report, `data.directExpense.mtd.actVsOL`)),
                        })}
                    </Col>
                </Row>
            </React.Fragment>
        )
    }

}

export default FinancialSummary
