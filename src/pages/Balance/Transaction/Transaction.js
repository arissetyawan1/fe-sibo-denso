import React, {useEffect} from "react";
import {useStore} from "../../../utils/useStores";
import {Table, PageHeader, Button, Menu, Dropdown, Card} from 'antd'
import {observer} from 'mobx-react-lite';
import { FilterOutlined, PlusOutlined} from '@ant-design/icons';
import {Filter} from '../../../component/Filter'
import moment from 'moment';

export const BalanceTransaction = observer((props) => {
  const store = useStore();
  useEffect(() => {
    store.transaction.getAll();
  }, []);

  const columnFin = [
    {
      title: 'Order ID',
      dataIndex: 'order_id',
      key: 'order_id',
      width:135,
      fixed: 'left',

    },
    {
      title: 'Transaction ID',
      dataIndex: 'id',
      key: 'id',
      width:310
    },
    {
      title: 'Va Number',
      dataIndex: 'va_number',
      key: 'va_number',
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: 'Customer',
      dataIndex: 'username',
      key: 'username',

    },
    {
      title: 'Partner Name',
      dataIndex: 'partner_name',
      key: 'partner_name',
    },
    {
      title: 'Parking Name',
      dataIndex: 'parking_name',
      key: 'parking_name',
    },
    {
      title: 'Booking Date',
      dataIndex: 'booking_date',
      key: 'booking_date',
    },
    {
      title: 'Transaction Date',
      dataIndex: 'transaction_date',
      key: 'transaction_date',
    },
    {
      title: 'Deposit Fee',
      dataIndex: 'deposit_fee',
      key: 'deposit_fee',

    },
    {
      title: 'Additional Fee 1',
      dataIndex: 'additional_fee_1',
      key: 'additional_fee_1',

    }, {
      title: 'Additional Fee 2',
      dataIndex: 'additional_fee_2',
      key: 'additional_fee_2',

    },

    {
      title: 'Insurance Fee',
      dataIndex: 'insurance_fee',
      key: 'insurance_fee',


    }, {
      title: 'Order Price',
      dataIndex: 'price',
      key: 'price',

    }, {
      title: 'Order Price',
      dataIndex: 'price',
      key: 'price',

    },

    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',

    },
    {
      title: 'Checkin',
      dataIndex: 'check_in',
      key: 'check_in',
    },
    {
      title: 'Checkout',
      dataIndex: 'check_out',
      key: 'check_out',
    },
    {
      title: 'Status',
      dataIndex: 'payment_status',
      key: 'payment_status',

    },
    {
      title: 'Method',
      dataIndex: 'payment_method',
      key: 'payment_method',

    },
    {
      title: 'Vehicle',
      dataIndex: 'vehicle_type',
      key: 'vehicle_type',

    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',

    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
    }
  ];
  const column = [
    {
      title: '',
      dataIndex: 'id',
      key: 'id',
      render: text => <a>{}</a>,
      width: 20,
      fixed: 'left',

    },
    {
      title: 'Order ID',
      dataIndex: 'order_id',
      key: 'order_id',
      ellipsis: true,
      fixed: 'left',

    },
    {
      title: 'Transaction ID',
      dataIndex: 'id',
      key: 'id',
      ellipsis: true,
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
      width:80
    },
    {
      title: 'Customer',
      dataIndex: 'username',
      key: 'username',
      ellipsis: true,
    },
    {
      title: 'Location',
      dataIndex: 'parking_name',
      key: 'parking_name',
      ellipsis: true,
    },
    {
      title: 'Checkin',
      dataIndex: 'check_in',
      key: 'check_in',
      ellipsis: true,
      render:(record)=> moment(record).format("DD/MM/YY, H:mm:ss")
    },
    {
      title: 'Checkout',
      dataIndex: 'check_out',
      key: 'check_out',
      ellipsis: true,
      render:(record)=> moment(record).format("DD/MM/YY, H:mm:ss")

    },
    {
      title: 'Status',
      dataIndex: 'payment_status',
      key: 'payment_status',
      ellipsis: true,
      width:80
    },
    {
      title: 'Method',
      dataIndex: 'payment_method',
      key: 'payment_method',
      ellipsis: true,
      width:80
    },
    {
      title: 'Vehicle',
      dataIndex: 'vehicle_type',
      key: 'vehicle_type',
      ellipsis: true,
      width:80
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      width:80,
      ellipsis: true,
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
      ellipsis: true,
    },
    {
      title: 'Action',
      key: 'operation',
      width: 65,
      fixed:"right",
      render: () => <Button size="small" style={{fontSize: 10, color:'#5469d4'}} onClick={() => {
      }}>Detail</Button>,
      className:'align-center'
    },
  ];

  return <div>
    <Card bordered={false} style={{}} className={'shadow'} bodyStyle={{padding: '0'}}>
    <PageHeader
      className={'card-page-header'}
      title={<Filter/>}
      subTitle="This is a subtitle"
      extra={<Button>Download</Button>}
    />
    <Table size={"small"}  scroll={{ x: 1500, y:380 }} dataSource={store.transaction.data} columns={column} />
    </Card>
  </div>
});
