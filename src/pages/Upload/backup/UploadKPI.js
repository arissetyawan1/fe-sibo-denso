import React, { Component } from 'react';
import { Col, Row, Button, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Label, Input } from 'reactstrap';
import Highlighter from "react-highlight-words";
import { Table, Icon, Upload, Input as antInput } from 'antd';
import 'antd/dist/antd.css';

const data = [
  {
    key: '1',
    operationaldata: 'Corporate Data',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '2',
    operationaldata: 'IT/Finance',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '3',
    operationaldata: 'Tax & Treasury/Finance',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '4',
    operationaldata: 'Maintenance & Planning',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '5',
    operationaldata: 'HCD',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '6',
    operationaldata: 'CSC',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '7',
    operationaldata: 'Quality Control/Operation',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '8',
    operationaldata: 'Finance',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '9',
    operationaldata: 'Public Relation',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '10',
    operationaldata: 'Toll Transaction Management',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  }, 
];


export default class UploadKPI extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      data,
    };

    this.toggle = this.toggle.bind(this);
    this.toggleWarning = this.toggleWarning.bind(this);
    this.toggleLarge = this.toggleLarge.bind(this);

  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }

  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }

  uploadData() {

    // data.forEach(element => {
      // Axios({
      //   method : 'post',
      //   url : '/entry-all',
      //   data : {
      //     data
      //   }
      // }).then((res) => {
      //   console.log(res.data)
      // })
  
  }
  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        console.log(this);
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  render() {
    const columns = [
      {
        title: 'Operational Data',
        dataIndex: 'operationaldata',
        key: 'operationaldata',
        width: '50%',
        ...this.getColumnSearchProps('operationaldata'),
      },
      {
        title: 'Last Updated',
        dataIndex: 'lastupdated',
        key: 'reconid',
        width: '50%',
        ...this.getColumnSearchProps('lastupdated'),
      },
      {
        title: 'File',
        dataIndex: 'file',
        key: 'file',
        width: '25%',
        ...this.getColumnSearchProps('file'),
        render: () => (
          <div>
              <Upload {...props}>
                <Button>
                  <Icon type="upload" /> Choose File
                </Button>
                {/* <CSVReader onFileLoaded={data => this.setState({data : data})} /> */}
              </Upload>
          </div>
        )
      },
      {
        title: 'Upload',
        dataIndex: 'upload',
        key: 'upload',
        width: '25%',
        ...this.getColumnSearchProps('upload'),

        render: () => (
          <div>
           <Button color="secondary" onClick={this.uploadData}>Upload</Button>
          </div>
        )
      },
    ];

    const props = {
      action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
      onChange({ file}) {
        if (file.status !== 'uploading') {
          console.log(file);
        }
      },
    };

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
          <Table columns={columns} dataSource={data} scroll={{ x:100, y: 1000 }} />
          </Col>
        </Row>
      </div>
    );
  }
}
