import React, { Component } from 'react';
import { Col, Row, Button, Modal, ModalBody, ModalFooter, ModalHeader, FormGroup, Label, Input } from 'reactstrap';
import Highlighter from "react-highlight-words";
import { Table, Icon, Upload, Input as antInput } from 'antd';
import Papa from 'papaparse'
import 'antd/dist/antd.css';
import Axios from 'axios';

const data = [
  {
    key: '1',
    keymetrics: 'Daily Revenue',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',    
    upload: '/dialy_revenue',
  },
  {
    key: '2',
    keymetrics: 'Daily Traffic Volume',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '3',
    keymetrics: 'Average Vehicle KM Travelled',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '4',
    keymetrics: 'Full Route Equivalent',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '5',
    keymetrics: 'Accident',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '6',
    keymetrics: 'Traffic Classification',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '7',
    keymetrics: 'Financial Summary',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '8',
    keymetrics: 'Cashflow',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '9',
    keymetrics: 'Balance Sheet',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },
  {
    key: '10',
    keymetrics: 'Income Statement',
    lastupdated: 'Sunday, 29/12/2019',
    file: '',
    upload: '',
  },

];

const data2 = [];
for (let i = 0; i < 11; i++) {
  data2.push({
    no: i.toString(),
    no: `${i}`,
    reconid: `2222${i}`,
    date: `22-12-2019`,
    name: `David Siozo ${i}`,
    partnername: `Daniel John ${i}`,
    partnerowner: `Manulife ${i}`,
    transactiondate: `22-12-2019`,
    orderprice: `500.000`,
    totalpayment: `700.000`,
    statusrecon: `Done`,
  });
}

export default class UploadKey extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      large: false,
      small: false,
      primary: false,
      success: false,
      warning: false,
      danger: false,
      info: false,
      data,
    };

    this.toggle = this.toggle.bind(this);
    this.toggleWarning = this.toggleWarning.bind(this);
    this.toggleLarge = this.toggleLarge.bind(this);

  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  toggleWarning() {
    this.setState({
      warning: !this.state.warning,
    });
  }

  toggleLarge() {
    this.setState({
      large: !this.state.large,
    });
  }

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        console.log(this);
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => (
      <Highlighter
        highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  render() {
    const columns = [
      {
        title: 'Key Metrics',
        dataIndex: 'keymetrics',
        key: 'keymetrics',
        width: '50%',
        ...this.getColumnSearchProps('keymetrics'),
      },
      {
        title: 'Last Updated',
        dataIndex: 'lastupdated',
        key: 'reconid',
        width: '50%',
        ...this.getColumnSearchProps('lastupdated'),
      },
      {
        title: 'File',
        dataIndex: 'file',
        key: 'file',
        width: '25%',
        ...this.getColumnSearchProps('file'),
        render: (text, record) => (
          <div>
            <Upload onChange={({ file }) => {
              // console.log(b)
              if (file.status !== 'uploading') {
                console.log(file);
                // this.setState({
                //   data :
                // })
                let selectedData = this.state.data.find(item => (item.key === record.key))
                const selectedDataIndex = this.state.data.findIndex(item => (item.key === record.key))
               
                console.log(selectedData)
              selectedData.file = file
                let editedData = this.state.data
                editedData.splice(selectedDataIndex, 1, selectedData)
              this.setState({
                data : editedData
              })
              }
            }}>
              <Button>
                <Icon type="upload" /> Choose File
                </Button>
            </Upload>
          </div>
        )
      },
      {
        title: 'Upload',
        dataIndex: 'upload',
        key: 'upload',
        width: '25%',
        ...this.getColumnSearchProps('upload'),

        render: (value, record) => (
          <div>
            <Button color="secondary"  onClick={async () => {
              console.log(record)
              const res = await Papa.parse(record.file.originFileObj, {
                header : true,
                skipEmptyLines : true,
                complete : (res, file) => {
                  // console.log(res)
                  const token = localStorage.getItem('usertoken')
                  const process = res.data.map(async (item) => {
                    console.log(item)
                    const data = await Axios({
                      method : "post",
                      url : 'http://localhost:5000' + value,
                      data : item,
                      headers : {
                        "Authorization" : "Bearer " + token
                      }
                    })
                    console.log(data)
                    return data
                  })
                  Promise.all(process).then((res) => {
                    console.log(res)
                  })
                },
                error : (err, file) => {
                  console.log(err)
                }
              })
              //  console.log(res)
            }}>Upload</Button>
          </div>
        )
      },
    ];

    const columns2 = [
      {
        title: 'No',
        dataIndex: 'no',
        key: 'no',
        width: '50%',
        fixed: 'left',
        ...this.getColumnSearchProps('no'),
      },
      {
        title: 'Recon ID',
        dataIndex: 'reconid',
        key: 'reconid',
        width: '100%',
        ...this.getColumnSearchProps('reconid'),
      },
      {
        title: 'Date',
        dataIndex: 'date',
        key: 'date',
        width: '100%',
        ...this.getColumnSearchProps('date'),
      },
      {
        title: 'Partner Name',
        dataIndex: 'partnername',
        key: 'partnername',
        width: '120%',
        ...this.getColumnSearchProps('partnername'),
      },
      {
        title: 'Partner Owner',
        dataIndex: 'partnerowner',
        key: 'partnerowner',
        width: '120%',
        ...this.getColumnSearchProps('partnerowner'),
      },
      {
        title: 'Transaction Date',
        dataIndex: 'transactiondate',
        key: 'transactiondate',
        width: '100%',
        ...this.getColumnSearchProps('transactiondate'),
      },
      {
        title: 'Order Price',
        dataIndex: 'orderprice',
        key: 'orderprice',
        width: '100%',
        ...this.getColumnSearchProps('orderprice'),
      },
      {
        title: 'Total Payment',
        dataIndex: 'totalpayment',
        key: 'totalpayment',
        width: '100%',
        ...this.getColumnSearchProps('totalpayment'),
      },
      {
        title: 'Status Recon',
        dataIndex: 'statusrecon',
        key: 'statusrecon',
        width: '100%',
        ...this.getColumnSearchProps('statusrecon'),
      },
    ];

    // const props = {
    //   action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    //   onChange({ file }) {
    //     if (file.status !== 'uploading') {
    //       console.log(file);
    //     }
    //   },
    // };

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Modal isOpen={this.state.warning} toggle={this.toggleWarning}
              className={'modal-lg ' + this.props.className}>
              <ModalHeader toggle={this.toggleWarning}>Daily Revenue</ModalHeader>
              <ModalBody>
                <div className="animated fadeIn">
                  <Table columns={columns2} dataSource={data2} pagination={{ pageSize: 50 }} scroll={{ x: 2000, y: 300 }} />
                </div>

                <p className="h5 text-center">Update Data ?</p>

                <Row>
                  <Col col="2" className="mb-3 mb-xl-0 text-center">
                    <Button color="warning" onClick={this.toggleWarning} size="lg">Cancel</Button>
                  </Col>
                  <Col col="2" className="mb-3 mb-xl-0 text-center">
                    <Button color="success" onClick={this.toggleWarning} size="lg">Ok</Button>{' '}
                  </Col>
                </Row>
              </ModalBody>
            </Modal>
            <Table columns={columns} dataSource={data} scroll={{ x:100, y: 1000 }} />
          </Col>
        </Row>
      </div>
    );
  }
}
