import React, { Component, useState, useEffect } from 'react';
import { Col, Row, ModalBody, ModalFooter, ModalHeader, FormGroup, Label, Input } from 'reactstrap';
import Highlighter from "react-highlight-words";
import { Table, Icon, Button, Upload, Input as antInput, message, Modal, Select, DatePicker, PageHeader } from 'antd';
import { useStore } from '../../utils/useStores';
import { useHistory } from "react-router-dom"
import Papa from 'papaparse'
import { LINKS } from "../../routes";
import 'antd/dist/antd.css';
import Axios from 'axios';
import CSVReader from "react-csv-reader";
import { inject, observer } from "mobx-react";
import * as _ from "lodash";
import moment from 'moment';
import resolve from 'resolve';
import { DownloadOutlined } from "@ant-design/icons";

const data = [
    {
        key: '1',
        keymetrics: 'Daily Revenue',
        type: 'keymetric_daily_revenue',
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Transaction'
    },
    {
        key: '2',
        keymetrics: 'Daily Traffic Volume',
        type: 'keymetric_daily_traffic',
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Transaction'
    },
    {
        key: '3',
        keymetrics: 'Average Vehicle KM Travelled',
        type: 'keymetric_avkt',
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Transaction'
    },
    {
        key: '4',
        keymetrics: 'Full Route Equivalente',
        type: 'keymetric_fre',
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Transaction'
    },
    {
        key: '5',
        keymetrics: 'Accident',
        type: 'keymetric_accident',
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Traffic'
    },
    {
        key: '6',
        keymetrics: 'Traffic Classification',
        type: 'keymetric_traffic_classification',
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Transaction'
    },
    {
        key: '7',
        keymetrics: 'Financial Summary',
        type: 'keymetric_financial_summary',
        isExcel: true,
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Finance'
    },
    {
        key: '8',
        keymetrics: 'Cashflow',
        type: 'keymetric_cashflow',
        isExcel: true,
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Finance'
    },
    {
        key: '9',
        keymetrics: 'Balance Sheet',
        type: 'keymetric_balance_sheet',
        isExcel: true,
        lastupdated: 'Sunday, 29/12/2019',
        file: '',
        upload: '',
        upload_index: 'Finance'
    },
];

const data2 = [];
for (let i = 0; i < 11; i++) {
    data2.push({
        no: i.toString(),

        reconid: `2222${i}`,
        date: `22-12-2019`,
        name: `David Siozo ${i}`,
        partnername: `Daniel John ${i}`,
        partnerowner: `Manulife ${i}`,
        transactiondate: `22-12-2019`,
        orderprice: `500.000`,
        totalpayment: `700.000`,
        statusrecon: `Done`,
    });
}

const {Option} = Select

const papaparseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
};


const UploadKey = observer(props => {
    const store = useStore();
    let history = useHistory();
    const [yearModal, setYearModal] = useState(false);
    const [yearSelected, setYearSelected] = useState('');
    const [selectedRow, setSelectedRow] = useState(['']);
    const [selectedFile, setSelectedFile] = useState('');
    // const [okClick, setOkClick] = useState(false);

    const [state, setState] = useState({
        modal: false,
        large: false,
        small: false,
        primary: false,
        success: false,
        warning: false,
        danger: false,
        info: false,
        data: [],
    })

    useEffect(() => {
        fetchData();
    }, [yearSelected]);
    
    async function fetchData() {
        await store.keymetric.getAllKMS();
        await store.adjust.getAllAdjust();
        await store.user.getUserID(store.userData.id);
        await store.keymetric.getKMLatest().then((res) => {
            const newData = data.map((d, i) => {
                const dataPicked =_.find(res,(dataAPI) => { return dataAPI.type === d.type})
                return {
                    key: `${i+1}`,
                    keymetrics: d.keymetrics,
                    type: d.type,
                    lastupdated: dataPicked ? String(moment(dataPicked.created_at).format('DD MMMM YYYY, HH:mm')) : String(moment(new Date()).format('DD MMMM YYYY, HH:mm')),
                    // lastupdated: String(moment(res[i].created_at).format('DD MMMM YYYY, HH:mm')),
                    file: dataPicked ? String(dataPicked.name_index) : '-',
                    upload: '',
                    upload_index: d.upload_index
                }
            });

            store.keymetric.dataLatest = newData
            setState({
                data: newData
            });
        });
    }

    const toggleWarning = () => {
        setState({
            warning: !state.warning,
        });
    }

    let user = store.userData.id
    let role = store.userData.role
    const handleOnChangeXL = (event) => {
        const file: File = event.target.files[0];
        setSelectedFile(file)
    };

    function uploadAllExcel(type, record) {
        const log = {
            user_id : user,
            event_name: "Update Key Metrics",
            data: {
                location:{
                    pathname:"/app/Key_Metrics/input-data-key-metric",
                    search: "",
                    hash: "",
                    key: role,
                },
                action:"PUSH",
            },
        }
        if (!type) {
            type = "all_operation"
        }

        store.log.createData(log)
        store.http.upload(selectedFile)
            .then(res => {
                const { path } = res.body;
                fetchData();
                store.keymetric.createRaw({ path, type, filename: selectedFile.name, year:yearSelected });
                setYearSelected('');

            })
            .then(res => {
                setYearSelected('');
                setSelectedFile('');
                fetchData();
                alert('Keymetric Uploaded!');
                if (record) {
                    history.push(LINKS[`${record.toUpperCase()}` || null])
                } else {
                    history.push(LINKS['KEY METRICS'])
                }
            })
            .catch(err => {
                console.log(err, "Error -> Uploading file")
                alert('Error on uploading keymetric');
            });
    }

    const columns = [
        {
            title: 'Key Metrics',
            dataIndex: 'keymetrics',
            key: 'keymetrics',
            width: '50%',
            // ...getColumnSearchProps('name_index'),
        },
        {
            title: 'Last Updated',
            dataIndex: 'lastupdated',
            key: 'lastupdated',
            width: '50%',
            render: (text, record) => {
                return text ? <span>{moment(text).format('DD-MM-YYYY HH:mm')}</span> : '-'
            }
            // ...getColumnSearchProps('created_at'),
        },
        {
            title: 'Key Metrics Type',
            dataIndex: 'type',
            key: 'type',
            width: '56%',
            // ...getColumnSearchProps('type'),
        },
        {
            title: 'Upload',
            dataIndex: 'upload',
            key: 'upload',
            width: '50%',
            // ...getColumnSearchProps('upload'),

            render: (value, record) => {
                if (store.userData.role === "super_admin") {
                    return <Button type={'primary'} onClick={() => {
                        valueYear();
                        setYearModal(true);
                        setSelectedRow(record);
                    }} style={{ fontSize: 11 }}>
                        Upload Data
                    </Button>
                } else {
                    let data = _.get(store.user.dataProfil, `position_user.position.organization`);
                    return data?.organization?.name === "FINANCE" && record.upload_index === "Finance" ? <Button type={'primary'} onClick={() => {
                            valueYear();
                            setYearModal(true);
                            setSelectedRow(record);
                        }} style={{ fontSize: 11 }}>
                            Upload Data
                        </Button> : data?.name === "Toll Transaction Mgt." && record.upload_index === "Transaction" ? <Button type={'primary'} onClick={() => {
                            valueYear();
                            setYearModal(true);
                            setSelectedRow(record);
                        }} style={{ fontSize: 11 }}>
                            Upload Data
                        </Button> : data?.name === "Traffic Mgt." && record.upload_index === "Traffic" ? <Button type={'primary'} onClick={() => {
                            valueYear();
                            setYearModal(true);
                            setSelectedRow(record);
                        }} style={{ fontSize: 11 }}>
                            Upload Data
                        </Button> : "You Can't Upload This Data"
                }           
            } 
        },
        // {
        //     title: "Template Excel",
        //     dataIndex: "file",
        //     key: 'file',
        //     width: '44%',
        //     render: (text, record) => {
        //         return <Button onClick={() => {console.log("clicked")}}>
        //             <DownloadOutlined /> Download
        //         </Button>
        //     }
        // },
    ];
    const columns2 = [
        {
            title: 'No',
            dataIndex: 'no',
            key: 'no',
            width: '50%',
            fixed: 'left',
            // ...getColumnSearchProps('no'),
        },
        {
            title: 'Recon ID',
            dataIndex: 'reconid',
            key: 'reconid',
            width: '100%',
            // ...getColumnSearchProps('reconid'),
        },
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
            width: '100%',
            // ...getColumnSearchProps('date'),
        },
        {
            title: 'Partner Name',
            dataIndex: 'partnername',
            key: 'partnername',
            width: '120%',
            // ...getColumnSearchProps('partnername'),
        },
        {
            title: 'Partner Owner',
            dataIndex: 'partnerowner',
            key: 'partnerowner',
            width: '120%',
            // ...getColumnSearchProps('partnerowner'),
        },
        {
            title: 'Transaction Date',
            dataIndex: 'transactiondate',
            key: 'transactiondate',
            width: '100%',
            // ...getColumnSearchProps('transactiondate'),
        },
        {
            title: 'Order Price',
            dataIndex: 'orderprice',
            key: 'orderprice',
            width: '100%',
            // ...getColumnSearchProps('orderprice'),
        },
        {
            title: 'Total Payment',
            dataIndex: 'totalpayment',
            key: 'totalpayment',
            width: '100%',
            // ...getColumnSearchProps('totalpayment'),
        },
        {
            title: 'Status Recon',
            dataIndex: 'statusrecon',
            key: 'statusrecon',
            width: '100%',
            // ...getColumnSearchProps('statusrecon'),
        },
    ];
    function valueYear() {
        let data = store?.adjust?.data[0]?.year;
        setYearSelected(data);
    }

    function modalYear(record) {
        return <Modal visible={yearModal} onOk={() => {
            if ((yearSelected == '') || (yearSelected == 'invalid date') || (!selectedFile)) {
                message.error("please select year or upload file first")
            } else {
                setYearModal(false)
                uploadAllExcel(record.type, record.keymetrics);
            }
        }} onCancel={() => { setYearModal(false) }} title={record.keymetrics ? "Please Choose File and Year For  " + record.keymetrics : "Please Choose File and Year for Key Metric All Operation"} >
            <DatePicker onChange={(value) => {
                setYearSelected(moment(value).format('YYYY'));
            }} aria-required="true" picker="year" style={{ 
                marginRight:15,
                marginBottom:store.ui.mediaQuery.isMobile?15:''
                }}/>
            <input
                onClick={event => event.target.files = null}
                onChange={event => handleOnChangeXL(event)}
                type="file"
                name={record.type}
                accept={".xlsx,.xls"} />
        </Modal>
    }

    function functionCheckUser () {
        let data = _.get(store.user.dataProfil, `position_user.position.organization`);
        if(data?.organization?.name !== "FINANCE") {
            return <Button onClick={() => {setYearModal(true)}} type={"primary"}>Upload All Operational</Button>
        } 
    }

    return (
        <div className="animated fadeIn" style={{padding:store.ui.mediaQuery.isMobile? '10px' : ''}}>
            {modalYear(selectedRow)}
            <PageHeader
                className={'card-page-header'}
                title={"Upload Key Metrics"}
                extra={functionCheckUser()}
            />
            <Row>
                <Col xs="12" lg="12">
                    <Modal isOpen={state.warning} toggle={toggleWarning}
                        className={'modal-lg ' + props.className}>
                        <ModalHeader toggle={toggleWarning}>Daily Revenue</ModalHeader>
                        <ModalBody>
                            <div className="animated fadeIn">
                                <Table columns={columns2} dataSource={data2} pagination={{ pageSize: 50 }} scroll={{ x: 2000, y: 300 }} />
                            </div>

                            <p className="h5 text-center">Update Data ?</p>

                            <Row>
                                <Col col="2" className="mb-3 mb-xl-0 text-center">
                                    <Button color="warning" onClick={toggleWarning} size="lg">Cancel</Button>
                                </Col>
                                <Col col="2" className="mb-3 mb-xl-0 text-center">
                                    <Button color="success" onClick={toggleWarning} size="lg">Ok</Button>{' '}
                                </Col>
                            </Row>
                        </ModalBody>
                    </Modal>
                    <Table columns={columns} dataSource={state.data} scroll={{ x: store.ui.mediaQuery.isMobile?500:100, y: 1000 }} />
                </Col>
            </Row>
        </div>
    );

});

export default UploadKey
