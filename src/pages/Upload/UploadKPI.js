import React, { Component, useState, useEffect } from 'react';
import { Col, Row, Button,  ModalBody, ModalFooter, ModalHeader, FormGroup, Label, Input } from 'reactstrap';
import Highlighter from "react-highlight-words";
import { Table, Icon,Modal ,Upload, Input as antInput, message } from 'antd';
import { useStore } from '../../utils/useStores';
import { useHistory } from "react-router-dom"
import Papa from 'papaparse'
import { LINKS } from "../../routes";
import 'antd/dist/antd.css';
import ReactFileReader from 'react-file-reader';
import Axios from 'axios';
import CSVReader from "react-csv-reader";
import { inject, observer } from "mobx-react";
import moment from 'moment';
// const data = [
//     {
        
//         created_at: "2020-06-05T04:41:06.918Z",
//         deleted_at: null,
//         id: "6d3cdf75-95e2-42f3-b7af-0aac68fd39fa",
//         name: "FINANCE",
//         organization_tree_id: null,
//         type: "division",
//         updated_at: null,

//     },
//     {
//         created_at: "2020-06-05T04:41:06.918Z",
//         deleted_at: null,
//         id: "db54a9d7-c2a7-4a94-95ff-ca6c04e0e6f9",
//         name: "HCGS",
//         organization_tree_id: null,
//         type: "division",
//         updated_at: null,
//     },
//     {
        
//         created_at: "2020-06-05T04:41:06.918Z",
//         deleted_at: null,
//         id: "75c44d20-8fb2-4720-9e44-88a1ee416480",
//         name: "CSBD",
//         organization_tree_id: null,
//         type: "division",
//         updated_at: null,
//     },
//     {
//         created_at: "2020-06-05T04:41:06.918Z",
//         deleted_at: null,
//         id: "7c0ff82f-17d3-4af8-8ef9-21388fb0023a",
//         name: "MAINTENANCE & PLANNING",
//         organization_tree_id: null,
//         type: "division",
//         updated_at: null,
//     },
//     {
//         created_at: "2020-06-05T04:41:06.918Z",
//         deleted_at: null,
//         id: "2a30dcc4-92e5-49d1-a12f-15f161b6a5f5",
//         name: "OPERATION",
//         organization_tree_id: null,
//         type: "division",
//         updated_at: null,
//     },
//     {
//         created_at: "2020-06-05T04:41:06.918Z",
//         deleted_at: null,
//         id: "38550b0d-cf63-4fe0-8ddb-0e60737c1b7c",
//         name: "SECRETARY",
//         organization_tree_id: null,
//         type: "division",
//         updated_at: null,
//     },
//     {
//         created_at: "2020-06-05T04:41:06.918Z",
//         deleted_at: null,
//         id: "06c5a73e-4908-481b-8019-31fe017cb1ea",
//         name: "QUALITY CONTROL",
//         organization_tree_id: null,
//         type: "division",
//         updated_at: null,
//     },
    
// ];

// console.log(data,'data opr')


const papaparseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
};


const UploadKPI = observer(props => {
    const store = useStore();
    let history = useHistory();
    const confirm = Modal.confirm;
    const [state, setState] = useState({
        modal: false,
        large: false,
        small: false,
        primary: false,
        success: false,
        warning: false,
        danger: false,
        info: false,
        dataDivision : [],
        operational :[],
        visible :false
    })

    useEffect(() => {
        
		fetchData();
        
        
    }, []);
   
    async function fetchData() {
			await store.operationalData.getLatest();
			await store.division.getAll();
        }
    
    // function showConfirm(event, divisionId) {
    //     console.log(event.target.files[0], "targetFiles")
    //     console.log(event, "eventnya")
    //     const file:File = event.target.files[0];
    //     console.log(divisionId, "ini")
    //     store.http.upload(file)
    //         .then(res => {
    //             const {path} = res;
    //             console.log(res.body.path, "res body path")
    //             store.operationalData.create({
    //                 file: file.name,
    //                 division_id:divisionId,
    //                 location: res.body.path
    //                 // filename: file.name
    //             }); 
                
    //         })
    //         .then(res => {
    //             alert('Operational data Uploaded!');
    //             history.push(LINKS["OPERATIONAL DATA"] +'/' + divisionId)
    //         })
    //         .catch(err => {
    //             alert('Error on uploading Operational data');
    //         })
    // }
    let user = store.userData.id
    let role = store.userData.role
    
    function showConfirm(event, divisionId) {
        const targetFiles = event.target.files[0]
        // console.log(targetFiles, "target Files")
        // console.log(event, "eventnya")
        confirm({
            title: 'Do you want to upload these file?',
            okText: 'yes',
            onOk() {
                const file: File = targetFiles;
                console.log(file, "filenya")
                const log = {
                    user_id : user,
                    event_name: "Upload Operational Data",
                    data: {
                        location:{
                            pathname:"/app/operational-data/input-data-operational",
                            search: "",
                            hash: "",
                            key: role,
                        },
                        action:"PUSH",
                    },
                }
                // console.log(divisionId, "ini")
                // console.log(log, "ini juga")
                store.log.createData(log)
                store.http.upload(file)
                    .then(res => {
                        const {path} = res;
                        console.log(res.body.path, "res body path")
                        store.operationalData.create({
                            file: file.name,
                            division_id:divisionId,
                            location: res.body.path,
                            user_id : store.userData.id
                            // filename: file.name
                        }); 
                        
                    })
                    .then(res => {
                        message.success('Operational Data Uploaded!');
                        history.push(LINKS["OPERATIONAL DATA"] +'/' + divisionId)
                    })
                    .catch(err => {
                        alert('Error on uploading Operational data');
                    })
            },
            onCancel() {},
        });
        }

    const toggle = () => {

        setState({
            modal: !state.modal,
        });
    }

    const toggleWarning = () => {
        setState({
            warning: !state.warning,
        });
    }

    const toggleLarge = () => {
        setState({
            large: !state.large,
        });
    }
    let searchInput
    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <input
                    ref={node => {
                        searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} >
                <Upload />
            </Icon>
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                console.log(this);
                setTimeout(() => searchInput.select());
            }
        },
        
        render: text => (
            // console.log(text, "textnii")
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[state.searchText]}
                autoEscape
                textToHighlight={text}
            />
        ),
    });

    const handleSearch = (selectedKeys, confirm) => {
        confirm();
        setState({ searchText: selectedKeys[0] });
    };

    const handleReset = clearFilters => {
        clearFilters();
        setState({ searchText: '' });
    };

    const handleFiles = (files) => {
        console.log(files.base64)
    };
    

    const onChangeFile = (event, divisionId) => {
        const file:File = event.target.files[0];
        console.log(divisionId, "ini")
        store.http.upload(file)
            .then(res => {
                const {path} = res;
                console.log(res.body.path, "res body path")
                store.operationalData.create({
                    file: file.name,
                    division_id:divisionId,
                    location: res.body.path
                    // filename: file.name
                }); 
                
            })
            .then(res => {
                alert('Operational data Uploaded!');
                history.push(LINKS["OPERATIONAL DATA"] +'/' + divisionId)
            })
            .catch(err => {
                alert('Error on uploading Operational data');
            })
    }


    // const {division} = store;
    const columns = [
        {
            title: 'Operational Data',
            dataIndex: 'name',
            key: 'name',
            width: '50%',
            ...getColumnSearchProps('name'),
        },
        {
            title: 'Last Updated',
            dataIndex: 'created_at',
            key: 'reconid',
            width: '50%',
            ...getColumnSearchProps('created_at'),
            render: (text, record) => {
                const data = store.operationalData.latest.find(d => d.division_id === record.id);
                console.log(data,'data ini bruh')
                if (!data) {
                    return '-';
                } else {
                    return moment(data.created_at).format('DD MMMM YYYY, HH:mm')
                };
            },
        },
        {
            title: 'Upload',
            dataIndex: 'upload',
            key: 'upload',
            width: '50%',
            ...getColumnSearchProps('upload'),

            render: (text, record) => (
                <div>
                    <input
                        onClick={event => event.target.files = null}
                        onChange={event => showConfirm(event, record.id)}
                        type="file"
                        name={record.type}/>
                </div>
            )
        },
    ];
    
    const ubahVisible =()=>{
        setState({
            visible:true
        })
    }

    {
        return <div className="animated fadeIn" style={{padding:store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet ? '10px' : ''}}>
            <Row>
                <Col xs="12" lg="12"> 
                {renderModal()}
                    <Table columns={columns} dataSource={store.division.data} scroll={{ x: store.ui.mediaQuery.isMobile?500:100, y: 1000 }} />
                </Col>
            </Row>
        </div>
   
    }

    function renderModal(event, divisionId) {
       
        console.log(state.modal)
        return <Modal visible={state.modal} closable={false} confirmLoading={false} destroyOnClose={true}
            title="KPI Department Golongan I-III"
            okText="Save"
            cancelText="Cancel"
            bodyStyle={{ background: '#f7fafc' }}
            onCancel={() => {alert("ga")}}
            onOk={() =>{alert("ok")}}>
        </Modal>
    }

});

export default UploadKPI