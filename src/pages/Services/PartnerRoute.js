import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../routes";
import {Partner2w} from "./Partner2w/Partner2w";
import {Subscriptions} from "./Subscriptions/Subcriptions";
import {Commissions} from "./Commissions/Commissions";
import {Deposits} from "./Deposits/Deposits";

export const PartnerRoute = () => {
    return (
        <Switch>
            <Route path={LINKS.SERVICE + "/2w"}>
                <Partner2w/>
            </Route>
            <Route path={LINKS.SERVICE + "/subscriptions"}>
                <Subscriptions/>
            </Route>
          <Route path={LINKS.SERVICE_COMMISSIONS}>
            <Commissions/>
          </Route>
          <Route path={LINKS.SERVICE_DEPOSITS}>
            <Deposits/>
          </Route>
            <Route path={LINKS.SERVICE + "/cari_service"}>
                cari service
            </Route>
        </Switch>
    )
};
