import React, {useEffect, useState} from "react";
import {
  Row,
  Col,
  Button,
  Statistic,
  Card,
  Empty,
  PageHeader,
  Dropdown,
  Table,
  Menu,
  Input,
  Select,
  Space,
  Typography,
  Tag
} from "antd";
import {FilterOutlined, PlusOutlined} from "@ant-design/icons";
import {useStore} from "../../../utils/useStores";
import moment from "moment";

const {Search} = Input;
const {Option} = Select;
const {Text} = Typography;
const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const gridStyle = {
  overflow: 'hidden',
  position: 'relative',
  padding: '16px 15px 8px',
  margin: '-1px -1px 0 0',
  height: 184,
  transition: 'transform .4s'
};

const optionFilter = [
  { key:'1', text: 'All products', value: 'All products' },
  { key:'2', text: 'MEMBERSHIP2W', value: 'MEMBERSHIP2W' },
  { key:'3', text: 'MEMBERSHIP4W', value: 'MEMBERSHIP' },


];

function handleMenuClick(e) {
  console.log('click', e);
}

const menu = (
  <Menu onClick={handleMenuClick} style={{background: '#fff'}}>
    {optionFilter.map((i => {
      return <Menu.Item key={i.key}>{i.value}</Menu.Item>
    }))
    }
  </Menu>
);

function onChange(pagination, filters, sorter, extra) {
  console.log('params', pagination, filters, sorter, extra);
}


const dataUser = [{
  id: 1,
  name: 'Aira 2',
  user: 'test@gmail',
  role: 'Administrator',
  role_desc:'',
  last_login:'',
  status: 'Active',
}];


export const Deposits = () => {
  const store = useStore();
  useEffect(() => {
    store.transaction.getAll();
  }, []);

  const [pagination, setPagination] = useState( {
    current: 1,
    pageSize: 10,
  });


  const [filteredInfo, setFilteredInfo] = useState({payment_method:['MEMBERSHIP2W', 'MEMBERSHIP']});
  const handleChange = (pagination, filters, sorter, extra) => {
    console.log('Various parameters', pagination, filters, sorter, extra);
     setFilteredInfo(filters);
     setPagination(pagination)
  };
  const filterInfo = filteredInfo || {};
  const filterInfoValue= filterInfo.payment_method;

  const optionList = (
    optionFilter.map((i => {
      return <Option value={i.value}>{i.value}</Option>
    }))

  );



 const columnDraft = [
    {
      title: '',
      dataIndex: 'id',
      render: text => <a>{}</a>,
      width: 20,
      ellipsis: true,

    },
    {
      title: 'Customer',
      dataIndex: 'username',
      key: 'username',
      width: 350,
      ellipsis: true,
    },

    {
      title: 'Status',
      dataIndex: 'status',
      width: 90,
      render: (record, text) => record ?
        <Tag style={{fontSize: 10, border: 0, borderRadius: 50, color: "#3d4eac", lineHeight: '18px'}}
             color="#d6ecff">Active</Tag> :
        <Tag style={{fontSize: 10, border: 0, borderRadius: 50, lineHeight: '18px'}} color={"#e3e8ee"}><Text
          type="secondary">Inactive</Text></Tag>,

    },
    {
      title: 'Product',
      dataIndex: 'payment_method',
      key: 'payment_method',
      // filters: [
      //   { text: 'MEMBERSHIP2W', value: 'MEMBERSHIP2W' },
      //   { text: 'MEMBERSHIP4W', value: 'MEMBERSHIP' },
      // ],
      width: 170,
      filteredValue: filterInfo.payment_method || null,
      onFilter: (value, record) => record.payment_method.indexOf(value) === 0,
      render: (value, record, text) => (value === "MEMBERSHIP2W" ? <span>{value}</span> : <span>MEMBERSHIP-4W</span>)
    },
   {
     title: '',
     dataIndex: 'check_out',
     key: 'check_out',
     ellipsis: true,
     width:200,
     render:(record)=> null
   },
   {
     title: 'Created',
     dataIndex: 'check_in',
     key: 'check_in',
     ellipsis: true,
     width:130,
     render:(record)=> moment(record).format("DD MMM YY, H:mm:ss")
   },
   {
     title: 'Action',
     key: 'operation',
     width: 90,
     render: () => <Button size="small" style={{fontSize: 12}} onClick={() => {
     }}>Detail</Button>,
     className:'align-center'
   },


  ];

  return <div>
    <Card bordered={false} className={'shadow'} bodyStyle={{padding: 0}}>
      <PageHeader
        className={'card-page-header'}
        title={<div>
          <Search
            placeholder="FIlter by name or email"
            onSearch={value => console.log(value)}
            style={{width: 200, padding: '2.5px 11px', marginRight:12}}
          />
          <Select
            style={{width: 160}}
            dropdownClassName={'select-dropdown'}
            className={'select'}
            size={'small'}
            bordered={true}
            onChange={handleChange}
            defaultValue={'All product'}>
            {optionList}
          </Select>
        </div>}
        subTitle=""
        extra={[
          <Button key="1">
            <PlusOutlined/> New
          </Button>
        ]}
      />
      {dataUser ? <Table size={"small"}  rowKey={record => record.id} pagination={pagination} onChange={handleChange}  hasEmpty dataSource={store.transaction.data} columns={columnDraft}/> :
        <div
          style={{maxHeight: 'calc(100vh - 450px)', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
          <Empty/>

        </div>
      }
    </Card>
  </div>
};
