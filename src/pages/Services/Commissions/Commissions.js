import React, { useEffect, useState } from "react";
import {
    Row,
    Col,
    Button,
    Statistic,
    Card,
    Empty,
    PageHeader,
    Dropdown,
    Table,
    Menu,
    Input,
    Select,
    Space,
    Typography,
    Tag
} from "antd";
import { FilterOutlined, PlusOutlined } from "@ant-design/icons";
import { useStore } from "../../../utils/useStores";
import moment from "moment";
import { observer } from "mobx-react-lite";

const { Search } = Input;
const { Option } = Select;
const { Text } = Typography;
const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    margin: '-1px -1px 0 0',
    height: 184,
    transition: 'transform .4s'
};

const optionFilter = [
    { key: '1', text: 'All commissions', value: 'All commissions' },
    { key: '2', text: 'Active', value: 'Active' },
    { key: '3', text: 'Inactive', value: 'Inactive' },


];

function handleMenuClick(e) {
    console.log('click', e);
}

const menu = (
    <Menu onClick={handleMenuClick} style={{ background: '#fff' }}>
        {optionFilter.map((i => {
            return <Menu.Item key={i.key}>{i.value}</Menu.Item>
        }))
        }
    </Menu>
);

function onChange(pagination, filters, sorter, extra) {
    console.log('params', pagination, filters, sorter, extra);
}


const dataUser = [{
    id: 1,
    name: 'Aira 2',
    user: 'test@gmail',
    role: 'Administrator',
    role_desc: '',
    last_login: '',
    status: 'Active',
}];


export const Commissions = observer(() => {
    const store = useStore();
    useEffect(() => {

        async function init() {
            let transactions = await store.transaction.getAll();
            let commisions = await store.commision.getAll();

            console.log({ commisions, transactions }, "useEffect Done");
        }

        init();
    }, []);

    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 10,
    });


    const [filteredInfo, setFilteredInfo] = useState({ payment_method: ['MEMBERSHIP2W', 'MEMBERSHIP'] });
    const handleChange = (pagination, filters, sorter, extra) => {
        console.log('Various parameters', pagination, filters, sorter, extra);
        setFilteredInfo(filters);
        setPagination(pagination)
    };
    const filterInfo = filteredInfo || {};
    const filterInfoValue = filterInfo.payment_method;

    const optionList = (
        optionFilter.map((i => {
            return <Option value={i.value}>{i.value}</Option>
        }))

    );

    const columnDraft = [
        {
            title: '',
            dataIndex: 'id',
            render: text => <a>{}</a>,
            width: 20,
            ellipsis: true,

        },
        {
            title: 'Commission',
            dataIndex: 'name',
            key: 'name',
            width: 200,
            ellipsis: true,
        },
        {
            title: 'Status',
            dataIndex: 'status',
            key: 'status',
            width: 65,
            render: (record, text) => record ?
                <Tag style={{ fontSize: 10, border: 0, borderRadius: 50, color: "#3d4eac", lineHeight: '18px' }}
                    color="#d6ecff">Active</Tag> :
                <Tag style={{ fontSize: 10, border: 0, borderRadius: 50, lineHeight: '18px' }} color={"#e3e8ee"}><Text
                    type="secondary">Inactive</Text></Tag>,

        },
        {
            title: '',
            dataIndex: 'check_out',
            key: 'check_out',
            ellipsis: true,
            width: 180,
            render: (record) => null
        },
        {
            title: 'Created',
            dataIndex: 'date_start',
            key: 'date_start',
            ellipsis: true,
            width: 130,
            render: (record) => moment(record).format("DD MMM YY, H:mm:ss")
        },
        {
            title: 'Expire',
            dataIndex: 'date_end',
            key: 'date_end',
            ellipsis: true,
            width: 130,
            render: (record) => record ? moment(record).format("DD MMM YY, H:mm:ss") : '-'
        },
        {
            title: 'Action',
            key: 'operation',
            width: 90,
            render: () => <Button size="small" style={{ fontSize: 12 }} onClick={() => {
            }}>Detail</Button>,
            className: 'align-center'
        },
    ];

    return (
        <p>PE</p>
        // <div>
        //     <Card bordered={false} className={'shadow'} bodyStyle={{padding: 0}}>
        //         <PageHeader
        //             className={'card-page-header'}
        //             title={<div>
        //                 <Select
        //                     style={{width: 160}}
        //                     dropdownClassName={'select-dropdown'}
        //                     className={'select'}
        //                     bordered={true}
        //                     size={'small'}
        //                     onChange={handleChange}
        //                     defaultValue={'All commissions'}>
        //                     {optionList}
        //                 </Select>
        //             </div>}
        //             subTitle=""
        //             extra={[
        //                 <Button key="1">
        //                     <PlusOutlined/> New
        //                 </Button>
        //             ]}
        //         />
        //         <Table
        //             rowKey={record => record.id}
        //             pagination={pagination}
        //             onChange={handleChange}
        //             hasEmpty={true}
        //             size={"small"}
        //             dataSource={store.commision.data}
        //             columns={columnDraft}/> :
        //         <div
        //             style={{
        //                 maxHeight: 'calc(100vh - 450px)',
        //                 display: 'flex',
        //                 justifyContent: 'center',
        //                 alignItems: 'center'
        //             }}>
        //             {/*<Empty/>*/}
        //         </div>
        //     </Card>
        // </div>
    )
});
