import React, {useEffect} from "react";
import {useStore} from "../../../utils/useStores";
import {Button, Card, Dropdown, Menu, PageHeader, Statistic, Table} from 'antd'
import {observer} from 'mobx-react-lite';
import {FilterOutlined, PlusOutlined} from "@ant-design/icons";
import {CreateDraft} from "./Modal/Draft";
import {Filter} from '../../../component/Filter'
const gridStyle = {
    width: '50%',
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    margin: '-1px -1px 0 0',
    height: 84,
    transition: 'transform .4s'
};

const optionFilter = [
    {
        key: 1,
        value: 'Enabled'
    }, {
        key: 2,
        value: 'Disabled'
    },
];

function handleMenuClick(e) {
    console.log('click', e);
}

const menu = (
  <Menu onClick={handleMenuClick} style={{background: '#fff'}}>

      <Menu.Item key={1}><CreateDraft/></Menu.Item>


  </Menu>
);


export const Partner2WDashboard = observer((props) => {
    const store = useStore();
    return <div>
        <PageHeader
          ghost={false}
          className={'card-page-header'}
          title={<Filter/>}
          subTitle="Collections"
          extra={<CreateDraft/>}
        />
        <Card.Grid style={gridStyle} hoverable={false}>
            <Statistic title="Gross Volume" precision={2} prefix={'Rp'} value={23343000}/>
        </Card.Grid>
        <Card.Grid style={gridStyle} hoverable={false}>
            <Statistic title="Successful payment to partners" precision={2} prefix={'Rp'} value={230800} valueStyle={{color: "#5469d4"}}/>
        </Card.Grid>
    </div>
});
