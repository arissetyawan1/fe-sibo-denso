import React, {useEffect, useState} from "react";
import {useStore} from "../../../utils/useStores";
import {Table, PageHeader, Button, Menu, Dropdown, Modal, Empty} from 'antd'
import {observer} from 'mobx-react-lite';
import { FilterOutlined, PlusOutlined} from '@ant-design/icons';
import {Filter} from '../../../component/Filter'
import {CreateDraft} from "./Modal/Draft";

const ReachableContext = React.createContext();
const UnreachableContext = React.createContext();

const config = {
    title: 'Use Hook!',
    content: (
      <div>
          <ReachableContext.Consumer>{name => `Reachable: ${name}!`}</ReachableContext.Consumer>
          <br />
          <UnreachableContext.Consumer>{name => `Unreachable: ${name}!`}</UnreachableContext.Consumer>
      </div>
    ),
};


export const Partner2WReconcile = observer((props) => {
    const store = useStore();
    useEffect(() => {
        store.transaction.getAll();

    }, []);


    const [modal, contextHolder] = Modal.useModal();


    const ReachableContext = React.createContext();
    const UnreachableContext = React.createContext();

    const config = {
        title: 'Use Hook!',
        content: (
          <div>
              <ReachableContext.Consumer>{name => `Reachable: ${name}!`}</ReachableContext.Consumer>
              <br />
              <UnreachableContext.Consumer>{name => `Unreachable: ${name}!`}</UnreachableContext.Consumer>
          </div>
        ),
    };

    const column = [
        {
            title: 'Order ID',
            dataIndex: 'order_id',
            key: 'order_id',
            ellipsis: true,
            fixed: 'left',

        },
        {
            title: 'Transaction ID',
            dataIndex: 'id',
            key: 'id',
            ellipsis: true,
            fixed: 'left',
        },
        {
            title: 'Code',
            dataIndex: 'code',
            key: 'code',
            width:80
        },
        {
            title: 'Customer',
            dataIndex: 'username',
            key: 'username',
            ellipsis: true,
        },
        {
            title: 'Partner Name',
            dataIndex: 'parking_name',
            key: 'parking_name',
            ellipsis: true,
        },
        {
            title: 'Checkin',
            dataIndex: 'check_in',
            key: 'check_in',
            ellipsis: true,
        },
        {
            title: 'Checkout',
            dataIndex: 'check_out',
            key: 'check_out',
            ellipsis: true,
        },
        {
            title: 'Status',
            dataIndex: 'payment_status',
            key: 'payment_status',
            ellipsis: true,
            width:80
        },
        {
            title: 'Method',
            dataIndex: 'payment_method',
            key: 'payment_method',
            ellipsis: true,
            width:80
        },
        {
            title: 'Vehicle',
            dataIndex: 'vehicle_type',
            key: 'vehicle_type',
            ellipsis: true,
            width:80
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
            width:80,
            ellipsis: true,
        },
        {
            title: 'Total',
            dataIndex: 'total',
            key: 'total',
            ellipsis: true,
        }
    ];

    const columnDraft = [
        {
            title: 'Date',
            dataIndex: 'date',
            key: 'date',
        },

        {
            title: 'Bill No',
            dataIndex: 'bill_id',
            key: 'id',

        }, {
            title: 'Bill Type',
            dataIndex: 'bill_type',
            key: 'id',

        }, {
            title: 'Bill Period',
            dataIndex: 'bill_period',
            key: 'id',

        }, {
            title: 'Bill Status',
            dataIndex: 'bill_status',
            key: 'id',

        }, {
            title: 'Partner Name',
            dataIndex: 'partner_name',
            key: 'id',

        },


    ];

    const dataDraft = [{
        id:'',
        date:'',
        bill_no:'',
        bill_type:'',
        bill_period:'',
        bill_status:'',
        partner_name:'',
    }];


    return <div>
        <PageHeader
          ghost={false}
          className={'card-page-header'}
          title={<Filter/>}
          extra={<CreateDraft/>}
        />
        <div style={{minHeight: 'calc(100vh - 450px)', display:'flex', justifyContent:'center', alignItems:'center'}}>
            {dataDraft.length !== null ? <Empty/> :
              <Table size={"small"} dataSource={dataDraft} columns={columnDraft}/>
            }
          </div>
    </div>
});
