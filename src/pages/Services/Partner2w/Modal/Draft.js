import React, {useEffect, useState} from "react";
import {observer} from "mobx-react-lite"
import {
  PageHeader,
  Divider,
  Space,

  Card,
  Col,
  Form,
  Input,
  Modal,
  Row,
  Select,
  notification,
  Button,
  Typography,
  Table
} from "antd";
import {useStore} from "../../../../utils/useStores";
import Highlighter from 'react-highlight-words';
import {PlusOutlined, SearchOutlined} from '@ant-design/icons';
import {FormMode} from './Form'

const {Title} = Typography;
const {Option} = Select;
const {TextArea} = Input;
const {Meta} = Card;
const formItemLayout = {
  labelCol: {span: 4},
  wrapperCol: {span: 10},
};
export const CreateDraft = observer(() => {
  const store = useStore();
  const [form] = Form.useForm();
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [checkPartner, setCheckPartner] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');


  useEffect(() => {
    form.validateFields(["partner_name"]);
  }, [checkPartner, form]);

  const onCheckboxChange = e => {
    setCheckPartner(e.target.checked);
  };

  const onCheck = async () => {
    try {
      const values = await form.validateFields();
      console.log('Success:', values);
    } catch (errorInfo) {
      console.log('Failed:', errorInfo);
    }
  };
  const onGenderChange = value => {
    switch (value) {
      case 'male':
        form.setFieldsValue({
          note: 'Hi, man!',
        });
        return;

      case 'female':
        form.setFieldsValue({
          note: 'Hi, lady!',
        });
        return;

      case 'other':
        form.setFieldsValue({
          note: 'Hi there!',
        });
    }
  };
  useEffect(() => {
    store.mitra.getAll();
    store.transaction.getAll();
    if (visible) {
      form.setFieldsValue({name: "form_in_modal"});
    }
  }, [visible, form]);


  const onPartnerChange = value => {
    if(value) {
      console.log(value);
      form.setFieldsValue({
          parking_name: `${value}`
        }
      )
    }
  };
  function onChange(value) {
    console.log(`selected ${value}`);
  }
  function onBlur() {
    console.log('blur');
  }

  function onFocus() {
    console.log('focus');
  }

  function onSearch(val) {
    console.log('search:', val);
  }

  function handleChange(value) {
    console.log(`selected ${value}`);
  }

  const onFinish = values => {
    console.log(values);
  };

  const onReset = () => {
    form.resetFields();
  };

  const onFill = () => {
    form.setFieldsValue({
      note: 'Hello world!',
      gender: 'male',
    });
  };



  const getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
         const searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={()=> handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: filtered => <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />,
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      // if (visible) {
      //   setTimeout(() =>  {
      //
      //   this.searchInput.select()
      //   });
      // }
    },
    render: text =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      ) : (
        text
      ),
  });
  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);

  };
 const handleReset = clearFilters => {
    clearFilters();
   setSearchText('');
  };

  const columns = [
    {
      title: 'Order ID',
      dataIndex: 'order_id',
      key: 'order_id',
      ellipsis: true,
      fixed: 'left',

    },
    {
      title: 'Transaction ID',
      dataIndex: 'id',
      key: 'id',
      ellipsis: true,
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
      width: 80
    },
    {
      title: 'Customer',
      dataIndex: 'username',
      key: 'username',
      ellipsis: true,
      ...getColumnSearchProps('username')
    },
    {
      title: 'Location',
      dataIndex: 'parking_name',
      key: 'parking_name',
      ellipsis: true,
    },
    {
      title: 'Checkin',
      dataIndex: 'check_in',
      key: 'check_in',
      ellipsis: true,
    },
    {
      title: 'Checkout',
      dataIndex: 'check_out',
      key: 'check_out',
      ellipsis: true,
    },
    {
      title: 'Status',
      dataIndex: 'payment_status',
      key: 'payment_status',
      ellipsis: true,
      width: 80
    },
    {
      title: 'Method',
      dataIndex: 'payment_method',
      key: 'payment_method',
      ellipsis: true,
      width: 80
    },
    {
      title: 'Vehicle',
      dataIndex: 'vehicle_type',
      key: 'vehicle_type',
      ellipsis: true,
      width: 80
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      width: 80,
      ellipsis: true,
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
      ellipsis: true,
    }
  ];
  const parking_type = [
  {parking_type:'M', name:'Motorcycle'},
  {parking_type: 'C', name:'Car'},
  {parking_type: 'B', name:'Bicycle'},
  ]

  const handleOk = () => {
    setLoading(false);
    setTimeout(() => {
      setVisible(false)
    }, 3000);
  };


  const onCreate = value => {
    console.log('Received values of form: ', value);
    setVisible(false);
  };

  return (<div>
      <Button onClick={() => setVisible(true)}>
        <PlusOutlined/> New
      </Button>
      <Modal
        title="Create draft"
        width={'100%'}
        wrapClassName={'modal-full'}
        style={{top: 0, borderRadius: 0,}}
        className={'modal-draft'}
        bodyStyle={{
          borderRadius: 0,
          margin: '0px auto',
          paddingTop: 92,
          maxWidth: '1024px',
          minWidth: '760px'
        }}
        closable={false}
        maskClosable={false}
        visible={visible}
        onOk={() => {
          form
            .validateFields()
            .then(value => {
              form.resetFields();
              onCreate(value);
            })
            .catch(info => {
              console.log('Validate Failed:', info);
            });
        }}
        destroyOnClose={() => setVisible(false)}
        footer={[
          <div className={'ant-modal-footer-div'} style={{position: "fixed", zIndex: 2, right: 0, top: 0}}>
            <Button key="back" disabled={loading} onClick={() => setVisible(false)}>
              Cancel
            </Button>
            <Button key="submit" type="primary" loading={loading} onClick={() => {
              form
                .validateFields()
                .then(value => {
                  form.resetFields();
                  setLoading(true);
                  setTimeout(() => {
                    setLoading(false);
                    onCreate(value);

                  }, 3000);

                })
                .catch(info => {
                  console.log('Validate Failed:', info);
                });
            }}>
              Submit
            </Button>
          </div>
        ]}
      >

        <Form form={form}  layout={'vertical'} name={'form_in_modal'} onFinish={onFinish}>
          <Form.Item
            {...formItemLayout}
            name={"partner_name"}

            rules={[
              {
                required: true,
              },
            ]}
          >
            <Title level={4} style={{fontWeight: 400}}>Partner</Title>
            <Divider/>
            <Select
              showSearch
              autoFocus={true}
              placeholder="Find a partner..."
              optionFilterProp="children"
              onChange={onChange}
              onFocus={onFocus}
              onBlur={onBlur}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
            >
              {store.mitra.data.map((i => {
                  return <Option style={{textTransform: 'capitalize'}} key={i.id} value={i.name}>{i.name}</Option>
                }))

              }
            </Select>
            <Divider/>

          </Form.Item>
          <div style={{display: 'flex', flexDirection: 'row'}}>
            <Form.Item
              style={{marginRight:10, width:'25%'}}
              name="type"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Select
                mode="multiple"
                showSearch
                style={{ width: '100%' }}
                placeholder="select one type"
                onChange={handleChange}
              >
                {parking_type.map((i => {
                  return  <Option value={i.parking_type} label={i.name}>
                    <div className="demo-option-label-item">
                      <span role="img" aria-label={i.name}/>
                      {i.name}
                    </div>
                  </Option>
                }))}
              </Select>
            </Form.Item>

            <Form.Item
              name="note"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input


              />
            </Form.Item>
          </div>
          {/*<Form.Item*/}
          {/*  name="gender"*/}
          {/*  label="Gender"*/}
          {/*  rules={[*/}
          {/*    {*/}
          {/*      required: true,*/}
          {/*    },*/}
          {/*  ]}*/}
          {/*>*/}
          {/*  <Select*/}
          {/*    placeholder="Select a option and change input text above"*/}
          {/*    onChange={onGenderChange}*/}
          {/*    allowClear*/}
          {/*  >*/}
          {/*    <Option value="male">male</Option>*/}
          {/*    <Option value="female">female</Option>*/}
          {/*    <Option value="other">other</Option>*/}
          {/*  </Select>*/}
          {/*</Form.Item>*/}





          {/*<Form.Item>*/}
          {/*  <Button type="primary" htmlType="submit">*/}
          {/*    Submit*/}
          {/*  </Button>*/}
          {/*  <Button htmlType="button" onClick={onReset}>*/}
          {/*    Reset*/}
          {/*  </Button>*/}
          {/*  <Button type="link" htmlType="button" onClick={onFill}>*/}
          {/*    Fill form*/}
          {/*  </Button>*/}
          {/*</Form.Item>*/}
        </Form>

        <Table size={"small" } scroll={{x: 1500, y: 300}} dataSource={store.transaction.data} columns={columns}/>
      </Modal>
    </div>
  )


});
