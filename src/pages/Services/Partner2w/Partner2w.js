import React, {useEffect, useState} from "react";
import {useStore} from "../../../utils/useStores";
import {Button, Table, Form, Radio, Card, Tabs, Menu, Affix} from 'antd'
import {observer} from 'mobx-react-lite';
import {Link, Route, useHistory} from "react-router-dom";
import {LINKS} from "../../../routes";
import {Partner2WRoute} from './Partner2WRoute'
import {Partner2WDashboard} from "./Partner2WDashboard";
import {Partner2WTransaction} from "./Partner2WTransaction";

const {TabPane} = Tabs;
const {Meta} = Card;

export const Partner2w = observer((props) => {
  const [active, setActive] = useState('dashboard');
  const [container, setContainer] = useState(null);
  const [top, setTop] = useState(10);

  function onChange(e) {
    console.log(e)
  }

  function callback(key) {
    setActive(key);
    console.log(key);
  }

  const handleClick = e => {
    console.log('click ', e);
    setActive(e.key)
  };
  const history = useHistory();
  return <div ref={setContainer}>
    <Affix style={{
      position: 'fixed',
      top: 88,
      zIndex: 1,
      maxWidth: 1084,
      minWidth: 760,
      width: 'calc(100vw - 302px)'
    }}>
      <Card bodyStyle={{padding: 0}} className={'medium-shadow '} bordered={false}>
        <Menu onClick={handleClick} className={'menu-flex'} selectedKeys={active}
              mode="horizontal">
          <Menu.Item key={'dashboard'} value="dashboard">
            <Link to={LINKS.SERVICE_2W_DASHBOARD}>
              Dashboard
            </Link>
          </Menu.Item>
          {/*<Menu.Item key={'transaction'} value="transaction">*/}
          {/*  <Link to={LINKS.SERVICE_2W_TRANSACTION}>*/}
          {/*    Transaction*/}
          {/*  </Link>*/}
          {/*</Menu.Item>*/}
          <Menu.Item key={'reconcile'} value="reconcile">
            <Link to={LINKS.SERVICE_2W_RECONCILE}>
              Draft
            </Link>
          </Menu.Item>
          <Menu.Item key={'transaction'} value="transaction">
            <Link to={LINKS.SERVICE_2W_TRANSACTION}>
             Outstanding
            </Link>
          </Menu.Item>
          <Menu.Item key={'payment'} value="payment">
            <Link to={LINKS.SERVICE_2W_PAYMENT}>
              Paid
            </Link>
          </Menu.Item>
          <Menu.Item key={'history'} value="history">
            <Link to={LINKS.SERVICE_2W_HISTORY}>
              History
            </Link>
          </Menu.Item>
          {/*<Menu.Item key={'transaction'} value="transaction">*/}
          {/*  <Link to={LINKS.SERVICE_2W_TRANSACTION}>*/}
          {/*    Transaction*/}
          {/*  </Link>*/}
          {/*</Menu.Item>*/}
          {/*<Menu.Item key={'fraud'} value="fraud">*/}
          {/*  <Link to={LINKS.SERVICE_2W_FRAUD}>*/}
          {/*    Fraud*/}
          {/*  </Link>*/}
          {/*</Menu.Item>*/}

        </Menu>
      </Card>
    </Affix>
    <Card
      style={{overflow:'hidden',marginTop: 65 }}
      className={"shadow"}
      bodyStyle={{background: "#f7fafc", marginRight: '-1px', padding: 0}}
    >
      <Partner2WRoute/>
    </Card>
  </div>
});
