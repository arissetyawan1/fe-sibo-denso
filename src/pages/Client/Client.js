import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import {
    Table,
    Input,
    Tooltip,
    Breadcrumb,
    Tag,
    Row,
    Col,
    PageHeader,
    Form,
    Select,
} from "antd";
import { HomeOutlined } from "@ant-design/icons";
import { observer } from "mobx-react";
import { Link, useHistory } from "react-router-dom";
import { LINKS } from "../../routes";
import { useParams } from "react-router-dom";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
const { Search } = Input;
const { Option } = Select;

export const Client = observer((initialData) => {
    const store = useStore();
    const clientStore = store.clients;
    const history = useHistory();
    const tableData = clientStore.data;
    const [dataSource, setDataSource] = useState(null);
    const [valueProvince, setValueProvince] = useState();
    const [valueCity, setValueCity] = useState();
    const [province, setProvince] = useState([]);
    const [city, setCity] = useState([]);

    useEffect(() => {
        loadInitialData();
    }, []);

    const loadInitialData = async () => {
        await clientStore.getAll();
    };

    const fetchDataProvince = () => {
        return fetch("https://dev.farizdotid.com/api/daerahindonesia/provinsi")
            .then((response) => response.json())
            .then((data) => setProvince(data.provinsi));
    };

    useEffect(() => {
        fetchDataProvince();
    }, []);

    const fetchDataCity = (value) => {
        return fetch(
            "https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=" +
            value
        )
            .then((response) => response.json())
            .then((data) => setCity(data.kota_kabupaten));
    };

    const handleSearchYankes = (value) => {
        if (isNaN(value)) {
            let string = value.toLowerCase();
            const searchData = tableData.filter((entry) =>
                entry.site?.name.toString().toLowerCase().includes(string)
            );
            setDataSource(searchData);
        } else {
            const searchData = tableData.filter((entry) =>
                entry.code.toString().toLowerCase().includes(value)
            );
            setDataSource(searchData);
        }
    };

    const searchYankes = (
        <Search
            placeholder="Cari Yankes Berdasarkan Nama atau Kode"
            allowClear
            onSearch={handleSearchYankes}
            onChange={(e) =>
                e.target.value === "" ? setDataSource(tableData) : ""
            }
            style={{ width: "100%" }}
        />
    );

    const dataProvince = province || [];

    const optionProvince = dataProvince.map((prop) => {
        return (
            <Option value={prop.nama} key={prop.nama}>
                {prop.nama}
            </Option>
        );
    });

    const handleProvinceChange = (value) => {
        if (value === "Seluruh Provinsi") {
            setDataSource(tableData);
            setValueProvince(value);
            setValueCity(null);
        } else {
            const valueName = dataProvince.filter((x) => x.nama === value);
            const filteredData = tableData.filter((entry) =>
                entry.province?.name.includes(valueName[0].nama)
            );
            setDataSource(filteredData);
            fetchDataCity(valueName[0].id);
            setValueProvince(valueName[0].id);
            setValueCity(null);
            console.log("CHECK DATA PROVINCE", valueName[0].id, valueName, filteredData);
        }
    };

    const selectProvince = (
        <Select
            showSearch
            placeholder="Pilih Provinsi"
            style={{ width: "100%" }}
            onChange={handleProvinceChange}
        >
            <Option value="Seluruh Provinsi" key="Seluruh Provinsi">
                Seluruh Provinsi
            </Option>
            {optionProvince}
        </Select>
    );

    const dataCity = city || [];

    const optionCity = dataCity.map((prop) => {
        return (
            <Option value={prop.nama} key={prop.nama}>
                {prop.nama}
            </Option>
        );
    });

    const handleCityChange = (value) => {
        if (value === "Seluruh Kota" && valueProvince === "Seluruh Provinsi") {
            setDataSource(tableData);
            setValueCity("Seluruh Kota");
        } else if (
            value === "Seluruh Kota" &&
            valueProvince !== "Seluruh Provinsi"
        ) {
            const data = dataProvince.filter((x) => x.id === valueProvince);
            const provinceFilter = tableData.filter((entry) =>
                entry.province?.name.includes(data[0].nama)
            );
            setDataSource(provinceFilter);
            setValueCity("Seluruh Kota");
        } else {
            const data = dataProvince.filter((x) => x.id === valueProvince);
            const provinceFilter = tableData.filter((entry) =>
                entry.province?.name.includes(data[0].nama)
            );
            const filteredData = provinceFilter.filter((entry) =>
                value
                    .toString()
                    .toLowerCase()
                    .includes(entry.city.toString().toLowerCase())
            );
            setDataSource(filteredData);
            setValueCity(value);
        }
    };

    const selectCity = (
        <Select
            showSearch
            placeholder="Pilih Kota"
            style={{ width: "100%" }}
            onChange={handleCityChange}
            value={valueCity}
        >
            <Option value="Seluruh Kota" key="Seluruh Kota">
                Seluruh Kota
            </Option>
            {valueProvince === "Seluruh Provinsi" ? "" : optionCity}
        </Select>
    );

    const dataStatus = clientStore.data.map((it) => {
        return it.status;
    });
    const status = [...new Set(dataStatus)];

    // const optionStatus = status.map((name) => {
    //   return (
    //     <Option value={name} key={name}>
    //       {name}
    //     </Option>
    //   );
    // });

    const handleStatusChange = (value) => {
        const currValue = value;
        if (currValue === "Seluruh Status") {
            setDataSource(tableData);
        } else {
            const filteredData = tableData.filter((entry) =>
                entry.status.includes(currValue)
            );
            setDataSource(filteredData);
        }
    };

    const selectStatus = (
        <Select
            showSearch
            placeholder="Pilih Status"
            style={{ width: "100%" }}
            onChange={handleStatusChange}
        >
            {/* {optionStatus} */}
            <Option value="Seluruh Status" key="Seluruh Status">
                Seluruh Status
            </Option>
            <Option value="sudah">Sudah Berkontrak</Option>
            <Option value="sedang negosiasi">Sedang Negosiasi</Option>
            <Option value="belum">Belum Kontrak</Option>
        </Select>
    );

    const handleTypeChange = (value) => {
        const currValue = value;
        if (currValue === "Seluruh Tipe") {
            setDataSource(tableData);
        } else {
            const filteredData = tableData.filter((entry) =>
                entry.site?.type.includes(currValue)
            );
            setDataSource(filteredData);
        }
    };

    const dataType = clientStore.data.map((it) => {
        return it.site?.type;
    });
    const typeYankes = [...new Set(dataType)].filter(
        (prop) => prop !== undefined
    );

    const optionType = typeYankes.map((name) => {
        return (
            <Option value={name} key={name}>
                {name}
            </Option>
        );
    });
    const selectType = (
        <Select
            showSearch
            placeholder="Pilih Tipe"
            style={{ width: 200 }}
            onChange={handleTypeChange}
        >
            <Option value="Seluruh Tipe" key="Seluruh Tipe">
                Seluruh Tipe
            </Option>
            {optionType}
        </Select>
    );

    const [selectBed, setSelectBed] = useState();
    const [inputBed, setInputBed] = useState();
    console.log(inputBed, "ini inputbed");

    const handleChangeBed = (e) => {
        const value = e.target.value;
        setInputBed(value);
        if (value === "") {
            setDataSource(tableData);
        } else if (selectBed === "kurang dari") {
            console.log("kurang dari input");

            const filteredData = tableData.filter(
                (entry) => entry.bed_capacity < value
            );
            setDataSource(filteredData);
        } else if (selectBed === "lebih dari") {
            console.log("lebih dari input");

            const filteredData = tableData.filter(
                (entry) => entry.bed_capacity > value
            );
            setDataSource(filteredData);
        }
    };

    const handleSelectBed = (value) => {
        setSelectBed(value);
        if (inputBed === "") {
            setDataSource(tableData);
        } else if (value === "kurang dari") {
            console.log(inputBed, "ini input 1");
            const filteredData = tableData.filter(
                (entry) => entry.bed_capacity < inputBed
            );
            setDataSource(filteredData);
        } else if (value === "lebih dari") {
            console.log(inputBed, "ini input 2");

            const filteredData = tableData.filter(
                (entry) => entry.bed_capacity > inputBed
            );
            setDataSource(filteredData);
        } else {
            console.log(value);
        }
    };

    const selectBefore = (
        <Select
            className="select-before"
            placeholder="Pilih Bed"
            style={{ width: 120 }}
            onChange={handleSelectBed}
        >
            <Option value="lebih dari">Lebih Dari</Option>
            <Option value="kurang dari">Kurang Dari</Option>
        </Select>
    );

    const filterBed = (
        <Input
            addonBefore={selectBefore}
            style={{ width: 250 }}
            placeholder="Input"
            type="number"
            addonAfter="Unit"
            min={0}
            onChange={handleChangeBed}
        />
    );

    const renderStatus = (text, record) => {
        switch (text) {
            case "sudah":
                return <Tag color="green">Sudah Berkontrak</Tag>;
            case "sedang negosiasi":
                return <Tag color="yellow">Sedang Negosiasi</Tag>;
            case "belum":
                return <Tag color="volcano">Belum Kontrak</Tag>;
        }
    };

    const viewContract = (record) => ({
        onClick: (event) => {
            history.push("/app/contract/" + record.contracts[0]?.id);
        },
    });

    // const viewContract = (record) => {
    //     history.push("/app/contract/" + record.contracts[0]?.id);
    // };

    const handleClickRow = (record, index) => ({
        onClick: (event) => {
            history.push(LINKS.DETAIL_CLIENT.replace(":id", record.id));
        },
    });

    const columns = [
        {
            title: "Tipe Yankes",
            dataIndex: "site.type",
            fixed: "left",
            render: (text, record) => record.site?.type || "-",
            // width: "10%",
            // filters: [
            //   {
            //     text: "RSU",
            //     value: "RSU",
            //   },
            //   {
            //     text: "Lab",
            //     value: "Lab",
            //   },
            //   {
            //     text: "PT",
            //     value: "PT",
            //   },
            //   {
            //     text: "Other",
            //     value: "Other",
            //   },
            // ],
            // onFilter: (value, record) => record.site.type.indexOf(value) === 0,
        },
        {
            title: "Kode yankes",
            dataIndex: "code",
            fixed: "left",
            // width: "25%",
            // render: (text, record) => record.site.name
        },
        {
            title: "Sandi Wilayah",
            dataIndex: "sandi_wilayah",
            fixed: "left",
            render: (text, record) => record.area_code?.code || '-',
            // width: "25%",
        },
        {
            title: "Kode Pelanggan",
            dataIndex: "client_code",
            fixed: "left",
            render: (text, record) => record.contracts[0]?.code || "-",
            // width: "5%",
        },
        {
            title: "Nama",
            dataIndex: "site.name",
            width: "25%",
            fixed: "left",
            render: (text, record) => record.site?.name,
            // render: (text, record) => (
            //     <Tooltip title="Lihat detail">
            //         <span>
            //             <Link
            //                 style={{ color: "#42a5f5" }}
            //                 onClick={() => viewDetail(record)}
            //             >
            //                 {record.site?.name}
            //             </Link>
            //         </span>
            //     </Tooltip>
            // ),
        },
        {
            title: "Alamat",
            dataIndex: "site.address",
            width: "25%",
            render: (text, record) => record.site?.address || "-",
        },
        {
            title: "Kota",
            dataIndex: "city",
            // width: "25%"
        },
        {
            title: "Provinsi",
            dataIndex: "province",
            render: (text, record) => record.province?.name || "-",
            // width: "25%",
        },
        {
            title: "Bed",
            dataIndex: "bed_capacity",
            sorter: (a, b) => b.bed_capacity - a.bed_capacity,
            // width: "25%",
        },
        {
            title: "BOR",
            dataIndex: "bor",
            sorter: (a, b) => b.bor - a.bor,
            // width: "25%",
        },
        {
            title: "Nomor Kontrak",
            dataIndex: "contracts.contract_number",
            render: (text, record) => (
                <Tooltip title="Lihat detail">
                    <span>
                        <Link
                            style={{ color: "#13BB1B", zIndex: 2, padding: 10, backgroundColor: "yellow" }}
                            onRow={() => viewContract()}
                        >
                            {record.contracts[0]?.contract_number}
                        </Link>
                    </span>
                </Tooltip>
            ),
            // width: "25%",
        },
        {
            title: "Index Limbah",
            dataIndex: "",
            // width: "25%",
        },
        {
            title: "Potensi Limbah",
            dataIndex: "",
            // width: "25%",
        },
        {
            title: "Festronik",
            dataIndex: "festronik_number",
            // width: "25%",
        },
        {
            title: "Status",
            dataIndex: "status",
            width: "15%",
            render: renderStatus,
            // filters: [
            //   {
            //     text: "Kontrak",
            //     value: "Kontrak",
            //   },
            //   {
            //     text: "Prospek",
            //     value: "Prospek",
            //   },
            //   {
            //     text: "Belum",
            //     value: "Belum",
            //   },
            // ],
            // onFilter: (value, record) => record.status.indexOf(value) === 0,
        },
    ];

    return (
        <div
            className="site-card-wrapper"
            style={{
                padding:
                    store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
                        ? "10px"
                        : "",
                marginTop: 10,
            }}
        >
            <Breadcrumb style={{ marginLeft: 12 }}>
                <Breadcrumb.Item>
                    <Link to={LINKS.DASHBOARD}>
                        <HomeOutlined />
                    </Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item style={{ fontWeight: "bold" }}>
                    Data Yankes
                </Breadcrumb.Item>
            </Breadcrumb>
            <Row>
                <Col flex="auto">
                    <PageHeader
                        className={"card-page-header"}
                        title={"Data Yankes"}
                        subTitle=""
                    />
                </Col>
            </Row>
            <Row
                gutter={16}
                style={{ marginBottom: 10 }}
            >
                <Col span={6}>{searchYankes}</Col>
                <Col span={6}>{selectProvince}</Col>
                <Col span={6}>{selectCity}</Col>
                <Col span={6}>{selectStatus}</Col>
            </Row>
            <Row gutter={16} style={{ marginBottom: 20 }}>
                <Col span={5}>{selectType}</Col>
                <Col span={5}>{filterBed}</Col>
            </Row>
            <Table
                scroll={{ x: 1750 }}
                columns={columns}
                dataSource={dataSource === null ? tableData : dataSource}
                bordered
                pagination={{
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "30", "50"],
                }}
                footer={() => {
                    return (
                        <>
                            {dataSource ? <p>Total Data : {dataSource.length}</p> : <></>}
                        </>
                    )
                }}
                onRow={handleClickRow}
            />
        </div>
    );
});
