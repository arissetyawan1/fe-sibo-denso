import React, { useEffect, useState } from "react";
import {
    Input,
    Form,
    Card,
    Breadcrumb,
} from "antd";
import {
    HomeOutlined,
    SearchOutlined,
    DeleteOutlined,
    SwapRightOutlined,
    EditOutlined,
} from "@ant-design/icons";
import { LINKS } from "../../routes";
import { Link, useHistory, useParams } from "react-router-dom";
import { useStore } from "../../utils/useStores";
import { observer } from "mobx-react-lite";

const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 14 },
};

export const FormClient = observer((props) => {
    const { mode } = props;
    const { client: clientStore } = useStore();
    const { id } = useParams();
    const [data, setData] = useState(null);
    useEffect(() => {
        getInitialData();
    }, []);

    const getInitialData = async () => {
        try {
            const result = await clientStore.getDetailData(id);
            setData(result);
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className="client-card-wrapper" style={{ marginTop: 10 }}>
            <Breadcrumb style={{ marginLeft: 12, marginBottom: 15 }}>
                <Breadcrumb.Item>
                    <Link to={LINKS.DASHBOARD}>
                        <HomeOutlined />
                    </Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to={LINKS.CLIENT}>Client</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item style={{ fontWeight: "bold" }}>
                    {mode === "edit" ? "Edit" : "Tambah"} Client
                </Breadcrumb.Item>
            </Breadcrumb>
            <Card title={"Form Client"}>
                <Form
                    {...layout}
                    // initialValues={}
                    scrollToFirstError={true}
                >
                    <Form.Item
                        label="Kelurahan"
                        name="code"
                        rules={[
                            {
                                required: true,
                                message: "Kelurahan tidak boleh kosong!",
                            },
                        ]}
                    >
                        <Input disabled />
                    </Form.Item>
                </Form>
            </Card>
        </div>
    );
});
