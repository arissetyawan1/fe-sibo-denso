import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import {
    Table,
    Input,
    Card,
    Button,
    Tooltip,
    Icon,
    Typography,
    Modal,
    Descriptions,
    Breadcrumb,
    Tag,
    Row,
    Col,
    PageHeader,
} from "antd";
import {
    UploadOutlined,
    PlusOutlined,
    PlusSquareOutlined,
    SearchOutlined,
    HomeOutlined,
    TeamOutlined,
    LeftOutlined,
    DeleteOutlined,
    EditOutlined,
    EyeOutlined,
    UserAddOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import { inject, observer } from "mobx-react";
import {
    Link,
    useHistory,
    useRouteMatch,
    BrowserRouter as Router,
    Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import moment from "moment";
import { useParams } from "react-router-dom";
import { useStore } from "../../utils/useStores";
import { appConfig } from "../../config/app";
import * as download from "downloadjs";
import FileViewer from "react-file-viewer";
import * as _ from "lodash";
import { http } from "../../utils/http";

export const DetailClient = observer((props) => {
    const store = useStore();
    const history = useHistory();
    const { id: paramId } = useParams();
    const [data, setData] = useState("");

    const clientStore = store.clients;

    useEffect(() => {
        getInitialData();
    }, []);

    const getInitialData = async () => {
        await clientStore.getDetail(paramId).then((res) => {
            setData(res.body);
        });
    };
    console.log("ini Data Body", data);

    const renderStatus = (status) => {
        switch (status) {
            case "sudah":
                return <Tag color="green">Sudah Berkontrak</Tag>;
            case "sedang negosiasi":
                return <Tag color="yellow">Sedang Negosiasi</Tag>;
            case "belum":
                return <Tag color="volcano">Belum Kontrak</Tag>;
        }
    };

    const backToYankes = () => {
        history.push("/app/client");
    };
    const actionButton = (
        <div style={{ margin: "12px 2px" }}>
            <Button onClick={backToYankes} icon={<LeftOutlined />}>
                Kembali
            </Button>
        </div>
    );

    return (
        <div className="site-card-less-wrapper" style={{ marginTop: 10 }}>
            <Breadcrumb style={{ marginLeft: 12, marginBottom: 15 }}>
                <Breadcrumb.Item>
                    <Link to={LINKS.DASHBOARD}>
                        <HomeOutlined />
                    </Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <Link to={LINKS.CLIENT}>Yankes</Link>
                </Breadcrumb.Item>
                <Breadcrumb.Item style={{ fontWeight: "bold" }}>
                    Detail Yankes
                </Breadcrumb.Item>
            </Breadcrumb>
            <Row>
                <Col>{actionButton}</Col>
            </Row>
            <Card title="Detail Yankes" extra={renderStatus(data.status)}>
                <Descriptions layout="horizontal" bordered column={1}>
                    <Descriptions.Item label="Kode Yankes">
                        {data.code || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Sandi Wilayah">
                        {data.area_code?.code || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Nama Yankes">
                        {data.site?.name || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Tipe">
                        {data.site?.type || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Alamat Yankes">
                        {data.site?.address || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Nomor Telepon">
                        {data.site?.phone || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Kapasitas Bed">
                        {data.bed_capacity || "-"} Unit
                    </Descriptions.Item>
                    <Descriptions.Item label="BOR">
                        {data.bor || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Index Limbah">
                        {data.bor || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Potensial Limbah">
                        {data.bor || "-"} kg
                    </Descriptions.Item>
                    <Descriptions.Item label="Koordinat">
                        {data.site?.lat || "-"} - {data.site?.lng || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Kepemilikan Festronik">
                        -
                    </Descriptions.Item>
                    <Descriptions.Item label="Status">
                        {renderStatus(data.status)}
                    </Descriptions.Item>
                    <Descriptions.Item label="Kota">
                        {data.city || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Provinsi">
                        {data.province?.name || "-"}
                    </Descriptions.Item>
                    <Descriptions.Item label="Action">
                        <Button
                            style={{
                                background: "red",
                                color: "white",
                                boxShadow: "none",
                            }}
                        >
                            <DeleteOutlined /> Hapus
                        </Button>
                        &nbsp;&nbsp;
                        <Link to={LINKS.EDIT_CLIENT.replace(":id", paramId)}>
                            <Button
                                style={{
                                    background: "gold",
                                    color: "white",
                                    boxShadow: "none",
                                }}
                            >
                                <EditOutlined /> Edit
                            </Button>
                        </Link>
                    </Descriptions.Item>
                </Descriptions>
            </Card>
        </div>
    );
});
