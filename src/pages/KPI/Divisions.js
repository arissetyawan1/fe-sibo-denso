import React, {Component, useEffect, useState} from "react";
import { Col, Row, CardBody, Card, DropdownItem, ButtonGroup, ButtonDropdown,DropdownToggle,DropdownMenu } from 'reactstrap';
import { Link } from "react-router-dom";
import { Progress, Button } from 'antd';
import { ArrowLeftOutlined } from "@ant-design/icons";
import {LINKS} from '../../routes';
import * as _ from 'lodash';
import {inject, observer} from "mobx-react";
import {useStore} from "../../utils/useStores";


let newData
let progress
const originData = []
export const Divisions = observer(() => {
    
    const store = useStore();
    const [dropdownOpen, setDropdownOpen] = useState(new Array(19).fill(false))
    const [data, setData] = useState(originData);

    useEffect(() => {
        loadData();
    }, [])

    async function loadData() {
        return await store.division.getAll()
    }
   
    function toggle(i) {
        const newArray = dropdownOpen.map((element, index) => { return (index === i ? !element : false); });
        setDropdownOpen({
            dropdownOpen: newArray,
        });
    }

    function progressBar(i){
        return(
            <Progress strokeColor={{ '0%': '#108ee9','100%': '#87d068',}} percent="60" />
        )
    }

    return (
        <div className="animated fadeIn" style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <Button type="primary"
                onClick={() => { window.history.back(); }}
                style={{ color: "#ffffff", width: 80, marginBottom: 10, zIndex:999 }}>
                <ArrowLeftOutlined /> Back
            </Button>
            {/* <div style={{float:"right"}}>
            </div> */} <br />
            <h3>Division</h3>
            {_.chunk(store.division.data, 4).map(d => {
                 
                
                return (
                    <Row justify="center">
                        {d.map(c => {
                            if(c.kpi_master_division.length > 0){
                                progress = _.sumBy(c.kpi_master_division, function (day) {
                                    // console.log(day, "weighttt")
                                    let achievement = day.progress.length > 0 ? day.weight : 0
                                    let percent = day.progress.length > 0 ? day.progress[0].percent : 0
                                     let sumAchievement  = parseFloat(achievement * percent / 100)
                                     return sumAchievement
                                 });
                                 
                             }else{
                                 progress=0
                             }
                            return (
                                <Col span={store.ui.mediaQuery.isTablet || store.ui.mediaQuery.isDesktop ? 4 : ''} lg={store.ui.mediaQuery.isMobile ? 3 : ''} md={store.ui.mediaQuery.isMobile ? 6:''} className=" d-sm-inline-block" outline="#2f353a">
                                    <Card
                                        style={{
                                            marginBottom : 20
                                        }}>
                                        <Link to={LINKS["DETAIL DIVISI"] + "/" + c.id}>
                                            <CardBody style={{paddingTop:30, paddingBottom:30}}>
                                                    <strong>{c.name}</strong>
                                                <br></br>
                                                <Progress strokeColor={{ '0%': '#108ee9','100%': '#87d068',}} percent={parseFloat(progress).toFixed(1)} />
                                            </CardBody>
                                        </Link>
                                    </Card>
                                </Col>
                            )
                        })}
                    </Row>
                )
            })}
        </div>
)
            // <div className="animated fadeIn">
            //     {store.division.data.map((d, i)=> {

            //         return (

            //             <Row justify="center">
            //                 <Col span={4} className="d-none d-sm-inline-block" outline="#2f353a">
            //                     <Card style={{marginBottom:10}}>
            //                         <CardBody>
            //                             <ButtonGroup className="float-right">
            //                             </ButtonGroup>
            //                             <Link to={LINKS["DETAIL DIVISI"] + "/" + d.id}>
            //                                 <strong>{d.name}</strong>
            //                             </Link>
            //                             <br></br>
            //                             <Progress strokeColor={{'0%': '#108ee9', '100%': '#87d068',}} percent="60"/>
            //                         </CardBody>
            //                     </Card>
            //                 </Col>


            //             </Row>
            //         )
            //     })}
            // </div>
       





})