import React, { Component, useState, Fragment, useEffect } from 'react';
import 'antd/dist/antd.css';
import axios from 'axios';
import { Card, Row, Col, Progress, PageHeader, Spin } from 'antd';
import { Select, Input, InputNumber, Table, DatePicker, Menu, Button, Icon, message, Popconfirm, Form, Dropdown, Modal, Empty, Typography } from 'antd';
import { ArrowLeftOutlined, DownloadOutlined } from "@ant-design/icons";
import { Line } from 'react-chartjs-2';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { inject, observer } from "mobx-react";
import * as moment from "moment";
import * as _ from 'lodash';
import { useStore } from "../../../utils/useStores";
import { blue } from '@material-ui/core/colors';
import { useParams } from 'react-router-dom';

const originData = []


const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandDanger = getStyle('--danger')
const { Option } = Select;
const mainChart = {
  labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
  datasets: [],
};

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function (tooltipItem, chart) {
        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      }],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          max: 250,
        },
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

let dataMapping = [];
const Divisi = (dataMapping) = observer(({
  editing,
  dataIndex,
  title,
  inputType,
  record,
  index,
  children,
  ...restProps
}) => {
  const inputNode = inputType === 'number' ? <InputNumber /> : <Input />;
  const inputNode1 =
    inputType === 'selectMonth' ?
      <Select name={"selFormula"} id={"selFormula"}
        style={{ width: 120 }}
      >
        <Option key='month' value='Not Done'>Not Done</Option>
        <Option key='month' value='January'>January</Option>
        <Option key='month' value='February'>February</Option>
        <Option key='month' value='March'>March</Option>
        <Option key='month' value='April'>April</Option>
        <Option key='month' value='May'>May</Option>
        <Option key='month' value='June'>June</Option>
        <Option key='month' value='July'>July</Option>
        <Option key='month' value='August'>August</Option>
        <Option key='month' value='September'>September</Option>
        <Option key='month' value='October'>October</Option>
        <Option key='month' value='November'>November</Option>
        <Option key='month' value='December'>December</Option>
      </Select>
      :
      (inputType === 'selectGrade') ?
        <Select name={"selFormula"} id={"selFormula"} style={{ width: 120 }}>
          {dataMapping ? _.filter(dataMapping, d => d.formula_key === 'grade').map(v => {
            return <Option key={v.formula_value} value={v.name}>{v.name}</Option>
          }) : []}
        </Select>
        : <Input />;
  return (
    <td {...restProps}>
      {editing ? (
        <Form.Item
          name={dataIndex}
          id={dataIndex}
          style={{
            margin: 0,
          }}
          rules={[
            {
              required: true,
              message: `Please Input ${title}!`,
            },
          ]}
        >
          {inputNode1}
        </Form.Item>
      ) : (
          children
        )}
    </td>
  );
});

const EditableTable = () => {
  const [form] = Form.useForm();
  const [data, setData] = useState(originData);
  const [editingKey, setEditingKey] = useState('');
  const [prevData, setPrevData] = useState({});
  const [selectedData, setSelectedData] = useState('')
  const [large, setLarge] = useState(false);
  const [yearDivision, setYearDivision] = useState("")
  // const [searchText, setSearchText] = useState('')
  // const [openModal, setOpenModal] = useState(false);
  // const [listEmployee, setListEmployee] = useState([]);

  const isEditing = record => {
    return record.key === editingKey
  };


  const store = useStore();
  const params = useParams()

  useEffect(() => {
    store.formula.getAll().then(res => {
      dataMapping = res.body;
    });
    store.kpiMaster.getDepartments({ include_progress: 'true' })
    store.kpiMaster.getStaffs({ include_progress: 'true' })
    loadData();
  }, [])

  let newData
  const loadData = async () => {
    await store.formula.getAll().then(res => {
      dataMapping = res.body;
    });
    return store.kpiMaster.getDivision({
      divisionId: params.id,
      year: yearDivision,
      include_progress: 'true'
    })
      .then(res => {
        store.kpiMaster.divisions = res.body.data
        // newData = res.body.data
        // dummyData()

        newData = store.kpiMaster.divisions.map((x, i) => {

          return {
            ...store.kpiMaster.divisions[i],
            // id: newData[i],
            achievement: _.get(store.kpiMaster.divisions[i], 'progress[0].progress'),
            key: i + 1
          }
        })
      }).then(() => {
        setData(newData)
      })
  }

  console.log(store.kpiMaster.divisions, "ini newData")
  const toggleLarge = () => {
    setLarge(!large);
  }

  const dummyData = () => {
    for (let i = 0; i < store.kpiMaster.divisions.length; i++) {
      originData.push({
        "id": "false",
        "year": '',
        "name": "",
        "description": '',
        "measurement": "",
        "weight": '',
        "target": "",
        "kpi_type": "",
        "kpi_master_id": '',
        "division_id": '',
        "department_id": '',
        "kpi_master": '',
        "division": '',
        "department": ''
      });
    }
  }
  function handleMenuClick(e) {
    message.info("Show Chart");
    console.log("click", e);
  }
  const columns = [
    {
      title: 'KPI',
      dataIndex: 'name',
      editable: false,
    },

    {
      title: 'Weight %',
      dataIndex: 'weight',
      editable: false,
    },
    {
      title: 'KPI Formula',
      dataIndex: 'kpi_type',
      editable: false,
      render: (text,record) => {
        if (text == "actvstarget"){
          return "Higher is Better"
      }
      else if (text == "targetvsact") {return "Lower is Better"}
      else return text
      }

    },
    {
      title: 'Target',
      dataIndex: 'target',
      editable: false,
      render: (text, record) => {
        if (!dataMapping.compare) {
          const data = _.find(dataMapping, d => d.id === record.target);
          const condition = (_.get(data, 'name'));
          if (condition) {
            return _.get(data, 'name');
          } else {
            return Number(record.target).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
          }
        }
      }
    },
    {
      title: "Achievement",
      dataIndex: "achievement",
      editable: true,
      render: (text, record) => {
        if ((record.kpi_type == "actvstarget") || (record.kpi_type == "targetvsact")) {
          if (text) {
            return Number(text).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') ||0;
          }
        } else {
          return text
        }
      }
    },

    {
      title: 'Percentage',
      dataIndex: 'percentage',
      editable: false,
      render: (text, record) => {
        const target = +record.target;

        const actual = _.get(record, 'progress[0].progress');
        // if (Number.isInteger(target)) {
        //   return ((Math.abs(actual) / Math.abs(target)) * 100).toFixed(1) + '%';
        // } else {
        return _.get(record, 'progress[0].percent', 0) + '%';
        // }

      }
    },
    {
      title: "Score",
      dataIndex: "score",
      editable: false,
      render: (text, record) => {
        const totalScore = _.get(record, "progress[0].percent", 0) * Number(record.weight) / 100;
        // console.log(totalScore, "ini nan");
        // console.log(record.weight, "ini weight");
        // console.log(_.ge/t(record, "progress[0].percent", 0), "ini progress");
        return totalScore + "%"
      }
    },
    // {
    //   title: 'Total',
    //   dataIndex: 'total',
    //   editable: true,
    // },
    // {
    //   title: "",
    //   key: "operation",
    //   render: () => (
    //     <a>
    //       <Dropdown overlay={menu}>
    //         <Button>
    //           Chart <Icon type="down" />
    //         </Button>
    //       </Dropdown>
    //     </a>
    //   ),
    // },
    {
      title: 'Chart',
      dataIndex: '',
      render: (text, record) => (
        <div>
          <Button color="light" onClick={() => {
            setSelectedData(record)
            toggleLarge();
          }}>Line Chart</Button>
        </div>
      )
    },
    {
      title: 'Action',
      dataIndex: 'operation',
      fixed: 'right',
      width:store.ui.mediaQuery.isMobile?100:'',
      render: (data, record) => {
        let dataDepart = _.filter(store.kpiMaster.departments, i => i.kpi_master_id === record.id)
        const editable = isEditing(record);
        if (dataDepart.length !== 0) {
          return <span>Check KPI Child</span>
        } else if (editable) {
          return <span>
            <a
              href="javascript:void(0)"
              onClick={() => save(record)}
              style={{
                marginRight: 8,
              }}
            >
              Save
                  </a>
            <Popconfirm title="Sure to cancel?" onConfirm={() => cancel(record)}>
              <a>Cancel</a>
            </Popconfirm>
            {/* <Popconfirm title="Sure to delete?" onConfirm={() => deleteData(record)}>
                  <a style={{ marginLeft: 10 }}>Delete</a>
                </Popconfirm> */}
          </span>
        } else {
          return (store.userData.role === "super_admin" &&
          <a disabled={editingKey !== ''} onClick={() => edit(record)}>
            <Typography.Text type={"danger"}>
              {record.id ? 'Update' : 'Add'}
            </Typography.Text>
          </a>
  
          );
        }
        // return editable ? (
        // ):
      },
    },
  ];

  function renderData(record) {
    let dataRelated = []
    const translatedDataDepart = _.filter(store.kpiMaster.departments, i => i.kpi_master_id === record.id).map(p => {
      dataRelated.push({
        id: p.id,
        name: p.name,
        departmentName: p.department.name,
        weight: p.weight,
        kpi_type: p.kpi_type,
        target: p.target,
        achievement: p.progress.length === 0 ? '' : p.progress[0].progress,
        percentage:  p.progress.length === 0 ? '0':p.progress[0].percent,
        level: 'department',
        created_at: moment(p.created_at).format('DD MMMM YYYY HH:mm')
      })
    });

    const translatedDataStaff = _.filter(store.kpiMaster.staffs, i => i.kpi_master_id === record.id).map(p => {
      dataRelated.push({
        name: p.name,
        staffName: p.staff.full_name,
        weight: p.weight,
        kpi_type: p.kpi_type,
        target: p.target,
        achievement: p.progress.length === 0 ? '' : p.progress[0].progress,
        percentage:p.progress.length === 0 ? '0' : p.progress[0].percent,
        level: 'staff',
        created_at: moment(p.created_at).format('DD MMMM YYYY HH:mm')
      })
    });

    return dataRelated;
  }

  const expandedRowRender = (record, index, indent, expanded) => {
    console.log(record, ' -> expandedRowRender')

    const columns = [
      {
        title: 'KPI',
        dataIndex: 'name',
      },
      {
        title: 'Level',
        dataIndex: 'level',
      },
      {
        title: 'Weight',
        dataIndex: 'weight',
      },
      {
        title: 'KPI Formula',
        dataIndex: 'kpi_type',
        render: (text,record) => {
          if (text == "actvstarget"){
            return "Higher is Better"
        }
        else if (text == "targetvsact") {return "Lower is Better"}
        else return text
        }
      },
      {
        title: 'Target',
        dataIndex: 'target',
        render: (text, record) => {
          if (dataMapping.compare !== null) {
              const data = _.find(dataMapping, d => d.id === record.target);
              const condition = (_.get(data, 'name'));
              if (condition) {
                  return _.get(data, 'name');
              } else {
                  return Number(record.target).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
              }
          }
        }
      },
      {
        title: "Achievement",
        dataIndex: "achievement",
        // editable: true,
        render: (text, record) => {
          if ((record.kpi_type == "actvstarget") || (record.kpi_type == "targetvsact")) {
            if (text) {
              return Number(text).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') ||0;
            }
          } else {
            return text
          }
        }
      },
      {
        title: 'Percentage',
        dataIndex: 'percentage',
        render: (text, record) => {
          return text + '%';
        }
      },
      {
        title: "Score",
        dataIndex: "score",
        editable: false,
        render: (text, record) => {
          const totalScore = record.percentage * Number(record.weight) / 100;
          return totalScore + "%"
        }
      },
      {
        title: 'Updated',
        dataIndex: 'created_at',
      },
    ];

    return <Table columns={columns} scroll={{ x: 1750 }} dataSource={renderData(record)} pagination={false} />;
  };

  const edit = record => {
    const dataTarget = _.find(dataMapping, v => v.id === record.target);
    if (record.id) {
      form.setFieldsValue({
        ...record,
        target: (dataTarget ? dataTarget.name : record.target),
        percentage: (record.progress[0] ? record.progress[0].percent : 0),
      });
    } else {
      form.setFieldsValue({
        key: record.key,
        name: "",
        measurement: "",
        weight: "",
        target: "",
      });
    }

    setPrevData(record)
    setEditingKey(record.key);
  };

  const cancel = (record) => {
    form.setFieldsValue({
      ...record,
    });
    setEditingKey('');
  };

  const deleteData = (key) => {
    store.kpiMaster.delete(key.id)
      .then(res => {
        loadData();
        message.success('data berhasil dihapus')
      }).catch(err => {
        message.error(err.message)
      }).finally(() => {
        setEditingKey('')
      })
  };

  let user = store.userData.id
  let role = store.userData.role
  const save = async key => {
    try {
      const row = await form.validateFields();
      if (row.percentage > 130) {
        message.error("Percentage Achievement Over 130!")
      } else {
        setEditingKey('');
        let resultPercent = 0;
        if (key.kpi_type === "month") {
          const dataTarget = _.find(dataMapping, (d) => d.id === key.target);

          console.log(dataTarget, "ini datatargetaw");
          const dataCur = _.find(
            dataMapping,
            (d) => d.name === row.achievement
          );
          // Dumb way to calculate month progress
          const targetMon = new Date(Date.parse(dataTarget.name +" 1, 2012")).getMonth()+1;
          const actualMon = new Date(Date.parse(dataCur.name +" 1, 2012")).getMonth()+1
          // resultPercent = lastLogic ? row.percentage : row.percentage;
          if (dataCur.name === "Not Done") {
            resultPercent = 100 * 0
          } else {
            resultPercent = 100 + ((targetMon - actualMon) * 5);
          }
          // resultPercent = 100 + ((targetMon - actualMon) * 5);
          //////////
          // let ruler = [];
          // _.forEach(
          //   _.filter(dataMapping, (d) => d.formula_key === "month"),
          //   (d) => {
          //     ruler.push(d);
          //     if (d.id === dataTarget.id) {
          //       return false;
          //     }
          //   }
          // );
          // const lastLogic = _.find(ruler, (v) => v.id === dataCur.id)
          //   ? true
          //   : false;
          // resultPercent = lastLogic ? row.percentage - 5 : row.percentage + 5;
          resultPercent = resultPercent >= 130 ? 130 : resultPercent;
        }
        if (key.kpi_type === 'grade') {
          const dataTarget = _.find(dataMapping, d => d.id === key.target);
          const dataCur = _.find(dataMapping, d => d.name === row.achievement);
          const math1 = (dataCur.formula_value - dataTarget.formula_value) * 20;
          const lastMath = 100 + math1;
          resultPercent = (lastMath >= 130 ? 130 : lastMath);
        }
        if (key.kpi_type === 'actvstarget') {
          const target = Number(row.achievement) / Number(key.target) * 100;
          resultPercent = target >= 130 ? 130 : target;
        }
        if (key.kpi_type === 'targetvsact') {
          const hasil = Number(key.target) / Number(row.achievement) * 100;
          resultPercent = hasil >= 130 ? 130 : hasil;
        }
        const sendData = {
          kpi_master_id: key.id,
          progress: row.achievement,
          percent: resultPercent,
        };

        const log = {
          user_id: user,
          event_name: "Update KPI Divisi",
          data: {
            location: {
              pathname: "/app/Kpi/Divisions",
              search: "",
              hash: "",
              key: role,
            },
            action: "PUSH",
          }
        }

        // const data = saveData[index];
        // console.log({data}, 'EditableTable -> ');

        store.log.createData(log)
        store.kpiProgress.create(sendData)
          .then(res => {
            message.success('KPI Updated!');
            return loadData();
          })
          .catch(err => {
            message.error('Error on updating KPI!');
          })
          .finally(() => {
            setEditingKey('');
          })

      }

    } catch (errInfo) {
      console.log('Validate Failed:', errInfo);
    }
  };
  const renderModal = (props) => {

    const { progress = [], name } = selectedData;

    const groupedByMonth = _.groupBy(progress.map(p => {
      console.log(p, "ini pnya")
      return {
        ...p,
        month: moment(p.created_at).format('MMMM')
      }
    }), 'month');

    const data = new Array(12).fill(1)
      .map((n, i) => {
        const month = moment().month(i).format('MMMM');

        if (!groupedByMonth[month]) {
          return 0;
        } else {
          return +(_.orderBy(groupedByMonth[month].map(n => ({
            ...n,
            unix: moment(n.created_at).unix(),
          })), ['unix'], ['desc'])[0].percent);
        }
      });

    console.log({ groupedByMonth, data }, 'EditableTable -> renderModal')

    mainChart.datasets = [
      {
        label: 'Revenue',
        backgroundColor: hexToRgba("#17a2b8", 12),
        borderColor: "#17a2b8",
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data,
      },
    ];

    return <Modal title={'Line Chart'}
      visible={large} onCancel={toggleLarge}
      footer={[
        <Button key="back" type="primary" onClick={toggleLarge}>
          Close
            </Button>
      ]} >
      <Typography.Paragraph strong className="mb-1">{name} KPI Achievement</Typography.Paragraph>
      <div className="text-muted mb-2">{moment().format("MMMM YYYY")}</div>

      <div className="chart-wrapper"
        style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
        {progress.length > 0 ? (
          <Line data={mainChart} options={mainChartOpts} height={300} />
        ) : <Empty />}
      </div>

    </Modal>;
  }

  async function setNewYear(param) {
    if (param) {
      await loadData();
    }
  }

  async function exportDivision(param) {
    if (param) {
      await store.kpiMaster.getExportDivisions({ divisionId: params.id, year: yearDivision, include_progress: 'true' })
        .then((res) => {
          message.success("KPI Exported!");
      })
      
    } else {
      await store.kpiMaster.getExportDivisions({ divisionId: params.id, include_progress: 'true' })
        .then((res) => {
          message.success("KPI Exported!");
      })
    }
  }

  const mergedColumns = columns.map(col => {
    if (!col.editable) {
      return col;
    }

    return {
      ...col,
      onCell: record => ({
        record,
        inputType: col.dataIndex === 'achievement' ?
          (record.kpi_type === 'grade') ? 'selectGrade' : (record.kpi_type === 'month') ? 'selectMonth' : 'text'
          :
          'text'
        ,
        dataIndex: col.dataIndex,
        title: col.title,
        editing: isEditing(record),
      }),
    };
  });
  let profit = _.sumBy(data, function (day) {

    return day.achievement

  });

  return ( <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
    <Button type="primary" style={{width:85}}
      onClick={() => { window.history.back(); }}>
      <ArrowLeftOutlined /> Back
    </Button> <br />
    <PageHeader
      style={{padding:'10px 0px'}}
      title={[<DatePicker picker="year" defaultValue={yearDivision}
        onChange={(value) => {
          setYearDivision(moment(value).format('YYYY'));
        }}
      />, <Button type="primary" onClick={() => {setNewYear(yearDivision)}} style={{marginLeft:5}}>
          Go
      </Button>]}
      extra={[
        <Button onClick={() => {
          exportDivision(yearDivision);
        }}>
          <DownloadOutlined /> Download
        </Button>
        ]}
      />

    <Fragment> <Spin spinning={store.kpiMaster.isLoading && store.formula.isLoading}>
      <Form form={form} component={false}>
        <Table
          components={{
            body: {
              cell: Divisi,
            },
          }}
          scroll={{ x: 1750 }}
          expandedRowRender={expandedRowRender}
          bordered
          dataSource={data}
          columns={mergedColumns}
          rowClassName="editable-row"
          pagination={{
            onChange: cancel,
          }}
        />
      </Form>
    </Spin>
      {renderModal()}
    </Fragment>
  </div>

  );
};


export default EditableTable;
