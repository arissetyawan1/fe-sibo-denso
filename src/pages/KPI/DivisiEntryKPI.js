import React, {Component, useEffect, useState} from "react";
import { Col, Row, CardBody, Card, DropdownItem, ButtonGroup, ButtonDropdown,DropdownToggle,DropdownMenu } from 'reactstrap';
import { Link } from "react-router-dom";
import { Progress } from 'antd';
import {LINKS} from '../../routes';
import * as _ from 'lodash';
import {inject, observer} from "mobx-react";
import {useStore} from "../../utils/useStores";


let newData
let progress
const originData = []
export const DivisiEntryKPI = observer(() => {
    
    const store = useStore();
    const [dropdownOpen, setDropdownOpen] = useState(new Array(19).fill(false))
    const [data, setData] = useState(originData);

    useEffect(() => {
        loadData();
    }, [])

    async function loadData() {
        return await store.division.getAll()
    }
   
    function toggle(i) {
        const newArray = dropdownOpen.map((element, index) => { return (index === i ? !element : false); });
        setDropdownOpen({
            dropdownOpen: newArray,
        });
    }

    function progressBar(i){
        return(
            <Progress strokeColor={{ '0%': '#108ee9','100%': '#87d068',}} percent="60" />
        )
    }

    return (
        <div className="animated fadeIn" style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <h3>Division</h3>
            {_.chunk(store.division.data, 4).map(d => {
                return (
                    <Row justify="center"  >
                        { d.map(c => {
                                    return(
                                        <Col span={store.ui.mediaQuery.isTablet || store.ui.mediaQuery.isDesktop ? 4 : ''} lg={store.ui.mediaQuery.isMobile ? 3 : ''} md={store.ui.mediaQuery.isMobile ? 6:''}   className=" d-sm-inline-block" outline="#2f353a">
                                            <Card
                                                style={{
                                                    marginBottom : 20
                                                }}>
                                                <Link to={LINKS["ENTRY DIVISI"] + "/" + c.id}>
                                                    <CardBody style={{paddingTop:30, paddingBottom:30}}>
                                                            <strong>{c.name}</strong>
                                                        <br></br>
                                                        {/* <Progress strokeColor={{ '0%': '#108ee9','100%': '#87d068',}} percent={parseInt(progress)} /> */}
                                                    </CardBody>
                                                </Link>
                                            </Card>
                                        </Col> 
                                    )
                                })
                        }
                         
                       
                    </Row>
                )
            })}
        </div>
)

})