import React, { Component, useEffect, useState } from 'react';
// import 'antd/dist/antd.css';
import { inputkpi_gol_4_5 } from './EntryFunction'
import {
    Row,
    Col,
    Button,
    Statistic,
    Card,
    Empty,
    PageHeader,
    Dropdown,
    Table,
    Menu,
    Input,
    InputNumber,
    Select,
    Space,
    Typography,
    Tag,
    message,
    Modal,
    Alert,
    Form
} from "antd";
import {
    FilterOutlined,
    PlusOutlined,
    CheckOutlined,
    DeleteOutlined,
    EditOutlined,
    ArrowLeftOutlined
} from "@ant-design/icons";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { inject, observer } from "mobx-react";
import moment from "moment";
import { useStore } from "../../../utils/useStores";
import InputKPIGol45Depart from './Gol4-5Depart';
import * as _ from "lodash";
import { useParams } from 'react-router-dom';

const dataSource = [];
const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    margin: '-1px -1px 0 0',
    height: 184,
    transition: 'transform .4s'
};
let dataRelated
let positionCheck
let divisionUser
let divisionId
let target_mid_years
let target_one_years
const InputKPIGol45Divisi = observer((initialData) => {
    const store = useStore();
    const params = useParams();
    const [form] = Form.useForm();
    const [state, setState] = useState({
        modal: false,
        success: false,
        division: '',
        plan: '',
        type: '',
        weight: '',
        target_mid_years: '',
        target_one_years: '',
        due_date: '',
        relasi: '',
        kpi_related: '',
        data: dataSource,
        isEdit: false,
        max: '',
        min: '',
        kpi_type: '',
        formula_key: '',
        weightBefore: 0
    });

    useEffect(() => {
        store.user.query.per_page = 1000
        fetchData();
        return ()=>{
            store.user.query.per_page =10
        }

    }, []);

    async function fetchData() {
        await store.formula.getAllFormula();
        await store.division.getAll();
        await store.adjust.getAllAdjust();
        await store.kpiMaster.getDivision({ divisionId: params.id, level: 'division-4-5' });
        await store.kpiMaster.getCorporate();
        await store.user.getAll();
        store.user.filterData = _.filter(store.user.data, i => i.id === store.userData.id);
    }

    const profit = _.sum(_.map(store.kpiMaster.divisions, function (day) {
    
        return +day.weight

    }));
    function totals() {
        let total = 100 - profit
        if (total <= 0) {
            return (<Alert
                message="Weight Already 100"
                type="error"
                showIcon
                style={{ marginBottom: 10 }}
            />)
        } else {
            return (
                <Alert
                    message="Weight Less Than 100"
                    type="success"
                    showIcon
                    style={{ marginBottom: 10 }}
                />
            )
        }
    }

    function createData(kpi, log) {
        store.log.createData(log)
        store.kpiMaster.create(kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }

    function updateData(id, kpi, log) {
        store.log.createData(log)
        console.log(kpi.weight, "gan")
        console.log(profit, "gan2")
        delete kpi.role
        delete kpi.user_login
        console.log(state.weightBefore, "weightbefore")
        if (kpi.weight + (profit - state.weightBefore) > 101) {
            return  message.error('Cannot update Corporate, Weight over 100');
        }
        else
        store.kpiMaster.update(id, kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }

    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        let dataAdjust = _.filter(store.adjust.data, i => i.menu_name === "kpi")
        let currentYear = dataAdjust.map(d => {
            return d?.year
        });
        console.log(currentYear[0]);
        
        let value_target_mid_years
        let value_target_one_years
        console.log(e.kpi_type,e.target_mid_years,e.target_one_years)
        // if(e.kpi_type === 'actvstarget'){
        //     value_target_mid_years = e.dtarget_mid_years
        //     value_target_one_years = e.target_one_years
        // } else if(e.kpi_type === 'targetvsact'){
        //     value_target_mid_years = e.target_mid_years
        //     value_target_one_years = e.target_one_years
        // }else{
        //      value_target_mid_years = _.filter(store.formula.data, i => i.id == target_mid_years)[0].formula_value
        //      value_target_one_years = _.filter(store.formula.data, i => i.id == target_one_years)[0].formula_value
        // }
        const kpi2 = {
            year: currentYear[0],
            division_id: e.division,
            name: e.plan,
            target: e.target_one_years,
            weight: e.weight,
            kpi_type: e.kpi_type,
            kpi_hierarki: 'corporate',
            role: role,
            formula_key: e.formula_key,
            data: {
                level: 'division-4-5',
                target_mid_years: e.target_mid_years ,
                // target_one_years: (value_target_one_years[0] ? value_target_one_years[0].formula_value : e.target_one_years ),
                due_date: e.due_date,
                type: e.type,
            },
            relation: e.relasi
        };

        const log = {
            user_id: user,
            event_name: "Add / Edit Divisi Gol. 4-5",
            data: {
                location: {
                    pathname: "/app/Kpi/entry-kpi-divisi/entry-kpi-divisi-gol-IV-V",
                    search: "",
                    hash: "",
                    key: role,
                },
                action: "PUSH",
            }
        }

        const kpi_related = _.filter(store.kpiMaster.corporates, i => i.name == e.kpi_related)
        if (kpi_related.length === 0) {
            const kpi_master = { 'kpi_master_id': e.kpi_related || null}
            const kpiFix = { ...kpi2, ...kpi_master }

            if (e.isEdit) {
                if (profit - e.weightHidden + parseInt(kpi2.weight) >= 100) {
                    message.error('Cannot update Corporate, Weight over 100');
                    toggleSuccess();
                } else {
                    updateData(e.isEdit, kpiFix, log)
                }
            } else {
                if (profit + parseInt(kpiFix.weight) > 100) {
                    message.error('Cannot Add KPI Divisi, Weight over 100');
                    toggleSuccess();
                } else {
                    if (profit >= 100) {
                        message.error('Cannot Add entry Divisi, Weight Already 100');
                        toggleSuccess();
                        form.resetFields();
                    }
                    else {
                        if (store.kpiMaster.divisions.length < 13) {
                            createData(kpiFix, log)
                        } else {
                            message.error('Error on creating KPI! KPI Already 13 Items');
                            toggleSuccess();
                        }

                    }
                }
            }
        } else {
            if (e.kpi_related === kpi_related[0].name) {
                const kpi_master = { 'kpi_master_id': e.kpi_related_id }
                const kpiFix = { ...kpi2, ...kpi_master }
                if (e.isEdit) {
                    updateData(e.isEdit, kpiFix, log)
                }
            }
        }

    }

    const toggleSuccess = (() => {
        setState({
            success: !state.success,
        });
        // form.setFieldsValue({
        //     weight:100-profit
        // })
    })
    function arrayId() {
        const bb = store.kpiMaster.divisions.map(d => {
            return d.id
        })
        const data = {
            ids: JSON.stringify(bb),
            status: "Accepted"
        }

        store.kpiMaster.updateStatus(data).then(res => {
            message.success("Submit KPI Success! Other Users Wont Be Able to Change It")
            fetchData();
        })
            .catch(err => {
                message.error(err.message);
            });
    }
    function setEditMode(record) {
        if (record.data.type == "Others") {
            setState(prevState => ({
                ...prevState,
                success: true,
                weightBefore: +record.weight,
                max: 5,
                min: 1
            }))
        } else {
            setState(prevState => ({
                ...prevState,
                success: true,
                weightBefore: +record.weight,
                max: 30,
                min: 5,
            }))
        }
        form.setFieldsValue({
            isEdit: record.id,
            success: true,
            division: record.division_id,
            plan: record.name,
            target_one_years: record.target,
            weight: +record.weight,
            weightHidden: record.weight,
            target_mid_years: record.data.target_mid_years,
            due_date: record.data.due_date,
            type: record.data.type,
            relasi: record.relation,
            kpi_related: record.kpi_master ? record.kpi_master.name : "",
            kpi_related_id: record.kpi_master_id,
            kpi_type: record.kpi_type,
            formula_key: record.formula_key,
        })

    }
    function loadRelated(value) {
        dataRelated = _.filter(store.kpiMaster.corporates, i => i.id === value);
        if (dataRelated) {
            form.setFieldsValue({
                target_one_years: dataRelated[0].target,
                kpi_type: dataRelated[0].kpi_type,
                // target_mid_years: dataRelated[0].data.target_mid_years || "-",
                target_one_years : dataRelated[0].target
            })
        }

    }

    function loadWeight(value) {
        if (form.getFieldValue('type') == "Others") {
            setState({
                max: 5,
                min: 1,
                success: true,
            })
        } else {
            setState({
                max: 30,
                min: 5,
                success: true,
            })
        }
    }

    {
        const { warning } = state;
        const columns = [
            // {
            //     title: 'No',
            //     dataIndex: 'id',
            //     width: 65,
            //     fixed: "left",
            //     render: record => {
            //         return record?.split('-')[0]
            //     }
            // },
            {
                title: 'Division',
                dataIndex: 'division',
                render: (text, record) => <span>{text.name || '-'}</span>,
            },
            {
                title: 'Individual Perfomance Plan',
                dataIndex: 'name',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'Type',
                dataIndex: 'data',
                render: (text, record) => <span>{text.type || '-'}</span>,
            },
            {
                title: 'Weight(%)',
                dataIndex: 'weight',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'KPI Formula',
                dataIndex: 'kpi_type',
                render: (text, record) => {
                        if (text == "actvstarget"){
                            return "Higer is Better"
                        }
                        else if (text == "targetvsact") {return "Lower is Better"}
                        else return text
                    }
                
            },
            {
                title: 'Target Mid Year',
                dataIndex: 'data',
                render: (text, record) => {
                    const data = _.find(store.formula.data, d => d.id === text?.target_mid_years);
                    const condition = (_.get(data, 'name'));
                    if(condition){
                        return _.get(data, 'name');
                    }else{
                        return Number(text?.target_mid_years).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
                    }
                },
            },
            {
                title: 'Target One Year',
                dataIndex: 'target',
                render: (text, record) => {
                    const data = _.find(store.formula.data, d => d.id === text);
                    const condition = (_.get(data, 'name'));
                    if(condition){
                        return _.get(data, 'name');
                    }else{
                        return Number(text).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
                    }
                },

            },
            {
                title: 'Due Date',
                dataIndex: 'data',
                render: (text, record) => <span>{text.due_date || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: 'relation',
                render: (text, record) => <span>{text || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master',
                render: (text, record) => text?.measurement || "No Related KPI"
            },
            {
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
                fixed: 'right',
                width: 100,
                render: (text, record) => (store.userData.role === "super_admin" &&
                    <span>
                    <Space size="small">
                        <Button onClick={() => {
                            setEditMode(record)
                        }} icon={<EditOutlined />} />
                        <Button onClick={() => {
                            Modal.confirm({
                                title: 'Do you want to delete these kpi?',
                                icon: <ExclamationCircleOutlined />,
                                onOk() {
                                    return deleteDataKpi(record.id);
                                },
                                onCancel() { },
                            });
                        }} style={{
                            marginLeft: 8
                            }} icon={<DeleteOutlined />} />
                        </Space>
                    </span>
                )
            },
        ];

        positionCheck = store.user.filterData.map(r => {
            return r.position_user
        })
        if (positionCheck[0] !== null) {
            divisionId = store.user.filterData.map(d => {
                return d.position_user.position.organization.organization.id
            })
            divisionUser = _.filter(store.kpiMaster.divisions, i => i.division_id === divisionId[0]);
        }

        return <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <Button icon={<ArrowLeftOutlined />} type="primary" style={{margin: '8px 1px'}} onClick={() => {window.history.back()}}>Back</Button>
            {totals()}
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> Entry KPI Divisi Golongan IV - V </div>}
                    subTitle=""
                    extra={store.userData.role === "super_admin" && [<div>
                        <Button color="secondary" onClick={toggleSuccess}>
                            <PlusOutlined /> Add KPI
                        </Button>
                    </div>
                    ]}
                />
                {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                {renderModal()}
                <Table columns={columns}
                    bordered
                    dataSource={store.kpiMaster.divisions}
                    scroll={{ x: 1500, y: 350 }}
                     />
            </Card>
        </div>

    }
    function changeMonth(value) {
        console.log(`selected ${value}`);
    }

    function deleteDataKpi(id) {
        store.kpiMaster.delete(id).then(res => {
            message.success('data deleted successfully');
            fetchData();
        }).catch(err => { message.error(err.message) })
    };

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
            title="KPI Golongan IV-V"
            okText="Save"
            cancelText="Cancel"
            bodyStyle={{ background: '#f7fafc' }}
            onCancel={() => {
                form.validateFields().then(values => {
                    form.resetFields();
                });
                toggleSuccess();
            }}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        // form.resetFields();
                        onSubmit(values);
                        form.setFieldsValue({});
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <div className="animated fadeIn">
                <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
                    <Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.isEdit !== currentValues.isEdit}
                    >
                        {({ getFieldValue }) => {
                            return !getFieldValue('isEdit') ? ( 
                        <Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                            <Select
                                placeholder="Pilih Divisi"
                                allowClear
                            >
                                <Select.Option>Divisi</Select.Option>
                                {(_.filter(store.division.data, i => i.id == params.id) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                            </Select>
                        </Form.Item>): <Form.Item name="division" label="Divisi">
                        <Select disabled={true}>
                            <Select.Option>Divisi</Select.Option>
                            {(_.filter(store.division.data, i => i.id == params.id) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                        </Select>
                    </Form.Item>;
                        }}
                    </Form.Item>
                    <Form.Item name="relasi" label="Relasi" rules={[{ required: true }]} >
                        <Select
                            allowClear
                            onChange={(value) => {
                                if (value !== 'langsung') {
                                    form.setFieldsValue({
                                        kpi_related: '',
                                        kpi_related_id : ''
                                    })
                                } 
                            }}
                        >
                            <Select.Option>Pilih Relasi</Select.Option>
                            <Select.Option value={'langsung'}>Langsung</Select.Option>
                            <Select.Option value={'tidak langsung'}>Tidak Langsung</Select.Option>
                            <Select.Option value={'tidak berhubungan'}>Tidak Berhubungan</Select.Option>

                        </Select>

                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === 'langsung' ? (
                                <Form.Item
                                    name="kpi_related"
                                    id="kpirelated"
                                    label="KPI Related" rules={[{ required: true }]}>
                                    <Select
                                        onChange={(value) => {
                                            loadRelated(value)
                                        }}
                                        allowClear
                                        value={state.kpi_related} >
                                        <Select.Option>Related KPI</Select.Option>
                                        {store.kpiMaster.corporates.map(c => <Select.Option value={c.id}>{c.measurement}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : null;
                        }}
                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => { 
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="plan" label="Individual Perfomance Plan:">
                                    <Input disabled={true} />
                                </Form.Item>
                            ) : (
                                    <Form.Item name="plan" label="Individual Perfomance Plan:" rules={[{ required: true }]}>
                                        <Input/>
                                    </Form.Item> )
                        }}
                    </Form.Item>

                    
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="type" label="Type" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select Type"
                                        onChange={(value) => {
                                            loadWeight(value)
                                        }}
                                        disabled={true}
                                    >
                                        {[
                                            'Routine',
                                            'Project',
                                            'Others',
                                        ].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : (
                                <Form.Item name="type" label="Type" rules={[{ required: true }]}>
                                <Select
                                    placeholder="Select Type"
                                    onChange={(value) => {
                                        loadWeight(value)
                                    }}
                                    allowClear
                                >
                                    <Select.Option></Select.Option>
                                    {[
                                        'Routine',
                                        'Project',
                                        'Others',
                                    ].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                                </Select>
                            </Form.Item>
                            )
                        }}
                        </Form.Item>

                        <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="weight" label="Weight%" rules={[{ type: "number"}]}>
                                    <InputNumber style={{ width: '100%' }} disabled={true}/>
                                </Form.Item>) : (
                                    <Form.Item name="weight" label="Weight%" rules={[{ type: "number", required: true, max: state.max, min: state.min }]}>
                                    <InputNumber style={{ width: '100%' }} />
                                </Form.Item>)
                        }}
                    </Form.Item>

                    <Form.Item name="weightHidden" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_related_id" hidden={true}>
                        <Input />
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}>
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === 'langsung' ? (
                                <Form.Item name="kpi_type" label="KPI Formula" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select KPI Formula"
                                        disabled={true}
                                        onChange={
                                            (e) => { 
                                                form.setFieldsValue({
                                                    target_mid_years : '',
                                                    target_one_years : ''
                                                })
                                                store.formula.filterData = _.filter(store.formula.data, i => i.formula_key === String(form.getFieldValue('kpi_type')))
                                            }
                                        } >
                                        <Select.Option>-</Select.Option>
                                        {(_.uniqBy(store.formula.data, e => e.formula_key) || ["-"]).map(d => <Select.Option
                                            value={d.formula_key}>{d.formula_key == 'month' || d.formula_key == 'grade'? d.formula_key : d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item> ) : <Form.Item name="kpi_type" label="KPI Formula" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select KPI Formula"
                                        allowClear
                                        onChange={
                                            (e) => {
                                                form.setFieldsValue({
                                                    target_mid_years : '',
                                                    target_one_years : ''
                                                })
                                                store.formula.filterData = _.filter(store.formula.data, i => i.formula_key === String(form.getFieldValue('kpi_type')))
                                            }
                                        }
                                    >
                                        <Select.Option>-</Select.Option>
                                        {(_.uniqBy(store.formula.data, e => e.formula_key) || ["-"]).map(d => <Select.Option
                                            value={d.formula_key}>{d.formula_key == 'month' || d.formula_key == 'grade'? d.formula_key : d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>  }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}
                    >
                        {({ getFieldValue }) => {
                            if ( getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ){
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "month"  && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "month"  && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') !== "langsung") {
                               return <div>
                            <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    onChange={(value) => {
                                        target_mid_years = value
                                        changeMonth(value)
                                    }}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula.data, i => i.formula_key === "month"  && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    onChange={(value) => {
                                        target_one_years = value
                                        changeMonth(value)
                                    }}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula.data, i => i.formula_key === "month"  && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                        </div>
                                
                            } else if ( getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') === "langsung" && !getFieldValue('isEdit') ){
                                return <div>
                            <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    onChange={(value) => {
                                        target_mid_years = value
                                        changeMonth(value)
                                    }}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula.data, i => i.formula_key === "month"  && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    disabled={true}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula.data, i => i.formula_key === "month"  && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                        </div>
                            }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}
                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') === 'langsung' && getFieldValue('isEdit')) {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') !== 'langsung') {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }} />
                                    </Form.Item>
                                </div>
                            } else if ( getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') === "langsung" && !getFieldValue('isEdit') ){
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            disabled={true} />
                                    </Form.Item>
                                </div>
                            }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') === "langsung" && getFieldValue('isEdit')) {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}/>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') !== "langsung") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                </div>
                            } else if ( getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') === "langsung" && !getFieldValue('isEdit') ) {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') === 'langsung' && getFieldValue('isEdit')) {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-">
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            disabled={true} >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                             } else if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') !== 'langsung') {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                            } else if ( getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') === "langsung" && !getFieldValue('isEdit') ) {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                             }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('kpi_type') ? null : <div>
                                <Form.Item name="target_mid_years" label="Target Mid Year :">
                                    <Input disabled={true}/>
                                </Form.Item>
                                <Form.Item name="target_one_years" label="Target One Year :">
                                    <Input disabled={true}/>
                                </Form.Item>
                            </div> ;
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="due_date" label="Due Date" >
                                    <Input type="date" disabled={true}/>
                                </Form.Item>) : (
                                    <Form.Item name="due_date" label="Due Date" rules={[{ required: true }]}>
                                    <Input type="date" />
                                </Form.Item>)
                        }}
                    </Form.Item>

                    {/* {state.relasi == 1 && <Form.Item 
                    name="kpi_related" 
                    id="kpirelated" 
                    label="KPI Related" rules={[{ required: true }]}>
                            <Select
                        allowClear
                        value={state.kpi_related} >
                        <Select.Option>Related KPI</Select.Option>
                        {store.kpiMaster.corporates.map(c => <Select.Option value={c.id}>{c.name}</Select.Option>)}
                        </Select>
                    </Form.Item> }        */}


                </Form>
            </div>
        </Modal>
    }


});

export default InputKPIGol45Divisi;