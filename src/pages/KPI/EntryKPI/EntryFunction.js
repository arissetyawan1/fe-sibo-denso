import axios from 'axios'

export const entry = newKpi => {
    return axios
    .post('/kpi/entry', {
        kpidivision: newKpi.kpidivision, 
        kpidivtitle: newKpi.kpidivtitle,
        kpidivdeskripsi: newKpi.kpidivdeskripsi,
        kpidivtarget: newKpi.kpidivtarget,
        kpidivcreateddate: newKpi.kpidivcreateddate,
        kpidivrelation: newKpi.kpidivrelation,
    })
    .then(res => {
        console.log("Berhasil")
    })
}

export const inputdept = newKpi => {
    return axios
    .post('/kpi/inputdept', {
        kpidepartment: newKpi.kpidepartment, 
        kpidepttitle: newKpi.kpidepttitle,
        kpideptdeskripsi: newKpi.kpideptdeskripsi,
        kpidepttarget: newKpi.kpidepttarget,
        kpideptcreateddate: newKpi.kpideptcreateddate,
        kpideptrelation: newKpi.kpideptrelation,
    })
    .then(res => {
        console.log("Berhasil")
    })
}

export const inputkpi_gol_1_3 = newKpi => {
    return axios
    .post('/kpi/inputkpi_gol_1_3', {
        division: newKpi.division,
        rincian_tugas: newKpi.rincian_tugas, 
        control_cek_point: newKpi.control_cek_point,
        target: newKpi.target,
        one_years_progres: newKpi.one_years_progres,
        relasi: newKpi.relasi,
        kpi_related: newKpi.kpi_related,
    })
    .then(res => {
        console.log("Berhasil")
    })
}

export const inputkpi_gol_4_5 = newKpi => {
    return axios
    .post('/kpi/inputkpi_gol_4_5', {
        division: newKpi.division,
        plan: newKpi.plan, 
        type: newKpi.type,
        weight: newKpi.weight,
        target_mid_years: newKpi.target_mid_years,
        target_one_years: newKpi.target_one_years,
        due_date: newKpi.due_date,
        relasi: newKpi.relasi,
        kpi_related: newKpi.kpi_related,
    })
    .then(res => {
        console.log("Berhasil")
    })
}

export const iptdepartment_gol_1_3 = newKpi => {
    return axios
    .post('/kpi/iptdepartment_gol_1_3', {
        department: newKpi.department,
        rincian_tugas: newKpi.rincian_tugas, 
        control_check_point: newKpi.control_check_point,
        target: newKpi.target,
        one_year_progress: newKpi.one_year_progress,
        relasi: newKpi.relasi,
        kpi_related: newKpi.kpi_related,
    })
    .then(res => {
        console.log("Berhasil")
    })
}

export const iptdepartment_gol_4_5 = newKpi => {
    return axios
    .post('/kpi/iptdepartment_gol_4_5', {
        department: newKpi.department,
        individual_perfomance_plan: newKpi.individual_perfomance_plan, 
        type: newKpi.type,
        weight: newKpi.weight,
        target_mid_year: newKpi.target_mid_year,
        target_one_year: newKpi.target_one_year,
        due_date: newKpi.due_date,
        relasi: newKpi.relasi,
        kpi_related: newKpi.kpi_related,
    })
    .then(res => {
        console.log("Berhasil")
    })
}