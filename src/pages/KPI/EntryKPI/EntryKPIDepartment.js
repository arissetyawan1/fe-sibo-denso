import React, { Component } from 'react';
import { inject, observer } from "mobx-react";
import {
  Row,
  Col,
  Button,
  Avatar,
  Typography,
  Statistic,
  Card,
  Empty,
  PageHeader,
  List,
  Divider,
  Dropdown,
  Table,
  Menu,
} from "antd";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  useRouteMatch
} from "react-router-dom";
import {LINKS} from "../../../routes";
import {createUseStyles} from "react-jss";

const {Meta} = Card;
const useStyle = createUseStyles({
    cardBodyDepartment: {
        background: "#fff",
        paddingTop: 30,
        paddingBottom: 20,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        boxShadow: '0 2px 5px 0 rgba(60, 66, 87, 0.12), 0 1px 1px 0 rgba(0, 0, 0, 0.12)'
    }, cardBodyDepartmentHovered: {"&:hover": `background: #000;`},

})
const KPIDepartment = observer((props) => {
  const {push} = useHistory();
  const classes = useStyle()
  const {Text} = Typography;

  // render() 
  {
    return (
      <Row gutter={[8, 8]} style={{justifyContent:"center", marginTop:50}}>
              <Col span={8}>
                  <Link to={LINKS["ENTRY DEPARTMENT GOLONGAN 1-3"]}>
                    <Card className={['small-shadow', 'no-padding', classes.cardBodyDepartmentHovered]}
                          hoverable
                          bordered={false}
                          bodyStyle={{padding: 0}}
                          style={{textAlign: "center", padding: "30px 22px 4px 22px", background: '#ed1f24' }}>
                        <Meta
                            className={[classes.cardBodyDepartment,]}
                            // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<Text strong style={{margin: 0}}>KPI Department</Text>}
                            description={"Gol I-III"}
                        />
                    </Card>
                  </Link>
              </Col>
              <Col span={1} />
              <Col span={8}>
                  <Link to={LINKS["ENTRY DEPARTMENT GOLONGAN 4-5"]}>
                    <Card className={['small-shadow', 'no-padding', classes.cardBodyDepartmentHovered]}
                          hoverable
                          bordered={false}
                          bodyStyle={{padding: 0}}
                          style={{textAlign: "center", padding: "30px 22px 4px 22px", background: '#ed1f24' }}>
                        <Meta
                            className={[classes.cardBodyDepartment,]}
                            // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<Text strong style={{margin: 0}}>KPI Department</Text>}
                            description={"Gol IV-V"}
                        />
                    </Card>
                  </Link>
              </Col>

      </Row>
          
    );
  }
  
})

export default KPIDepartment;