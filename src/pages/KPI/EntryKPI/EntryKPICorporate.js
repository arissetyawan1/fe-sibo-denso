import React, { Component, useEffect, useState } from 'react';
// import 'antd/dist/antd.css';
import { inputkpi_gol_4_5 } from './EntryFunction'
import {
    Row,
    Col,
    Button,
    Statistic,
    Card,
    Empty,
    PageHeader,
    Dropdown,
    Table,
    Menu,
    Input,
    InputNumber,
    Select,
    Space,
    Typography,
    Tag,
    message,
    Modal,
    Alert,
    Form
} from "antd";
import {
    FilterOutlined,
    PlusOutlined,
    CheckOutlined,
    DeleteOutlined,
    EditOutlined,
    ArrowLeftOutlined
} from "@ant-design/icons";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { inject, observer } from "mobx-react";
import moment from "moment";
import { useStore } from "../../../utils/useStores";
import InputKPIGol45Depart from './Gol4-5Depart';
import * as _ from "lodash";
import { useParams } from 'react-router-dom';

const dataSource = [];
const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    margin: '-1px -1px 0 0',
    height: 184,
    transition: 'transform .4s'
};
let dataRelated
let positionCheck
let divisionUser
let divisionId
let target
const EntryKPICorporate = observer((initialData) => {
    const store = useStore();
    const params = useParams();
    const [form] = Form.useForm();
    const [state, setState] = useState({
        modal: false,
        success: false,
        name: '',
        description: '',
        measurement: '',
        weight: '',
        kpi_type: '',
        target: '',
        kpi_master: '',
        division: '',
        department: '',
        data: dataSource,
        isEdit: false,
        loading:false,
        weightBefore: 0

    });

    useEffect(() => {
        fetchData();

    }, []);

    async function fetchData() {
        await store.division.getAll();
        await store.adjust.getAllAdjust();
        await store.kpiMaster.getCorporate();
        await store.user.getAll();
        store.user.filterData = _.filter(store.user.data, i => i.id === store.userData.id);
        await store.formula.getAllFormula();
    }

    const profit = _.sumBy(store.kpiMaster.corporates, function (day) {
        let weight =  day.weight 
        return +weight
    })
    
    
    function totals() {
        let total = 100 - profit
        console.log(total, "ini total")
        if (total > 0) {
            return (
                <Alert
                    message="Weight Less Than 100"
                    type="success"
                    showIcon
                    style={{ marginBottom: 10 }}
                />
            )
            
        } else {
            return (<Alert
                message="Weight Already 100"
                type="error"
                showIcon
                style={{ marginBottom: 10 }}
            />)
        }
    }
    function createData(kpi, log) {
        store.log.createData(log)
        store.kpiMaster.create(kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }

    function updateData(id, kpi, log) {
        store.log.createData(log)
        console.log(state.weightBefore, "weightbefore")
        if (kpi.weight + (profit - state.weightBefore) > 100) {
            return  message.error('Cannot update Corporate, Weight over 100');
        }
        else
        store.kpiMaster.update(id, kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }

    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        let dataAdjust = _.filter(store.adjust.data, i => i.menu_name === "kpi")
        let currentYear = dataAdjust.map(d => {
            return d?.year
        });
        console.log(currentYear[0]);
        // let value_target_mid_years
        let value_target_one_years
        console.log(e.kpi_type,e.target_mid_years,e.target_one_years)
        if(e.kpi_type === 'actvstarget'){
            // value_target_mid_years = e.one_year_progress
            value_target_one_years = e.target
        } else if(e.kpi_type === 'targetvsact'){
            // value_target_mid_years = e.one_year_progress
            value_target_one_years = e.target
        }else{
            //  value_target_mid_years = _.filter(store.formula.data, i => i.id == one_year_progress)[0].formula_value
             value_target_one_years = _.filter(store.formula.data, i => i.id == e.target)[0].formula_value
        }
        const kpi2 = {
            year: currentYear[0],
            name: e.name,
            target: e.target,
            measurement: e.measurement,
            weight: e.weight,
            status: "Accepted",
            kpi_type: e.kpi_type,
            role: role,
            formula_key: e.formula_key,
        };

        const log = {
            user_id : user,
            event_name : "Add / Edit Divisi Corporate",
            data: {
                location: {
                    pathname:"/app/Kpi/entry-kpi-corporate",
                    search:"",
                    hash:"",
                    key:role
                },
                action:"PUSH",
            }
        }

        const kpi_related = _.filter(store.kpiMaster.corporates, i => i.name == e.kpi_related)
        if (kpi_related.length === 0) {
            const kpi_master = { 'kpi_master_id': e.kpi_related }
            const kpiFix = { ...kpi2, ...kpi_master }

            if (e.isEdit) {
                if (profit - e.weightHidden + parseInt(kpi2.weight) >= 100) {
                    message.error('Cannot update Corporate, Weight over 100');
                    toggleSuccess();
                } else {
                    updateData(e.isEdit, kpiFix, log)
                }
            } else {
                if (profit + parseInt(kpiFix.weight) > 100) {
                    message.error('Cannot Add KPI Corporate, Weight over 100');
                    toggleSuccess();
                } else {
                    if (profit >= 100) {
                        message.error('Cannot Add Corporate, Weight Already 100');
                        toggleSuccess();
                        form.resetFields();
                    }
                    else {
                        if (store.kpiMaster.corporates.length < 14) {
                            console.log(store.kpiMaster.corporates.length)
                            createData(kpiFix, log)
                        } else {
                            message.error('Error on creating KPI! KPI Already 13 Items');
                            toggleSuccess();
                        }

                    }
                }
            }
        } else {
            if (e.kpi_related === kpi_related[0].name) {
                const kpi_master = { 'kpi_master_id': e.kpi_related_id }
                const kpiFix = { ...kpi2, ...kpi_master }
                if (e.isEdit) {
                    updateData(e.isEdit, kpiFix, log)
                }
            }
        }

    }

    const newRelasiKPIID = store.kpiMaster.divisions.map(r => {
        return r.kpi_master_id
    })

    const newRelasiKPI = _.filter(store.kpiMaster.corporates, i => i.id != newRelasiKPIID)

    const newRelasiKPIFiltered = newRelasiKPI.filter(function (item) {
        return !newRelasiKPIID.includes(item.id);
    })

    const toggleSuccess = (() => {
        setState({
            success: !state.success,
        });
    })
    
    function setEditMode(record) {
        setState(prevState => ({
            ...prevState,
            success: true,
            weightBefore: +record.weight
        }))
        
        form.setFieldsValue({
            isEdit: record.id,
            success: true,
            name: record.name,
            measurement: record.measurement,
            target: record.target,
            weight: +record.weight,
            kpi_type: record.kpi_type,
            formula_key: record.formula_key,
        })

    }

    {
        const { warning } = state;
        const columns = [
            // {
            //     title:Index: 'id',
            //     width: 65,
            //     fixed: "left",
            //     render: record => {
            //         return record?.split('-')[0]
            //     }
            // }, 'No',
            //     data
            {
                title: 'Strategic Objectives',
                dataIndex: 'name',
                render: (text, record) => <span>{record.name || '-'}</span>,
            },
            {
                title: 'Measurement',
                dataIndex: 'measurement',
                render: (text, record) => <span>{record.measurement || '-'}</span>,
            },
            {
                title: 'Weight(%)',
                dataIndex: 'weight',
                render: (text, record) => <span>{record.weight || '-'}</span>,
            },
            {
                title: 'KPI Formula',
                dataIndex: 'kpi_type',
                render: (text, record) => {
                   if (text == 'actvstarget') {
                       return "Higher is Better"
                   }
                   else if (text == 'targetvsact'){
                       return "Lower is Better"
                   }
                   else return text
                },
            },
            {
                title: 'Target',
                dataIndex: 'target',
                render: (text, record) => {
                    const target = +record.target;
                    const data = _.find(store.formula.data, d => d.id === record.target);
                    const condition = (_.get(data, 'name'));
                    if(condition){
                        return _.get(data, 'name');
                    }else{
                        return Number(record.target).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
                    }
                },

            },
            {
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
                fixed: 'right',
                width:  store.ui.mediaQuery.isMobile?100:'',
                render: (text, record) => ( store.userData.role === "super_admin" &&
                    <span>
                       <Button icon={<EditOutlined />} onClick={() => { setEditMode(record) }} style={{margin:5}} />
                        <Button icon={<DeleteOutlined />} onClick={() => {
                            Modal.confirm({
                                title: 'Do you want to delete these kpi?',
                                icon: <ExclamationCircleOutlined />,
                                onOk() {
                                    return deleteDataKpi(record.id);
                                },
                                onCancel() { },
                            }); }} />
                    </span>
                )
            },
        ];
        
        return <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <Button type={"primary"} onClick={() => {window.history.back()}} style={{margin: '8px 1px'}} icon={<ArrowLeftOutlined />}>Back</Button>
            {totals()}
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> Entry KPI Corporate </div>}
                    subTitle=""
                    extra={store.userData.role === "super_admin" && [<div>
                        <Button color="secondary" onClick={toggleSuccess}>
                            <PlusOutlined /> Add KPI
                        </Button>
                    </div>
                    ]}
                />
                {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                {renderModal()}
                <Table columns={columns}
                    bordered
                    dataSource={store.kpiMaster.corporates}
                    pagination={{ pageSize: 50 }}
                    scroll={{ x: store.ui.mediaQuery.isMobile? 1000 : '', y: 350 }} />
            </Card>
        </div>

    }

    function deleteDataKpi(id) {
        store.kpiMaster.delete(id).then(res => {
            message.success('data deleted successfully');
            fetchData();
        }).catch(err => { message.error(err.message) })
    };

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
        title="KPI Corporate"
            okText="Save"
            cancelText="Cancel"
            bodyStyle={{ background: '#f7fafc' }}
            onCancel={() => {
                form.validateFields().then(values => {
                    form.resetFields();
                });
                toggleSuccess();
            }}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        // form.resetFields();
                        onSubmit(values);
                        form.setFieldsValue({});
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <div className="animated fadeIn">
                    <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData} >

                    <Form.Item name="name" label="Strategic Objectives" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="measurement" label="measurement" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="weight" label="Weight%" rules={[{ type: "number", required: true, max: state.max, min: state.min }]}>
                        <InputNumber style={{ width: '100%' }} />
                    </Form.Item>
                    <Form.Item name="weightHidden" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_type" label="KPI Formula" rules={[{ required: true }]}>
                        <Select
                            placeholder="Select KPI Formula"
                            allowClear
                            onChange={
                                (e) => {
                                    store.formula.filterData = _.filter(store.formula.data, i => i.formula_key === String(form.getFieldValue('kpi_type')))
                                }
                            }
                        >
                            <Select.Option>-</Select.Option>
                            {(_.uniqBy(store.formula.data, e => e.formula_key) || ["-"]).map(d => <Select.Option
                                value={d.formula_key}>{d.formula_key == 'month' || d.formula_key == 'grade'? d.formula_key : d.name}</Select.Option>
                            )}
                        </Select>
                    </Form.Item>
                    <Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.kpi_type !== currentValues.kpi_type}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('kpi_type') === 'month' ? (
                                <div>
                                    <Form.Item name="target" label="Target" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                            ) : null;
                        }}
                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.kpi_type !== currentValues.kpi_type}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('kpi_type') === 'actvstarget' ? (
                                <div>
                                    <Form.Item name="target" label="Target :" rules={[{ required: true }]}>
                                        <Input  formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}/>
                                    </Form.Item>
                                </div>
                            ) : null;
                        }}
                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.kpi_type !== currentValues.kpi_type}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('kpi_type') === 'targetvsact' ? (
                                <div>
                                    <Form.Item name="target" label="Target :" rules={[{ required: true }]}>
                                        <InputNumber  formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                </div>
                            ) : null;
                        }}
                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.kpi_type !== currentValues.kpi_type}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('kpi_type') === 'grade' ? (
                                <div>
                                    <Form.Item name="target" label="Target" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                            ) : null;
                        }}
                    </Form.Item>
                </Form>
                </div>
        </Modal>
    }


});

export default EntryKPICorporate;