import React, {useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { Alert, Form, Input, Select, Card,Space, message, Modal, Popconfirm, Table, Button, PageHeader} from 'antd';
import {FilterOutlined, PlusOutlined,CheckOutlined, DeleteOutlined, ExclamationCircleOutlined, EditOutlined } from "@ant-design/icons";
import {inject, observer} from "mobx-react";
import moment from "moment";
import { useStore } from "../../../utils/useStores";
import * as _ from 'lodash';
import { update } from 'lodash';

const dataSource = [];

let selectedDiv 
let dataRelated
let kpiDivision
let departmentUser
let departmentId
let positionCheck
const InputKPIGol13Depart = observer((initialData) => {
    const [form] = Form.useForm();
    const store = useStore();

    const [state, setState] = useState({
        modal: false,
        success: false,
        department: '',
        rincian_tugas: '',
        control_check_point: '',
        target: '',
        one_year_progress: '',
        relasi: '',
        division: '',
        kpi_related: '',
        rerender: 0,
        id_divisi : "",
        isEdit: false,
    })

    useEffect(() => {
        fetchData();
    }, []);
    async function fetchData() {
        await store.division.getAll();
        await store.organizations.getChilds(selectedDiv);
        await store.organizations.getAll();
        // await store.kpiMaster.getDivision({level: 'department-1-3'});
        await store.kpiMaster.getDepartments();
        await store.kpiMaster.getCorporate();
        await store.kpiMaster.getDivision();
        await store.user.getAll();
        store.organizations.filterData = _.filter(store.organizations.data, i => i.type === "department");
        kpiDivision = _.filter(store.division.data, i => i.id === selectedDiv);
        store.user.filterData = _.filter(store.user.data, i => i.id === store.userData.id);
        store.kpiMaster.filterData = _.filter(store.kpiMaster.departments, i => i.data.level === "department-1-3");
    }

    // 
    
    function onChange(e) {
        const name = e.name;
        const value = e.target.value;
        setState({ [e.target.name]: e.target.value })
    }

    function createData(kpi,log){
        store.log.createData(log)
                    store.kpiMaster.create(kpi)
                    .then(res => {
                        toggleSuccess();
                        message.success('KPI Saved Successfully!');
                        form.resetFields();
                        fetchData();
                    })
                    .catch(err => {
                        message.error(`Error on creating KPI, ${err.message}`);
                        form.resetFields();
                        message.error(err.message);
                    });
    }

    function updateData(id,kpi,log){
        store.log.createData(log)
            store.kpiMaster.update(id, kpi)
                .then(res => {
                    message.success('KPI Saved Successfully!');
                    toggleSuccess();
                    fetchData();
                    setState({
                        success : false
                    })
                    form.resetFields();
                })
                .catch(err => {
                    message.error(`Error on creating KPI, ${err.message}`);
                    message.error(err.message);
                    setState({
                        success : false
                    })
                    form.resetFields();
                });
    }

    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        // e.preventDefault();
        // console.log(e)
        const kpi = {
            year: moment().year(),
            division_id: e.division,
            department_id: e.department,
            name: e.rincian_tugas,
            target: e.target,
            data: {
                one_year_progress: e.one_year_progress,
                control_check_point: e.control_check_point,
                level: 'department-1-3',
            },
            relation : e.relasi
        };

        const kpi2 = {
            year: moment().year(),
            division_id: e.division,
            department_id: e.department,
            name: e.rincian_tugas,
            target: e.target,
            data: {
                one_year_progress: e.one_year_progress,
                control_check_point: e.control_check_point,
                level: 'department-1-3',
            },
            status: "Accepted",
            relation : e.relasi
        };

        const log = {
            user_id : user,
            event_name : "Add / Edit Department Gol. 1-3",
            data: {
                location: {
                    pathname:"/app/Kpi/entry-kpi-department/entry-kpi-department-gol-I-III",
                    search: "",
                    hash: "",
                    key: role,
                },
                action:"PUSH",
            }
        }
        const kpi_related = _.filter(store.kpiMaster.divisions, i => i.name == e.kpi_related)
        console.log(kpi_related, "ini ya")
        if(kpi_related.length === 0) {
            const kpi_master = {'kpi_master_id' : e.kpi_related}
            const kpiFix = {...kpi, ...kpi_master }
            console.log(e.kpi_related,e.kpi_related_id, "e")
            if (e.isEdit) {
                updateData(e.isEdit,kpiFix,log)
            } else {
                if (store.kpiMaster.departments.length <12 ) {
                    if(store.userData.role === "super_admin") {
                        const kpiFix ={...kpi2,...kpi_master}
                        createData(kpiFix,log)
                    }else{
                        createData(kpiFix,log)
                    }
                } else {
                    message.error('Error on creating KPI! KPI Already 12 Items');
                    toggleSuccess();
                }
            }
        }else{
            if(e.kpi_related === kpi_related[0].name ){
                const kpi_master = {'kpi_master_id' : e.kpi_related_id}
                const kpiFix = {...kpi, ...kpi_master }
                console.log(kpiFix)
                updateData(e.isEdit, kpiFix,log)
            } 
        }

    }
    
    const toggleSuccess = (() => {
        setState({
            success: !state.success,
        });
    });

    function arrayId() {
        const bb = store.kpiMaster.departments.map(d => {
            return d.id
        })
        const data = {
            ids : JSON.stringify(bb),
            status : "Accepted"
        }

        store.kpiMaster.updateStatus(data).then(res => {
            message.success("Submit KPI Success! You Wont Be Able to Change It")
            fetchData();
        })
        .catch(err => {
            message.error(err.message);
        });
    }

    const newRelasiKPIID = store.kpiMaster.departments.map(r => {
        return r.kpi_master_id
    })

    const newRelasiKPI = _.filter(store.kpiMaster.divisions, i => i.division_id == selectedDiv)

    const newRelasiKPIFiltered = newRelasiKPI.filter(function(item) {
        return !newRelasiKPIID.includes(item.id); 
      })

    const setEditMode = (record) => {
        store.department.divisionId = record.division_id;
        store.department.getAll();
        setState(prevState => ({
            ...prevState,
            success:true
        }))
        selectedDiv = record.division_id
        fetchData()
        form.setFieldsValue({
            isEdit: record.id,
            success: true,
            division: record.division_id,
            department: record.department.id,
            rincian_tugas: record.name,
            target: record.target,
            status: record.status,
            relasi: record.relation,
            kpi_related: record.kpi_master.name,
            kpi_related_id: record.kpi_master_id,
            one_year_progress: record.data.one_year_progress,
            control_check_point: record.data.control_check_point
        })
        
    }
    // render()
    
    {
        const {warning} = state;
        const columns = [
            
            {
                title: 'Department',
                dataIndex: 'department.name',
                render: (text, record) => <span>{record.department.name || '-'}</span>,
            },
            {
                title: 'Rincian Tugas',
                dataIndex: 'name',
                render: (text, record) => <span>{record.name || '-'}</span>,
            },
            {
                title: 'Control Check Point',
                dataIndex: 'data.control_check_point',
                render: (text, record) => <span>{record.data.control_check_point || '-'}</span>,
            },
            {
                title: 'Target / Periode',
                dataIndex: 'target',
                render: (text, record) => <span>{record.target || '-'}</span>,
            },
            {
                title: 'One Year Progress',
                dataIndex: 'data.one_year_progress',
                render: (text, record) => <span>{record.data.one_year_progress || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: '',
                render: record => <span>{record.relation || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master_id',
                render: (data, record) => {
                    // console.log({record}, ' -> render')
                    return record?.kpi_master?.name;
                }
            },
            {   
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
                fixed: 'right',
                width: 150,
                margin : 5,
                render: (text, record) => ( store.userData.role != "analyst" && store.userData.role != "staff" && store.userData.role != "BOD" &&
                    <span>
                         <CheckOutlined style={{display:record.status === "Accepted" &&  store.userData.role === "head" ? '':'none' }}  />
                    <Space size="small">        
                    <Button style={{ display: record.status === "Accepted" && store.userData.role != "super_admin" ? 'none' : '' }} onClick={() => { setEditMode(record) }}>
                            Edit
                            </Button>
                            <Button onClick={() => {
                                Modal.confirm({
                                    title: 'Do you want to delete these kpi?',
                                    icon: <ExclamationCircleOutlined />,
                                    onOk() {
                                        return deleteKpiData(record.id);
                                    },
                                    onCancel() { form.resetFields() },
                                });
                            }} style={{ marginLeft: 8, display:record.status === "Accepted" && store.userData.role != "super_admin"?'none':'' }}>Delete</Button>
                          
                          </Space>
                       
                    </span>
                )
            },
        ];
        const columns2 = [
            
            {
                title: 'Department',
                dataIndex: 'department.name',
                render: (text, record) => <span>{record.department.name || '-'}</span>,
            },
            {
                title: 'Rincian Tugas',
                dataIndex: 'name',
                render: (text, record) => <span>{record.name || '-'}</span>,
            },
            {
                title: 'Control Check Point',
                dataIndex: 'data.control_check_point',
                render: (text, record) => <span>{record.data.control_check_point || '-'}</span>,
            },
            {
                title: 'Target / Periode',
                dataIndex: 'target',
                render: (text, record) => <span>{record.target || '-'}</span>,
            },
            {
                title: 'One Year Progress',
                dataIndex: 'data.one_year_progress',
                render: (text, record) => <span>{record.data.one_year_progress || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: '',
                render: record => <span>{record.relation || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master_id',
                render: (data, record) => {
                    // console.log({record}, ' -> render')
                    return record?.kpi_master?.name;
                }
            }
        ];

        const newS = _.filter(store.kpiMaster.filterData, i => i.status != null)

         positionCheck = store.user.filterData.map(r => {
            return r.position_user
        })
        if(positionCheck[0] !== null){
            departmentId = store.user.filterData.map(d => {
                return d.position_user.position.organization.id
            })
            departmentUser = _.filter(store.kpiMaster.filterData, i => i.department_id === departmentId[0]);
        }
        return <div>
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> Entry KPI Department Golongan I-III </div>}
                    subTitle=""
                    extra={ store.userData.role != "analyst" && store.userData.role != "staff" && store.userData.role != "BOD" && [<div>
                         {store.userData.role === "head" &&
                         <Button style={{marginRight:20}} onClick={arrayId}>
                            <CheckOutlined />
                            Submit
                        </Button>
                         }
                         
                        <Button color="secondary" onClick={toggleSuccess}>
                            <PlusOutlined /> Add KPI
                        </Button>
                    </div>
                       
                    ]}
                />
                {/* {console.log(newS, "ini ke drop?")} */}
                    {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                      {renderModal()} 
                        <Table columns={store.userData.role != "super_admin" && store.userData.role != "head" ?columns2 : columns} bordered
                               dataSource={positionCheck[0] == null ? newS :departmentUser}
                               pagination={{pageSize: 50}}
                               scroll={{x: 1500, y: 300}}/>
                               {/* {console.log(store.kpiMaster.divisions, "ini department")} */}
                </Card>
            </div>
    }

    function deleteKpiData(id) {
        store.kpiMaster.delete(id)
            .then(res => {
                message.success('data berhasil di hapus')
                fetchData()
            }).catch(err => {
            message.error(err.message)
        })
    };


    function loadRelated(value){
        dataRelated = _.filter(store.kpiMaster.divisions, i => i.id === value);
        if(form.getFieldValue('target')== null){
            form.setFieldsValue({
                target:dataRelated[0].target
            })
        }
        
    }

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
		title="KPI Department Golongan I-III"
		okText="Save"
        cancelText="Cancel"
        bodyStyle={{ background: '#f7fafc' }}
        onCancel={() => {
			form.setFieldsValue({});
			toggleSuccess();
        }}
        onOk={() => {
			form
				.validateFields()
				.then(values => {
					form.resetFields();
					onSubmit(values);
					form.setFieldsValue({});
				})
				.catch(info => {
					console.log('Validate Failed:', info);
				});
		}}
        >
            <div className="animated fadeIn">
                <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData} >
                    <Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                        <Select
                        placeholder="Pilih Divisi"
                        allowClear
                        onChange={async (value) => {
                            selectedDiv = value;
                            fetchData();    
                          }} 
                        // onSelect={(value) =>{selectedDiv = value}}
                        >
                            
                        <Select.Option>Divisi</Select.Option>
                        {store.userData.role === "head" && positionCheck[0] !== null && store.user.filterData.map(d => <Select.Option  value={d.position_user.position.organization.organization.id}>{d.position_user.position.organization.organization.name}</Select.Option>)}
                        {store.userData.role === "super_admin" && (store.division.data || ["-"]).map(d => <Select.Option  value={d.id}>{d.name}</Select.Option>)}
                        </Select>
                    </Form.Item>
                    
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.division !== currentValues.division}
                    >
                        {({ getFieldValue }) => {
                        return getFieldValue('division') != '' ? (
                            <Form.Item 
                            name="department" 
                            label="Department" rules={[{ required: true }]}>
                                    <Select
                                allowClear >
                                <Select.Option>-</Select.Option>
                                {store.userData.role === "head" && positionCheck[0] !== null && store.user.filterData.map(d => <Select.Option  value={d.position_user.position.organization.id}>{d.position_user.position.organization.name}</Select.Option>)}
                                {store.userData.role === "super_admin" && store.organizations.dataDepart.map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                        ) : null;
                        }}
                    </Form.Item>

                    <Form.Item name="rincian_tugas" label="Rincian Tugas" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="control_check_point" label="Control Check Point" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_related_id" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item  name="status" hidden={true}>
                        <Input  />
                    </Form.Item>

                    <Form.Item name="target"  label="Target / Periode" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="one_year_progress" label="One Year Progress" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="relasi" label="Relasi" rules={[{ required: true }]}>
                        <Select allowClear > 
                        <Select.Option>Pilih Relasi</Select.Option>
                        <Select.Option value={'langsung'}>Langsung</Select.Option>
                        <Select.Option value={'tidak langsung'}>Tidak Langsung</Select.Option>
                        <Select.Option value={'tidak berhubungan'}>Tidak Berhubungan</Select.Option>
                        </Select>
                    </Form.Item>

                    <Form.Item
                    noStyle
                    shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                        return getFieldValue('relasi') === 'langsung' ? (
                            <Form.Item 
                            name="kpi_related" 
                            id="kpirelated" 
                            label="KPI Related" rules={[{ required: true }]}>
                                    <Select
                                     onChange={(value)=>
                                        loadRelated(value)
                                    }
                                allowClear >
                                <Select.Option>Related KPI</Select.Option>
                                {newRelasiKPIFiltered.map(c => <Select.Option value={c.id}>{c.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                        ) : null;
                        }}
                    </Form.Item>
                    </Form>
                </div>
        </Modal>
    }
})

export default InputKPIGol13Depart;