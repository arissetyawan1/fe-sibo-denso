import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { Alert, Form, Input, Select, Card, message, Menu, Modal, Popconfirm, Dropdown, Table, Button, PageHeader } from 'antd';
import { ClockCircleOutlined, CloseOutlined, FileProtectOutlined ,ArrowLeftOutlined, PlusOutlined, DeleteOutlined, CheckOutlined, ExclamationCircleOutlined, EditOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";
import moment from "moment";
import { useStore } from "../../../utils/useStores";
import * as _ from 'lodash';
import { useParams } from 'react-router-dom';

const dataSource = [];

let selectedDiv
let selectedDep
let dataRelated
let staffUser
let staffId
let positionCheck
let target
let one_year_progress
const InputKPIGol13Staff = observer((initialData) => {
    const [form] = Form.useForm();
    const store = useStore();
    const params = useParams();
    
    let user = store.userData.id
    let role = store.userData.role

    const [state, setState] = useState({
        modal: false,
        success: false,
        department: '',
        rincian_tugas: '',
        control_check_point: '',
        target: '',
        one_year_progress: '',
        relasi: '',
        division: '',
        kpi_related: '',
        rerender: 0,
        id_divisi: "",
        isEdit: false,
        kpi_type: '',
        formula_key: '',
    })
    const [selectedUser, setSelectedUser] = useState(null)

    useEffect(() => {
        fetchData();
    }, []);
    async function fetchData() {
        await store.division.getAll();
        await store.adjust.getAllAdjust();
        await store.organizations.getChilds(selectedDiv);
        await store.organizations.getAll();
        await store.kpiMaster.getStaffsByUser({ staffId: user, level:'staff-1-3', departmentId: params.id_depart, include_progress: 'true' });
        await store.kpiMaster.getStaffs({ departmentId: params.id_depart, include_progress: 'true', level: 'staff-1-3' });
        await store.user.getAll();
        store.user.filterData = _.filter(store.user.data, i => i.id === user);
        await store.formula.getAllFormula();
    }

    function createData(kpi, log) {
        store.kpiMaster.create(kpi)
            .then(res => {
                store.log.createData(log)
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                setState({
                    success: false
                })
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                setState({
                    success: false
                })
                form.resetFields();
            });
    }
    function updateData(id, kpi, log) {
        store.kpiMaster.updateGol13(id, kpi)
            .then(res => {
                store.log.createData(log)
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                setState({
                    success: false
                })
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                setState({
                    success: false
                })
                form.resetFields();
            });
    }

    function onSubmit(e) {
        let dataAdjust = _.filter(store.adjust.data, i => i.menu_name === "kpi")
        let currentYear = dataAdjust.map(d => {
            return d?.year
        });
        const kpi = {
            year: currentYear[0],
            division_id: e.division,
            department_id: e.department,
            user_id: role === "super_admin" ? e.user : user,
            name: e.rincian_tugas,
            role: role,
            user_login: user,
            status: role === "super_admin" ? 'Accepted' : 'Approval',
            data: {
                level: 'staff-1-3',
                control_check_point: e.control_check_point,
            },
        }; 

        const log = {
            user_id: user,
            event_name: "Add / Edit Staff Gol. 1-3",
            data: {
                location: {
                    pathname: "/app/Kpi/entry-kpi-department/entry-kpi-staff-gol-I-III",
                    search: "",
                    hash: "",
                    key: role,
                },
                action: "PUSH",
            }
        }
        
        if (e.isEdit) {
            updateData(e.isEdit, kpi, log)
        } else {
            if (store.kpiMaster.staffs.length < 12) {
                createData(kpi, log)
            } else {
                message.error('Error on creating KPI! KPI Already 12 Items');
                toggleSuccess();
            }
        }

    }

    const toggleSuccess = (() => {
        setState({
            success: !state.success,
        });
    });

    function arrayId(value, id) {
        const data = {
            ids: id,
            status: "Accepted"
        }

        if (value.key == 2) {
            data.status = "Rejected";
        }

        store.kpiMaster.updateStatus(data).then(res => {
            message.success('Submit KPI Success! You Wont Be Able to Change it')
            fetchData()
        })
            .catch(err => {
                message.error(err.message);
            });
    }

    const setEditMode = (record) => {
        setState(prevState => ({
            ...prevState,
            success: true
        }))
        selectedDiv = record.division_id
        selectedDep = record.department_id
        fetchData()
        if(role === "head_division") {
            setSelectedUser(_.filter(store.user.data, i => i.id === user))
        }else {
            setSelectedUser(_.filter(store.user.data, i => i?.position_user !== null && i?.position?.organization?.id === record?.department_id))
        }
        form.setFieldsValue({
            isEdit: record.id,
            success: true,
            division: record.division_id,
            department: record.department_id,
            user: record.user_id,
            rincian_tugas: record.name,
            status: record.status,
            control_check_point: record.data.control_check_point,
        })

    }

    const approveOrReject = (val) => (<Menu style={{backgroundColor:'white'}} onClick={(value) => { arrayId(value, val)}}>
        <Menu.Item key="1">Approve KPI</Menu.Item>
        <Menu.Item key="2">Reject KPI</Menu.Item>
    </Menu>)

    const checkApprove = (val) => {
        if (val === "Accepted" && store.userData.role !== "super_admin") {
            return "none"
        }else {
            return ''
        }
    }

    const approvalCheckButton = (value) => {
        if (value.status === "Accepted" && role !== "super_admin") {
            return <Button icon={<CheckOutlined />}>Accepted</Button>
        }else if( value.status === "Rejected") {
            return <Button icon={<CloseOutlined />}>Rejected</Button>
        }else if (value.status === "Approval"&& ((role === "head") || (role === "head_division")) && value.user_approve !== user) {
            return <Button icon={<ClockCircleOutlined onClick={() => { message.info('waiting kpi approval')}} />} />
        }else if (value.status === "Accepted" && role === "super_admin") {
            return <Button icon={<CheckOutlined />} />
        }else if(value.status === "Approval" && role === "super_admin") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }else if (((role === "head") || (role === "head_division")) && value.status === "Approval") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }
    }

    {
        const { warning } = state;
        const columns = [

            {
                title: 'Staff',
                dataIndex: 'staff.name',
                render: (text, record) => {
                    if (record.staff == null) {
                        return <span>-</span>
                    } else {
                        return <span>{record.staff.full_name}</span>
                    }
                },
            },
            {
                title: 'Rincian Tugas',
                dataIndex: 'name',
                render: (text, record) => <span>{record.name || '-'}</span>,
            },
            {
                title: 'Control Check Point',
                dataIndex: 'data.control_check_point',
                render: (text, record) => <span>{record.data.control_check_point || '-'}</span>,
            },
            {
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
                fixed: 'right',
                width: 150,
                render: (text, record) => (
                    <span>
                        <Button onClick={() => { setEditMode(record) }} style={{display: checkApprove(record.status)}} icon={<EditOutlined />} />
                        <Button icon={<DeleteOutlined />} onClick={() => {
                            Modal.confirm({
                                title: 'Do you want to delete these kpi?',
                                icon: <ExclamationCircleOutlined />,
                                onOk() {
                                    return deleteKpiData(record.id);
                                },
                                onCancel() { },
                            });
                        }} style={{
                            margin: '1px 8px',
                            display: checkApprove(record.status)
                         }} />
                         {approvalCheckButton(record)}
                    </span>
                )
            },
        ];
        const columns2 = [

            {
                title: 'Staff',
                dataIndex: 'staff.name',
                render: (text, record) => {
                    if (record.staff == null) {
                        return <span>-</span>
                    } else {
                        return <span>{record.staff.name}</span>
                    }
                },
            },
            {
                title: 'Rincian Tugas',
                dataIndex: 'name',
                render: (text, record) => <span>{record.name || '-'}</span>,
            },
            {
                title: 'Control Check Point',
                dataIndex: 'data.control_check_point',
                render: (text, record) => <span>{record.data.control_check_point || '-'}</span>,
            },
        ];

        positionCheck = store.user.filterData.map(r => {
            return r.position_user
        })
        if (positionCheck[0] !== null) {
            staffId = store.user.filterData.map(d => {
                return d.user_id
            })

            staffUser = _.filter(store.kpiMaster.staffs, i => i.user_id === store.userData.id);
        }

        const renderSourceData = () => {
            if((role === "super_admin") || (role === "BOD")) {
                // console.log("you are superadmin")
                return store.kpiMaster.staffs
            }else if((role === "head") || (role === "head_division")) {
                // console.log("you are head")
                return _.filter(store.kpiMaster.staffs, i => (i.user_id === user) || (i.user_approve === user) )
            } else {
                // console.log("either staff o r analyst")
                return store.kpiMaster.staffsUser
            }
        }

        return <div>
            <Button type={"primary"} icon={<ArrowLeftOutlined />} onClick={() => window.history.back()} style={{marginBottom:20}}>Back</Button>
            {renderModal()}
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> Entry KPI Staff Golongan I-III </div>}
                    subTitle=""
                    extra={store.userData.role !== "BOD" && <Button color="secondary" onClick={toggleSuccess}>
                            <PlusOutlined /> Add KPI
                       </Button>}
                />
                {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                <Table columns={store.userData.role !== "BOD" ? columns : columns2} bordered
                    dataSource={renderSourceData()}
                    pagination={{ pageSize: 50 }}
                    scroll={{y: 350 }} />

            </Card>
        </div>
    }

    function deleteKpiData(id) {
        store.kpiMaster.delete(id)
            .then(res => {
                message.success('data berhasil di hapus')
                fetchData()
            }).catch(err => {
                message.error(err.message)
            })
    };

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
            title="KPI Staff Golongan I-III"
            okText="Save"
            cancelText="Cancel"
            bodyStyle={{ background: '#f7fafc' }}
            onCancel={() => {
                form.resetFields();
                toggleSuccess();
            }}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        form.resetFields();
                        onSubmit(values);
                        form.setFieldsValue({});
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <div className="animated fadeIn">


                <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData} >
                    <Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                        <Select
                            placeholder="Pilih Divisi"
                            allowClear
                            onChange={async (value) => {
                                selectedDiv = value;
                                fetchData();
                            }}
                        // onSelect={(value) =>{selectedDiv = value}}
                        >
                            {(_.filter(store.division.data, i => i.id == params.id_divisi) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                        </Select>
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.division !== currentValues.division}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('division') != '' ? (
                                <Form.Item
                                    name="department"
                                    label="Department" rules={[{ required: true }]}>
                                    <Select
                                        allowClear
                                        onChange={async (value) => {
                                            selectedDep = value;
                                            if(role === "head_division") {
                                                setSelectedUser(_.filter(store.user.data, i => i.id === user))
                                            }else {
                                                setSelectedUser(_.filter(store.user.data, i => i.position_user !== null && i?.position?.organization?.id === value))   
                                            }

                                        }}>
                                        {(_.filter(store.organizations.dataDepart, i => i.id == params.id_depart) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : null;
                        }}
                    </Form.Item>
                    {store.userData.role === "super_admin" &&
                        <Form.Item
                            noStyle
                            shouldUpdate={(prevValues, currentValues) => prevValues.department !== currentValues.department}
                        >
                            {({ getFieldValue }) => {
                                return getFieldValue('department') != '' ? (
                                    <Form.Item
                                        name="user"
                                        label="User" rules={[{ required: true }]}>
                                        <Select

                                            allowClear >
                                            <Select.Option>-</Select.Option>
                                            {store.userData.role === "super_admin" && selectedUser == null ? ['-'] : selectedUser.map(d => <Select.Option value={d.id}>{d.full_name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                ) : null;
                            }}
                        </Form.Item>
                    }
                    <Form.Item name="rincian_tugas" label="Rincian Tugas" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="control_check_point" label="Control Check Point" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="status" hidden={true}>
                        <Input />
                    </Form.Item>
                </Form>
            </div>
        </Modal>
    }
})

export default InputKPIGol13Staff;