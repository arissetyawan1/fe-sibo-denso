import React, {useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import { Alert, Space,Form, Input, Select, Card, message, Modal, Popconfirm, Table, Button, PageHeader} from 'antd';
import {FilterOutlined, PlusOutlined, DeleteOutlined, ExclamationCircleOutlined, EditOutlined, CheckOutlined } from "@ant-design/icons";
import {inject, observer} from "mobx-react";
import moment from "moment";
import { useStore } from "../../../utils/useStores";
import * as _ from 'lodash';


const dataSource = [];
let dataRelated
let dataId
const InputKPIGol13Divisi = observer((initialData) => {
    const store = useStore();
    const [form] = Form.useForm();

    const [state, setState] = useState({
        modal: false,
        success: false,
        division: '',
        rincian_tugas: '',
        control_cek_point: '',
        target: '',
        one_years_progres: '',
        relasi: '',
        kpi_related: '',
        persons: [],
        rerender: 0,
        data: dataSource,
        isEdit: false,
        dataIseng : {},
        loading:false,
        kpi_related_name : ''
    }); 

    useEffect(() => {
		fetchData();

    }, []);
    async function fetchData() {
        await store.division.getAll();
        await store.kpiMaster.getDivision();
        await store.kpiMaster.getCorporate();
        store.kpiMaster.filterData = _.filter(store.kpiMaster.divisions, i => i.data.level === "division-1-3");
    }

    function onChange(e) {
        const name = e.name;
        const value = e.target.value;
        // console.log({name, value}, 'InputKPIGol13Divisi -> onChange')
        setState({[name]: value, success:true})
    }
    function createData(kpi,log){
        store.log.createData(log)
        store.kpiMaster.create(kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess(); 
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }

    function updateData(id, kpi,log){
        store.log.createData(log)
        store.kpiMaster.update(id, kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess(); 
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }
    
    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        const kpi = {
            year: moment().year(),
            division_id: e.division,
            name: e.rincian_tugas,
            target: e.target,
            data: {
                control_cek_point: e.control_cek_point,
                one_years_progress: e.one_years_progress,
                level: 'division-1-3'
            },
            relation : e.relasi
        }
        const log = {
            user_id : user,
            event_name : "Add / Edit Divisi Gol. 1-3",
            data: {
                location: {
                    pathname:"/app/Kpi/entry-kpi-divisi/entry-kpi-divisi-gol-I-III",
                    search:"",
                    hash:"",
                    key:role
                },
                action:"PUSH",
            }
        }
        const kpi_related = _.filter(store.kpiMaster.corporates, i => i.name == e.kpi_related)
        if(kpi_related.length === 0) {
            const kpi_master = {'kpi_master_id' : e.kpi_related}
            const kpiFix = {...kpi, ...kpi_master }
            console.log(e.kpi_related,e.kpi_related_id, "e")
            if (e.isEdit) {
                updateData(e.isEdit, kpiFix,log)
                } else {
                    if (store.kpiMaster.divisions.length <12 ) {
                        createData(kpiFix,log)
                    } else{
                        message.error('Error on creating KPI! KPI Already 12 Items');
                        toggleSuccess();
                    }
            }
        } else{
            if(e.kpi_related === kpi_related[0].name ){
                const kpi_master = {'kpi_master_id' : e.kpi_related_id}
                const kpiFix = {...kpi, ...kpi_master }
                console.log(kpiFix, "kpiFix")
                if (e.isEdit) {
                    updateData(e.isEdit, kpiFix,log)
            } 
        }
        
    }

        
    }
    // const toggle = () => {
    //     setState({
    //         modal: !state.modal,
    //     });
    // }
    const toggleSuccess = () => {
        setState({
            success: !state.success,
        });
    }

    function arrayId() {
        const bb = store.kpiMaster.divisions.map(d => {
            return d.id
        })
        const data = {
            ids : JSON.stringify(bb),
            status : "Accepted"
        }

        store.kpiMaster.updateStatus(data).then(res => {
            message.success("Submit KPI Success! Other Users Wont Be Able to Change It")
            fetchData();
        })
        .catch(err => {
            message.error(err.message);
        });
    }
    const newRelasiKPIID = store.kpiMaster.divisions.map(r => {
        return r.kpi_master_id
    })

    const newRelasiKPI = _.filter(store.kpiMaster.corporates, i => i.id != newRelasiKPIID)

    const newRelasiKPIFiltered = newRelasiKPI.filter(function(item) {
        return !newRelasiKPIID.includes(item.id); 
      })
    
    // let modifiedData = {}
    const setEditMode = (record) => {
        console.log(store.kpiMaster.corporates,  record.kpi_master_id)
        const kpi_related = _.filter(store.kpiMaster.corporates, i => i.id == record.kpi_master_id)
        setState(prevState => ({
            ...prevState,
            success:true,
        }))
       form.setFieldsValue({
            loading : true,
            success: true,
            isEdit: record.id,
            division: record.division_id,
            rincian_tugas: record.name,
            target: record.target,
            kpi_related: record.kpi_master.name,
            kpi_related_id: record.kpi_master_id,
            relasi: record.relation,
            control_cek_point: record.data.control_cek_point,
            one_years_progress: record.data.one_years_progress,
        })
        // console.log(state);
    } 

    {
        const {warning} = state;
        const columns = [
            // {
            //     title: 'No',
            //     dataIndex: 'id',
            //     width: 65,
            //     fixed: "left",
            //     render: record => {
            //         return record?.split('-')[0]
            //     }
            // },
            {
                title: 'Division',
                dataIndex: 'division.name',
                render: (text, record) => <span>{record.division.name || '-'}</span>,
            },
            {
                title: 'Rincian Tugas',
                dataIndex: 'name',
                render: (text, record) => <span>{record.name || '-'}</span>,
            },
            {
                title: 'Control Check Point',
                dataIndex: 'data.control_cek_point',
                render: (text, record) => <span>{record.data.control_cek_point || '-'}</span>,
            },
            {
                title: 'Target / Periode',
                dataIndex: 'target',
                render: (text, record) => <span>{record.target || '-'}</span>,
            },
            {
                title: 'One Year Progress',
                dataIndex: 'data.one_years_progress',
                render: (text, record) => <span>{record.data.one_years_progress || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: '',
                render: record =>  <span>{record.relation || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master_id',
                render: (text, record) => {
                    // console.log({record}, ' -> render')
                    return record?.kpi_master?.name;
                }
            },
            {
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
                fixed: 'right',
                width: 150,
                margin : 5,
                render: (text, record) => ( store.userData.role === "super_admin" &&
                    <span>
                    <Space size="small">
                       <Button onClick={() => { setEditMode(record) }} style={{display:record.status === "Accepted" && store.userData.role != "super_admin"?'none':'' }}>
                            Edit
                        </Button>
                        <Button onClick={() => {
                            Modal.confirm({
                                title: 'Do you want to delete these kpi?',
                                icon: <ExclamationCircleOutlined />,
                                onOk() {
                                    // store.kpiMaster.delete(record.id);
                                    // console.log(record.id)
                                    return deleteKpi(record.id);
                                },
                                onCancel() { },
                            });
                        }} style={{marginLeft: 8, display:record.status === "Accepted" && store.userData.role != "super_admin"?'none':'' }}>Delete</Button>
                    </Space>
                    </span>
                )
            },
        ];
        // console.log({lengh: store.kpiMaster.data.length}, 'InputKPIGol13Divisi -> render')
        return <div> 
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> Entry KPI Divisi Golongan I-III </div>}
                    subTitle=""
                    extra={store.userData.role === "super_admin" && [ <div>
                        {/* <Button style={{marginRight:20}} onClick={arrayId}>
                            <CheckOutlined />
                            Submit
                        </Button> */}
                        <Button color="secondary" onClick={toggleSuccess}>
                            <PlusOutlined /> Add KPI
                        </Button> </div>
                    ]}
                />
                      {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                      {renderModal()} 
                      <Table columns={columns}
                             hasEmpty
                             bordered
                             size={"small"}
                             dataSource={store.kpiMaster.filterData}
                             pagination={{pageSize: 50}}
                             scroll={{x: 1250, y: 300}}/> 
          </Card>
        </div>

    }

    function deleteKpi(id) {
        store.kpiMaster.delete(id)
            .then(res => {
                message.success('data berhasil dihapus')
                fetchData()
            })
            .catch(err => {
                message.error(err.message)
            });
    };

    // function editKpi(id, data) {
    //     store.kpiMaster.update(id, data)
    //       .then(res => {
    //           message.success('data berhasil di edit')
    //       }).catch(err => {
    //         throw err
    //     })
    // }
    
    function loadRelated(value){
        dataRelated = _.filter(store.kpiMaster.corporates, i => i.id === value);
        if(form.getFieldValue('target')== null){
            form.setFieldsValue({
                target:dataRelated[0].target
            })
        }
        
    }

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
		title="KPI Golongan I-III"
		okText="Save"
        cancelText="Cancel"
        bodyStyle={{ background: '#f7fafc' }}
        onCancel={() => {
			form.setFieldsValue({});
			toggleSuccess();
        }}
        onOk={() => {
			form
				.validateFields()
				.then(values => {
					form.resetFields();
					onSubmit(values);
					form.setFieldsValue({});
				})
				.catch(info => {
					console.log('Validate Failed:', info);
				});
		}}
        >
                <div className="animated fadeIn">
                    <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData} >

                    <Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                        <Select
                        placeholder="Pilih Divisi"
                        allowClear
                        >
                        <Select.Option>Divisi</Select.Option>
                        {(store.division.data || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                        </Select>
                    </Form.Item>
                    {/* {console.log(modifiedData.target, "target")} */}
                    {/*{console.log(state.relasi, "relasi")} */}
                    <Form.Item name="rincian_tugas" label="Rincian Tugas" rules={[{ required: true }]}>
                        {/* {console.log(state.rincian_tugas)} */}
                        <Input />
                    </Form.Item>

                    <Form.Item name="control_cek_point" label="Control Check Point" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_related_id" hidden={true}>
                        <Input />
                    </Form.Item>

                    <Form.Item name="target"  label="Target / Periode" disabled={true}>
                        <Input />
                     </Form.Item>
                    <Form.Item name="one_years_progress" label="One Year Progress" rules={[{ required: true }]}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="relasi" label="Relasi" rules={[{ required: true }]}>
                        <Select allowClear > 
                        <Select.Option>Pilih Relasi</Select.Option>
                        <Select.Option value={'langsung'}>Langsung</Select.Option>
                        <Select.Option value={'tidak langsung'}>Tidak Langsung</Select.Option>
                        <Select.Option value={'tidak berhubungan'}>Tidak Berhubungan</Select.Option>
                        </Select>
                    </Form.Item>
                    
                    <Form.Item
                    noStyle
                    shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                        return getFieldValue('relasi') === 'langsung' ? (
                            <Form.Item 
                            name="kpi_related" 
                            id="kpirelated" 
                            label="KPI Related" rules={[{ required: true }]}>
                                    <Select onChange={(value)=>
                                        loadRelated(value)
                                    }
                                allowClear >
                                <Select.Option>Related KPI</Select.Option>
                                {console.log(getFieldValue('isEdit'),state.kpi_related_name, "kkkkkkkkk")}
                                {/* {getFieldValue('isEdit') != null && <Select.Option value={state.kpi_related_id}>pppppppppp</Select.Option>} */}
                                {newRelasiKPIFiltered.map(c => <Select.Option selected value={c.id}>{c.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                        ) : null;
                        }}
                    </Form.Item>
                </Form>
                </div>
                </Modal>
    }
})

export default InputKPIGol13Divisi;
