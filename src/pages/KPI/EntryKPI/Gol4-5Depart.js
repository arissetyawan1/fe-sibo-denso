import React, { Component, useEffect, useState } from 'react';
// import 'antd/dist/antd.css';
import { inputkpi_gol_4_5 } from './EntryFunction'
import {
    Row,
    Col,
    Button,
    Statistic,
    Card,
    Empty,
    PageHeader,
    Dropdown,
    Table,
    Menu,
    Input,
    InputNumber,
    Select,
    Space,
    Typography,
    Tag,
    message,
    Modal,
    Alert,
    Form,
    DatePicker
} from "antd";
import {
    ArrowLeftOutlined,
    PlusOutlined,
    CheckOutlined,
    DeleteOutlined,
    CloseOutlined,
    ClockCircleOutlined,
    FileProtectOutlined,
    EditOutlined
} from "@ant-design/icons";
import moment from 'moment';
import * as _ from "lodash";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { inject, observer } from "mobx-react";
import { useStore } from "../../../utils/useStores";
import { values } from 'lodash';
import { useParams } from 'react-router-dom';

let selectedDiv
let dataRelated
let target_mid_years
let target_one_years
const InputKPIGol45Depart = observer((initialData) => {

    const store = useStore();
    const [form] = Form.useForm();
    const params = useParams();
    const [KPIChoosen, setKPIChoosen ] = useState("")

    const [state, setState] = useState({
        modal: false,
        success: false,
        department: '',
        individual_perfomance_plan: '',
        type: '',
        weight: '',
        kpi_type: '',
        formula_key: '',
        target_mid_years: '',
        target_one_years: '',
        due_date: '',
        relasi: '',
        kpi_related: '',
        division: '',
        rerender: 0,
        isEdit: false,
        min: 0,
        maax: 0,
        elmForm: "",
        weightBefore: 0
    });

    useEffect(() => {
        fetchData();
    }, []);

    async function fetchData() {
        await store.formula.getAllFormula();
        await store.adjust.getAllAdjust();
        await store.department.getAll();
        await store.organizations.getChilds(selectedDiv);
        await store.organizations.getAll();
        await store.division.getAll();
        await store.kpiMaster.getDepartments({ departmentId: params.id_depart, include_progress: 'true' });
        await store.kpiMaster.getCorporate();
        await store.kpiMaster.getDivision();
    }

    function createData(kpi, log) {
        store.kpiMaster.create(kpi)
            .then(res => {
                store.log.createData(log)
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }

    function updateData(id, kpi, log) {
        store.log.createData(log)
        delete kpi.role;
        delete kpi.user_login;
        if (kpi.weight + (profit - state.weightBefore) > 100) {
            return  message.error('Cannot update Corporate, Weight over 100');
        }
        else
        store.kpiMaster.update(id, kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                form.resetFields();
            });
    }

    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        let dataAdjust = _.filter(store.adjust.data, i => i.menu_name === "kpi")
        let currentYear = dataAdjust.map(d => {
            return d?.year
        });
        console.log(currentYear[0]);
        console.log(e.kpi_type,e.target_mid_years,e.target_one_years)
        const kpi = {
            year: currentYear[0],
            division_id: e.division,
            department_id: e.department,
            name: e.plan,
            weight: e.weight,
            kpi_type: e.kpi_type,
            formula_key: e.formula_key,
            role: role,
            user_login: user,
            target: e.target_one_years,
            status: "Approval",
            kpi_hierarki: e.kpi_hierarki,
            data: {
                due_date: e.due_date,
                type: e.type,
                target_mid_years: e.target_mid_years,
                level: 'department-4-5',
            },
            relation: e.relasi
        };
        const kpi2 = {
            year: currentYear[0],
            division_id: e.division,
            department_id: e.department,
            name: e.plan,
            weight: e.weight,
            kpi_type: e.kpi_type,
            role: role,
            user_login: user,
            formula_key: e.formula_key,
            target: e.target_one_years,
            data: {
                due_date: e.due_date,
                type: e.type,
                target_mid_years: e.target_mid_years,
                level: 'department-4-5',
            },
            status: "Accepted",
            kpi_hierarki: e.kpi_hierarki,
            relation: e.relasi
        };
        const log = {
            user_id: user,
            event_name: "Add / Edit Department Gol. 4-5",
            data: {
                location: {
                    pathname: "/app/Kpi/entry-kpi-department/entry-kpi-department-gol-IV-V",
                    search: "",
                    hash: "",
                    key: role,
                },
                action: "PUSH",
            }
        }

        const kpi_related = _.filter(store.kpiMaster.divisions, i => i.name == e.kpi_related)
        console.log(kpi_related, "haha")
        if (kpi_related.length === 0) {
            const kpi_master = { 'kpi_master_id': e.kpi_related || null}
            const kpiFix = { ...kpi, ...kpi_master }
            const kpiUser = { ...kpi2, ...kpi_master }

            if (e.isEdit) {
                if (profit - e.weightHidden + parseInt(kpiFix.weight) >= 100) {
                    message.error('Cannot update KPI Department, Weight over 100');
                    toggleSuccess();
                } else {
                    updateData(e.isEdit, kpiFix, log)
                }

            } else {
                if (profit + parseInt(kpi.weight) > 100) {
                    message.error('Cannot Add KPI Department, Weight over 100');
                    toggleSuccess();
                } else {
                    if (profit >= 100) {
                        message.error('Cannot Add entry Department, Weight Already 100');
                        toggleSuccess();
                        form.resetFields();
                    }
                    else {
                        if (store.kpiMaster.departments.length < 13) {
                            if (store.userData.role === "super_admin") {
                                createData(kpiUser, log)
                            } else {
                                createData(kpiFix, log)
                            }
                        } else {
                            message.error('Error on creating KPI! KPI Already 13 Items');
                            toggleSuccess();
                        }

                    }
                }
            }

        } else {
            if (e.kpi_related === kpi_related[0].name) {
                const kpi_master = { 'kpi_master_id': e.kpi_related_id || null}
                const kpiFix = { ...kpi, ...kpi_master }
                if (e.isEdit) {
                    updateData(e.isEdit, kpiFix, log)
                }
            }
        }

    }

    function arrayId(value, val) {
        const data = {
            ids: val,
            status: "Accepted"
        }

        if(value.key == 2) {
            data.status = "Rejected"
        }

        store.kpiMaster.updateStatus(data).then(res => {
            message.success("Approve KPI Success!")
            fetchData();
        })
            .catch(err => {
                message.error(err.message);
            });
    }

    const toggleSuccess = (() => {
        setState({
            success: !state.success,
        });
        // form.setFieldsValue({
        //     weight:100-profit
        // })
    })


    const profit = _.sum(_.map(store.kpiMaster.departments, function (day) {
        
        return +day.weight

    }));

    function totals() {
        let total = 100 - profit
        if (total <= 0) {
            return (<Alert
                message="Weight Already 100"
                type="info"
                showIcon
                style={{ marginBottom: 10 }}
            />)
        } else {
            return (
                <Alert
                    message="Weight Less Than 100"
                    type="success"
                    showIcon
                    style={{ marginBottom: 10 }}
                />
            )
        }
    }

    function loadWeight(value, a, x) {
        if (x === "type") {
            if (form.getFieldValue('type') == "Others") {
                setState({
                    max: 5,
                    min: 1,
                    success: true,
                })
            } else {
                setState({
                    max: 30,
                    min: 5,
                    success: true,
                })
            }
        } else {
            // setState({
            //     elmForm: a.children,
            // });
        }
    }

    const chooseKPI = KPIChoosen == "corporate" ? store.kpiMaster.corporates : store.kpiMaster.divisions
    const newRelasiKPIID =  store.kpiMaster.departments.map(r => {
        return r.kpi_master_id
    })

    const newRelasiKPI = KPIChoosen == "division" ? _.filter(chooseKPI, i => i.division_id == selectedDiv) : chooseKPI
    const newRelasiKPIFiltered = newRelasiKPI.filter(function (item) {
        return !newRelasiKPIID.includes(item.id);
    })

    const setEditMode = (record) => {
        if (record.data.type == "Others") {
            setState(prevState => ({
                ...prevState,
                success: true,
                max: 5,
                min: 1,
                weightBefore: +record.weight
            }))
        } else {
            setState(prevState => ({
                ...prevState,
                success: true,
                max: 30,
                min: 5,
                weightBefore: +record.weight
            }))
        }

        selectedDiv = record.division_id
        fetchData()
        form.setFieldsValue({
            isEdit: record.id,
            success: true,
            division: record.division_id,
            department: record.department.id,
            weight: +record.weight,
            weightHidden: record.weight,
            plan: record.name,
            target_one_years: record.target,
            relasi: record.relation,
            kpi_type: record.kpi_type,
            formula_key: record.formula_key,
            kpi_related: record.kpi_master ? record.kpi_master.name : "",
            kpi_related_id: record.kpi_master_id,
            status: record.status,
            due_date: record.data.due_date,
            target_mid_years: record.data.target_mid_years,
            kpi_hierarki: record.kpi_hierarki,
            type: record.data.type
        })
    }
    
    const approveOrReject = (val) => (<Menu style={{backgroundColor:'white'}} onClick={(value) => { arrayId(value, val)}}>
        <Menu.Item key="1">Approve KPI</Menu.Item>
        <Menu.Item key="2">Reject KPI</Menu.Item>
    </Menu>)

    const approvalCheckButton = (value) => {
        if (value.status === "Accepted" && role !== "super_admin") {
            return <Button icon={<CheckOutlined />}>Accepted</Button>
        }else if( value.status === "Rejected") {
            return <Button icon={<CloseOutlined />}>Rejected</Button>
        }else if (value.status === "Approval"&& ((role === "head") || (role === "head_division")) && value.user_approve !== user) {
            return <Button icon={<ClockCircleOutlined />} />
        }else if (value.status === "Accepted" && role === "super_admin") {
            return <Button icon={<CheckOutlined />} />
        }else if(value.status === "Approval" && role === "super_admin") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }else if (((role === "head") || (role === "head_division")) && value.status === "Approval") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }
}

    const checkApprove = (val) => {
        if (val === "Accepted" && role !== "super_admin") {
            return "none"
        }else {
            return ''
        }
    }

    {
        const { warning } = state;
        const columns = [

            {
                title: 'Department',
                dataIndex: 'department',
                render: (text, record) => <span>{text?.name || '-'}</span>,
            },
            {
                title: 'Individual Perfomance Plan',
                dataIndex: 'name',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'Type',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.type || '-'}</span>,
            },
            {
                title: 'Weight(%)',
                dataIndex: 'weight',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'KPI Formula',
                dataIndex: 'kpi_type',
                render: (text, record) => {
                    if (text === "actvstarget") {
                      return "Higher is Better"
                    }
                    else if (text === "targetvsact"){
                      return "Lower is Better"
                    }
                      return text?.charAt(0).toUpperCase() + text.slice(1)
                  }
            },
            {
                title: 'Target Mid Year',
                dataIndex: 'data',
                render: (text, record) => {
                    const data = _.find(store.formula?.data, d => d.id === text?.target_mid_years);
                    const condition = (_.get(data, 'name'));
                    if(condition){
                        return _.get(data, 'name');
                    }else{
                        return Number(text?.target_mid_years).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
                    }
                },
            },
            {
                title: 'Target One Year',
                dataIndex: 'target',
                render: (text, record) => {
                    const data = _.find(store.formula?.data, d => d.id === text);
                    const condition = (_.get(data, 'name'));
                    if(condition){
                        return _.get(data, 'name');
                    }else{
                        return Number(text).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
                    }
                },

            },
            {
                title: 'Due Date',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.due_date || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: 'relation',
                render: (text, record) => <span>{text || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master',
                render: (text, record) => {
                    return text ?  text?.name : "No direct relation";
                }
            },
            {
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
                fixed: 'right',
                width: store.ui.mediaQuery.isMobile? 130 :150,
                render: (text, record) => (
                    <span>
                        <Space size="small">
                        <Button icon={<EditOutlined />} style={{display: checkApprove(record.status)}} onClick={() => { 
                            if(record.kpi_hierarki === "corporate") {
                                setKPIChoosen('corporate')
                            }else if (record.kpi_hierarki === "division") {
                                setKPIChoosen('division')
                            }else if(record.kpi_hierarki === "department") {
                                setKPIChoosen('department')
                            }
                            setEditMode(record) 
                            }} />
                        <Button onClick={() => {
                            Modal.confirm({
                                title: 'Do you want to delete these kpi?',
                                icon: <ExclamationCircleOutlined />,
                                onOk() {
                                    return deleteKpiDepart(record.id);
                                },
                                onCancel() { },
                            });
                        }} style={{
                            margin: '1px 5px',
                            display: checkApprove(record.status)
                        }} icon={<DeleteOutlined />}
                         />
                         {approvalCheckButton(record)}
                    </Space>
                    </span>
                )
            },
        ];
        const columns2 = [

            {
                title: 'Department',
                dataIndex: 'department',
                render: (text, record) => <span>{text?.name || '-'}</span>,
            },
            {
                title: 'Individual Perfomance Plan',
                dataIndex: 'name',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'Type',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.type || '-'}</span>,
            },
            {
                title: 'Weight(%)',
                dataIndex: 'weight',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'KPI Formula',
                dataIndex: '',
                render: (text, record) => {
                    if (record?.kpi_type == null) {
                        return <span>-</span>
                    } else {
                        let formula1 = _.uniqBy(store.formula?.data, e => e.formula_key);
                        let formula = _.filter(formula1, e => e.formula_key === record?.kpi_type)
                        let formula2 = _.filter(formula, e => e.formula_key !== null)
                        let text = formula2.map(d => {
                            return d?.formula_key
                        })
                        return <span>{text}</span>
                    }
                },
            },
            {
                title: 'Target Mid Year',
                dataIndex: 'data',
                render: (text, record) => {
                    if(record?.kpi_type === 'month'){
                         let formula_value = _.find(store.formula?.data, {'id':text?.target_mid_years})
                        return _.get(formula_value, 'formula_value')
                    }else if(record?.kpi_type === 'grade'){ 
                        let formula_value =  _.find(store.formula?.data, {'id':text?.target_mid_years})
                       return _.get(formula_value, 'formula_value')
                    }else{
                        return text?.target_mid_years
                    }
                },  
            },
            {
                title: 'Target One Year',
                dataIndex: 'target',
                render: (text, record) => {
                    if(record?.kpi_type === 'month'){
                         let formula_value = _.find(store.formula?.data, {'id':text})
                        return _.get(formula_value, 'formula_value')
                    }else if(record?.kpi_type === 'grade'){ 
                        let formula_value =  _.find(store.formula?.data, {'id':text})
                       return _.get(formula_value, 'formula_value')
                    }else{
                        return Number(text).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                    }
                },

            },
            {
                title: 'Due Date',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.due_date || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: 'relation',
                render: (text, record) => <span>{text || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master',
                render: (text, record) => {
                    return text?.name;
                }
            }
        ];

        return <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
        <Button icon={<ArrowLeftOutlined />} type="primary" style={{margin: '8px 1px'}} onClick={() => {window.history.back()}}>Back</Button>
            {totals()}
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> Entry KPI Department Golongan IV - V </div>}
                    subTitle=""
                    extra={((role === "super_admin") || (role === "head")) && [<div>
                        <Button color="secondary" onClick={toggleSuccess}>
                            <PlusOutlined /> Add KPI
                       </Button>
                    </div>
                    ]}
                />

                {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                {renderModal()}
                <Table columns={role !== "super_admin" && role !== "head" && role !== "head_division" ? columns2 : columns}
                    bordered
                    dataSource={store.kpiMaster.departments}
                    pagination={{ pageSize: 50 }}
                    scroll={{ x: 1500, y: 300 }} />

            </Card>
        </div>

    }

    function deleteKpiDepart(id) {
        store.kpiMaster.delete(id)
            .then(res => {
                message.success('data deleted successfully');
                // this.componentDidMount();
                fetchData();
            }).catch(err => { message.error(err.message) })
    };
    function loadRelated(value) {
        dataRelated = KPIChoosen === "corporate" ? _.filter(store.kpiMaster.corporates, i => i.id === value) : _.filter(store.kpiMaster.divisions, i => i.id === value);
        if (dataRelated) {
            form.setFieldsValue({
                target_one_years: dataRelated[0]?.target,
                kpi_type: dataRelated[0]?.kpi_type,
                target_mid_years: dataRelated[0]?.data?.target_mid_years || "-",
                target_one_years : dataRelated[0]?.target
            })
        }

    }

    function changeMonth(value) {
        console.log(`selected ${value}`);
    }

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
            title="KPI Golongan IV-V"
            okText="Save"
            cancelText="Cancel"
            bodyStyle={{ background: '#f7fafc' }}
            onCancel={() => {
                form.validateFields().then(values => {
                    form.resetFields();
                });
                toggleSuccess();
            }}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        // form.resetFields();
                        onSubmit(values);
                        form.setFieldsValue({});
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <div className="animated fadeIn">
                <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
                    <Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="status" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_related_id" hidden={true}>
                        <Input />
                    </Form.Item>

                    
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.isEdit !== currentValues.isEdit}
                    >
                        {({ getFieldValue }) => {
                            return !getFieldValue('isEdit') ? (
                                <Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Pilih Divisi"
                                        allowClear
                                        onChange={async (value) => {
                                            selectedDiv = value;
                                            fetchData();

                                        }}
                                    >
                                        <Select.Option>Divisi</Select.Option>
                                        {(_.filter(store.division.data, i => i.id == params.id_divisi) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>) : (
                                    <Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Pilih Divisi"
                                        disabled={true}
                                        onChange={async (value) => {
                                            selectedDiv = value;
                                            fetchData();

                                        }}
                                    >
                                        <Select.Option>Divisi</Select.Option>
                                        {(_.filter(store.division.data, i => i.id == params.id_divisi) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                                )
                        }}
                        </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.divisi !== currentValues.divisi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('divisi') != '' && !getFieldValue('isEdit')? (
                                <Form.Item
                                    name="department"
                                    label="Department" rules={[{ required: true }]}>
                                    <Select
                                        allowClear>
                                        <Select.Option>-</Select.Option>
                                        {(_.filter(store.organizations.dataDepart, i => i.id == params.id_depart) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) :
                                <Form.Item
                            name="department"
                            label="Department" rules={[{ required: true }]}>
                            <Select disabled={true}>
                                <Select.Option>-</Select.Option>
                                {(_.filter(store.organizations.dataDepart, i => i.id == params.id_depart) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                            </Select>
                        </Form.Item>;
                        }}
                    </Form.Item>

                    <Form.Item name="relasi" label="Relasi" rules={[{ required: true }]} >
                        <Select
                            allowClear
                            onChange={(value) => {
                                if (value !== 'langsung') {
                                    form.setFieldsValue({
                                        kpi_related: '',
                                        kpi_related_id : ''
                                    })
                                } 
                            }}
                        >
                            <Select.Option>Pilih Relasi</Select.Option>
                            <Select.Option value={'langsung'}>Langsung</Select.Option>
                            <Select.Option value={'tidak langsung'}>Tidak Langsung</Select.Option>
                            <Select.Option value={'tidak berhubungan'}>Tidak Berhubungan</Select.Option>

                        </Select>

                    </Form.Item>
                    
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === 'langsung' ? [
                                <Form.Item
                                    label="KPI Hierarchy" name="kpi_hierarki" rules={[{ required: true }]}>
                                    <Select
                                        allowClear
                                        placeholder="select hierarchy kpi"
                                        onChange={(value) => {
                                           setKPIChoosen(value);
                                           form.setFieldsValue({
                                                kpi_related: '',
                                                kpi_related_id : ''
                                            })
                                        }}
                                    >
                                        <Select.Option value={'corporate'}>KPI Corporate</Select.Option>
                                        <Select.Option value={'division'}>KPI Division</Select.Option>
                                    </Select>
                                </Form.Item>,
                                <Form.Item
                                    name="kpi_related"
                                    id="kpirelated"
                                    label="KPI Related" rules={[{ required: true }]}>
                                    <Select
                                        onChange={(value) => {
                                            loadRelated(value)
                                        }}
                                        allowClear
                                        placeholder="select kpi related"
                                        value={state.kpi_related} >
                                        {newRelasiKPIFiltered.map(c => <Select.Option value={c.id}>{c.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ] : null;
                        }}
                    </Form.Item>
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => { 
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="plan" label="Individual Perfomance Plan:">
                                    <Input disabled={true}/>
                                </Form.Item>
                            ) : (
                                    <Form.Item name="plan" label="Individual Perfomance Plan:" rules={[{ required: true }]}>
                                        <Input />
                                    </Form.Item> )
                        }}
                    </Form.Item>

                    
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="type" label="Type" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select Type"
                                        onChange={(value) => {
                                            loadWeight(value, null, 'type')
                                        }}
                                        disabled={true}
                                    >
                                        {[
                                            'Routine',
                                            'Project',
                                            'Others',
                                        ].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : (
                                <Form.Item name="type" label="Type" rules={[{ required: true }]}>
                                <Select
                                    placeholder="Select Type"
                                    onChange={(value) => {
                                        loadWeight(value, null, 'type')
                                    }}
                                    allowClear
                                >
                                    <Select.Option></Select.Option>
                                    {[
                                        'Routine',
                                        'Project',
                                        'Others',
                                    ].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                                </Select>
                            </Form.Item>
                            )
                        }}
                        </Form.Item>

                        <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="weight" label="Weight%" rules={[{ type: "number"}]}>
                                    <InputNumber style={{ width: '100%' }} disabled={true}/>
                                </Form.Item>) : (
                                    <Form.Item name="weight" label="Weight%" rules={[{ type: "number", required: true, max: state.max, min: state.min }]}>
                                    <InputNumber style={{ width: '100%' }} />
                                </Form.Item>)
                        }}
                    </Form.Item>

                    <Form.Item name="weightHidden" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_related_id" hidden={true}>
                        <Input />
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}>
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === 'langsung' ? (
                                <Form.Item name="kpi_type" label="KPI Formula" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select KPI Formula"
                                        disabled={true}
                                        onChange={
                                            (e) => {
                                                form.setFieldsValue({
                                                    target_mid_years : '',
                                                    target_one_years : ''
                                                })
                                                store.formula.filterData = _.filter(store.formula?.data, i => i.formula_key === String(form.getFieldValue('kpi_type')))
                                            }
                                        } >
                                        <Select.Option>-</Select.Option>
                                        {(_.uniqBy(store.formula?.data, e => e.formula_key) || ["-"]).map(d => 
                                        <Select.Option

                                            value={d.formula_key}>{d.formula_key == 'month' || d.formula_key == 'grade'? d.formula_key : d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item> ) : <Form.Item name="kpi_type" label="KPI Formula" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select KPI Formula"
                                        allowClear
                                        onChange={
                                            (e) => {
                                                form.setFieldsValue({
                                                    target_mid_years : '',
                                                    target_one_years : ''
                                                })
                                                store.formula.filterData = _.filter(store.formula?.data, i => i.formula_key === String(form.getFieldValue('kpi_type')))
                                                console.log(store.formula.filterData, "brou")
                                            }
                                        }
                                        
                                    >
                                        <Select.Option>-</Select.Option>
                                        {(_.uniqBy(store.formula?.data, e => e.formula_key) || ["-"]).map(d => <Select.Option
                                            value={d.formula_key}>{d.formula_key == 'month' || d.formula_key == 'grade'? d.formula_key : d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>  }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}
                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') !== 'langsung') {
                                console.log("only month")
                               return <div>
                            <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    onChange={(value) => {
                                        target_mid_years = value
                                        changeMonth(value)
                                    }}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula?.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    onChange={(value) => {
                                        target_one_years = value
                                        changeMonth(value)
                                    }}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula?.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                        </div>
                                
                            } else if ( getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') === "langsung" && KPIChoosen == "division") {
                                console.log("month and langsung")
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                            } else if(getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') === "langsung" && KPIChoosen == "corporate") {
                                console.log("month, corporate, langsung")
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                            }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}
                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') === "langsung"  && KPIChoosen == "corporate") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') !== "langsung") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') === "langsung" && KPIChoosen == "division") {
                                return <div> 
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                            </div>
                            }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') === "langsung" && KPIChoosen === "division") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') !== "langsung") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') === "langsung" && KPIChoosen === "corporate") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } 
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') === "langsung" && KPIChoosen === "division") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            disabled={true}>
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            disabled={true} >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                             } else if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') !== "langsung") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                             } else if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') === "langsung" && KPIChoosen === "corporate") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-">
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            disabled={true} >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula?.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                             }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.kpi_type !== currentValues.kpi_type}

                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('kpi_type') ? null : <div>
                                <Form.Item name="target_mid_years" label="Target Mid Year :">
                                    <Input disabled={true}/>
                                </Form.Item>
                                <Form.Item name="target_one_years" label="Target One Year :">
                                    <Input disabled={true}/>
                                </Form.Item>
                            </div> ;
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.relasi !== currentValues.relasi) || (prevValues.isEdit !== currentValues.isEdit)}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="due_date" label="Due Date" >
                                    <Input type="date" disabled={true}/>
                                </Form.Item>) : (
                                    <Form.Item name="due_date" label="Due Date" rules={[{ required: true }]}>
                                    <Input type="date" />
                                </Form.Item>)
                        }}
                    </Form.Item>
                </Form>
            </div>
        </Modal>
    }


});

export default InputKPIGol45Depart;
