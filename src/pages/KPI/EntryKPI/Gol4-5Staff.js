import React, { Component, useEffect, useState } from 'react';
// import 'antd/dist/antd.css';
import { inputkpi_gol_4_5 } from './EntryFunction'
import {
    Row,
    Col,
    Button,
    Statistic,
    Card,
    Empty,
    PageHeader,
    Dropdown,
    Table,
    Menu,
    Input,
    InputNumber,
    Select,
    Space,
    Typography,
    Tag,
    message,
    Modal,
    Alert,
    Form
} from "antd";
import {
    ClockCircleOutlined,
    PlusOutlined,
    DeleteOutlined,
    CloseOutlined,
    FileProtectOutlined,
    EditOutlined,
    CheckOutlined,
    ArrowLeftOutlined
} from "@ant-design/icons";
import * as _ from "lodash";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { inject, observer } from "mobx-react";
import moment from "moment";
import { useStore } from "../../../utils/useStores";
import { includes, values } from 'lodash';
import { useParams } from 'react-router-dom';

let selectedDiv
let selectedDep
let dataRelated
let departmentUser
let divisiUser
let staffUser
let target_mid_years
let target_one_years
const InputKPIGol45Staff = observer((initialData) => {

    const store = useStore();
    const [form] = Form.useForm();
    const params = useParams();
    const positionCheck = store.user.filterData.map(r => {
        return r.position_user
    })

    const [state, setState] = useState({
        modal: false,
        success: false,
        department: '',
        individual_perfomance_plan: '',
        type: '',
        weight: '',
        target_mid_years: '',
        target_one_years: '',
        due_date: '',
        relasi: '',
        kpi_related: '',
        division: '',
        rerender: 0,
        isEdit: false,
        min: '',
        maax: '',
        kpi_type: '',
        formula_key: '',
        weightBefore: ''
    });

    const [selectedUser, setSelectedUser] = useState(null)
    const [kpiDepartment, setKpiDepartment] = useState(null)
    const [KPIChoosen, setKPIChoosen] = useState("");

    useEffect(() => {
        fetchData();
    }, []);

    async function fetchData() {
        await store.formula.getAllFormula();
        await store.division.getAll();
        await store.organizations.getChilds(selectedDiv);
        await store.staff.getStaffByDepartment(selectedDep);
        await store.organizations.getAll();
        await store.adjust.getAllAdjust();
        await store.kpiMaster.getStaffsByUser({staffId: store.userData.id, level:'staff-1-3', departmentId: params.id_depart, include_progress: 'true' });
        await store.kpiMaster.getStaffs({departmentId : params.id_depart, include_progress : 'true', level : "staff-4-5"});
        await store.kpiMaster.getCorporate();
        await store.kpiMaster.getDivision({ divisionId: params.id_divisi, level: 'division-4-5' });
        await store.kpiMaster.getDepartments({ departmentId: params.id_depart, include_progress: 'true' });
        await store.user.getAll();
        store.user.filterData = _.filter(store.user.data, i => i.id === store.userData.id);
    }

    function createData(kpi,log){
        store.log.createData(log)
        store.kpiMaster.create(kpi)
            .then(res => {
                message.success('KPI Saved Successfully!');
                toggleSuccess();
                fetchData();
                setState({
                    success: false
                })
                form.resetFields();
            })
            .catch(err => {
                message.error(`Error on creating KPI, ${err.message}`);
                message.error(err.message);
                setState({
                    success: false
                })
                form.resetFields();
            });
    }

    function updateData(id,kpi,log){
        store.log.createData(log)
        if (kpi.weight + (profit - state.weightBefore) > 100) {
            return  message.error('Cannot update Corporate, Weight over 100');
        }
        else            store.kpiMaster.update(id, kpi)
                .then(res => {
                    message.success('KPI Saved Successfully!');
                    toggleSuccess();
                    fetchData();
                    setState({
                        success : false
                    })
                    form.resetFields();
                })
                .catch(err => {
                    message.error(`Error on creating KPI, ${err.message}`);
                    message.error(err.message);
                    setState({
                        success : false
                    })
                    form.resetFields();
                });
    } 

    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        let dataAdjust = _.filter(store.adjust.data, i => i.menu_name === "kpi")
        let currentYear = dataAdjust.map(d => {
            return d?.year
        });
        console.log(currentYear[0]);
        const kpi = {
            year: currentYear[0],
            division_id: e.division,
            department_id: e.department,
            user_id:store.userData.role === "super_admin" ? e.user : user,
            name: e.plan,
            role: role,
            user_login: user,
            weight: e.weight,
            kpi_type: e.kpi_type,
            formula_key: e.formula_key,
            target: e.target_one_years,
            status: "Approval",
            data: {
                due_date: e.due_date,
                type: e.type,
                target_mid_years: e.target_mid_years,
                level: 'staff-4-5',
            },
            relation: e.relasi
        };
        const kpi2 = {
            year: currentYear[0],
            division_id: e.division,
            department_id: e.department,
            user_id:store.userData.role === "super_admin" ? e.user : user,
            name: e.plan,
            weight: e.weight,
            role: role,
            user_login: user,
            kpi_type: e.kpi_type,
            formula_key: e.formula_key,
            target: e.target_one_years,
            data: {
                due_date: e.due_date,
                type: e.type,
                target_mid_years: e.target_mid_years,
                level: 'staff-4-5',
            },
            status: "Accepted",
            relation: e.relasi
        };
        const log = {
            user_id: user,
            event_name: "Add / Edit Staff Gol. 4-5",
            data: {
                location: {
                    pathname: "/app/Kpi/entry-kpi-department/entry-kpi-staff-gol-IV-V",
                    search: "",
                    hash: "",
                    key: role,
                },
                action: "PUSH",
            }
        }
        const kpi_related = _.filter(store.kpiMaster.departments, i => i.name == e.kpi_related)
        if(kpi_related.length === 0) {
            const kpi_master = {'kpi_master_id' : e.kpi_related || null}
            const kpiFix = {...kpi, ...kpi_master }
            if (e.isEdit) {
                if (profit - e.weightHidden + parseInt(kpi.weight) >= 100) {
                    message.error('Cannot update KPI Staff, Weight over 100');
                    toggleSuccess();
                } else {
                    updateData(e.isEdit, kpiFix,log)
                }
            } else {
                if (profit + parseInt(kpi.weight) > 100) {
                    message.error('Cannot Add KPI Staff, Weight over 100');
                    toggleSuccess();
                } else {
                    if (profit >= 100) {
                        message.error('Cannot Add entry Staff, Weight Already 100');
                        toggleSuccess();
                        form.resetFields();
                    }
                    else {
                        if (store.kpiMaster.staffs.length < 13) {
                            if(store.userData.role === "super_admin") {
                                const kpiFix = { ...kpi2, ...kpi_master }
                                createData(kpiFix,log)
                            } else {
                                createData(kpiFix,log)
                            }
                        } else {
                            message.error('Error on creating KPI! KPI Already 13 Items');
                            toggleSuccess();
                        }

                    }
                }

            }
        }else{
            if(e.kpi_related === kpi_related[0].name ){
                const kpi_master = {'kpi_master_id' : e.kpi_related_id || null}
                const kpiFix = {...kpi, ...kpi_master }
                updateData(e.isEdit, kpiFix,log)
            } 
        }
    }

    // const toggle = (() => {
    //     this.setState({
    //         modal: !state.modal,
    //     });
    // })

    function arrayId(value, val) {
        const data = {
            ids: val,
            status: "Accepted"
        }

        if(value.key == 2) {
            data.status = "Rejected"
        }

        store.kpiMaster.updateStatus(data).then(res => {
            message.success('Approval KPI Success!')
            fetchData()
        })
            .catch(err => {
                message.error(err.message);
            });
    }

    const toggleSuccess = (() => {
        setState({
            success: !state.success,
        });
    })

    const profit = _.sum(_.map(store.kpiMaster.staffs, function (day) {
        
        return +day.weight

    }));

    function totals() {
        let total = 100 - profit
        if (total <= 0) {
            return (<Alert
                message="Weight Already 100"
                type="info"
                showIcon
                style={{ marginBottom: 10 }}
            />)
        } else {
            return (
                <Alert
                    message="Weight Less Than 100"
                    type="success"
                    showIcon
                    style={{ marginBottom: 10 }}
                />
            )
        }
    }

    function loadWeight(value) {
        if (form.getFieldValue('type') == "Others") {
            setState({
                max: 5,
                min: 1,
                success: true,
            })
        } else {
            setState({
                max: 30,
                min: 5,
                success: true,
            })
        }
    }

    const chooseKPI = KPIChoosen === "corporate" ? store.kpiMaster.corporates : KPIChoosen === "division" ? store.kpiMaster.divisions : store.kpiMaster.departments 

      const setEditMode = (record) => {
        if (record.data.type == "Others") {
            setState(prevState => ({
                ...prevState,
                weightBefore: +record.weight,
                success: true,
                max: 5,
                min: 1,
            }))
        } else {
            setState(prevState => ({
                ...prevState,
                weightBefore: +record.weight,
                success: true,
                max: 30,
                min: 5,
            }))
        }
        selectedDiv = record.division_id
        selectedDep = record.department_id
        setKpiDepartment( _.filter(store.kpiMaster.departments, i => i?.department_id === record?.department_id && i?.user_id === null))
        setSelectedUser( _.filter(store.user.data, i => i?.position_user !==null && i?.position?.organization?.id === record?.department_id ))
        fetchData()
        form.setFieldsValue({
            isEdit: record.id,
            success: true,
            division: record.division_id,
            department: record.department_id,
            user: record.user_id,
            plan: record.name,
            target_one_years: record.target,
            status: record.status,
            weight : +record.weight,
            relasi: record.relation,
            kpi_related: record.kpi_master ? record.kpi_master.name : "",
            kpi_related_id: record.kpi_master_id,
            due_date: record.data.due_date,
            target_mid_years: record.data.target_mid_years,
            type: record.data.type,
            kpi_type: record.kpi_type,
            kpi_hierarki: record.kpi_hierarki,
            formula_key: record.formula_key,

        })
    }


    const approveOrReject = (val) => (<Menu style={{backgroundColor:'white'}} onClick={(value) => { arrayId(value, val)}}>
        <Menu.Item key="1">Approve KPI</Menu.Item>
        <Menu.Item key="2">Reject KPI</Menu.Item>
    </Menu>)

    const checkApprove = (val) => {
        if (val === "Accepted" && store.userData.role !== "super_admin") {
            return "none"
        }else {
            return ''
        }
    }

    const approvalCheckButton = (value) => {
        if (value.status === "Accepted" && role !== "super_admin") {
            return <Button icon={<CheckOutlined />}>Accepted</Button>
        }else if( value.status === "Rejected") {
            return <Button icon={<CloseOutlined />}>Rejected</Button>
        }else if (value.status === "Approval"&& role === "head" && value.user_approve !== user) {
            return <Button icon={<ClockCircleOutlined />} />
        }else if (value.status === "Accepted" && role === "super_admin") {
            return <Button icon={<CheckOutlined />} />
        }else if(value.status === "Approval" && role === "super_admin") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }else if (role === "head" && value.status === "Approval") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }
    }
    
    {
        const { warning } = state;
        const columns = [

            {
                title: 'Staff',
                dataIndex: 'staff',
                render: (text, record) => {
                    if (text == null) {
                        return <span>-</span>
                    } else {
                        return <span>{text?.full_name}</span>
                    }
                },
            },
            {
                title: 'Individual Perfomance Plan',
                dataIndex: 'name',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'Type',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.type || '-'}</span>,
            },
            {
                title: 'Weight(%)',
                dataIndex: 'weight',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'KPI Formula',
                dataIndex: 'kpi_type',
                render: (data, record) => {
                    if (data == null) {
                        return <span>-</span>
                    } else {
                        let formula1 = _.uniqBy(store.formula.data, e => e.formula_key);
                        let formula = _.filter(formula1, e => e.formula_key === data)
                        let formula2 = _.filter(formula, e => e.formula_key !== null)
                        let text = formula2.map(d => {
                            return d?.formula_key
                        })
                        return <span>{text}</span>
                    }
                },
            },
            {
                title: 'Target Mid Year',
                dataIndex: 'data',
                render: (text, record) => {
                    if(record?.kpi_type === 'month'){
                         let formula_value = _.find(store.formula.data, {'id':text?.target_mid_years})
                        return _.get(formula_value, 'name')
                    }else if(record?.kpi_type === 'grade'){ 
                        let formula_value =  _.find(store.formula.data, {'id':text?.target_mid_years})
                       return _.get(formula_value, 'name')
                    }else{
                        return Number(text?.target_mid_years).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                    }
                },
            },
            {
                title: 'Target One Year',
                dataIndex: 'target',
                render: (text, record) => {
                    if(record?.kpi_type === 'month'){
                         let formula_value = _.find(store.formula.data, {'id':text})
                        return _.get(formula_value, 'name')
                    }else if(record?.kpi_type === 'grade'){ 
                        let formula_value =  _.find(store.formula.data, {'id':text})
                       return _.get(formula_value, 'name')
                    }else{
                        return Number(text).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
                    }
                },

            },
            {
                title: 'Due Date',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.due_date || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: 'relation',
                render: (text, record) => <span>{ text || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master',
                render: (text, record) => {
                    return text ? text?.name : 'No Related KPI';
                }
            },
            {
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
                fixed: 'right',
                width: 150,
                render: (text, record) => (
                    <span>
                        <Space size="small">
                        <Button style={{display: checkApprove(record?.status)}} onClick={() => {
                            if(record.kpi_hierarki === "corporate") {
                                setKPIChoosen('corporate')
                            }else if (record.kpi_hierarki === "division") {
                                setKPIChoosen('division')
                            }else if(record.kpi_hierarki === "department") {
                                setKPIChoosen('department')
                            } 
                            setEditMode(record)
                             }} icon={<EditOutlined />} />
                        <Button icon={<DeleteOutlined />} onClick={() => {
                            Modal.confirm({
                                title: 'Do you want to delete these kpi?',
                                icon: <ExclamationCircleOutlined />,
                                onOk() {
                                    return deleteKpiStaff(record.id);
                                },
                                onCancel() { },
                            });
                        }} style={{
                            margin: '1px 8px', display: checkApprove(record.status)}} />
                        {approvalCheckButton(record)}
                    </Space>
                    </span>
                )
            },
        ];
        const columns2 = [

            {
                title: 'Staff',
                dataIndex: 'staff',
                render: (text, record) => {
                    if (text == null) {
                        return <span>-</span>
                    } else {
                        return <span>{text?.full_name}</span>
                    }
                },
            },
            {
                title: 'Individual Perfomance Plan',
                dataIndex: 'name',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'Type',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.type || '-'}</span>,
            },
            {
                title: 'Weight(%)',
                dataIndex: 'weight',
                render: (text, record) => <span>{text || '-'}</span>,
            },
            {
                title: 'KPI Formula',
                dataIndex: 'kpi_type',
                render: (data, record) => {
                    if (data == null) {
                        return <span>-</span>
                    } else {
                        let formula1 = _.uniqBy(store.formula.data, e => e.formula_key);
                        let formula = _.filter(formula1, e => e.formula_key === data)
                        let formula2 = _.filter(formula, e => e.formula_key !== null)
                        let text = formula2.map(d => {
                            return d?.formula_key
                        })
                        return <span>{text}</span>
                    }
                },
            },
            {
                title: 'Target Mid Year',
                dataIndex: 'data',
                render: (text, record) => {
                    const data = _.find(store.formula.data, d => d.id === text?.target_mid_years);
                    const condition = (_.get(data, 'name'));
                    if(condition){
                        return _.get(data, 'name');
                    }else{
                        return text?.target_mid_years;
                    }
                },
            },
            {
                title: 'Target One Year',
                dataIndex: 'target',
                render: (text, record) => {
                    const data = _.find(store.formula.data, d => d.id === text);
                    const condition = (_.get(data, 'name'));
                    if(condition){
                        return _.get(data, 'name');
                    }else{
                        return text;
                    }
                },

            },
            {
                title: 'Due Date',
                dataIndex: 'data',
                render: (text, record) => <span>{text?.due_date || '-'}</span>,
            },
            {
                title: 'Relasi',
                dataIndex: 'relation',
                render: (text, record) => <span>{text || '-'}</span>
            },
            {
                title: 'KPI Related',
                dataIndex: 'kpi_master',
                render: (text, record) => {
                    return text ? text?.name : 'No Related KPI';
                }
            }
        ];

        if (positionCheck[0] !== null) {
            let staffId = store.user.filterData.map(d => {
                return d.user_id
            })
            let departmentId = store.user.filterData.map(d => {
                return d.position_user.position.organization.id
            })
            let divisiId = store.user.filterData.map(d => {
                return d.position_user.position.organization.organization.id
            })
            staffUser = _.filter(store.kpiMaster.staffs, i => i.user_id === store.userData.id);
            departmentUser = _.filter(store.kpiMaster.departments, i => i.department_id === departmentId[0]);
            divisiUser = _.filter(store.division.data, i => i.id === divisiId[0]);
        }
        
        const renderSourceData = () => {
            if((role === "super_admin") || (role === "BOD")) {
                // console.log("you are superadmin")
                return store.kpiMaster.staffs
            }else if(role === "head") {
                // console.log("you are head")
                return _.filter(store.kpiMaster.staffs, i => (i.user_id === user) || (i.user_approve === user) )
            } else {
                // console.log("either staff o r analyst")
                return store.kpiMaster.staffsUser
            }
        }
        
        return <div  style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
        <Button type={"primary"} icon={<ArrowLeftOutlined />} onClick={() => window.history.back()} style={{margin:'12px 1px'}}>Back</Button>
            {totals()}
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> Entry KPI Staff Golongan IV - V </div>}
                    subTitle=""
                    extra={<Button color="secondary" onClick={toggleSuccess}>
                           <PlusOutlined /> Add KPI
                       </Button>}
                />

                {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                {renderModal()}
                <Table columns={ store.userData.role !== "BOD" ? columns : columns2}
                    bordered
                    dataSource={renderSourceData()}
                    pagination={{ pageSize: 50 }}
                    scroll={{ x: 1500, y: 300 }} />

            </Card>
        </div>

    }

    function deleteKpiStaff(id) {
        store.kpiMaster.delete(id)
            .then(res => {
                message.success('data deleted successfully');
                // this.componentDidMount();
                fetchData();
            }).catch(err => { message.error(err.message) })
    };
    function loadRelated(value) {
        dataRelated = KPIChoosen === "corporate" ? _.filter(store.kpiMaster.corporates, i => i.id === value) : KPIChoosen === "division" ? _.filter(store.kpiMaster.divisions, i => i.id === value) : _.filter(store.kpiMaster.departments, i => i.id === value);
        if (dataRelated) {
            form.setFieldsValue({
                target_one_years: dataRelated[0].target,
                kpi_type: dataRelated[0].kpi_type,
                target_mid_years: dataRelated[0]?.data?.target_mid_years || "-",
                target_one_years : dataRelated[0].target
            })
        }

    }

    function changeMonth(value) {
        console.log(`selected ${value}`);
      }

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
            title="KPI Golongan IV-V"
            okText="Save"
            cancelText="Cancel"
            bodyStyle={{ background: '#f7fafc' }}
            onCancel={() => {
                form.validateFields().then(values => {
                    form.resetFields();
                });
                toggleSuccess();
            }}
            onOk={() => {
                form
                    .validateFields()
                    .then(values => {
                        // form.resetFields();
                        onSubmit(values);
                        form.setFieldsValue({});
                    })
                    .catch(info => {
                        console.log('Validate Failed:', info);
                    });
            }}
        >
            <div className="animated fadeIn">
                <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
                    <Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_related_id" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="status" hidden={true}>
                        <Input />
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.isEdit !== currentValues.isEdit}
                    >
                        {({ getFieldValue }) => { 
                            return getFieldValue('isEdit') ? (<Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                            <Select disabled={true}>
                                <Select.Option>Divisi</Select.Option>
                                {(_.filter(store.division.data, i=> i.id == params.id_divisi) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                            </Select>
                        </Form.Item>) : (<Form.Item name="division" label="Divisi" rules={[{ required: true }]}>
                        <Select
                            placeholder="Pilih Divisi"
                            allowClear
                            onChange={async (value) => {
                                selectedDiv = value;
                                fetchData();
                            }}
                        >
                            <Select.Option>Divisi</Select.Option>
                            {(_.filter(store.division.data, i=> i.id == params.id_divisi) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                        </Select>
                    </Form.Item>)
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.division !== currentValues.division}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('division') != '' && getFieldValue('isEdit') ? (
                                <Form.Item
                                    name="department"
                                    label="Department" rules={[{ required: true }]}>
                                    <Select disabled={true}>
                                        <Select.Option>-</Select.Option>
                                        {(_.filter(store.organizations.dataDepart, i=> i.id == params.id_depart) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : (<Form.Item
                                name="department"
                                label="Department" rules={[{ required: true }]}>
                                <Select
                                    allowClear
                                    onChange={async (value) => {
                                        selectedDep = value;
                                        setKpiDepartment( _.filter(store.kpiMaster.departments, i => i?.department_id === value && i?.user_id === null))
                                        setSelectedUser( _.filter(store.user.data, i => i?.position_user !==null && i?.position?.organization?.id === value ))
                                    }}>
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.organizations.dataDepart, i=> i.id == params.id_depart) || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>);
                        }}
                    </Form.Item>
                    {store.userData.role === "super_admin" &&
                        <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.department !== currentValues.department}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('department') != '' && getFieldValue('isEdit') ? (
                                <Form.Item
                                    name="user"
                                    label="User">
                                    <Select disabled={true}
                                        allowClear >
                                        <Select.Option>-</Select.Option>
                                         {store.userData.role === "super_admin" && selectedUser == null? ['-'] : selectedUser.map(d => <Select.Option value={d.id}>{d.full_name}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : (<Form.Item
                                name="user"
                                label="User" rules={[{ required: true }]}>
                                <Select
                                    allowClear >
                                    <Select.Option>-</Select.Option>
                                     {store.userData.role === "super_admin" && selectedUser == null? ['-'] : selectedUser.map(d => <Select.Option value={d.id}>{d.full_name}</Select.Option>)}
                                </Select>
                            </Form.Item>);
                        }}
                    </Form.Item> 
                    }
                    <Form.Item name="relasi" label="Relasi" rules={[{ required: true }]} >
                        <Select
                            allowClear
                            onChange={(value) => {
                                if (value !== 'langsung') {
                                    form.setFieldsValue({
                                        kpi_related: '',
                                        kpi_related_id : ''
                                    })
                                } 
                            }}
                        >
                            <Select.Option>Pilih Relasi</Select.Option>
                            <Select.Option value={'langsung'}>Langsung</Select.Option>
                            <Select.Option value={'tidak langsung'}>Tidak Langsung</Select.Option>
                            <Select.Option value={'tidak berhubungan'}>Tidak Berhubungan</Select.Option>

                        </Select>

                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === 'langsung' ? (
                                <Form.Item
                                    label="KPI Hierarchy" name="kpi_hierarki" rules={[{ required: true }]}>
                                    <Select
                                        onChange={(value) => {
                                            setKPIChoosen(value)
                                            form.setFieldsValue({
                                                kpi_related: '',
                                                kpi_related_id : ''
                                            })
                                        }}
                                        allowClear >
                                        <Select.Option value={"corporate"}>KPI Corporate</Select.Option>
                                        <Select.Option value={"division"}>KPI Division</Select.Option>
                                        <Select.Option value={"department"}>KPI Department</Select.Option>
                                    </Select>
                                </Form.Item>
                            ) : null;
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === 'langsung' ? (
                                <Form.Item
                                    name="kpi_related"
                                    id="kpirelated"
                                    label="KPI Related" rules={[{ required: true }]}>
                                    <Select
                                        onChange={(value) => {
                                            loadRelated(value)
                                        }}
                                        allowClear
                                        value={state.kpi_related} >
                                        {chooseKPI.map(c => <Select.Option value={c.id}>{c.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : null;
                        }}
                    </Form.Item>
                   <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => { 
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="plan" label="Individual Perfomance Plan:">
                                    <Input disabled={true} />
                                </Form.Item>
                            ) : (
                                    <Form.Item name="plan" label="Individual Perfomance Plan:" rules={[{ required: true }]}>
                                        <Input/>
                                    </Form.Item> )
                        }}
                    </Form.Item>

                    
                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="type" label="Type" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select Type"
                                        onChange={(value) => {
                                            loadWeight(value)
                                        }}
                                        disabled={true}
                                    >
                                        {[
                                            'Routine',
                                            'Project',
                                            'Others',
                                        ].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                                    </Select>
                                </Form.Item>
                            ) : (
                                <Form.Item name="type" label="Type" rules={[{ required: true }]}>
                                <Select
                                    placeholder="Select Type"
                                    onChange={(value) => {
                                        loadWeight(value)
                                    }}
                                    allowClear
                                >
                                    <Select.Option></Select.Option>
                                    {[
                                        'Routine',
                                        'Project',
                                        'Others',
                                    ].map(d => <Select.Option value={d}>{d}</Select.Option>)}
                                </Select>
                            </Form.Item>
                            )
                        }}
                        </Form.Item>

                        <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="weight" label="Weight%" rules={[{ type: "number"}]}>
                                    <InputNumber style={{ width: '100%' }} disabled={true}/>
                                </Form.Item>) : (
                                    <Form.Item name="weight" label="Weight%" rules={[{ type: "number", required: true, max: state.max, min: state.min }]}>
                                    <InputNumber style={{ width: '100%' }} />
                                </Form.Item>)
                        }}
                    </Form.Item>

                    <Form.Item name="weightHidden" hidden={true}>
                        <Input />
                    </Form.Item>
                    <Form.Item name="kpi_related_id" hidden={true}>
                        <Input />
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}>
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === 'langsung' ? (
                                <Form.Item name="kpi_type" label="KPI Formula" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select KPI Formula"
                                        disabled={true}
                                        onChange={
                                            (e) => {
                                                form.setFieldsValue({
                                                    target_mid_years : '',
                                                    target_one_years : ''
                                                })
                                                store.formula.filterData = _.filter(store.formula.data, i => i.formula_key === String(form.getFieldValue('kpi_type')))
                                            }
                                        } >
                                        <Select.Option>-</Select.Option>
                                        {(_.uniqBy(store.formula.data, e => e.formula_key) || ["-"]).map(d => <Select.Option
                                            value={d.formula_key}>{d.formula_key == 'month' || d.formula_key == 'grade'? d.formula_key : d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item> ) : <Form.Item name="kpi_type" label="KPI Formula" rules={[{ required: true }]}>
                                    <Select
                                        placeholder="Select KPI Formula"
                                        allowClear
                                        onChange={
                                            (e) => {
                                                form.setFieldsValue({
                                                    target_mid_years : '',
                                                    target_one_years : ''
                                                })
                                                store.formula.filterData = _.filter(store.formula.data, i => i.formula_key === String(form.getFieldValue('kpi_type')))
                                            }
                                        }
                                    >
                                        <Select.Option>-</Select.Option>
                                        {(_.uniqBy(store.formula.data, e => e.formula_key) || ["-"]).map(d => <Select.Option
                                            value={d.formula_key}>{d.formula_key == 'month' || d.formula_key == 'grade'? d.formula_key : d.name}</Select.Option>)}
                                    </Select>
                                </Form.Item>  }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}
                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') !== 'langsung') {
                                console.log("only month")
                               return <div>
                            <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    onChange={(value) => {
                                        target_mid_years = value
                                        changeMonth(value)
                                    }}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                            <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                <Select
                                    placeholder="-"
                                    allowClear
                                    onChange={(value) => {
                                        target_one_years = value
                                        changeMonth(value)
                                    }}
                                >
                                    <Select.Option>-</Select.Option>
                                    {(_.filter(store.formula.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                </Select>
                            </Form.Item>
                        </div>
                                
                            } else if ( getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') === "langsung" && KPIChoosen == "division") {
                                console.log("month and langsung")
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                            } else if(getFieldValue('kpi_type') === 'month' && getFieldValue('relasi') === "langsung" && KPIChoosen == "corporate") {
                                console.log("month, corporate, langsung")
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            disabled={true}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "month" && i.name !== 'Not Done') || ["-"]).map(d => <Select.Option key={d.id} value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>
                            }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}
                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') === "langsung"  && KPIChoosen == "corporate") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') !== "langsung") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}}
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'actvstarget' && getFieldValue('relasi') === "langsung" && KPIChoosen == "division") {
                                return <div> 
                                    <Form.Item name="target_mid_years" label="Target Mid Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :">
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                            </div>
                            }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') === "langsung" && KPIChoosen === "division") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') !== "langsung") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                </div>
                            } else if (getFieldValue('kpi_type') === 'targetvsact' && getFieldValue('relasi') === "langsung" && KPIChoosen === "corporate") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} />
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Year :" rules={[{ required: true }]}>
                                        <InputNumber formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} 
                                            parser={(value) => value.replace(/\$\s?|(,*)/g, "")} style={{width:'100%'}} disabled={true} />
                                    </Form.Item>
                                </div>
                            } 
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') === "langsung" && KPIChoosen === "division") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            disabled={true}>
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            disabled={true} >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                             } else if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') !== "langsung") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_mid_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            allowClear
                                            onChange={(value) => {
                                                target_one_years = value
                                                changeMonth(value)
                                            }}
                                        >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                             } else if (getFieldValue('kpi_type') === 'grade' && getFieldValue('relasi') === "langsung" && KPIChoosen === "corporate") {
                                return <div>
                                    <Form.Item name="target_mid_years" label="Target Mid Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-">
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                    <Form.Item name="target_one_years" label="Target One Years" rules={[{ required: true }]}>
                                        <Select
                                            placeholder="-"
                                            disabled={true} >
                                            <Select.Option>-</Select.Option>
                                            {(_.filter(store.formula.data, i => i.formula_key === "grade") || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                </div>  
                             }
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => (prevValues.kpi_type !== currentValues.kpi_type) || (prevValues.relasi !== currentValues.relasi)}

                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('kpi_type') ? null : <div>
                                <Form.Item name="target_mid_years" label="Target Mid Year :">
                                    <Input disabled={true}/>
                                </Form.Item>
                                <Form.Item name="target_one_years" label="Target One Year :">
                                    <Input disabled={true}/>
                                </Form.Item>
                            </div> ;
                        }}
                    </Form.Item>

                    <Form.Item
                        noStyle
                        shouldUpdate={(prevValues, currentValues) => prevValues.relasi !== currentValues.relasi}
                    >
                        {({ getFieldValue }) => {
                            return getFieldValue('relasi') === "langsung" && getFieldValue('isEdit') ? (
                                <Form.Item name="due_date" label="Due Date" >
                                    <Input type="date" disabled={true}/>
                                </Form.Item>) : (
                                    <Form.Item name="due_date" label="Due Date" rules={[{ required: true }]}>
                                    <Input type="date" />
                                </Form.Item>)
                        }}
                    </Form.Item>
                </Form>
            </div>
        </Modal>
    }


});

export default InputKPIGol45Staff;