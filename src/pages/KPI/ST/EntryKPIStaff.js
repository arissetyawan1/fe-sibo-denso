import React, { Component } from 'react';
import { inject, observer } from "mobx-react";
import {
  Row,
  Col,
  Button,
  Avatar,
  Typography,
  Statistic,
  Card,
  Empty,
  PageHeader,
  List,
  Divider,
  Dropdown,
  Table,
  Menu,
} from "antd";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useHistory,
  useParams,
  useRouteMatch
} from "react-router-dom";
import {LINKS} from "../../../routes";
import {createUseStyles} from "react-jss";
import {ArrowLeftOutlined} from "@ant-design/icons"

const {Meta} = Card;
const useStyle = createUseStyles({
    cardBodyStaff: {
        background: "#fff",
        paddingTop: 30,
        paddingBottom: 20,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        boxShadow: '0 2px 5px 0 rgba(60, 66, 87, 0.12), 0 1px 1px 0 rgba(0, 0, 0, 0.12)'
    }, cardBodyStaffHovered: {"&:hover": `background: #000;`},

})
const KPIStaff = observer((props) => {
  const {push} = useHistory();
  const history = useHistory();
  const classes = useStyle();
  const params = useParams();
  const {Text} = Typography;

  {
    return (
      <div>
        <PageHeader title={<Button onClick={() => {window.history.back()}} icon={<ArrowLeftOutlined />} type="primary">Back</Button>} />
        <Row gutter={[8, 8]} style={{justifyContent:"center"}}>
              <Col span={8}>
                  <Link to={LINKS["ENTRY STAFF GOLONGAN 1-3"] + "/" + params.id_divisi + "/" + params.id_depart}>
                    <Card className={['small-shadow', 'no-padding', classes.cardBodyStaffHovered]}
                          hoverable
                          bordered={false}
                          bodyStyle={{padding: 0}}
                          style={{textAlign: "center", padding: "30px 22px 4px 22px", background: '#ed1f24' }}>
                        <Meta
                            className={[classes.cardBodyStaff,]}
                            // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<Text strong style={{margin: 0}}>KPI Staff</Text>}
                            description={"Gol I-III"}
                        />
                    </Card>
                  </Link>
              </Col>
              <Col span={1} />
              <Col span={8}>
                  <Link to={LINKS["ENTRY STAFF GOLONGAN 4-5"] + "/" + params.id_divisi + "/" + params.id_depart}>
                    <Card className={['small-shadow', 'no-padding', classes.cardBodyStaffHovered]}
                          hoverable
                          bordered={false}
                          bodyStyle={{padding: 0}}
                          style={{textAlign: "center", padding: "30px 22px 4px 22px", background: '#ed1f24' }}>
                        <Meta
                            className={[classes.cardBodyStaff,]}
                            // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<Text strong style={{margin: 0}}>KPI Staff</Text>}
                            description={"Gol IV-V"}
                        />
                    </Card>
                  </Link>
              </Col>
        </Row>
      </div>    
    );
  }
  
})

export default KPIStaff;