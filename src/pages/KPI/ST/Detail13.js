
import React, { Component, useState, Fragment, useEffect } from 'react';
import 'antd/dist/antd.css';
import axios from 'axios';
import { Card, Row, Col, Progress, DatePicker, Input, InputNumber, Table, Menu, PageHeader, Button, Icon, message, Popconfirm, Form, Dropdown, Modal, Empty, Typography, Select, Spin } from 'antd';
import { ArrowLeftOutlined, DownloadOutlined, CheckOutlined, ClockCircleOutlined, CloseOutlined, FileProtectOutlined } from "@ant-design/icons";
import * as moment from "moment";
import * as _ from 'lodash';
import { useStore } from "../../../utils/useStores";
import { blue } from '@material-ui/core/colors';
import { useParams } from 'react-router-dom';

const originData = []


let selectedUser
let positionCheck
const { Option } = Select;

export const StaffDetail13 = (() => {
    const [form] = Form.useForm();
    const [data, setData] = useState(originData);
    const [prevData, setPrevData] = useState({});
    const [selectedDataUser, setSelectedDataUser] = useState(null);
    const [yearStaff, setYearStaff] = useState("");
    const store = useStore();
    const params = useParams();

    useEffect(() => {
        loadData();
    }, []);

    let newData
    let user = store.userData.id
    let role = store.userData.role
    const loadData = async () => {
        await store.user.getAll();
        return store.kpiMaster.getStaffs({
            departmentId: params.id,
            include_progress: 'true',
            level: 'staff-1-3',
            status: "Accepted",
            year: yearStaff
        })
            .then(res => {
                store.kpiMaster.staffsUser = res.body.data
                // newData = res.body.data
                // dummyData()

                newData = store.kpiMaster.staffs.map((x, i) => {

                    return {
                        ...store.kpiMaster.staffs[i],
                        // id: newData[i],
                        achievement: _.get(store.kpiMaster.staffs[i], 'progress[0].progress'),
                        key: i + 1
                    }
                })
            }).then(() => {
                setData(newData)
            })
    }

    positionCheck = store.user.filterData.map(r => {
        return r.position_user
    })

    const approveOrReject = (val) => (<Menu style={{backgroundColor:'white'}} onClick={(value) => { console.log(value, "whata re u")}}>
        <Menu.Item key="1">Approve KPI</Menu.Item>
        <Menu.Item key="2">Reject KPI</Menu.Item>
    </Menu>)

    const approvalCheckButton = (value) => {
        if (value.status === "accepted" && role !== "super_admin") {
            return <Button icon={<CheckOutlined />}>Accepted</Button>
        }else if( value.status === "rejected") {
            return <Button icon={<CloseOutlined />}>Rejected</Button>
        }else if (value.status === "approval"&& role === "head" && value.user_approve !== user) {
            return <Button icon={<ClockCircleOutlined />} />
        }else if (value.status === "accepted" && role === "super_admin") {
            return <Button icon={<CheckOutlined />} />
        }else if(value.status === "approval" && role === "super_admin") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }else if (role === "head" && value.status === "approval") {
            return <Button icon={<Dropdown overlay={approveOrReject(value.id)}><FileProtectOutlined /></Dropdown> } />
        }
    }

    const columns = [
        {
            title: 'Staff',
            dataIndex: 'staff',
            render: (text) => <span>{text.full_name}</span>

        },
        {
            title: 'KPI Rincian Tugas',
            dataIndex: 'name',
            editable: false,
        },
        {
            title: "Control Check Point",
            dataIndex: 'data',
            editable: false,
            render: (text) => <span>{text.control_check_point || "-"}</span>
        },
        {
            title: 'Action',
            fixed: 'right',
            dataIndex: 'operation',
            render: (data, record) => {
                return [
                    <Button onClick={() => {save('Done', record)}} icon={<CheckOutlined />}>
                    Done
                </Button>,
                <Button style={{margin:'2px 8px'}} onClick={() => {save('Not Done', record)}} icon={<CloseOutlined />}>Not Done</Button>,
                approvalCheckButton(record)
                ]
            },
        },
    ];

    const cancel = (record) => {
        form.setFieldsValue({
            ...record,
        });
    };

    const save = async (key, val) => {
        const sendData = {
            kpi_master_id: val.id,
            progress: key,
            data: {
                status: "Achieved"
            },
            status: 'approval'
        };
        if(role === "super_admin") {
            sendData.status = 'accepted'
        }

        const log = {
            user_id: user,
            event_name: "Update KPI Staff 1-3",
            data: {
                location: {
                    pathname: "/app/Kpi/Departments",
                    search: "",
                    hash: "",
                    key: role,
                },
                action: "PUSH"
            }
        }

            store.kpiProgress.create(sendData)
                .then(res => {
                    store.log.createData(log)
                    message.success('KPI Updated!');
                    return loadData();
                })
                .catch(err => {
                    message.error('Error on updating KPI!');
                })
    };
    
    async function setNewYear(params) {
        if (params) {
          await loadData();
        }
      }

      async function exportExcelStaff(param) {
        if (param) {
          await store.kpiMaster.getExportStaff({ departmentId: params.id, include_progress: 'true', year: yearStaff }).then((res) => {
            message.success("KPI Exported!");
          })
        } else {
          await store.kpiMaster.getExportStaff({ departmentId: params.id, include_progress: 'true'}).then((res) => {
            message.success("KPI Exported!");
          })
        }
      }


    return <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
      <Button type="primary" style={{width:85}}
          onClick={() => { window.history.back(); }}>
        <ArrowLeftOutlined /> Back
    </Button> <br />
        <PageHeader
          style={{ padding: '10px 0px' }}
            title={[<DatePicker picker="year" onChange={(value)=> {setYearStaff(moment(value).format('YYYY'))}}/>,
                <Button type="primary" style={{ marginLeft: 5 }} onClick={() => {setNewYear(yearStaff)}}>
          Go
        </Button>]}
          subTitle={((store.userData.role === "super_admin") || (store.userData.role === "BOD")) &&
          <Select
              style={{ width: store.ui.mediaQuery.isMobile? 150 : 220 }}
              className={'select-'}
              bordered={true}
              onChange={(value) => {
                  selectedUser = value
                  setSelectedDataUser(_.filter(data, i => i.user_id === value))
              }} defaultValue="Select User">
              <Option value={''}>-</Option>
              {_.filter(store.user.data, i => i?.position_user !== null && i?.position?.organization?.id === params.id).map(r => {
                  return <Option value={r.id}>{r.full_name}</Option>
              })}
          </Select>}
          extra={[
            <Button onClick={() => {
              exportExcelStaff(yearStaff)
            }}>
              <DownloadOutlined /> Download
            </Button>
          ]}
        />
        
        <Spin spinning={store.kpiMaster.isLoading && store.formula.isLoading}>
            <Form form={form} component={false}>
                {((store.userData.role === "super_admin") || (store.userData.role === "BOD")) &&
                    <Table
                        bordered
                        dataSource={selectedUser == null ? data : selectedDataUser}
                        columns={columns}
                        rowClassName="editable-row"
                        pagination={{
                            onChange: cancel,
                        }}
                    />
                    }
                {store.userData.role !== "super_admin" && store.userData.role !== "BOD" &&
                    <Table
                        bordered
                        dataSource={_.filter(data, i => i.user_id === store.userData.id)}
                        columns={columns}
                        rowClassName="editable-row"
                        pagination={{
                            onChange: cancel,
                        }}
                    />
                }
            </Form>
        </Spin>
    </div>
});