import React, {Component, useEffect, useState} from "react";
import { Col, Row, CardBody, Card, DropdownItem, ButtonGroup, ButtonDropdown,DropdownToggle,DropdownMenu } from 'reactstrap';
import { Link } from "react-router-dom";
import { Progress } from 'antd';
import {inject, observer} from "mobx-react";
import * as _ from 'lodash';
import {LINKS} from "../../routes";
import {useStore} from "../../utils/useStores";


let progressDivisi
let progressDepartment
export  const DepartEntryKPI = observer(() => {
    const store = useStore();
    const [dropdownOpen, setDropdownOpen] = useState(new Array(19).fill(false));
    const [selectedDivisionId, setSelectedDivisionId] = useState('');

    useEffect(() => {
    loadData()
        }, [])

   function loadData() {

        return store.division.getAll()
           // .finally(() => this.forceUpdate()) 
    }

    function toggle(i) {
        const newArray = dropdownOpen.map((element, index) => { return (index === i ? !element : false); });
        setDropdownOpen(newArray);
    }

    function loadDepartment(divId) {
        store.department.divisionId = divId;
        store.department.getDepartmentByDivision();
    }

        return (
            <div className="animated fadeIn"  style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
                <h3>Department</h3>
                {_.chunk(store.division.data, 4).map(d => {
                     
                     return (
                        <Row justify="center">
                            {d.map(c => {
                                if(c.kpi_master_division.length > 0){
                                    progressDivisi= _.sumBy(c.kpi_master_division, function (day) {
                                         let achievement =  day.progress.length > 0 ?  parseInt(day.progress[0].progress) :0
                                         let sumAchievement  = parseInt(achievement)
                                         return sumAchievement/c.kpi_master_division.length
                                     });
                                     
                                 //     
                                 }else{
                                     progressDivisi=0
                                 }
                                return (
                                    <Col span={store.ui.mediaQuery.isTablet || store.ui.mediaQuery.isDesktop ? 4 : ''} lg={store.ui.mediaQuery.isMobile ? 3 : ''} md={store.ui.mediaQuery.isMobile ? 6:''} className="d-sm-inline-block" outline="#2f353a">
                                        <Card
                                            onClick={() => {
                                                setSelectedDivisionId(c.id)
                                                loadDepartment(c.id);
                                                loadData();

                                            }}
                                            style={{
                                                marginBottom : 20,
                                                border: selectedDivisionId === c.id ? '2px solid #43A047' : ''
                                            }}>
                                            <CardBody style={{paddingTop:30, paddingBottom:30}}>
                                                <strong>{c.name}</strong>
                                                {/* <br></br>
                                                <Progress strokeColor={{ '0%': '#108ee9','100%': '#87d068',}} percent={parseInt(progressDivisi)} /> */}
                                            </CardBody> 
                                        </Card>
                                    </Col>
                                )
                            })}
                        </Row>
                    )
                })}
                <hr/>
                {!selectedDivisionId && (
                    <div style={{
                        height: 150,
                        textAlign: 'center'
                    }}>
                        Select division from above!
                    </div>
                )}
                {(selectedDivisionId && !_.size(store.department.dataDepart)) && (
                    <div style={{
                        height: 150,
                        textAlign: 'center'
                    }}>
                        This division dont have department yet!
                    </div>
                )}
                {selectedDivisionId && (
                    <React.Fragment>
                        {_.chunk(store.department.dataDepart, 4).map(d => {
                            return (
                                <Row justify="center">
                                    {d.map(c => {
                                         if(c.kpi_master_department.length > 0){
                                            progressDepartment= _.sumBy(c.kpi_master_department, function (day) {
                                                 let achievement = day.progress.length > 0 ?  parseInt(day.progress[0].progress) :0
                                                 let sumAchievement  = parseInt(achievement)
                                                 return sumAchievement/c.kpi_master_department.length
                                             });
                                             console.log(progressDepartment, "progressDepartment")
                                             
                                         //     
                                         }else{
                                             progressDepartment=0
                                         }
                                        return (
                                            <Col span={store.ui.mediaQuery.isTablet || store.ui.mediaQuery.isDesktop ? 4 : ''} lg={store.ui.mediaQuery.isMobile ? 3 : ''} md={store.ui.mediaQuery.isMobile ? 6:''}  className=" d-sm-inline-block" outline="#2f353a">
                                                <Link to={LINKS["ENTRY DEPARTMENT"] + "/" + c.organization_tree_id + "/" + c.id}>
                                                    <Card style={{
                                                            marginBottom : 20}}
                                                            >
                                                        <CardBody style={{paddingTop:30, paddingBottom:30}}>
                                                            <strong>{c.name}</strong>
                                                            {/* <br></br>
                                                            <Progress
                                                                strokeColor={{'0%': '#108ee9', '100%': '#87d068',}}
                                                                percent={parseInt(progressDepartment)}/> */}
                                                        </CardBody>
                                                    </Card>
                                                </Link>
                                            </Col>
                                        )
                                    })}
                                </Row>
                            )
                        })}
                    </React.Fragment>
                )}
            </div>
)


})
