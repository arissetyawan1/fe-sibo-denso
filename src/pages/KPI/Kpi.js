import React, { useEffect, useState } from "react";
import {
    Row,
    Col,
    Avatar,
    Button,
    Typography,
    Statistic,
    Card,
    Empty,
    PageHeader,
    List,
    Divider,
    Dropdown,
    Table,
    Menu,
    Spin,
    Progress
} from "antd";
import { ArrowRightOutlined, FilterOutlined, PlusOutlined, UploadOutlined } from "@ant-design/icons";
import {
    Link,
    useHistory,
    useRouteMatch,
    BrowserRouter as Router,
    Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import * as _ from 'lodash';
// import { stripLeadingSlash } from "history/PathUtils";


const { Meta } = Card
export const Kpi = observer((props) => {
    const store = useStore();
    const { Text } = Typography;
    const [state, setState] = useState({
        data: '',
    })
    const [progress, setProgress] = useState(0)
    const history = useHistory();
    const [dropdownOpen, setDropdownOpen] = useState(new Array(19).fill(false))
    
    useEffect(() => {
        loadData();
    }, [])
    
    async function loadData() {
        await store.user.getUserID(store.userData.id);
        await store.division.getAll();
        await store.department.getDepartmentByDivision(idDivision);
        await store.adjust.getAllAdjust();
        await store.kpiMaster.getCorporateKPI({include_progress: "true"});
        let department_id = _.get(store.user.dataProfil, `position_user.position.organization.id`)
        if (store.userData.role !== "super_admin" && store.userData.role !== "BOD") {
            await store.kpiMaster.getStaffs({departmentId : store.userData.role !== "head_division" ? department_id : '', status: 'Accepted', level:'staff-4-5', include_progress : 'true'})
        }
    }

    const roleUser = store.userData.role
    
    let progresskpi = _.sumBy(store.kpiMaster.corporateKPI, function (day) {
                let weight =  day.weight 
                let percentage = day.progress != 0? day.progress[0].percent : 0
                let sumAchievement = parseFloat(weight * percentage / 100)
                return sumAchievement
    })
    
    const departmentId = roleUser === "head_division" ? '' : _.get(store.user.dataProfil, `position_user.position.organization.id`);
    const idDivision =  roleUser === "head_division" ? _.get(store.user.dataProfil, 'position_user.position.id') : _.get(store.user.dataProfil, `position_user.position.organization.organization.id`);

    function linkDepartment (){
        if (roleUser !== "super_admin" && roleUser !== "BOD"){
            return LINKS.DEPARTMENTS +'/'+ departmentId
        } else if(roleUser === "head_division"){
            return LINKS.DEPARTMENTS
        }else {
            return LINKS.DEPARTMENTS   
        }
    }

    function linkStaff (){
        if (roleUser !== "super_admin" && roleUser !== "BOD" && roleUser !== "head_division") {
            return LINKS["ENTRY DETAIL STAFF"] +'/'+ departmentId
        } else {
            return LINKS.STAFF
        }
    }

    function linkEntryStaff() {
        if (roleUser !== "super_admin" && roleUser !== "BOD" && roleUser !== "head_division") {
            return LINKS["ENTRY STAFF"] + "/" + idDivision + "/" + departmentId
        } else if ((roleUser === "super_admin") || (roleUser === "head_division")) {
            return LINKS["STAFF ENTRY KPI"];
        } else {
            return "";
        }
    }

    function linkEntryDepart() {
        if (roleUser !== "super_admin" && roleUser !== "BOD" && roleUser !== "head_division") {
            return LINKS["ENTRY DEPARTMENT"] + "/" + idDivision + "/" + departmentId
        } else if ((roleUser === "super_admin") || (roleUser === "head_division")) {
            return LINKS["ENTRY DEPARTMENT"];
        }
    }

    function linkDivision(){
        if ((roleUser === "head") || (roleUser === "head_division")){
            return LINKS.DIVISIONS +'/'+ idDivision
        } else{
            return LINKS.DIVISIONS
        }
    }

    function progressDivisi(idDivisi) {
        if (roleUser !== "super_admin" && roleUser !== "BOD") {
            let findDivisi = _.filter(store.division.data, { 'id': idDivisi });
            let result = findDivisi.map(c => {
                if (c.kpi_master_division.length > 0) {
                    let dataNilai =  _.sumBy(c.kpi_master_division, function (day) {
                        let achievement = day.progress.length > 0 ? day.weight : 0
                        let percent = day.progress.length > 0 ? day.progress[0].percent : 0
                         let sumAchievement = parseFloat(achievement * percent / 100)
                        return sumAchievement
                    });
                    return dataNilai;
                } else {
                    return 0;
                }
            });
            return <Row gutter={[16, 24]}>
            <Col span={24}>
                <Card title="KPI Division"  style={{ background: '#f6f9fc' }}
                    bordered={false} bodyStyle={{ padding: 12, border:false }} className={['small-shadow']}>
                    <Progress strokeColor={{ '0%': '#108ee9', '100%': '#87d068', }} percent={parseFloat(result).toFixed(1)} style={{ marginBottom: 12 }}/>
                    <Link to={linkDivision()}><Button style={{ padding: 0 }} type={'link'} >Report - Division<ArrowRightOutlined /></Button></Link>
                </Card>
            </Col>
        </Row>;
        }
    }

    function progressDepartment(idDepart) {
        if (roleUser !== "super_admin" && roleUser !== "BOD" && roleUser !== "head_division") {
            let findDepart = _.filter(store.department.dataDepart, { 'id': idDepart });
            let result = findDepart.map(d => {
                if (d.kpi_master_department.length > 0) {
                    let dataNilai = _.sumBy(d.kpi_master_department, function (day) {
                        let achievement = day.progress.length > 0 ? day.weight : 0
                        let percent = day.progress.length > 0 ? day.progress[0].percent : 0
                        let sumAchievement = parseFloat(achievement * percent / 100)
                        return sumAchievement
                     });
                    return dataNilai;
                } else {
                    return 0;
                }
            })

            return <Row gutter={[16, 24]}>
            <Col span={24}>
                <Card title="KPI Department" extra={((roleUser === "head") || (roleUser === "super_admin") )&& <a style={{color:'crimson'}} onClick={() => { history.push(linkEntryDepart()) }}> <PlusOutlined /> Entry </a>} style={{ background: '#f6f9fc' }}
                    bordered={false} bodyStyle={{ padding: 12, border:false }} className={['small-shadow']}>
                    <Progress strokeColor={{ '0%': '#108ee9', '100%': '#87d068', }} percent={parseFloat(result).toFixed(1)} style={{ marginBottom: 12 }}/>
                    <Link to={linkDepartment()}><Button style={{ padding: 0 }} type={'link'} >Report - Department<ArrowRightOutlined /></Button></Link>
                </Card>
            </Col>
        </Row>;
        }else if (roleUser === "head_division") {
            return <Col md={8} lg={8} xs={24} sm={24}>
            <Card bordered={false} title="KPI Department" bodyStyle={{ padding: 12 }} className={['small-shadow']} extra={<a style={{color:'crimson'}} onClick={() => { history.push(linkEntryDepart()) }}> <PlusOutlined /> Entry </a>} style={{ background: '#f6f9fc' }} >
                    <Link to={linkDepartment()}><Button style={{ padding: 0 }} type={'link'} >Report - Department<ArrowRightOutlined /></Button></Link>
                </Card>
            </Col>
        }
    }

    function progressStaff(idStaff) {
        if (roleUser !== "super_admin" && roleUser !== "BOD" && roleUser !== "head_division") {
            let findUserKPI = _.filter(store.kpiMaster.staffs, { 'user_id': idStaff });
            let dataNilaiArray = []
            let result = findUserKPI.map(e => {
                if (e.progress.length > 0) {
                    let sumData = [];
                    sumData.push(e);
                    let dataNilai = _.sumBy(sumData, function (day) {
                        let achievement = day.progress.length > 0 ? day.weight : 0
                        let percent = day.progress.length > 0 ? day.progress[0].percent : 0
                        let sumAchievement = parseFloat(achievement * percent / 100)
                        return sumAchievement
                    });
                    return dataNilaiArray.push(dataNilai);
                } else {
                    return 0;
                }
            });
            const nilai = _.sum(dataNilaiArray);

            return <Row gutter={[16, 24]}>
            <Col span={24}>
                <Card title="KPI Staff" extra={<a style={{color:'crimson'}} onClick={() => { history.push(linkEntryStaff()) }}> <PlusOutlined /> Entry </a>} style={{ background: '#f6f9fc' }}
                    bordered={false} bodyStyle={{ padding: 12, border:false }} className={['small-shadow']}>
                    <Progress strokeColor={{ '0%': '#108ee9', '100%': '#87d068', }} percent={parseFloat(nilai).toFixed(1)} style={{ marginBottom: 12 }}/>
                    <Link to={linkStaff()}><Button style={{ padding: 0 }} type={'link'} >Report - Staff<ArrowRightOutlined /></Button></Link>
                </Card>
            </Col>
        </Row>;
        }else if(roleUser === "head_division") {
            return <Col  md={8} lg={8} xs={24} sm={24}>
            <Card extra={<a onClick={() => {history.push(linkEntryStaff())}} style={{color:'crimson'}}> <PlusOutlined /> Entry</a>}
                title="KPI Staff" bordered={false} bodyStyle={{ padding: 12 }} className={['small-shadow']} style={{ background: '#f6f9fc' }} >
                <Link to={linkStaff()}><Button style={{ padding: 0 }} type={'link'} >Report - Staff<ArrowRightOutlined /></Button></Link>
            </Card>
        </Col>
        }
    }

    return (
        <div className="site-card-wrapper">
            <PageHeader
                className={'card-page-header'}
                title={"Key Performance Index"}
                subTitle={<span>{store?.adjust?.data ? "Data KPI from year " +  _.filter(store?.adjust?.data, i => i.menu_name == "kpi")?.map(d => {
                    return d?.year
                }) : "-"}</span>}
            />

            <div className={'cariparkir-container'}>
                <Spin spinning={store.kpiMaster.isLoading}>
                    <Row gutter={[16, 24]}>
                        <Col span={24}>
                            <Card title="KPI Corporate"
                                bordered={false} bodyStyle={{ padding: 12, border:false }} className={['small-shadow']} extra={roleUser === "super_admin" && <a style={{color:'crimson'}} onClick={() => { history.push(LINKS["ENTRY CORPORATE"])}}> <PlusOutlined /> Entry </a> } style={{ background: '#f6f9fc' }} >
                                <Progress strokeColor={{ '0%': '#108ee9', '100%': '#87d068', }} percent={parseFloat(progresskpi).toFixed(1)} style={{ marginBottom: 12 }} />
                                
                                <Link to={LINKS.CORPORATE}><Button style={{ padding: 0 }} type={'link'} >Report - Corporate<ArrowRightOutlined /></Button></Link>
                            </Card>
                        </Col>
                    </Row>
                    {/* Special for Head Only */}
                    {((roleUser === "head") || roleUser === "head_division") &&
                        <div>{progressDivisi(idDivision)}</div>
                    }
                    {/* Special for Head Division Only */}
                    {roleUser === "head_division" &&
                        <Row justify="center">
                            {progressDepartment(departmentId)}
                            <Col style={{margin:'0px 10px'}} />
                            {progressStaff(store.userData.id)}
                        </Row>
                    }
                    {/* Except SuperAdmin and BOD */}
                    { roleUser !== "super_admin" && roleUser !== "BOD" && roleUser !== "head_division" &&
                        <div>
                            {progressDepartment(departmentId)}
                            {progressStaff(store.userData.id)}
                        </div>
                    }
                    {/* Only For BOD and SuperAdmin */}
                    { roleUser !== "staff" && roleUser !== "analyst" && roleUser !== "head" && roleUser !== "head_division" &&
                        <Row gutter={[16, 24]}>
                            <Col span={24}>
                                <Row gutter={[8, 24]}>
                                    <Col md={8} lg={8} xs={24} sm={24}>
                                        <Card bordered={false} title="KPI Divisions" bodyStyle={{ padding: 12 }} className={['small-shadow']} extra={roleUser === "super_admin" && <a style={{color:'crimson'}} onClick={() => {history.push(LINKS["ENTRY DIVISI"])}}> <PlusOutlined />Entry </a>} style={{ background: '#f6f9fc' }} >
                                            <Link to={LINKS.DIVISIONS}><Button style={{ padding: 0 }} type={'link'} >Report - Divisions<ArrowRightOutlined /></Button></Link>
                                        </Card>
                                    </Col>
                                    <Col  md={8} lg={8} xs={24} sm={24}>
                                    <Card bordered={false} title="KPI Department" bodyStyle={{ padding: 12 }} className={['small-shadow']} extra={roleUser === "super_admin" && <a style={{color:'crimson'}} onClick={() => { history.push(linkEntryDepart()) }}> <PlusOutlined /> Entry </a>} style={{ background: '#f6f9fc' }} >
                                            <Link to={linkDepartment()}><Button style={{ padding: 0 }} type={'link'} >Report - Department<ArrowRightOutlined /></Button></Link>
                                        </Card>
                                    </Col>
                                    <Col  md={8} lg={8} xs={24} sm={24}>
                                        <Card title="KPI Staff" extra={roleUser === "super_admin" && <a onClick={() => {history.push(linkEntryStaff())}} style={{color:'crimson'}}> <PlusOutlined /> Entry</a>}
                                            bordered={false} bodyStyle={{ padding: 12 }} className={['small-shadow']} style={{ background: '#f6f9fc' }} >
                                            <Link to={linkStaff()}><Button style={{ padding: 0 }} type={'link'} >Report - Staff<ArrowRightOutlined /></Button></Link>
                                        </Card>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    }
                </Spin>
            </div>
        </div>
    );
});
