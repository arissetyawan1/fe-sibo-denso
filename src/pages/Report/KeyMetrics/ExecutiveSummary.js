import React, { Component, useState, useEffect } from 'react';
import Highlighter from 'react-highlight-words';
import 'antd/dist/antd.css';
import { Table, Menu, Input, Button, Icon, Dropdown, Progress, Card as CardReact} from 'antd';
import {
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    Button as ButtonBot,
    CardBody,
    CardColumns,
    CardHeader,
    CardLink,
    CardSubtitle,
    CardText,
    CardTitle,
    UncontrolledButtonDropdown
} from 'reactstrap';
import {Empty} from "antd";
import { Bar } from 'react-chartjs-2';
import { RoundedBarChart } from 'rounded-bar-chartjs'
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle, hexToRgba } from "@coreui/coreui/dist/js/coreui-utilities";
import { inject, observer } from "mobx-react-lite";
import { useStore } from "../../../utils/useStores";
import { observable } from "mobx";
import moment from "moment";
import { Legend, LineChart, Tooltip, XAxis, YAxis, Line, RadialBarChart, RadialBar, ResponsiveContainer } from "recharts";
import * as _ from "lodash";
import * as bb from 'bluebird';
import { useHistory } from "react-router-dom";

const line = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
    axisX: {
        gridThickness: 0,
        tickLength: 0,
        lineThickness: 0,
        labelFormatter: function () {
            return " ";
        }
    },
    axisY: {
        gridThickness: 0,
        tickLength: 0,
        lineThickness: 0,
        labelFormatter: function () {
            return " ";
        }
    },
    datasets: [
        {
            label: 'My First dataset',
            fill: false,
            lineTension: 0,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: 'rgba(75,192,192,1)',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: 'rgba(75,192,192,1)',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: 'rgba(75,192,192,1)',
            pointHoverBorderColor: 'rgba(220,220,220,1)',
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: [65, 59, 80, 81, 56, 55, 40],
        },
    ],

};

const data = [
    {
        key: '1',
        name: 'John Brown',
        age: 32,
        address: 'New York No. 1 Lake Park',
    },
    {
        key: '2',
        name: 'Joe Black',
        age: 42,
        address: 'London No. 1 Lake Park',
    },
    {
        key: '3',
        name: 'Jim Green',
        age: 32,
        address: 'Sidney No. 1 Lake Park',
    },
    {
        key: '4',
        name: 'Jim Red',
        age: 32,
        address: 'London No. 2 Lake Park',
    },
];

const options = {
    tooltips: {
        enabled: false,
        custom: CustomTooltips
    },

    maintainAspectRatio: false,

}

const labelMapping = {
    OL3: 'Target',
    actVsOl: 'Actual vs Target',
    actVsYL: 'Actual vs LY'
};

const colors = [
    '#e91e63',
    '#9c27b0',
    '#3f51b5',
    '#2196f3',
    '#00bcd4',
    '#009688'
];




export const ExecutiveSummary = (observer((props) => {
    let history = useHistory();
    const store = useStore();

    const [state, setState] = useState({
        isLoadingRevenue: true,
        isLoadingTraffic: true,
        isLoadingAVKT: true,
        isLoadingFRE: true,
        isLoadingAccident: true,
        isLoadingTrafficClass: true,
        isLoadingFinancial: true,
        isLoadingCashflow: true,
        isLoadingBalance: true,

        dataRevenue: '',
        dataTraffic: '',
        dataAVKT: '',
        dataFRE: '',
        dataAccident: '',
        dataTrafficClass: '',
        dataFinancial: '',
        dataCashflow: '',
        dataBalance: '',
    })

    const setData = key => res => {



        setState((prevState) => ({
            ...prevState,
            ['isLoading' + key]: false,
            ['data' + key]: res.body
        }));
    };


    useEffect(() => {
        const loadData = async () => {
            // await store.keymetric.getDailyRevenue().then(setData('Revenue'))
            // await store.keymetric.getAVKT().then(setData('AVKT'))
            // await store.keymetric.getFRE().then(setData('FRE'))
            // await store.keymetric.getAccident().then(setData('Accident'))
            // await store.keymetric.getClassification().then(setData('TrafficClass'))
            // await store.keymetric.getFinancialSummary().then(setData('Financial'))
            // await store.keymetric.getCashflow().then(setData('Cashflow'))
            // await store.keymetric.getBalanceSheet().then(setData('Balance'))
            // await store.keymetric.getDailyTraffic().then(setData('Traffic'))
        }
        loadData();
        // Promise.all([
        //     store.keymetric.getDailyRevenue().then(setData('Revenue')),
        //     store.keymetric.getDailyTraffic().then(setData('Traffic')),
        //     store.keymetric.getAVKT().then(setData('AVKT')),
        //     store.keymetric.getFRE().then(setData('FRE')),
        //     store.keymetric.getAccident().then(res => {
        //         setData('Accident')(res);
        //     }),
        //     store.keymetric.getClassification().then(setData('TrafficClass')),
        //     store.keymetric.getFinancialSummary().then(setData('Financial')),
        //     store.keymetric.getCashflow().then(setData('Cashflow')),
        //     store.keymetric.getBalanceSheet().then(setData('Balance')),
        // ])
    }, []);


    const getLabel = (k) => labelMapping[k] ? labelMapping[k] : _.startCase(k);

    let searchInput
    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    ref={node => {
                        searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),
        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),
        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => searchInput.select());
            }
        },
        render: text => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[state.searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        ),
    });

    const handleSearch = (selectedKeys, confirm) => {
        confirm();
        setState({ searchText: selectedKeys[0] });
    };

    const handleReset = clearFilters => {
        clearFilters();
        setState({ searchText: '' });
    };

    const generateCard = (props) => {
        return (
            <Card className={'card-group-row__card'}>
                <CardBody className='d-flex flex-column align-items-start flex-0'>
                    <div className="d-flex flex-row align-items-start flex-1 w-100 justify-content-between">
                        <div className="d-flex flex-1 ">
                            <p className="mb-0"><strong>{props.title}</strong>
                            </p>
                        </div>
                        <div>
                        </div>
                    </div>
                    <div className="flex">
                        <span className="h2 m-0">{props.mainNumber}<small className="text-muted"> / {props.secondaryNumber}</small>
                        </span>
                        <p className={'text-50 mt-2 mb-0 d-flex'}>
                            <small className="text-black">
                                {props.subtitle}
                            </small>
                        </p>
                    </div>
                </CardBody>
                <CardBody
                    className={'text-muted flex d-flex flex-column align-items-center justify-content-center'}>
                    <div className={'chart w-100'} style={{ height: 200, position: 'relative' }}>
                        {props.children}
                    </div>
                </CardBody>
            </Card>
        )
    }

    const generateRevenueChart = (props) => {
        if (!state.dataRevenue) {
            return [];
        }

        return new Array(12).fill(1)
            .map((n, i) => {
                return {
                    name: moment().month(i).format('MMM'),
                    ...Object.keys(state.dataRevenue.data || {}).reduce((all, cur) => {
                        if (cur === 'rawExcel') {
                            return all;
                        }
                        const label = getLabel(cur);
                        let value = state.dataRevenue.data[cur];
                        const data = Array.isArray(value) ? value : value.data;
                        all[label] = data[i];
                        return all;
                    }, {})
                }
            });
    }

    const generateChartData = (report) => {
        if (!report) {
            return [];
        }

        return new Array(12).fill(1)
            .map((n, i) => {

                return {
                    name: moment().month(i).format('MMM'),
                    ...Object.keys(report.data).reduce((all, cur) => {
                        if (cur === 'rawExcel') {
                            return all;
                        }
                        const label = getLabel(cur);
                        let value = report.data[cur];
                        const data = Array.isArray(value) ? value : value.data;
                        all[label] = data[i];
                        return all;
                    }, {})
                }
            });
    }
    const emptyChart = (title)=>{
        return (
            <div className={'col-lg-4 col-md-6 card-group-row__col'} style={{
              minHeight: 'calc(100vh-450px)'}}>
              <Card className={'shadow'} >
                  <CardBody className={'d-flex flex-row align-items-center flex-0'}>
                  <div className="flex">
                              <div className="d-flex flex-row align-items-start flex-1 w-100 justify-content-between">
                                  <div className="d-flex flex-1 ">
                                      <p className="mb-0"><strong>{title}</strong>
                                      </p>
                                  </div>
                              </div>
                              <span className="h2 m-0">
                                  0
                                  <small className="text-muted"> / 0</small>
                              </span>
                              <p className={'text-50 mt-2 mb-0 d-flex'}>
                                  <Empty />
                              </p>
                          </div>
                  </CardBody>
              </Card>
          </div>
          )
      }


      const renderRadialChart = (title, desc, actual, budget, ket) => {
        let percent = ((Math.abs(actual) / Math.abs(budget)) * 100).toFixed(2);
        return (
            <div className={'col-lg-4 col-md-6 card-group-row__col'}>
                <CardReact className={'shadow  border-0'}  style={{height: 380}}>
                    <CardBody className={'d-flex flex-row align-items-center flex-0'}>
                        <div className="flex">
                            <div className="d-flex flex-row align-items-start flex-1 w-100 justify-content-between">
                                <div className="d-flex flex-1">
                                    <p className="mb-0"><strong>{title}</strong><small style={{marginLeft:10}}>{ket}</small>
                                    </p>
                                </div>

                            </div>
                            {/* <span className="h2 m-0">
                                {actual}
                                <small className="text-muted"> / {budget}</small>
                            </span><br/> */}
                            <span className="h2 m-0">
                                {(actual).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}
                                <small className="text-muted"> / {(budget).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')}</small>
                            </span>
                            <p className={'text-50 mt-2 mb-0 d-flex'}>
                                <small className="text-black">
                                    {desc}
                                </small>
                            </p>
                        </div>

                    </CardBody>
                    <CardBody style={{display: 'flex', justifyContent: 'center'}}>
                        {/*<ResponsiveContainer width={"100%"} height={235}>*/}
                        {/*    <RadialBarChart*/}
                        {/*        cx={170}*/}
                        {/*        // cy={100}*/}
                        {/*        innerRadius={20} outerRadius={100}*/}
                        {/*        barSize={25}*/}

                        {/*        data={[*/}
                        {/*            {*/}
                        {/*                name: 'Actual',*/}
                        {/*                value: Math.abs(actual),*/}
                        {/*                fill: '#e91e63'*/}
                        {/*            },*/}
                        {/*            {*/}
                        {/*                name: 'Budget',*/}
                        {/*                value: Math.abs(budget),*/}
                        {/*                fill: '#9c27b0'*/}
                        {/*            }*/}
                        {/*        ]}>*/}
                        {/*        <RadialBar minAngle={15} label={{ position: 'insideStart', fill: '#fff' }} background clockWise={true} dataKey='value'/>*/}
                        {/*        <Legend*/}
                        {/*            margin={{ top: 20, left: 0, right: 0, bottom: 0 }}*/}
                        {/*            iconSize={10} width={120} height={140} layout='horizontal' verticalAlign='bottom' align="center" />*/}
                        {/*    </RadialBarChart>*/}
                        {/*</ResponsiveContainer>*/}
                        <Progress type={"dashboard"}
                                  format={p => `${percent}%`}
                                  percent={percent}/>
                    </CardBody>
                </CardReact>
            </div>
        )
    }

    const brandSuccess = getStyle('--success')
    const brandInfo = getStyle('--info')
    const brandDanger = getStyle('--danger')

    const datas = {
        config: {
            plugins: {
                ChartDataLabels: false
            }
        },
        labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May'],
        datasets: [
            {
                label: 'Income',
                backgroundColor: ' #00bcc2',
                borderColor: '#00bcc2',
                barThickness: 8,
                borderSkipped: 'top',
                data: [650, 340, 990, 477, 800]
            }, {
                label: 'Expense',
                backgroundColor: '#4aa2ee',
                borderColor: '#4aa2ee',
                barThickness: 8,
                borderSkipped: 'top',
                data: [220, 550, 700, 433, 300]
            }

        ],


    };
    const optionsBar = {
        layout: { padding: 0 },
        plugins: {
            datalabels: {
                opacity: 0
            }
        },
        title: {
            display: 10,
            fontSize: 12,
            fontColor: "rgba(54, 76, 102, 0.54)",
            position: "top",
            text: "GENERATED INCOME"
        },
        legend: {
            display: false
        },
        scales: {
            xAxes: [{
                gridLines: {
                    display: false,
                    offsetGridLines: false,
                    drawBorder: false,
                    drawOnChartArea: true,
                    drawTicks: true,

                },
                ticks: {
                    beginAtZero: true,
                    fontSize: 10

                },

            }],
            yAxes: [{
                gridLines: {
                    drawBorder: false,
                    offsetGridLines: false,
                    drawTicks: true,
                    drawOnChartArea: true,
                    borderDash: [2],
                    color: '#E3EBF6'
                },
                ticks: {
                    beginAtZero: true,
                    minor: {
                        fontSize: 10
                    },
                    autoSkip: true,
                    suggestedMin: 0,
                    suggestedMax: 1000,
                    stepSize: 250
                }
            }]
        },
        maintainAspectRatio: false,

    }

    const menu = (
        <Menu>
            <Menu.Item key="0">
                <a href="#">Last years</a>
            </Menu.Item>
            <Menu.Item key="1">
                <a href="#">2017</a>
            </Menu.Item>

        </Menu>
    );

    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
            width: '30%',
            ...getColumnSearchProps('name'),
        },
        {
            title: 'Age',
            dataIndex: 'age',
            key: 'age',
            width: '20%',
            ...getColumnSearchProps('age'),
        },
        {
            title: 'Address',
            dataIndex: 'address',
            key: 'address',
            ...getColumnSearchProps('address'),
        },
    ];

    let revenueChart = generateRevenueChart();
    let trafficChart = generateChartData(state.dataTraffic);
    let avktChart = generateChartData(state.dataAVKT);
    let freChart = generateChartData(state.dataFRE);



    return (
        <div>

            <div className={'page-section'}>
                <div className={'page-separator'}>
                    <div className={'page-separator__text'}>Overview</div>
                </div>

                <div className="row card-group-row mb-2  ">
            {state.dataRevenue ?
                renderRadialChart(
                    "Toll Revenue",
                    "YTD Toll Revenue",
                    _.sum(_.get(state.dataRevenue.data, `year1Data`, [])),
                    _.sum(_.get(state.dataRevenue.data, `OL3`, [])),
                    _.get(state.dataRevenue.data, `rawExcel[0].DAILY REVENUE`, []),
                )
                

                :
                emptyChart("Toll Revenue")
            }
            {state.dataTraffic ?
                renderRadialChart(
                    "Traffic",
                    "YTD Traffic",
                    _.sum(_.get(state.dataTraffic.data, `year1Data`, [])),
                    _.sum(_.get(state.dataTraffic.data, `OL3`, [])),
                    _.get(state.dataTraffic.data, `rawExcel[0].DAILY TRAFFIC VOLUME`, []),
                )
                :
                emptyChart("Traffic")
            }

            {state.dataAccident ?
                renderRadialChart(
                    "Accident",
                    "YTD Accident",
                    _.sum(
                        _.flattenDeep(Object.keys(state.dataAccident.data || {})
                            .filter(key => key.endsWith('Year1'))
                            .filter(key => Array.isArray(state.dataAccident.data[key]))
                            .map(key => state.dataAccident.data[key]))
                    ),
                    _.sum(
                        _.flattenDeep(Object.keys(state.dataAccident.data || {})
                            .filter(key => key.endsWith('Year2'))
                            .filter(key => Array.isArray(state.dataAccident.data[key]))
                            .map(key => state.dataAccident.data[key]))
                    ),
                    "Cases"
                )
                :
                emptyChart("Accident")
            }
        </div>
        <div className="row card-group-row mb-3 mt-3 ">
            {state.dataCashflow ?
                renderRadialChart(
                    "Opex",
                    "YTD Operating Expense",
                    _.get(state.dataCashflow.data, `operatingExpense.ytd.${_.get(state.dataCashflow, 'data.month')}'${_.get(state.dataCashflow, 'year')}`),
                    _.get(state.dataCashflow, 'data.operatingExpense.ytd.OL3'),
                    _.get(state.dataCashflow.data, `rawExcel[0].Account`, []),
                )
                :
                emptyChart("Opex")
            }
            {state.dataFinancial ?
                renderRadialChart(
                    "Net Profit",
                    "YTD Total Revenue",
                    _.get(state.dataFinancial.data, `totalRevenue.ytd.${_.get(state.dataFinancial, 'data.month')}'${_.get(state.dataFinancial, 'year')}`),
                    _.get(state.dataFinancial.data, `totalRevenue.ytd.OL3`),
                    _.get(state.dataFinancial.data, `rawExcel[0].Account`, []),
                )
                :
                emptyChart("Net Profit")
            }
            {state.dataBalance ?
                renderRadialChart(
                    "Cash Reserve",
                    "YTD Cash Equivalent",
                    _.get(state.dataBalance.data, `cashEquivalent.ytd.${_.get(state.dataBalance, 'data.month')}'${_.get(state.dataBalance, 'year')}`),
                    _.get(state.dataBalance.data, `cashEquivalent.ytd.OL3`),
                    _.get(state.dataFinancial.data, `rawExcel[0].Account`, []),
                )
                :
                emptyChart("Cash Reserve")
            }
        </div>

            </div>


        </div>
    )
}))


