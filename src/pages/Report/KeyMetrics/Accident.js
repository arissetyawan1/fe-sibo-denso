import React, { useEffect, useState } from "react";
import style from "../../../assets/css/fsum.module.css";
import "antd/dist/antd.css";
import { Table, Input, Button, Icon, Empty, Select, Collapse, PageHeader, DatePicker } from "antd";
import {
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import { ArrowLeftOutlined, CaretRightOutlined } from "@ant-design/icons";
import { inject, observer } from "mobx-react";
import moment from "moment";
import * as _ from "lodash";
import { useStore } from "../../../utils/useStores";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

import Chart from "react-google-charts";

const { Option } = Select;
const { Panel } = Collapse;

const data = [
  {
    years: "Kecelakaan Tunggal",
    month1: "87",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "87",
  },
  {
    years: "Kecelakaan Dengan Kendaraan Lain",
    month1: "31",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "31",
  },
];

const data10 = [
  {
    years: "Luka Berat",
    month1: "113",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "113",
  },
  {
    years: "Luka Ringan",
    month1: "31",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "31",
  },
  {
    years: "Meninggal Dunia",
    month1: "10",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "10",
  },
];

const data20 = [
  {
    years: "Kurang Antisipasi / Mengantuk / Over Speed",
    month1: "96",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "96",
  },
  {
    years: "Gangguan Roda",
    month1: "24",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "25",
  },
  {
    years: "Pecah Ban",
    month1: "22",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "22",
  },
  {
    years: "Rem Blong",
    month1: "0",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "0",
  },
  {
    years: "Hambatan Kelistrikan / Mesin",
    month1: "0",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "0",
  },
  {
    years: "Kelebihan Muatan",
    month1: "0",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "0",
  },
  {
    years: "Jalan Licin",
    month1: "0",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "0",
  },
  {
    years: "Menabrak Hambatan",
    month1: "0",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "0",
  },
  {
    years: "Menghindari Hambatan",
    month1: "0",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "0",
  },
  {
    years: "Lain-lain",
    month1: "0",
    month2: "-",
    month3: "-",
    month4: "-",
    month5: "-",
    month6: "-",
    month7: "-",
    month8: "-",
    month9: "-",
    month10: "-",
    month11: "-",
    month12: "-",
    total: "0",
  }
];

const Accident = observer((props) => {
  const store = useStore();
  const [state, setState] = useState({
    seacrhText: "",
    isLoaded: false,
    report: "",
    DataPie: [],
    selectedMonth: 0,
  });

  const [selectedMonth, setSelectedMonth] = useState(0);
  const [yearKey, setYearKey] = useState("");

  useEffect(() => {
    loadData();
  }, [selectedMonth]);

  async function loadData() {
    await store.adjust.getAllAdjust();
    await store.keymetric.getAccident(yearKey).then((res) => {
      console.log(res, "ini res");
      setState({
        report: res.body,
        isLoaded: true,
      });
    });
  }

  const onChange = (event) => {
    setSelectedMonth(event);
  };

  // function generateDataPie(labels, data) {
  //   let colum = [["Ket", "Data"]];
  //   let dataReturn = labels.map(function (x, i) {
  //     return [x, data[i]];
  //   });
  //   let data2 = colum.concat(dataReturn);
  //   console.log(data2, "eh whyy")
  //   return data2;
  // }

  const colours = ["#3366CC", "#DC3912", "#FF9900", "#109618", "#990099", "#0099C6", "#E88B6C", "#8F9965", "#4D608A", "#DB53F5"]

  function renderPie(title, labelsa, data) {
    return (
      <Col lg={12}>
        <Row style={{ marginTop: "4%" }}>
          <Col lg={12}>
            <Card>
              <CardBody>
                <Row>
                  <Col>
                    <CardTitle className="mb-0">{title}</CardTitle>
                    {/* <div className="small text-muted">{year}</div> */}
                  </Col>
                </Row>
                <div
                  className="chart-wrapper"
                  style={{ marginTop: 15 + "px"}}
                >
                  {state.isLoaded && (
                    <BarChart
                      width={900}
                      height={400}
                      data={data}
                      key={selectedMonth}
                      margin={{
                        top: 20,
                        right: 30,
                        left: 20,
                        bottom: 5,
                      }}>
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="name" />
                      <YAxis />
                      <Tooltip />
                      <Legend />
                      {labelsa.map((l, i) => <Bar stackId="a" dataKey={l} fill={colours[i]} />)}
                    </BarChart>
                  )}
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </Col>
    );
  }

  const emptyChart = () => {
    return (
      <Card>
        <CardHeader className={"d-flex align-items-center"}>
          <strong>Accident</strong>
        </CardHeader>
        <div
          style={{
            minHeight: "calc(100vh - 450px)",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Empty />
        </div>
      </Card>
    );
  };

  const year = _.get(state.report, "data.year1");
  const year2 = _.get(state.report, "data.year2");

  const renderKecTunggal = (index, year = 1) => (text, record, i) => {
    const key = ["kecelakaanTunggalYear" + year, "kecelakaanKrLainYear" + year];

    return _.get(state.report, `data.${key[i]}.${index}`);
  };

  const renderKondisi = (index, year = 1) => (text, record, i) => {
    const key = [
      "lukaBeratYear" + year,
      "lukaRinganYear" + year,
      "meninggalYear" + year,
    ];

    return _.get(state.report, `data.${key[i]}.${index}`);
  };

  const renderPenyebab = (index, year = 1) => (text, record, i) => {
    const key = [
      "mengantukYear" + year,
      "gangguanRodaYear" + year,
      "pecahBanYear" + year,
      "remBlongYear" + year,
      "hambatanKelistrikanYear" + year,
      "kelebihanMuatanYear" + year,
      "jalanLicinYear" + year,
      "menabrakHambatanYear" + year,
      "menghindariHambatanYear" + year,
      "lainLainYear" + year,
    ];

    return _.get(state.report, `data.${key[i]}.${index}`);
  };

  const totals = Object.keys(_.get(state.report, "data", {}))
    .filter((i) => ["year1", "year2"].indexOf(i) < 0)
    .reduce((all, i) => {
      const dt = _.get(state.report, "data." + i, []) || [];
      all[i] = dt.reduce((a, b) => a + b, 0);
      return all;
    }, {});

  const columnJumlahYear1 = [
    {
      title: "Jumlah Kecelakaan Tunggal & Kecelakaan dengan Kendaraan Lain",
      children: [
    {
      title: year,
      dataIndex: "years",
      key: "years",
      width: 275,
    },
    {
      title: "Jan",
      dataIndex: "month1",
      key: "month1",
      render: renderKecTunggal(0, 1),
    },
    {
      title: "Feb",
      dataIndex: "month2",
      key: "month2",
      render: renderKecTunggal(1, 1),
    },
    {
      title: "Mar",
      dataIndex: "month3",
      key: "month3",
      render: renderKecTunggal(2, 1),
    },
    {
      title: "Apr",
      dataIndex: "month4",
      key: "month4",
      render: renderKecTunggal(3, 1),
    },
    {
      title: "May",
      dataIndex: "month5",
      key: "month5",
      render: renderKecTunggal(4, 1),
    },
    {
      title: "Jun",
      dataIndex: "month6",
      key: "month6",
      render: renderKecTunggal(5, 1),
    },
    {
      title: "Jul",
      dataIndex: "month7",
      key: "month7",
      render: renderKecTunggal(6, 1),
    },
    {
      title: "Aug",
      dataIndex: "month8",
      key: "month8",
      render: renderKecTunggal(7, 1),
    },
    {
      title: "Sept",
      dataIndex: "month9",
      key: "month9",
      render: renderKecTunggal(8, 1),
    },
    {
      title: "Oct",
      dataIndex: "month10",
      key: "month10",
      render: renderKecTunggal(9, 1),
    },
    {
      title: "Nov",
      dataIndex: "month11",
      key: "month11",
      render: renderKecTunggal(10, 1),
    },
    {
      title: "Dec",
      dataIndex: "month12",
      key: "month12",
      render: renderKecTunggal(11, 1),
    },
    {
      title: "Total",
      dataIndex: "total",
      key: "total",
      render: (text, r, i) =>
        i === 0
          ? totals["kecelakaanTunggalYear1"]
          : totals["kecelakaanKrLainYear1"],
    },
    ]
  }
  ];

  const columnJumlahYear2 = [
        {
          title: year2,
          dataIndex: "years",
          key: "years",
          width: 275,
        },
        {
          title: "Jan",
          dataIndex: "month1",
          key: "month1",
          render: renderKecTunggal(0, 2),
        },
        {
          title: "Feb",
          dataIndex: "month2",
          key: "month2",
          render: renderKecTunggal(1, 2),
        },
        {
          title: "Mar",
          dataIndex: "month3",
          key: "month3",
          render: renderKecTunggal(2, 2),
        },
        {
          title: "Apr",
          dataIndex: "month4",
          key: "month4",
          render: renderKecTunggal(3, 2),
        },
        {
          title: "May",
          dataIndex: "month5",
          key: "month5",
          render: renderKecTunggal(4, 2),
        },
        {
          title: "Jun",
          dataIndex: "month6",
          key: "month6",
          render: renderKecTunggal(5, 2),
        },
        {
          title: "Jul",
          dataIndex: "month7",
          key: "month7",
          render: renderKecTunggal(6, 2),
        },
        {
          title: "Aug",
          dataIndex: "month8",
          key: "month8",
          render: renderKecTunggal(7, 2),
        },
        {
          title: "Sept",
          dataIndex: "month9",
          key: "month9",
          render: renderKecTunggal(8, 2),
        },
        {
          title: "Oct",
          dataIndex: "month10",
          key: "month10",
          render: renderKecTunggal(9, 2),
        },
        {
          title: "Nov",
          dataIndex: "month11",
          key: "month11",
          render: renderKecTunggal(10, 2),
        },
        {
          title: "Dec",
          dataIndex: "month12",
          key: "month12",
          render: renderKecTunggal(11, 2),
        },
        {
          title: "Total",
          dataIndex: "total",
          key: "total",
          render: (text, r, i) =>
            i === 0
              ? totals["kecelakaanTunggalYear2"]
              : totals["kecelakaanKrLainYear2"],
        },
  ];

  const columnKondisiYear1 = [
    {
      title: "Kondisi Korban",
      children: [
        {
          title: year,
          dataIndex: "years",
          key: "years",
          width: 275,
        },
        {
          title: "Jan",
          dataIndex: "month1",
          key: "month1",
          render: renderKondisi(0, 1),
        },
        {
          title: "Feb",
          dataIndex: "month2",
          key: "month2",
          render: renderKondisi(1, 1),
        },
        {
          title: "Mar",
          dataIndex: "month3",
          key: "month3",
          render: renderKondisi(2, 1),
        },
        {
          title: "Apr",
          dataIndex: "month4",
          key: "month4",
          render: renderKondisi(3, 1),
        },
        {
          title: "May",
          dataIndex: "month5",
          key: "month5",
          render: renderKondisi(4, 1),
        },
        {
          title: "Jun",
          dataIndex: "month6",
          key: "month6",
          render: renderKondisi(5, 1),
        },
        {
          title: "Jul",
          dataIndex: "month7",
          key: "month7",
          render: renderKondisi(6, 1),
        },
        {
          title: "Aug",
          dataIndex: "month8",
          key: "month8",
          render: renderKondisi(7, 1),
        },
        {
          title: "Sept",
          dataIndex: "month9",
          key: "month9",
          render: renderKondisi(8, 1),
        },
        {
          title: "Oct",
          dataIndex: "month10",
          key: "month10",
          render: renderKondisi(9, 1),
        },
        {
          title: "Nov",
          dataIndex: "month11",
          key: "month11",
          render: renderKondisi(10, 1),
        },
        {
          title: "Dec",
          dataIndex: "month12",
          key: "month12",
          render: renderKondisi(11, 1),
        },
        {
          title: "Total",
          dataIndex: "total",
          key: "total",
          render: (text, r, i) => {
            const key = ["lukaBeratYear1", "lukaRinganYear1", "meninggalYear1"];

            return totals[key[i]];
          },
        },
      ]
    }
  ];

  const columnKondisiYear2 = [
        {
          title: year2,
          dataIndex: "years",
          key: "years",
          width: 275,
        },
        {
          title: "Jan",
          dataIndex: "month1",
          key: "month1",
          render: renderKondisi(0, 2),
        },
        {
          title: "Feb",
          dataIndex: "month2",
          key: "month2",
          render: renderKondisi(1, 2),
        },
        {
          title: "Mar",
          dataIndex: "month3",
          key: "month3",
          render: renderKondisi(2, 2),
        },
        {
          title: "Apr",
          dataIndex: "month4",
          key: "month4",
          render: renderKondisi(3, 2),
        },
        {
          title: "May",
          dataIndex: "month5",
          key: "month5",
          render: renderKondisi(4, 2),
        },
        {
          title: "Jun",
          dataIndex: "month6",
          key: "month6",
          render: renderKondisi(5, 2),
        },
        {
          title: "Jul",
          dataIndex: "month7",
          key: "month7",
          render: renderKondisi(6, 2),
        },
        {
          title: "Aug",
          dataIndex: "month8",
          key: "month8",
          render: renderKondisi(7, 2),
        },
        {
          title: "Sept",
          dataIndex: "month9",
          key: "month9",
          render: renderKondisi(8, 2),
        },
        {
          title: "Oct",
          dataIndex: "month10",
          key: "month10",
          render: renderKondisi(9, 2),
        },
        {
          title: "Nov",
          dataIndex: "month11",
          key: "month11",
          render: renderKondisi(10, 2),
        },
        {
          title: "Dec",
          dataIndex: "month12",
          key: "month12",
          render: renderKondisi(11, 2),
        },
        {
          title: "Total",
          dataIndex: "total",
          key: "total",
          render: (text, r, i) => {
            const key = ["lukaBeratYear2", "lukaRinganYear2", "meninggalYear2"];

            return totals[key[i]];
          },
        },
  ];

  const columnPenyebabYear1 = [
    {
      title: "Penyebab Kecelakaan",
      children: [
        {
          title: year,
          dataIndex: "years",
          key: "years",
          width: 275,
        },
        {
          title: "Jan",
          dataIndex: "month1",
          key: "month1",
          render: renderPenyebab(0, 1),
        },
        {
          title: "Feb",
          dataIndex: "month2",
          key: "month2",
          render: renderPenyebab(1, 1),
        },
        {
          title: "Mar",
          dataIndex: "month3",
          key: "month3",
          render: renderPenyebab(2, 1),
        },
        {
          title: "Apr",
          dataIndex: "month4",
          key: "month4",
          render: renderPenyebab(3, 1),
        },
        {
          title: "May",
          dataIndex: "month5",
          key: "month5",
          render: renderPenyebab(4, 1),
        },
        {
          title: "Jun",
          dataIndex: "month6",
          key: "month6",
          render: renderPenyebab(5, 1),
        },
        {
          title: "Jul",
          dataIndex: "month7",
          key: "month7",
          render: renderPenyebab(6, 1),
        },
        {
          title: "Aug",
          dataIndex: "month8",
          key: "month8",
          render: renderPenyebab(7, 1),
        },
        {
          title: "Sept",
          dataIndex: "month9",
          key: "month9",
          render: renderPenyebab(8, 1),
        },
        {
          title: "Oct",
          dataIndex: "month10",
          key: "month10",
          render: renderPenyebab(9, 1),
        },
        {
          title: "Nov",
          dataIndex: "month11",
          key: "month11",
          render: renderPenyebab(10, 1),
        },
        {
          title: "Dec",
          dataIndex: "month12",
          key: "month12",
          render: renderPenyebab(11, 1),
        },
        {
          title: "Total",
          dataIndex: "total",
          key: "total",
          render: (text, r, i) => {
            const key = [
              "mengantukYear1",
              "gangguanRodaYear1",
              "pecahBanYear1",
              "remBlongYear1",
              "hambatanKelistrikanYear1",
              "kelebihanMuatanYear1",
              "jalanLicinYear1",
              "menabrakHambatanYear1",
              "menghindariHambatanYear1",
              "lainLainYear1"
            ];

            return totals[key[i]];
          },
        },
      ]
    }
  ];

  const columnPenyebabYear2 = [
        {
          title: year2,
          dataIndex: "years",
          key: "years",
          width: 275,
        },
        {
          title: "Jan",
          dataIndex: "month1",
          key: "month1",
          render: renderPenyebab(0, 2),
        },
        {
          title: "Feb",
          dataIndex: "month2",
          key: "month2",
          render: renderPenyebab(1, 2),
        },
        {
          title: "Mar",
          dataIndex: "month3",
          key: "month3",
          render: renderPenyebab(2, 2),
        },
        {
          title: "Apr",
          dataIndex: "month4",
          key: "month4",
          render: renderPenyebab(3, 2),
        },
        {
          title: "May",
          dataIndex: "month5",
          key: "month5",
          render: renderPenyebab(4, 2),
        },
        {
          title: "Jun",
          dataIndex: "month6",
          key: "month6",
          render: renderPenyebab(5, 2),
        },
        {
          title: "Jul",
          dataIndex: "month7",
          key: "month7",
          render: renderPenyebab(6, 2),
        },
        {
          title: "Aug",
          dataIndex: "month8",
          key: "month8",
          render: renderPenyebab(7, 2),
        },
        {
          title: "Sept",
          dataIndex: "month9",
          key: "month9",
          render: renderPenyebab(8, 2),
        },
        {
          title: "Oct",
          dataIndex: "month10",
          key: "month10",
          render: renderPenyebab(9, 2),
        },
        {
          title: "Nov",
          dataIndex: "month11",
          key: "month11",
          render: renderPenyebab(10, 2),
        },
        {
          title: "Dec",
          dataIndex: "month12",
          key: "month12",
          render: renderPenyebab(11, 2),
        },
        {
          title: "Total",
          dataIndex: "total",
          key: "total",
          render: (text, r, i) => {
            const key = [
              "mengantukYear2",
              "gangguanRodaYear2",
              "pecahBanYear2",
              "remBlongYear2",
              "hambatanKelistrikanYear2",
              "kelebihanMuatanYear2",
              "jalanLicinYear2",
              "menabrakHambatanYear2",
              "menghindariHambatanYear2",
              "lainLainYear2"
            ];

            return totals[key[i]];
          },
    },
  ];

  async function onClickedYear(years) {
    await store.keymetric.getAccident(years).then((res) => {
      console.log(res + "ini res");
      setState({
        report: res.body,
        isLoaded: true,
      });
    });
  }

  {
    return (
      <div className={"page-section " + state.report} style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}> 
        <Button type="primary"
          onClick={() => { window.history.back(); }}
          style={{ color: "#ffffff", width: 80, zIndex:999 }}>
          <ArrowLeftOutlined /> Back
        </Button>
        <PageHeader style={{padding:0, margin:'10px 0px'}} title={[<DatePicker picker="year" onChange={(value) => {setYearKey(moment(value).format('YYYY'))}} />,
          <Button style={{marginLeft:5}} type="primary" onClick={() => { onClickedYear(yearKey)}}>Go</Button>
        ]} />
        <div className={"page-separator"}>
          <div className={"page-separator__text"}>Overview Key Metric from year {yearKey ? yearKey : store?.adjust?.data ?  _.filter(store?.adjust?.data, i => i.menu_name == "key_metrics")?.map(d => {
                return d?.year
            }) : "-"}</div>
        </div>
          <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
            <Panel header={<strong>Accidents (Stack Chart)</strong>} key="1">
              <div>
                <Select
                  style={{ width: 270, marginLeft: 10 }}
                  className={"select-"}
                  bordered={true}
                  onChange={onChange}
                  defaultValue="All Month"
                >
                  <Option value={"0"}>All Month</Option>
                  <Option value={"1"}>Januari</Option>
                  <Option value={"2"}>Februari</Option>
                  <Option value={"3"}>Maret</Option>
                  <Option value={"4"}>April</Option>
                  <Option value={"5"}>Mei</Option>
                  <Option value={"6"}>Juni</Option>
                  <Option value={"7"}>Juli</Option>
                  <Option value={"8"}>Agustus</Option>
                  <Option value={"9"}>September</Option>
                  <Option value={"10"}>Oktober</Option>
                  <Option value={"11"}>November</Option>
                  <Option value={"12"}>Desember</Option>
              </Select>
            </div>
              <Row style={{ justifyContent: "center", padding: 1}}>
              {/* Render PIE CHART KECELAKAAN TUNGGAL */}
              {state.report
                ? selectedMonth == 0
                  ? renderPie(
                    "Jumlah Kecelakaan Tunggal & Kecelakaan dengan Kr. Lain",
                    ["Kecelakaan Tunggal", "Kecelakaan dengan Kr. Lain"],
                    [
                      {
                      "name" : year,
                      "Kecelakaan Tunggal": totals.kecelakaanTunggalYear1,
                      "Kecelakaan dengan Kr. Lain": totals.kecelakaanKrLainYear1
                      },
                      {
                        "name": year2,      
                        "Kecelakaan Tunggal": totals.kecelakaanTunggalYear2,
                        "Kecelakaan dengan Kr. Lain": totals.kecelakaanKrLainYear2
                      }
                    ])
                  : selectedMonth
                  ? renderPie(
                    "Jumlah Kecelakaan Tunggal & Kecelakaan dengan Kr. Lain",
                    ["Kecelakaan Tunggal", "Kecelakaan dengan Kr. Lain"],
                    [{
                      "name": year,
                      "Kecelakaan Tunggal":  _.get(state.report, `data.kecelakaanTunggalYear1[${selectedMonth-1}]`),
                      "Kecelakaan dengan Kr. Lain": _.get(state.report, `data.kecelakaanKrLainYear1[${selectedMonth-1}]`)
                    },
                    {
                      "name": year2,
                      "Kecelakaan Tunggal": _.get(state.report, `data.kecelakaanTunggalYear2[${selectedMonth-1}]`),
                      "Kecelakaan dengan Kr. Lain": _.get(state.report, `data.kecelakaanKrLainYear2[${selectedMonth-1}]`)
                    }]) : emptyChart()
                : emptyChart()}
              {/* Render KONDISI KORBAN */}
              {state.report
                ? selectedMonth == 0
                  ? renderPie(
                    "Kondisi Korban",
                    ["Luka Berat", "Luka Ringan", "Meninggal Dunia"],
                    [
                      {
                        "name": year,
                        "Luka Berat": totals.lukaBeratYear1,
                        "Luka Ringan": totals.lukaRinganYear1,
                        "Meninggal Dunia": totals.meninggalYear1
                      },
                      {
                        "name" : year2,
                        "Luka Berat": totals.lukaBeratYear2,
                        "Luka Ringan": totals.lukaRinganYear2,
                        "Meninggal Dunia": totals.meninggalYear2
                    }])
                  : selectedMonth
                  ? renderPie(
                      "Kondisi Korban",
                      ["Luka Berat", "Luka Ringan", "Meninggal Dunia"],
                      [{
                        "name": year,
                        "Luka Berat": _.get(state.report, `data.lukaBeratYear1[${selectedMonth-1}]`),
                        "Luka Ringan": _.get(state.report, `data.lukaRinganYear1[${selectedMonth-1}]`),
                        "Meninggal Dunia": _.get(state.report, `data.meninggalYear1[${selectedMonth-1}]`)
                      },
                      {
                        "name" : year2,
                        "Luka Berat":  _.get(state.report, `data.lukaBeratYear2[${selectedMonth-1}]`),
                        "Luka Ringan": _.get(state.report, `data.lukaRinganYear2[${selectedMonth-1}]`),
                        "Meninggal Dunia": _.get(state.report, `data.meninggalYear2[${selectedMonth-1}]`)
                    }])
                  : emptyChart()
                : emptyChart()}

              {/* Render PENYEBAB KECELAKAAN */}
              {state.report
                ? selectedMonth == 0
                  ? renderPie(
                    "Penyebab Kecelakaan",
                    ["Kurang Antisipasi / Mengantuk / Over Speed", "Gangguan Roda", "Pecah Ban", "Rem Blong", "Hambatan Kelistrikan / Mesin",
                      "Kelebihan Muatan", "Jalan Licin", "Menabrak Hambatan", "Menghindari Hambatan", "Lain-lain"],
                    [{
                      "name" : year,
                      "Kurang Antisipasi / Mengantuk / Over Speed": totals.mengantukYear1,
                      "Gangguan Roda": totals.gangguanRodaYear1,
                      "Pecah Ban": totals.pecahBanYear1,
                      "Rem Blong": totals.remBlongYear1,
                      "Hambatan Kelistrikan / Mesin": totals.hambatanKelistrikanYear1,
                      "Kelebihan Muatan": totals.kelebihanMuatanYear1,
                      "Jalan Licin": totals.jalanLicinYear1,
                      "Menabrak Hambatan": totals.menabrakHambatanYear1,
                      "Menghindari Hambatan": totals.menghindariHambatanYear1,
                      "Lain-lain": totals.lainLainYear1,
                    },
                    {
                      "name" : year2,
                      "Kurang Antisipasi / Mengantuk / Over Speed": totals.mengantukYear2,
                      "Gangguan Roda": totals.gangguanRodaYear2,
                      "Pecah Ban": totals.pecahBanYear2,
                      "Rem Blong": totals.remBlongYear2,
                      "Hambatan Kelistrikan / Mesin": totals.hambatanKelistrikanYear2,
                      "Kelebihan Muatan": totals.kelebihanMuatanYear2,
                      "Jalan Licin": totals.jalanLicinYear2,
                      "Menabrak Hambatan": totals.menabrakHambatanYear2,
                      "Menghindari Hambatan": totals.menghindariHambatanYear2,
                      "Lain-lain": totals.lainLainYear2,
                    }])
                  : selectedMonth
                  ? renderPie(
                      "Penyebab Kecelakaan",
                      ["Kurang Antisipasi / Mengantuk / Over Speed", "Gangguan Roda", "Pecah Ban", "Rem Blong", "Hambatan Kelistrikan / Mesin",
                        "Kelebihan Muatan", "Jalan Licin", "Menabrak Hambatan", "Menghindari Hambatan", "Lain-lain"],
                    [
                      {
                        "name": year,
                        "Kurang Antisipasi / Mengantuk / Over Speed": _.get(state.report, `data.mengantukYear1[${selectedMonth - 1}]`),
                        "Gangguan Roda": _.get(state.report, `data.gangguanRodaYear1[${selectedMonth-1}]`),
                        "Pecah Ban": _.get(state.report, `data.pecahBanYear1[${selectedMonth-1}]`),
                        "Rem Blong": _.get(state.report, `data.remBlongYear1[${selectedMonth-1}]`),
                        "Hambatan Kelistrikan / Mesin": _.get(state.report, `data.hambatanKelistrikanYear1[${selectedMonth-1}]`),
                        "Kelebihan Muatan": _.get(state.report, `data.kelebihanMuatanYear1[${selectedMonth-1}]`),
                        "Jalan Licin": _.get(state.report, `data.jalanLicinYear1[${selectedMonth-1}]`),
                        "Menabrak Hambatan": _.get(state.report, `data.menabrakHambatanYear1[${selectedMonth-1}]`),
                        "Menghindari Hambatan": _.get(state.report, `data.menghindariHambatanYear1[${selectedMonth-1}]`),
                        "Lain-lain": _.get(state.report, `data.lainLainYear1[${selectedMonth-1}]`)
                      },
                      {
                        "name": year2,
                        "Kurang Antisipasi / Mengantuk / Over Speed": _.get(state.report, `data.mengantukYear2[${selectedMonth - 1}]`),
                        "Gangguan Roda": _.get(state.report, `data.gangguanRodaYear2[${selectedMonth-1}]`),
                        "Pecah Ban": _.get(state.report, `data.pecahBanYear2[${selectedMonth-1}]`),
                        "Rem Blong": _.get(state.report, `data.remBlongYear2[${selectedMonth-1}]`),
                        "Hambatan Kelistrikan / Mesin": _.get(state.report, `data.hambatanKelistrikanYear2[${selectedMonth-1}]`),
                        "Kelebihan Muatan": _.get(state.report, `data.kelebihanMuatanYear2[${selectedMonth-1}]`),
                        "Jalan Licin": _.get(state.report, `data.jalanLicinYear2[${selectedMonth-1}]`),
                        "Menabrak Hambatan": _.get(state.report, `data.menabrakHambatanYear2[${selectedMonth-1}]`),
                        "Menghindari Hambatan": _.get(state.report, `data.menghindariHambatanYear2[${selectedMonth-1}]`),
                        "Lain-lain": _.get(state.report, `data.lainLainYear2[${selectedMonth-1}]`)
                      }])
                  : emptyChart()
                : emptyChart()}
              </Row>
            </Panel>
          </Collapse> <br />
          <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
          <Panel header={<strong>Jumlah Kecelakaan Tunggal & Kecelakaan dengan Kendaraan Lain (Table)</strong>} key="2">
            {/* Render TABLE KECELAKAAN */}
              {state.report ? (
                <Table
                  rowClassName={(record, index) =>
                    index % 2 === 0 ? style.even : style.odd
                  }
                  columns={columnJumlahYear1}
                  pagination={false}
                  dataSource={data}
                  scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
                />
              ) : (
                emptyChart()
              )}
              {state.report ? (
                <Table
                  rowClassName={(record, index) =>
                    index % 2 === 0 ? style.even : style.odd
                  }
                  columns={columnJumlahYear2}
                  pagination={false}
                  dataSource={data}
                  scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
                />
              ) : (
                emptyChart()
              )}
            </Panel>
          </Collapse> <br />
          <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
            <Panel header={<strong>Kondisi Korban (Table)</strong>} key="3">
                {/* Render TABLE KONDISI */}
                {state.report ? (
                  <Table
                    rowClassName={(record, index) =>
                      index % 2 === 0 ? style.even : style.odd
                    }
                    columns={columnKondisiYear1}
                    pagination={false}
                    dataSource={data10}
                    scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
                  />
                ) : (
                  emptyChart()
              )}
              {state.report ? (
                <Table
                  rowClassName={(record, index) =>
                    index % 2 === 0 ? style.even : style.odd
                  }
                  columns={columnKondisiYear2}
                  pagination={false}
                  dataSource={data10}
                  scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
                />
              ) : (
                emptyChart()
              )}
            </Panel>
          </Collapse> <br />
          <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
            <Panel header={<strong>Penyebab Kecelakaan (Table)</strong>} key="4">
                {/* Render TABLE PENYEBAB KECELAKAAN */}
                {state.report ? (
                  <Table
                    rowClassName={(record, index) =>
                      index % 2 === 0 ? style.even : style.odd
                    }
                    columns={columnPenyebabYear1}
                    pagination={false}
                    dataSource={data20}
                    scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
                  />
                ) : (
                  emptyChart()
                )}
                {state.report ? (
                  <Table
                    rowClassName={(record, index) =>
                      index % 2 === 0 ? style.even : style.odd
                    }
                    columns={columnPenyebabYear2}
                    pagination={false}
                    dataSource={data20}
                    scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
                  />
                ) : (
                  emptyChart()
                )}
            </Panel>
          </Collapse>
      </div>
    );
  }
});

export default Accident;
