import React, { useState, useEffect } from "react";
import Highlighter from "react-highlight-words";
import "antd/dist/antd.css";
import { Table, Input, Button, Icon, Empty, Select, Collapse, DatePicker, PageHeader } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";
import { Card, CardBody, CardColumns, CardHeader, Row, Col } from "reactstrap";
import { Doughnut, Pie } from "react-chartjs-2";
import { inject, observer } from "mobx-react";
import { useStore } from "../../../utils/useStores";
import * as _ from "lodash";
import { useHistory } from "react-router-dom";
import { Chart } from "react-google-charts";
import { ArrowLeftOutlined } from "@ant-design/icons";
import moment from "moment";

import CanvasJSReact from '../canvasjs/canvasjs.react';
var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const { Option } = Select;
const { Panel } = Collapse;
const TrafficClassification = observer((props) => {
  let history = useHistory();
  const store = useStore();

  const [state, setState] = useState({
    seacrhText: '',
    isLoaded: false,
    report: '',
    dataDough: [],
    selectedMonth: 0
  });

  const [selectedMonth, setSelectedMonth] = useState(0);
  const [yearKey, setYearKey] = useState("");

  useEffect(() => {
    loadData();
  }, [selectedMonth]);

  async function loadData() {
    await store.adjust.getAllAdjust();
    await store.keymetric.getClassification(yearKey)
      .then(res => {
        setState({
          ...state,
          isLoaded: true,
          report: res.body
        })
      }).catch(res => console.log(res))
  }

  const onChange = event => {
      setSelectedMonth(event)
    };

  let labels = ['I', 'II', 'III', 'IV','V']
  const generateDataDough = (key, year) => {
    let dough = _.get(state.report, `data.${key}.${year}`)

    return {
      animationEnabled:true,
      exportFileName: key + " " + year,
      exportEnabled: true,
      legend:{
        cursor: "pointer",
        itemclick: explodePie
      },
      data: [{
        type: "doughnut",
        indexLabel: "{y}",
        indexLabelFontSize: 12,
        showInLegend: true,
        startAngle: -90,
        dataPoints: labels.map( function(x, i){
          return {"y": dough[i], "label": x, "name":x}        
      })
    }]
    }
  }

  let label = ['I', 'II', 'III', 'IV','V']
  const dataDoughnut = (key, year) => {
    let dough = _.get(state.report, `data.${key}.${year}`)
    let index = [["MTD", "Data"]]
    console.log(dough, "isi dough")
    let data = label.map( function(x, i) {
      return [x, dough[i]]
    })
    let mix = index.concat(data)
    return (mix)
  }

  function explodePie (e) {
    if(typeof (e.dataSeries.dataPoints[e.dataPointIndex].exploded) === "undefined" || !e.dataSeries.dataPoints[e.dataPointIndex].exploded) {
      e.dataSeries.dataPoints[e.dataPointIndex].exploded = true;
    } else {
      e.dataSeries.dataPoints[e.dataPointIndex].exploded = false;
    }
    e.chart.render();
  }
  const getDataPrevMonth = () => {
    return {
      data : _.get(state.report, `data.mtdPrevMonths`)
    }
  }

  const generateDataPie = (key, year) => {
    return {
      labels: [
        'I',
        'II',
        'III',
        'IV',
        'V',
      ],
      datasets: [
        {
          data: _.get(state.report, `data.${key}.${year}`),
          backgroundColor: [
            '#2678bb',
            '#f25204',
            '#a8a9ad',
            '#2196f3',
            '#00bcd4',
            '#009688'
        ],
        hoverBackgroundColor: [
            '#85aee2',
            '#f5a136',  
            '#e5ddd5',
            '#2196f3',
            '#00bcd4',
            '#009688'
        ]
        }],
        
    };
  }

  const emptyChart = () =>{
    return(
        <Card >
          <CardHeader className={'d-flex align-items-center'}>
              <strong>Traffic Clasification</strong>
          </CardHeader>
              <div
                  style={{
                      minHeight: 'calc(100vh - 450px)',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center'
                  }}>
                  <Empty />

              </div>
        </Card> 
    )
}
  const year1 = _.get(state.report, 'data.year1');
  const year2 = _.get(state.report, 'data.year2');
  const monthPrevYear = _.get(state.report, 'data.mtdPrevMonths');

  const columns2 = [
    {
      title: 'Month',
      dataIndex: 'month',
      key: 'month',
    },
    {
      title: 'I',
      dataIndex: 'data1',
      key: 'data1',
    },
    {
      title: 'II',
      dataIndex: 'data2',
      key: 'data2',

    },
    {
      title: 'III',
      dataIndex: 'data3',
      key: 'data3',

    },
    {
      title: 'IV',
      dataIndex: 'data4',
      key: 'data4',

    },
    {
      title: 'V',
      dataIndex: 'data5',
      key: 'data5',

    },

  ];

  const columns = [
    {
      title: 'Year',
      dataIndex: 'year',
      key: 'year',
    },
    {
      title: 'I',
      dataIndex: 'gol1',
      key: 'gol1',
      render : text => text ? parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')  : '' || 0
    },
    {
      title: 'II',
      dataIndex: 'gol2',
      key: 'gol2',
      render : text =>text ?  parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') :''|| 0
    },
    {
      title: 'III',
      dataIndex: 'gol3',
      key: 'gol3',
      render : text =>text ?  parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : '' || 0
    },
    {
      title: 'IV',
      dataIndex: 'gol4',
      key: 'gol4',
      render : text => text ? parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')  : '' || 0
    },
    {
      title: 'V',
      dataIndex: 'gol5',
      key: 'gol5',
      render : text => parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') || 0
    },

  ];

  const mtdYear1 = generateDataPie('mtd', 'year1');
  const mtdYear2 = generateDataPie('mtd', 'year2');
  const ytdYear1 = generateDataPie('ytd', 'year1');
  const ytdYear2 = generateDataPie('ytd', 'year2');
  const mtdPrevMonthsAll = getDataPrevMonth();

  const dataMTD = new Array(2).fill(1)
    .map((n, i) => {
      const mtd = i === 0 ? mtdYear1 : mtdYear2;
      return {
        year: i === 0 ? _.get(state.report, 'data.year1') : _.get(state.report, 'data.year2'),
        gol1: _.get(mtd, 'datasets[0].data[0]'),
        gol2: _.get(mtd, 'datasets[0].data[1]'),
        gol3: _.get(mtd, 'datasets[0].data[2]'),
        gol4: _.get(mtd, 'datasets[0].data[3]'),
        gol5: _.get(mtd, 'datasets[0].data[4]'),
      }
    });

  const dataYTD = new Array(2).fill(1)
    .map((n, i) => {
      const mtd = i === 0 ? ytdYear1 : ytdYear2;
      return {
        year: i === 0 ? _.get(state.report, 'data.year1') : _.get(state.report, 'data.year2'),
        gol1: _.get(mtd, 'datasets[0].data[0]'),
        gol2: _.get(mtd, 'datasets[0].data[1]'),
        gol3: _.get(mtd, 'datasets[0].data[2]'),
        gol4: _.get(mtd, 'datasets[0].data[3]'),
        gol5: _.get(mtd, 'datasets[0].data[4]'),
      }
    });
  const dataMTDPrevMonth = _.map(_.get(mtdPrevMonthsAll, 'data'), function(value, key) {
    return {
    month: key,
    data1: parseInt((_.get(value, `[0]`))*100) + "%",
    data2: parseInt((_.get(value, `[1]`))*100) + "%",
    data3: parseInt((_.get(value, `[2]`))*100) + "%",
    data4: parseInt((_.get(value, `[3]`))*100) + "%",
    data5: parseInt((_.get(value, `[4]`))*100) + "%",
    }
  });
  let coba 

  async function onClickedYear(years) {
    await store.keymetric.getClassification(years)
      .then(res => {
        setState({
          ...state,
          isLoaded: true,
          report: res.body
        })
      }).catch(res => console.log(res))
  }
  return (
    <div className={'page-section ' + state.report} style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
      <Button type="primary"
          onClick={() => { window.history.back(); }}
          style={{ color: "#ffffff", width: 80, marginBottom: 10, zIndex:999 }}>
          <ArrowLeftOutlined /> Back
      </Button> <br /><br />
      <PageHeader style={{ padding: 0, marginBottom: 10 }} title={[<DatePicker picker="year" onChange={(value) => {setYearKey(moment(value).format('YYYY'))}} />,
        <Button type="primary" style={{marginLeft:5}} onClick={() => { onClickedYear(yearKey)}}>Go</Button>
      ]} />
      <div className={'page-separator'}>
        <div className={'page-separator__text'}>Overview Key Metric from year {yearKey ? yearKey : store?.adjust?.data ?  _.filter(store?.adjust?.data, i => i.menu_name == "key_metrics")?.map(d => {
                return d?.year
            }) : "-"}</div>
      </div>
      <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
        <Panel key="1" header={<strong>Doughnut Chart Traffic Classification</strong>}>
          <Row>
            <div className="col-md-6">
            {state.report ?
              <Card>
              <div className="chart-wrapper">
                {/* <Doughnut data={mtdYear2} /> */}
                {/* <CanvasJSChart options={generateDataDough('mtd', 'year1')} /> */}
                <Chart 
                chartType="PieChart"
                loader={<div>Loading Chart</div>}
                height={390}
                options={{
                title: "MTD ".concat(_.get(state.report, 'data.month')+" "+_.get(state.report, 'data.year2')),
                titleColor : "crimson",
                pieHole: 0.5,
                pieStartAngle: -90,
                legend: 'labeled',
                backgroundColor:"none",
                chartArea: {
                  height : 300,
                  width: '95%'
                },
                pieSliceText: 'value',
                sliceVisibilityThreshold: -100
              }} 
                rootProps={{ 'data-testid': '3' }} 
                data={dataDoughnut('mtd', 'year2')} 
                /> </div>
              </Card>
                : emptyChart()
              }
            </div>
            <div className="col-md-6">
            {state.report ?
              <Card>
                  <div className="chart-wrapper">
                    {/* <Doughnut data={mtdYear1} /> */}
                    {/* <CanvasJSChart options={generateDataDough('mtd', 'year1')} /> */}
                    <Chart 
                    chartType="PieChart"
                    loader={<div>Loading Chart</div>}
                    height={390}
                    options={{
                    title: "MTD ".concat(_.get(state.report, 'data.month')+" "+_.get(state.report, 'data.year1')),
                    titleColor : "crimson",
                    pieHole: 0.5,
                    pieStartAngle: -90,
                    legend: 'labeled',
                    backgroundColor:"none",
                    chartArea: {
                      height : 300,
                      width: '95%'
                    },
                    pieSliceText : 'value',
                    sliceVisibilityThreshold: -100
                  }} 
                    rootProps={{ 'data-testid': '3' }} 
                    data={dataDoughnut('mtd', 'year1')} 
                    /> </div>
              </Card>
              : emptyChart()
              }
            </div>
          </Row>
          <br /><br />
          <Row>
            <div className="col-md-6">
            {state.report ?
              <Card>
                  <div className="chart-wrapper">
                    {/* <Doughnut data={ytdYear2} /> */}
                    {/* <CanvasJSChart options={generateDataDough('mtd', 'year1')} /> */}
                    <Chart 
                    chartType="PieChart"
                    loader={<div>Loading Chart</div>}
                    height={390}
                    options={{
                    title: "YTD ".concat(_.get(state.report, 'data.month')+" "+_.get(state.report, 'data.year2')),
                    titleColor : "crimson",
                    pieHole: 0.5,
                    pieStartAngle: -90,
                    legend: 'labeled',
                    backgroundColor:"none",
                    chartArea: {
                      height : 300,
                      width: '95%'
                    },
                    pieSliceText : 'value',
                    sliceVisibilityThreshold: -100
                  }} 
                    rootProps={{ 'data-testid': '3' }} 
                    data={dataDoughnut('ytd', 'year2')} 
                    /> </div>
              </Card>
                : emptyChart()
              }
            </div>
            <div className="col-md-6">
            {state.report ?
              <Card>
                  <div className="chart-wrapper">
                    {/* <Doughnut data={ytdYear1} /> */}
                    {/* <CanvasJSChart options={generateDataDough('mtd', 'year1')} /> */}
                    <Chart 
                    chartType="PieChart"
                    loader={<div>Loading Chart</div>}
                    height={390}
                    options={{
                    title: "YTD ".concat(_.get(state.report, 'data.month')+" "+_.get(state.report, 'data.year1')),
                    titleColor : "crimson",
                    pieHole: 0.5,
                    legend: 'labeled',
                    pieStartAngle: -80,
                    chartArea: {
                      height : 300,
                      width: '95%'
                    },
                    pieSliceText : 'value',
                    sliceVisibilityThreshold: -100
                  }} 
                    rootProps={{ 'data-testid': '3' }} 
                    data={dataDoughnut('ytd', 'year1')} 
                    /> </div>
              </Card>
              : emptyChart()
              }
            </div>
          </Row>
        </Panel>
        </Collapse>
        <br />
      <Collapse  expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
        <Panel key="2" header={<strong>MTD (Table)</strong>}>
          {state.report ?
            <Table
            columns={columns}
            dataSource={dataMTD}
            scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
            />
            : emptyChart()
          }
        </Panel>
      </Collapse> <br />
      <Collapse  expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
        <Panel key="3" header={<strong>YTD (Table)</strong>}>
          {state.report ?
            <Table
              columns={columns}
              pagination={false}
              dataSource={dataYTD}
              scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }} />
              : emptyChart()
          }
        </Panel>
      </Collapse> <br />
      <Collapse  expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
        <Panel key="4" header={<strong>MTD Previous Month (Table)</strong>}>
          {state.report ?
            <Table
              columns={columns2}
              dataSource={dataMTDPrevMonth} 
              scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}/>
              : emptyChart()
          }
        </Panel>
      </Collapse>
    </div>
  );

});

export default TrafficClassification;