import React, { useEffect, useState } from 'react';
import 'antd/dist/antd.css';
import {Table, Input, Button, Icon, Collapse, PageHeader, DatePicker } from 'antd';
import {Empty} from "antd";
import { ArrowLeftOutlined, CaretRightOutlined } from "@ant-design/icons";
import { LineChart, Line, Legend, Tooltip, XAxis, YAxis, CartesianGrid } from 'recharts';
import moment from 'moment';
import { Card } from "reactstrap";
import {inject, observer} from "mobx-react";
import * as _ from "lodash";
import {useStore} from '../../../utils/useStores';
var createReactClass = require('create-react-class');
const { Panel } = Collapse;

const data = [
    {
        month: 'Januari',
        da: '3,983',
        target: '3,983',
        aol: '0%',
        di: '3,501',
        aly: '14%',
    },
    {
        month: 'Februari',
        da: '3,811',
        target: '3,811',
        aol: '0%',
        di: '3,204',
        aly: '19%',
    },
    {
        month: 'Maret',
        da: '3,795',
        target: '3,795',
        aol: '0%',
        di: '3,158',
        aly: '20%',
    },
    {
        month: 'April',
        da: '4,113',
        target: '4,113',
        aol: '0%',
        di: '3,207',
        aly: '28%',
    },
    {
        month: 'Mei',
        da: '3,709',
        target: '3,709',
        aol: '0%',
        di: '3,189',
        aly: '16%',
    },
    {
        month: 'Juni',
        da: '6,765',
        target: '6,765',
        aol: '0%',
        di: '6,483',
        aly: '4%',
    },
    {
        month: 'Juli',
        da: '4,092',
        target: '4.092',
        aol: '0%',
        di: '4.023',
        aly: '2%',
    },
    {
        month: 'Agustus',
        da: '4,623',
        target: '3,791',
        aol: '22%',
        di: '3,853',
        aly: '20%',
    },
    {
        month: 'September',
        da: '-',
        target: '3,887',
        aol: '0%',
        di: '3,501',
        aly: '14%',
    },
    {
        month: 'Oktober',
        da: '-',
        target: '4,065',
        aol: '0%',
        di: '3,501',
        aly: '14%',
    },
    {
        month: 'November',
        da: '-',
        target: '4,191',
        aol: '0%',
        di: '3,501',
        aly: '14%',
    },
    {
        month: 'Desember',
        da: '-',
        target: '5,767',
        aol: '0%',
        di: '3,501',
        aly: '14%',
    },
    {
        month: 'Average To Desember',
        da: '-',
        target: '-',
        aol: '-',
        di: '-',
        aly: '-',
    },
];

const colors = [
    '#f25204',
    '#a8a9ad',
    '#2678bb',
    '#2196f3',
    '#00bcd4',
    '#009688'
]

const labelMapping = {
    OL3: 'Target',
    year1Data: "Current Year",
    year2Data: "Prev Year"
};

const Daily = observer((props) => {
    const store = useStore();
    const [state, setState] = useState({
        seacrhText: '',
        isLoaded: false,

        report: '',
    });
    const [yearKey, setYearKey] = useState("")

    const setData = key => res => {
        setState((prevState) => ({
            ...prevState,
            isLoaded: true,
            report: res.body
        }));
    };

    useEffect(() => {
        loadData();
    }, []);

    async function loadData() {
        await store.adjust.getAllAdjust();
        await store.keymetric.getDailyRevenue(yearKey).then(setData('Revenue'))
    }

    const CustomizedLabel = createReactClass({
        render() {
          const {
            x, y, value, ref_data
          } = this.props;

          if(ref_data === "year1Data"){
            return <text x={x} y={y} dy={-20} fill="#f25204" fontSize={14} textAnchor="middle">{value}</text>;

          }else if(ref_data === "OL3"){
            return <text x={x} y={y} dy={0} fill="#a8a9ad" fontSize={14} textAnchor="middle">{value}</text>;

          }else{
            return <text x={x} y={y} dy={20} fill="#2678bb" fontSize={14} textAnchor="middle">{value}</text>;
          }

        }
      })

    const getLabel = (k) => labelMapping[k] ? labelMapping[k] : _.startCase(k);

    function generateChartData() {
        if (!state.report) {
            return [];
        }

        return new Array(12).fill(1)
            .map((n, i) => {
                return {
                    name: moment().month(i).format('MMM'),
                    ...Object.keys(state?.report?.data).reduce((all, cur) => {
                        if (cur === 'rawExcel') {
                            return all;
                        }
                        // console.log(state?.report?.data[cur], "data revenue")
                        const label = getLabel(cur);
                        let value = state?.report?.data[cur] ;
                        const data = Array.isArray(value) ? value : value.data;
                        all[label] = data[i]?parseInt(data[i]).toFixed(0):undefined;
                        return all;
                    }, {})
                }
            });
    }

    async function onClickedYear(years) {
        await store.keymetric.getDailyRevenue(years).then(setData('Revenue'))
    }

    {
        const data4 = generateChartData();

        const yearNew = _.get(state.report, 'data.year2Data.year');
        const reportData = _.get(state.report, 'data', []);
        const rawExcelTitle = _.get(state.report, `data.rawExcel[1]`);
        const month = _.get(rawExcelTitle, `B`);
        const year = _.get(rawExcelTitle, `DAILY REVENUE`)
        const target = _.get(rawExcelTitle, `D`);
        const actvstarget = _.get(rawExcelTitle, `E`);
        const lastyear   = _.get(rawExcelTitle, `F`);
        const lyvsact = _.get(rawExcelTitle, `G`);

        const columns = [
            {
                title: month ? month : "Month",
                dataIndex: 'month',
                key: 'month',
            },
            {
                title: year,
                dataIndex: 'year1Data',
                key: 'year1Data', 
                render: text =>text ?  parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') :''|| 0
            },
            {
                title: target,
                dataIndex: 'OL3',
                key: 'OL3',
                render: text => text ?parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : '' || 0
            },
            {
                title: actvstarget,
                dataIndex: 'actVsOL',
                key: 'actVsOL',
                render: text => text ? parseFloat(text).toFixed(2) + '%'  :0 + '%'
            },
            {
                title: lastyear ,
                dataIndex: 'year2Data',
                key: 'year2Data',
                render: text =>text ?  parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : '' || 0
            },
            {
                title: lyvsact,
                dataIndex: 'actVsYL',
                key: 'actVsYL',
                render: text =>text ? parseFloat(text).toFixed(2)  + '%' : 0 + '%' 
            },
        ]; 
        
        const newData = data.map((d, i) => {
            let value = _.dropWhile(reportData.rawExcel, ["B", null])
                return {
                    month: d.month,
                    year1Data: _.get(value, `[${i}][DAILY REVENUE]`, 0),
                    OL3: _.get(value, `[${i}][D]`, 0),
                    actVsOL: _.get(value, `[${i}][E]`, 0)*100,
                    year2Data: _.get(value, `[${i}][F]`, 0),
                    actVsYL: _.get(value, `[${i}][G]`, 0)*100,
                }
        });
        const dataTotal = {
            month: "Total",
            year1Data: _.sum(reportData.year1Data),
            OL3: _.sum(reportData.OL3),
            actVsOL: _.mean(reportData.actVsOL)*100,
            year2Data: _.sum(_.get(reportData.year2Data, 'data')),
            actVsYL: _.mean(reportData.actVsYL)*100
        }
        newData.push(dataTotal);

        const renderName = (val, i) => {
            let newTitle = [rawExcelTitle['DAILY REVENUE'], rawExcelTitle.D, rawExcelTitle.F]
            return newTitle[i]
        }

        return <div  style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <Button type="primary"
                onClick={() => { window.history.back(); }}
                style={{ color: "#ffffff", width: 80}}>
                <ArrowLeftOutlined /> Back
            </Button> 
            <br /> <br />
            <PageHeader style={{padding:0, paddingBottom:10}} title={[<DatePicker picker="year" onChange={ (value) => {setYearKey(moment(value).format('YYYY'))}}/>, <Button type={"primary"} onClick={() => {onClickedYear(yearKey)}} style={{marginLeft:5}}>Go</Button>]} />
            <div className={'page-section'}>
                <div className={'page-separator'}>
                    <div className={'page-separator__text'}>Overview Key Metrics from year {yearKey ? yearKey : store?.adjust?.data ?  _.filter(store?.adjust?.data, i => i.menu_name == "key_metrics")?.map(d => {
                return d?.year
            }) : "-"}</div>
                </div>
                <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
                    <Panel header={<strong>Line Chart Daily Revenue (in BIDR)</strong>} key="1">
                        <Card>
                            {state.report ?
                                <div className="chart-wrapper" style={{height: 380 + 'px', marginTop: 40 + 'px'}}>
                                    {state.isLoaded && (
                                        <LineChart height={300} width={ 999} data={data4}
                                        margin={{
                                            top: 40, right: 30, left: 20, bottom: 10,
                                        }}
                                        >
                                            <XAxis dataKey="name" />
                                            <Tooltip />
                                            <Legend />
                                            {Object.keys(state.report?.data)
                                                    .filter(key => !key.includes('actVsOL'))
                                                    .filter(key => !key.includes('actVsYL'))
                                                    .filter(key => !key.includes('rawExcel'))
                                                    .map((key, i) => {
                                                        return (<Line type="monotone" name={renderName(key, i)} dataKey={getLabel(key)} stroke={colors[i]} strokeWidth={3} label={<CustomizedLabel ref_data={key} />} />)
                                                    })}
                                        </LineChart>
                                    )}
                                </div>
                            :
                            <Card >
                                <div
                                    style={{
                                        minHeight: 'calc(100vh - 450px)',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Empty />

                                </div>
                            </Card>
                            }
                        </Card>
                    </Panel>
                </Collapse>
                <br />
                <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
                    <Panel key="2" header={<strong>Table Daily Revenue (in BIDR)</strong>}>
                        {state.report ?
                        <Table pagination={false} columns={columns}  dataSource={newData}  scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }} /> 
                        : 
                        <Card >
                            <div
                                style={{
                                    minHeight: 'calc(100vh - 450px)',
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center'
                                }}>
                                <Empty />

                            </div>
                        </Card>
                        }
                    </Panel>
                </Collapse>
            </div>
        </div>
    }
} );

export default Daily;