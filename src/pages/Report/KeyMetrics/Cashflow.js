import React, { Suspense, lazy, useState, useEffect } from "react";
import { useStore } from "../../../utils/useStores";
import Highlighter from "react-highlight-words";
import "antd/dist/antd.css";
import { Table, Input, Button, Icon, Empty, PageHeader, DatePicker } from "antd";
import { Card, CardBody, CardColumns, CardHeader, CardTitle } from "reactstrap";
import { Bar, Line } from "react-chartjs-2";
import { CustomTooltips } from "@coreui/coreui-plugin-chartjs-custom-tooltips";
import { inject, observer } from "mobx-react";
import moment from "moment";
import { Legend, LineChart, Tooltip, XAxis, YAxis } from "recharts";
import style from "../../../assets/css/fsum.module.css";
import { useHistory } from "react-router-dom";
import * as _ from "lodash";
import { props } from "bluebird";
import { ArrowLeftOutlined } from "@ant-design/icons";

const colors = [
  "#e91e63",
  "#9c27b0",
  "#3f51b5",
  "#2196f3",
  "#00bcd4",
  "#009688",
];

const labelMapping = {
  depreciation: "Depreciation & Amortisation",
  EBITDAMargin: "EBITDA Margin",
  EBITDAConstruction: "EBITDA ex. Construction",
  netProfit: "Net Profit (Loss)",
  tradingProfit: "Trading Profit Astra Portion",
  PPELand: "PPE & Land",
  investingOthers: "Others",
  equity: "Equity / Shareholder Loan Injection",
  paymentLongTerm: "Payment of long-term loans",
  paymentDividend: "Payment of dividend",
  financingOthers: "Others",
};

const line = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "My First dataset",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(75,192,192,0.4)",
      borderColor: "rgba(75,192,192,1)",
      borderCapStyle: "butt",
      borderDash: [],
      borderDashOffset: 0.0,
      borderJoinStyle: "miter",
      pointBorderColor: "rgba(75,192,192,1)",
      pointBackgroundColor: "#fff",
      pointBorderWidth: 1,
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(75,192,192,1)",
      pointHoverBorderColor: "rgba(220,220,220,1)",
      pointHoverBorderWidth: 2,
      pointRadius: 1,
      pointHitRadius: 10,
      data: [65, 59, 80, 81, 56, 55, 40],
    },
  ],
};

const data = [
  {
    key: "1",
    name: "John Brown",
    age: 32,
    address: "New York No. 1 Lake Park",
  },
  {
    key: "2",
    name: "Joe Black",
    age: 42,
    address: "London No. 1 Lake Park",
  },
  {
    key: "3",
    name: "Jim Green",
    age: 32,
    address: "Sidney No. 1 Lake Park",
  },
  {
    key: "4",
    name: "Jim Red",
    age: 32,
    address: "London No. 2 Lake Park",
  },
];

const options = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
  },
  maintainAspectRatio: false,
};

export const Cashflow = observer((props) => {
  let history = useHistory();
  const store = useStore();

  const [state, setState] = useState({
    seacrhText: "",
    isLoaded: false,
    report: "",
  });
  const [yearKey, setYearKey] = useState("");

  useEffect(() => {
    loadData();
  }, []);

  async function loadData() {
    await store.adjust.getAllAdjust();
    await store.keymetric
      .getCashflow(yearKey)
      .then((res) => {
        let data = res.body;
        data.data.rawExcel = _.map(data.data.rawExcel, (it) => {
          return _.mapValues(it, (value) => {
            if (!isNaN(Number(value)) && value != null) {
              return Math.round((Number(value) + Number.EPSILON) * 100) / 100;
            } else {
              return value;
            }
          });
        });
        setState({
          ...state,
          isLoaded: true,
          report: data,
        });
      })
      .catch((res) => console.log(res));
  }

  let searchInput;
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.select());
      }
    },
    render: (text) => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  const handleSearch = (selectedKeys, confirm) => {
    confirm();
    setState({ searchText: selectedKeys[0] });
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setState({ searchText: "" });
  };

  const getLabel = (k) => (labelMapping[k] ? labelMapping[k] : _.startCase(k));

  const dynamicColors = function () {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
  };

  const generateMTD = (keys = []) => {
    let datasetLabels = Object.keys(
      _.get(state.report, "data.tollRevenue.mtd") || {}
    );
    let labels = Object.keys(state.report.data || {})
      .filter((k) => ["year", "month"].indexOf(k) < 0)
      .filter((k) => (keys.length > 0 ? keys.indexOf(k) >= 0 : true));

    return {
      labels: labels.map((k) => getLabel(k)),
      datasets: datasetLabels.map((k) => {
        return {
          label: getLabel(k),
          // barPercentage: 0.5,
          barThickness: 20,
          // maxBarThickness: 8,
          // minBarLength: 2,
          backgroundColor: dynamicColors(),
          data: labels.map((l) => _.get(state.report, `data.${l}.mtd.${k}`)),
        };
      }),
      options: {},
    };
  };

  const generateYTD = (keys = []) => {
    let datasetLabels = Object.keys(
      _.get(state.report, "data.tollRevenue.ytd") || {}
    );
    let labels = Object.keys(state.report.data || {})
      .filter((k) => ["year", "month"].indexOf(k) < 0)
      .filter((k) => (keys.length > 0 ? keys.indexOf(k) >= 0 : true));

    return {
      labels: labels.map((k) => getLabel(k)),
      datasets: datasetLabels.map((k) => {
        return {
          label: getLabel(k),
          // barPercentage: 0.5,
          barThickness: 20,
          // maxBarThickness: 8,
          // minBarLength: 2,
          backgroundColor: dynamicColors(),
          data: labels.map((l) => _.get(state.report, `data.${l}.ytd.${k}`)),
        };
      }),
      options: {},
    };
  };

  const generateFY = (keys = []) => {
    let datasetLabels = Object.keys(
      _.get(state.report, "data.tollRevenue.fy") || {}
    );
    let labels = Object.keys(state.report.data || {})
      .filter((k) => ["year", "month"].indexOf(k) < 0)
      .filter((k) => (keys.length > 0 ? keys.indexOf(k) >= 0 : true));

    return {
      labels: labels.map((k) => getLabel(k)),
      datasets: datasetLabels.map((k) => {
        return {
          label: getLabel(k),
          // barPercentage: 0.5,
          barThickness: 20,
          // maxBarThickness: 8,
          // minBarLength: 2,
          backgroundColor: dynamicColors(),
          data: labels.map((l) => _.get(state.report, `data.${l}.fy.${k}`)),
        };
      }),
      options: {},
    };
  };

  const generateCard = (props) => {
    return (
      <Card>
        <CardHeader>
          <CardTitle className="mb-0">
            {props.title}
            <span
              style={{
                marginLeft: 10,
              }}
              className={"text-muted"}
            >
              {parseFloat(props.diff).toFixed(2)}%
            </span>
          </CardTitle>
          <div
            className="large"
            style={{
              fontSize: "3em",
            }}
          >
            {props.mainNumber}{" "}
            <span style={{ fontSize: 32 }} className={"text-muted"}>
              {" "}
              / {props.secondNumber}
            </span>
          </div>
        </CardHeader>
        <CardBody></CardBody>
      </Card>
    );
  };

  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      width: "30%",
      ...getColumnSearchProps("name"),
    },
    {
      title: "Age",
      dataIndex: "age",
      key: "age",
      width: "20%",
      ...getColumnSearchProps("age"),
    },
    {
      title: "Address",
      dataIndex: "address",
      key: "address",
      ...getColumnSearchProps("address"),
    },
  ];

  const month = _.get(state.report, "data.month");
  const year = _.get(state.report, "data.year");

  const operatingCashflow = [
    "tollRevenue",
    "otherRevenue",
    "operatingExpense",
    "maintenance",
    "taxes",
    "others",
    // 'totalOperatingCashFlow',
  ];

  const investingCashflow = [
    "PPELand", //
    "construction",
    "investingOthers", //
    // 'totalInvestingCashFlow',
  ];

  const financingCashflow = [
    "equity", //
    "paymentLongTerm", //
    "paymentDividend", //
    "proceedsNewLoan",
    "netInterest",
    "financingOthers", //
    // 'totalFinancingCashFlow'
  ];

  const columns2 = Object.keys(_.get(state.report, "data.rawExcel[0]", {})).map(
    (k) => {
      return {
        title: k.length > 1 ? _.startCase(k) : "",
        dataIndex: k,
        key: k,
        colSpan: k == "Account" ? 2 : k == "MTD" ? 3 : k == "YTD" ? 5 : k == "FY" ? 2 : 0,
      };
    }
  );

  const dataku = _.get(state.report, "data.rawExcel[0]", []);
  let headerData;

  async function onClickedYear(years) {
    await store.keymetric
      .getCashflow(years)
      .then((res) => {
        let data = res.body;
        if (data) {
          data.data.rawExcel = _.map(data.data.rawExcel, (it) => {
            return _.mapValues(it, (value) => {
              if (!isNaN(Number(value)) && value != null) {
                return Math.round((Number(value) + Number.EPSILON) * 100) / 100;
              } else {
                return value;
              }
            });
          });
        }
        setState({
          ...state,
          isLoaded: true,
          report: data,
        });
      })
      .catch((res) => console.log(res));
  }

  return (
    <div className={"page-section " + state.report} style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
      {/*<Table columns={columns} dataSource={data} />*/}
      <Button type="primary"
          onClick={() => { window.history.back(); }}
          style={{ color: "#ffffff", width: 80, marginBottom: 10, zIndex:999 }}>
          <ArrowLeftOutlined /> Back
      </Button>
      <PageHeader style={{ padding: 0, marginBottom: 10 }} title={[<DatePicker picker="year" onChange={(value) => {setYearKey(moment(value).format('YYYY'))}} />,
        <Button type="primary" style={{marginLeft:5}} onClick={() => { onClickedYear(yearKey)}}>Go</Button>
      ]} />
      {state.report ? (
        <Table
          className="table-cell-cashflow"
          columns={columns2}
          pagination={false}
          rowClassName={(record, index) =>
            record["Account"] === "In BIDR" ? style.styleSide :  index % 2 === 0 ? style.even : style.odd
          }
          scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
          bordered
          dataSource={_.get(state.report, "data.rawExcel", [])}
        />
      ) : (
        <Card className={"shadow"}>
          <div
            style={{
              minHeight: "calc(100vh - 450px)",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Empty />
          </div>
        </Card>
      )}
      {/* <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Operating Cash Flow
                </div>
            </div>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(operatingCashflow)} />
                    </div>
                </CardBody>
                : <Empty />
            }
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(operatingCashflow)} />
                    </div>
                </CardBody>
                 : <Empty />
                }
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                    <CardBody>
                        <div className="chart-wrapper">
                            <Bar
                                data={generateFY(operatingCashflow)} />
                        </div>
                    </CardBody>
                    : <Empty />
                }
            </Card>

            <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Investing Cash Flow
                </div>
            </div>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                    <CardBody>
                        <div className="chart-wrapper">
                            <Bar
                                data={generateMTD(investingCashflow)} />
                        </div>
                    </CardBody>
                    : <Empty />
                }
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                    <CardBody>
                        <div className="chart-wrapper">
                            <Bar
                                data={generateYTD(investingCashflow)} />
                        </div>
                    </CardBody>
                : <Empty />
                }
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(investingCashflow)} />
                    </div>
                </CardBody>
                : <Empty />
            }
            </Card>

            <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Financing Cash Flow
                </div>
            </div>


            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(financingCashflow)} />
                    </div>
                </CardBody>
                 : <Empty />
                }
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(financingCashflow)} />
                    </div>
                </CardBody>
                : <Empty />
                }
            </Card>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                {state.report ? 
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(financingCashflow)} />
                    </div>
                </CardBody>
                 : <Empty />
                }
            </Card> */}
    </div>
  );
});
