import React, { Component, useEffect, useState } from 'react';
import { ArrowLeftOutlined, CaretRightOutlined } from "@ant-design/icons";
import 'antd/dist/antd.css';
import { Table, Input, Button, Icon, Empty, Select, Collapse, PageHeader, DatePicker } from 'antd';
import {
    Card,
    Col,
    Row,
} from 'reactstrap';
import { BarChart, Bar, XAxis, YAxis, Tooltip, CartesianGrid, Legend, Line, LineChart, ComposedChart, Cell } from 'recharts';
import { inject, observer } from "mobx-react";
import { useStore } from '../../../utils/useStores';
import moment from "moment";
import * as _ from "lodash";
import { parse } from 'semver';
import { findLast } from 'lodash';
//You need this npm package to do createReactClass
var createReactClass = require('create-react-class');

const { Option } = Select;
const { Panel } = Collapse;
const colors = [
    '#f25204',
    '#a8a9ad',
    '#2678bb',
    '#2196f3',
    '#00bcd4',
    '#009688'
];

const labelMapping = {
    OL3: 'Target',
    year1Data: "Current Year",
    year2Data: "Prev Year"
};

const data = [ 
    {
        month: 'Januari',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    },
    {
        month: 'Februari',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    },
    {
        month: 'Maret',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    },
    {
        month: 'April',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'Mei',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'Juni',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'Juli',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'Agustus',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'September',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'Oktober',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'November',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    }, {
        month: 'Desember',
        da: null,
        target: null,
        aol: null,
        di: null,
        aly: null,
    },{
        month: 'Average To Desember',
        da: '-',
        target: '-',
        aol: '-',
        di: '-',
        aly: '-',
    },
];

const FullRouteEquivalent = observer(props => {
    const store = useStore();
    const [state, setState] = useState({
        seacrhText: '',
        isLoaded: false,
        report: '',
        barData: [],
        selectedMonth: 0
    });

    const [selectedMonth, setSelectedMonth] = useState(0);

    useEffect(() => {
        loadData();
    }, [selectedMonth]);

    async function loadData() {
        await store.adjust.getAllAdjust();
        await store.keymetric.getFRE(yearKey)
            .then(res => {
                // console.log(res + "ini res");
                setState({
                    report: res.body,
                    isLoaded: true
                })
            });
    }

    const [yearKey, setYearKey] = useState("");

    const onChange = event => {
        setSelectedMonth(event)
      };

    const generateChartData = () => {
        if (!state.report) {
            return [];
        }


        return new Array(12).fill(1)
            .map((n, i) => {
                return {
                    name: moment().month(i).format('MMM'),
                    ...Object.keys(state.report.data).reduce((all, cur) => {
                        const label = getLabel(cur);
                        let value = state.report.data[cur];
                        const data = Array.isArray(value) ? value : value.data;
                        all[label] = data[i]?parseInt(data[i]).toFixed(0):undefined;
                        return all;
                    }, {})
                }
            });
    }

    const labelChartMapping = [
        "Prev Year",
        "Current Year",
        "Target"
    ]
    const colorsChartMapping = [
        '#2678bb',
        '#f25204',
        '#a8a9ad',
        
    ];
    
    const generateBarData = () => {
        let index = newData.length - 1
        let dataA ;
        let dataB
        if(selectedMonth != 0){
             dataA =  [Number(newData[selectedMonth-1].year2Data),Number(newData[selectedMonth-1].year1Data),Number(newData[selectedMonth-1].OL3)]
             dataB =  [(newData[selectedMonth-1].actVsLY.toFixed(2) + '%') , , (newData[selectedMonth-1].actVsOL.toFixed(2) + '%')]
        } else {
             dataA =  [Number(newData[index].year2Data),Number(newData[index].year1Data),Number(newData[index].OL3)]
             dataB =  [_.sum(_.get(reportData, 'actVsLY')).toFixed(2) + "%", , _.sum(_.get(reportData, 'actVsOL')).toFixed(2)+"%"]
        }
        // console.log(selectedMonth, 'asdd');
        // let dataB =  ["+"+Number(newData[index].actVsLY).toFixed(2)+"%","+"+Number(newData[index].actVsOL).toFixed(2)+"%"]
        let datasets  =  dataA.map((k,i)=> {
            return {
                label: `${labelChartMapping[i]}`,
                backgroundColor: colorsChartMapping[i],
                data : k,
                percent: dataB[i],
                line : dataA[2]
            }
        })
        return (datasets)
    }
    
    const getLabel = (k) => labelMapping[k] ? labelMapping[k] : _.startCase(k);
    
    const CustomizedLabel3 = createReactClass({
        render() {
          const {
            x, y, ref_data, value,
          } = this.props;

          if(ref_data === "year1Data"){
            return <text x={x} y={y} dy={-20} fill="#f25204" fontSize={14} textAnchor="middle">{value}</text>;

          }else if(ref_data === "OL3"){
            return <text x={x} y={y} dy={5} fill="darkgrey" fontSize={14} textAnchor="middle">{value}</text>;

          }else{
            return <text x={x} y={y} dy={20} fill="#2678bb" fontSize={14} textAnchor="middle">{value}</text>;
          }
        }
      })
    
    const data4 = generateChartData();
    // const bar = generateBarData();
    const year = _.get(state.report, 'year');
    const yearNew = _.get(state.report, 'data.year2Data.year');
    const reportData = _.get(state.report, 'data', []);
    const rawExcelTitle = _.get(state.report, `data.rawExcel[0]`);
    // console.log(rawExcelTitle, "title excel")
    const month = _.get(rawExcelTitle, `B`);
    const yearactual = _.get(rawExcelTitle, `Full Route Equivalent`)
    const target = _.get(rawExcelTitle, `D`);
    const actvstarget = _.get(rawExcelTitle, `E`);
    const lastyear   = _.get(rawExcelTitle, `F`);
    const lyvsact = _.get(rawExcelTitle, `G`);
    
    const columns = [
        {
            title: month? month : 'Month',
            dataIndex: 'month',
            key: 'name',
        },
        {
            title: yearactual,
            dataIndex: 'year1Data',
            key: 'year1Data',
            render: text => text ? parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : '' || 0
        },
        {
            title: target,
            dataIndex: 'OL3',
            key: 'OL3',
            render: text => text ? parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : '' || 0
        },
        {
            title: actvstarget,
            dataIndex: 'actVsOL',
            key: 'actVsOL',
            render: text =>text ?  parseFloat(text).toFixed(2) + '%' : 0 + '%'
        },
        {
            title: lastyear,
            dataIndex: 'year2Data',
            key: 'year2Data',
            render: text =>text? parseFloat(text).toFixed().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,'):'' || 0
        },
        {
            title: lyvsact,
            dataIndex: 'actVsLY',
            key: 'actVsLY',
            render: text =>text? parseFloat(text).toFixed(2)  + '%' : 0 + '%'
        },
    ];

    let thisMonth = '';

    const newData = data.map((d, i) => {
        let value = _.dropWhile(reportData.rawExcel, ["B", null])
        let value2 = _.filter(reportData.rawExcel, v => v.B !== null)
        let value3 = _.filter(value2, v => {
            return _.get(v, `[Full Route Equivalent]`) !== 0;
        })
        if (_.get(value2, `[${i}][Full Route Equivalent]`, 0) !== 0) {
            thisMonth = _.findLast(value3).B;
        }
        return {
            month: d.month,
            year1Data: _.get(value, `[${i}][Full Route Equivalent]`, 0),
            OL3: _.get(value, `[${i}][D]`, 0),
            actVsOL: _.get(value, `[${i}][E]`, 0)*100,
            year2Data: _.get(value, `[${i}][F]`, 0),
            actVsLY: _.get(value, `[${i}][G]`, 0)*100,
        }
    });

    const dataTotal = {
        month: "Total",
        year1Data: _.sum(_.get(reportData.year1Data, 'data')),
        OL3: _.sum(reportData.OL3),
        actVsOL: _.sum(reportData.actVsOL)*100,
        year2Data: _.sum(_.get(reportData.year2Data, 'data')),
        actVsLY: _.sum(_.get(reportData,'actVsLY'))*100
    }

    newData.push(dataTotal);
    // console.log(newData, " TOTAL NYA");

    const CustomizedLabel2 = createReactClass({
        render () {
          const {x, y,index, stroke, value,ref_data} = this.props;
            return <text x={x+50} y={y} dy={20} fill={stroke} fontSize={18} textAlign={'center'}textAnchor="center">{value}</text> 
        }
      });    

    const CustomizedLabel = createReactClass({
        render () {
            const {x, y,index, stroke, value,ref_data} = this.props;
                return <text x={x} y={y+8} dy={-10} fill={stroke} fontSize={15} textAnchor="top">{ref_data[index].percent}</text>
        }
    });

    async function onClickedYear(years) {
        await store.keymetric.getFRE(years)
            .then(res => {
                // console.log(res + "ini res");
                setState({
                    report: res.body,
                    isLoaded: true
                })
            });
    }

    const renderName = (val, i) => {
        let newTitle = [rawExcelTitle['Full Route Equivalent'], rawExcelTitle.D, rawExcelTitle.F]
        return newTitle[i]
    }

    return (
        <div  style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <Row>
                <Col md="12" className={'page-section ' + state.report} >
                    <Button type="primary"
                        onClick={() => { window.history.back(); }}
                        style={{ color: "#ffffff", width: 80, marginBottom: 10, zIndex:999 }}>
                        <ArrowLeftOutlined /> Back
                    </Button> <br /><br />
                    
                    <PageHeader style={{padding:0, marginBottom:10}} title={[<DatePicker picker="year" onChange={(value) => {setYearKey(moment(value).format('YYYY'))}} />,
                        <Button type="primary" style={{marginLeft:5}} onClick={() => {onClickedYear(yearKey)}}>Go</Button>
                    ]} />
                    
                    <div className={'page-separator'}>
                        <div className={'page-separator__text'}>Overview Key Metrics from year {yearKey ? yearKey : store?.adjust?.data ?  _.filter(store?.adjust?.data, i => i.menu_name == "key_metrics")?.map(d => {
                            return d?.year
                        }) : "-"}</div>
                    </div>
                    <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
                        <Panel header={<strong>Full Route Equivalent (Line Chart)</strong>} key="1">
                            {state.report ? 
                                <Card>
                                    <div className="chart-wrapper" style={{ height: 380 + 'px', marginTop: 40 + 'px' }}>
                                        {state.isLoaded && (
                                            <LineChart height={300} width={1000} data={data4}
                                            margin={{
                                                top: 40, right: 30, left: 20, bottom: 10,
                                            }}
                                            >
                                                <XAxis dataKey="name" />
                                                <Tooltip />
                                                <Legend /> 
                                                {Object.keys(state.report.data)
                                                    .filter(key => !key.includes('actVsOL'))
                                                    .filter(key => !key.includes('actVsLY'))
                                                    .filter(key => !key.includes('actVsYear3'))
                                                    .filter(key => !key.includes('year3Data'))
                                                    .filter(key => !key.includes('rawExcel'))
                                                    .map((key, i) => {
                                                        return (<Line type="monotone" name={renderName(key, i)} dataKey={getLabel(key)} strokeWidth={3} stroke={colors[i]} label={<CustomizedLabel3 ref_data={key} />} />)
                                                    })}
                                            </LineChart>
                                        )}
                                    </div>
                                </Card>
                                :
                                <Card >
                                    <div
                                        style={{
                                            minHeight: 'calc(100vh - 450px)',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Empty />

                                    </div>
                                </Card>
                                }
                        </Panel>
                    </Collapse>
                    <br />
                    <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
                        <Panel header={ <strong>Grafik Bar Full Route Equivalent: Prev Year, Current Year, Target  <span>({thisMonth} {year})</span></strong> } key="2">
                            {state.report ? 
                                <div>
                                    <div className="card-header-actions">
                                        <Select
                                            style={{width:270, marginLeft:10}}
                                            className={'select-'}
                                            bordered={true}
                                            onChange={onChange} defaultValue="All Month">
                                                    <Option value={'0'}>All Month</Option>
                                                    <Option value={'1'}>Januari</Option>
                                                    <Option value={'2'}>Februari</Option>
                                                    <Option value={'3'}>Maret</Option>
                                                    <Option value={'4'}>April</Option>
                                                    <Option value={'5'}>Mei</Option>
                                                    <Option value={'6'}>Juni</Option>
                                                    <Option value={'7'}>Juli</Option>
                                                    <Option value={'8'}>Agustus</Option>
                                                    <Option value={'9'}>September</Option>
                                                    <Option value={'10'}>Oktober</Option>
                                                    <Option value={'11'}>November</Option>
                                                    <Option value={'12'}>Desember</Option>
                                        </Select>
                                        </div>
                                    {state.isLoaded && ( <div className="chart-wrapper" style={{width:'70%', marginLeft:'17.5%', height: 400 }}>
                                        <div >
                                            <ComposedChart width={600}  height={390} data={generateBarData()} key={selectedMonth} >
                                                    <XAxis dataKey="label" />
                                                    <Tooltip />
                                                    <Bar label={<CustomizedLabel2 />} dataKey='data' >
                                                    {
                                                        data.map((entry, index) => (
                                                            <Cell key={`cell-${index}`} fill={colorsChartMapping[index]}/>
                                                        ))
                                                    }
                                                    </Bar>
                                                    <Line type='monotone' dataKey='data' stroke='transparent' label={<CustomizedLabel ref_data={generateBarData()} />}  />
                                            </ComposedChart>
                                        </div>
                                        </div> )} 
                                </div>
                                :<Card >
                                <div
                                    style={{
                                        minHeight: 'calc(100vh - 450px)',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center'
                                    }}>
                                    <Empty />
                                    </div>
                                </Card>
                            } <br/>
                        </Panel>
                    </Collapse>
                    <br />
                    <Collapse expandIcon={({ isActive }) => <CaretRightOutlined rotate={isActive ? 90 : 0} />}>
                        <Panel header={<strong>Full Route Equivalent (Table)</strong>} key="3">
                            {state.report ? 
                            <Table pagination={false} columns={columns} dataSource={newData}  scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }} /> 
                            : <Card >
                                    <div
                                        style={{
                                            minHeight: 'calc(100vh - 450px)',
                                            display: 'flex',
                                            justifyContent: 'center',
                                            alignItems: 'center'
                                        }}>
                                        <Empty />

                                    </div>
                                </Card>
                                }
                        </Panel>
                    </Collapse>
                </Col>
            </Row>
        </div>
    )

})


export default FullRouteEquivalent;