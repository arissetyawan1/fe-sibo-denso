import React, { Component, useState, useEffect } from "react";
import "antd/dist/antd.css";
import { ArrowLeftOutlined } from "@ant-design/icons";
import style from "../../../assets/css/fsum.module.css";
import { Table, Input, Button, Icon, Empty, PageHeader, DatePicker } from "antd";
import {
  Card,
  CardBody,
  CardColumns,
  CardHeader,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import { inject, observer } from "mobx-react";
import * as _ from "lodash";
import { useStore } from "../../../utils/useStores";
import moment from "moment";
const labelMapping = {
  depreciation: "Depreciation & Amortisation",
  EBITDAMargin: "EBITDA Margin",
  EBITDAConstruction: "EBITDA ex. Construction",
  netProfit: "Net Profit (Loss)",
  tradingProfit: "Trading Profit Astra Portion",
};

const FinancialSummary = observer((props) => {
  const store = useStore();
  const [state, setState] = useState({
    seacrhText: "",
    isLoaded: false,
    report: "",
  });
  useEffect(() => {
    loadData();
  }, []);

  const [yearKey, setYearKey] = useState("")

  async function loadData() {
    await store.adjust.getAllAdjust();
    // await store.keymetric.getFinancialSummary(yearKey).then((res) => {
    //   let data = res.body;
    //   data.data.rawExcel = _.map(data.data.rawExcel, (it) => {
    //     return _.mapValues(it, (value) => {
    //       if (!isNaN(Number(value)) && value != null) {
    //         return Math.round((Number(value) + Number.EPSILON) * 100) / 100;
    //       } else {
    //         return value;
    //       }
    //     });
    //   });

    //   data.data.rawExcel.forEach((it) => {
    //     for (const [key, value] of Object.entries(it)) {
    //       console.log(key, value);
    //     }
    //   });

    //   setState({
    //     report: data,
    //     isLoaded: true,
    //   });
    // });
  }


  const getLabel = (k) => (labelMapping[k] ? labelMapping[k] : _.startCase(k));

  const dynamicColors = function () {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);
    return "rgb(" + r + "," + g + "," + b + ")";
  };

  const generateMTD = (keys = []) => {
    let datasetLabels = Object.keys(
      _.get(state.report, "data.tollRevenue.mtd") || {}
    );
    let labels = Object.keys(state.report.data || {})
      .filter((k) => ["year", "month"].indexOf(k) < 0)
      .filter((k) => (keys.length > 0 ? keys.indexOf(k) >= 0 : true));

    return {
      labels: labels.map((k) => getLabel(k)),
      datasets: datasetLabels.map((k) => {
        return {
          label: getLabel(k),
          // barPercentage: 0.5,
          barThickness: 20,
          // maxBarThickness: 8,
          // minBarLength: 2,
          backgroundColor: dynamicColors(),
          data: labels.map((l) => _.get(state.report, `data.${l}.mtd.${k}`)),
        };
      }),
      options: {},
    };
  };

  const generateYTD = (keys = []) => {
    let datasetLabels = Object.keys(
      _.get(state.report, "data.tollRevenue.ytd") || {}
    );
    let labels = Object.keys(state.report.data || {})
      .filter((k) => ["year", "month"].indexOf(k) < 0)
      .filter((k) => (keys.length > 0 ? keys.indexOf(k) >= 0 : true));

    return {
      labels: labels.map((k) => getLabel(k)),
      datasets: datasetLabels.map((k) => {
        return {
          label: getLabel(k),
          // barPercentage: 0.5,
          barThickness: 20,
          // maxBarThickness: 8,
          // minBarLength: 2,
          backgroundColor: dynamicColors(),
          data: labels.map((l) => _.get(state.report, `data.${l}.ytd.${k}`)),
        };
      }),
      options: {},
    };
  };

  const generateFY = (keys = []) => {
    let datasetLabels = Object.keys(
      _.get(state.report, "data.tollRevenue.fy") || {}
    );
    let labels = Object.keys(state.report.data || {})
      .filter((k) => ["year", "month"].indexOf(k) < 0)
      .filter((k) => (keys.length > 0 ? keys.indexOf(k) >= 0 : true));

    return {
      labels: labels.map((k) => getLabel(k)),
      datasets: datasetLabels.map((k) => {
        return {
          label: getLabel(k),
          // barPercentage: 0.5,
          barThickness: 20,
          // maxBarThickness: 8,
          // minBarLength: 2,
          backgroundColor: dynamicColors(),
          data: labels.map((l) => _.get(state.report, `data.${l}.fy.${k}`)),
        };
      }),
      options: {},
    };
  };

  const generateCard = (props) => {
    return (
      <Card>
        <CardHeader>
          <CardTitle className="mb-0">
            {props.title}
            <span
              style={{
                marginLeft: 10,
              }}
              className={"text-muted"}
            >
              {parseFloat(props.diff).toFixed(2)}%
            </span>
          </CardTitle>
          <div
            className="large"
            style={{
              fontSize: "3em",
            }}
          >
            {props.mainNumber}{" "}
            <span style={{ fontSize: 32 }} className={"text-muted"}>
              {" "}
              / {props.secondNumber}
            </span>
          </div>
        </CardHeader>
        <CardBody></CardBody>
      </Card>
    );
  };

  const month = _.get(state.report, "data.month");
  const year = _.get(state.report, "data.year");

  const revenueKeys = ["tollRevenue", "constructionRevenue", "totalRevenue"];

  const expenseKeys = [
    "constructionCost",
    "overlayProvision",
    "directExpense",
    "contributionMargin",
    "CM",
    "indirectExpense",
    "depreciation",
    "operatingProfit",
    "EBITDAMargin",
    "EBITDAConstruction",
  ];

  const otherIncomeExpenseKeys = [
    "netInterest",
    "others",
    "profitBeforeTax",
    "deferredTaxBenefit",
    "netProfit",
    "tradingProfit",
  ];
  // console.log(Object.keys(_.get(state.report, 'data.rawExcel[0]', {})))
  const columns2 = Object.keys(_.get(state.report, "data.rawExcel[0]", {})).map(
    (k) => {
      console.log(k, "k nya");
      return {
        title: k.length > 1 ? _.startCase(k) : "",
        dataIndex: k,
        colSpan : k== "Account" ? 2 : k =="MTD" ? 3 : k == "YTD" ? 5 : k == "FY"? 2 : 0,
        key: k,
        render: (value, row, index) => {
          const obj = {
            children: value,
            props: {}
          };
          console.log(row.Account, "ini lohh")
          // if (row.Account == "Financial Summary" &&  row.B == null) {
          //   obj.props.colSpan = 12
          // // } else if (row.Account == "Financial Summary" && index != 0) {
          // //   obj.props.colSpan = 0
          // }
          // else {
          //   obj.props.colSpan = 0
          // }
          // else if (row !== null) {
          //   obj.props.colSpan = 1
          // } else {
          //   obj.props.colSpan = 0
          // }
          return obj;
        }
      };
    }
  );

  const datanyaBre = _.get(state.report, "data.rawExcel", []);
  const datanyaResult = datanyaBre.map((item, i) => {
    if (item.E && !isNaN(item.E)) {
      let data = parseFloat(item.E).toFixed(2) * 100 + '%' || 0 + '%';
      // return item = item.splice(item.indexOf(item.H), 1, data)
    } else if (item.H && !isNaN(item.H)) {
      let data = parseFloat(item.H).toFixed(2) * 100 + '%' || 0 + '%';
      return data;
    } else if (item.J && !isNaN(item.J)) {
      let data = parseFloat(item.J).toFixed(2) * 100 + '%' || 0 + '%';
      return data;
    }
    return item;
  });
  
  async function onClickedYear(years) {
    // await store.keymetric.getFinancialSummary(years).then((res) => {
    //   let data = res.body;
    //   if (data) {
    //     data.data.rawExcel = _.map(data.data.rawExcel, (it) => {
    //       return _.mapValues(it, (value) => {
    //         if (!isNaN(Number(value)) && value != null) {
    //           return Math.round((Number(value) + Number.EPSILON) * 100) / 100;
    //         } else {
    //           return value;
    //         }
    //       });
    //     });
    //   }

    //   setState({
    //     report: data,
    //     isLoaded: true,
    //   });
    // });
  }

  return (
    <div className={"page-section " + state.report} style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
      <div id={"canvas-grid-container"}></div>

      <Button type="primary"
          onClick={() => { window.history.back(); }}
          style={{ color: "#ffffff", width: 80, marginBottom: 10, zIndex:999 }}>
          <ArrowLeftOutlined /> Back
      </Button>

      <PageHeader style={{marginBottom:10, padding:0}} title={[<DatePicker picker="year" onChange={(value) => {setYearKey(moment(value).format('YYYY'))}} />,
        <Button type="primary" onClick={() => {onClickedYear(yearKey)}} style={{marginLeft:5}}>Go</Button>
      ]} />
      
      {state.report ? (
        <Table
          columns={columns2}
          bordered={true}
          rowClassName={(record, index) =>
            record["Account"] === "In BIDR" ? style.styleSide :  index % 2 === 0 ? style.even : style.odd
          }
          pagination={false}
          dataSource={_.get(state.report, "data.rawExcel", [])}
          scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }}
        />
      ) : (
        <Card className={"shadow"}>
          <div
            style={{
              minHeight: "calc(100vh - 450px)",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Empty />
          </div>
        </Card>
      )}
      {/*<HotTable data={_.get(state.report, 'data.rawExcel', [])} width="600" height="300" />*/}
      {/* <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Revenue
                </div>
            </div>

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(revenueKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(revenueKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Revenue FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(revenueKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Expense
                </div>
            </div>
            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Expense MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(expenseKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Expense YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(expenseKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Expense FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(expenseKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <div className={'page-separator'}>
                <div className={'page-separator__text'}>
                    Other Income (Expense)
                </div>
            </div>
            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Other Income (Expense) MTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateMTD(otherIncomeExpenseKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Other Income (Expense) YTD</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateYTD(otherIncomeExpenseKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br />

            <Card>
                <CardHeader>
                    <CardTitle className="mb-0">Other Income (Expense) FY</CardTitle>
                    <div className="small text-muted">{year}</div>
                </CardHeader>
                <CardBody>
                    <div className="chart-wrapper">
                        <Bar
                            data={generateFY(otherIncomeExpenseKeys)} />
                    </div>
                </CardBody>
            </Card>
            <br /> */}
    </div>
  );
});

export default FinancialSummary;
