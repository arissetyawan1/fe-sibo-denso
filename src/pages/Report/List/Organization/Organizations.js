import React, {Component, useState} from 'react';
import Division from './Division';
import {
    ButtonDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
    Card,
    Button,
    CardBody,
    CardColumns,
    CardHeader,
    CardLink,
    CardSubtitle,
    CardText,
    CardTitle,
    UncontrolledButtonDropdown, Row, Col, FormGroup, Label, Input, Form, FormText
} from 'reactstrap';

import {} from '@ant-design/icons'
import { useStore } from "../../../../utils/useStores";

import {Tree, Table, Button as ButtonAnd, Modal, TreeSelect, message, Spin} from 'antd';
import {inject, observer} from "mobx-react";
import {FormInputGroup} from "./forminputgroup";

const {TreeNode} = Tree;


export const Organizations = observer((props) => {
    const store = useStore();
    const [activeTab, setActiveTab] = useState(new Array(4).fill('1'));
    const [data, setData] = useState([]);
    const [type, setType] = useState(['Org', 'Dept/Div', 'Pos']);
    const [loading, setLoading] = useState(false);
    const [visible, setVisible] = useState(false);
    const [form, setForm] = useState({});
    const [selectedKeys, setSelectedKeys] = useState({});

    const onChange = (e) => {
        setForm({
             ...form,
            [e.target.name]: e.target.value
        })
    };

    const handleOk = () => {
        return store.organizations.create({
            ...form,
            organization_tree_id: selectedKeys.id
        })
            .then(res => {
                message.info("Success Creating Organization!")
            })
            .finally(res => {
                    setVisible(visible);
                    setForm({});
                
            })
    };

    const handleCancel = () => {
        setVisible(!visible);
    };


    const renderModal = (visible, loading) =>{
        return <Modal
            visible={visible}
            title="Title"
            onOk={handleOk}
            onCancel={handleCancel}
            footer={[
                <ButtonAnd key="back" onClick={handleCancel}>
                    Return
                </ButtonAnd>,
                <ButtonAnd key="submit" type="primary" loading={store.organizations.isLoading} onClick={handleOk}>
                    Submit
                </ButtonAnd>,
            ]}
        >
 <Form>
      <FormGroup row>
        <Label for="NameID" sm={2}>Name</Label>
        <Col sm={10}>
          <Input type="name" name="name" id="nameID" placeholder="Name" onChange={onChange}/>
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="typeID" sm={2}>Type</Label>
        <Col sm={10}>
          <Input type="select" name="type" id="typeID">
            <option key="Division">Division</option>
            <option key="Department">Department</option>
            <option key="Position">Position</option>
          </Input>
        </Col>
      </FormGroup>
      <FormGroup row>
        <Label for="underID" sm={2}>Under</Label>
        <Col sm={10}>
          <Input type="name" name="under" id="underID" placeholder="" />
        </Col>
      </FormGroup>
      
      </Form>

        
      
        </Modal>;
    }

    /*componentDidMount() {
        const {store} = this.props;
        store.organization.getAll()
            .then(res => {
                setData({
                    data: res.data.map(d => ({
                        ...d,
                        title: d.name,
                        key: d.id,
                        children: []
                    }))
                })
            })
    }
    */
    const onSelect = (selectedKeys, info) => {
        console.log('selected', selectedKeys, info);
    };

    const onCheck = (checkedKeys, info) => {
        console.log('onCheck', checkedKeys, info);
    };

    const toggle = (tabPane, tab) =>{
        const newArray = activeTab.slice()
        newArray[tabPane] = tab
        setActiveTab({activeTab: newArray});
    }

    const showModal = () => {
        setVisible(!visible);
    };


  //  const {visible, loading};
    //const {store};
    const rowSelection = {
            onChange: (selectedRowKeys, selectedRows) => {
                console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
            },
            onSelect: (record, selected, selectedRows) => {
                console.log(record, selected, selectedRows);
            },
            onSelectAll: (selected, selectedRows, changeRows) => {
                console.log(selected, selectedRows, changeRows);
            },
        };

    const columns = [
            {
                title: 'Name',
                dataIndex: 'name',
                key: 'name',
                className: 'pl-16pt',
            },
            {
                title: 'Type',
                dataIndex: 'type',
                key: 'type',
                width: '12%'

            },  

        ];

        const rowNew = () => {
            return <table className=' mb-0 thead-border-top-0 table-nowrap'/>
        };

        const components = {
            table: rowNew
        };
         return (
            <div>
                {renderModal(visible, loading)}

                <Row>
                    <Col lg={'9'}>
                        <div className='page-section'>
                            <div className={'page-separator'}>
                                <div className={'page-separator__text'}>Structure</div>
                            </div>
                            <Card>
                                <CardHeader className='py-12pt d-flex align-items-center'>
                                    <ButtonAnd onClick={showModal} type={'primary'}
                                               className='rounded font-size-14pt'
                                               size={'large'}>Create</ButtonAnd>
                                </CardHeader>
                                <Spin spinning={store.organizations.isLoading}>
                                    <Tree
                                        onSelect={(selectedKeys, e) => {
                                         console.log({e: e.selectedNodes}, 'Org -> ')
                                            setSelectedKeys({selectedKeys: e.selectedNodes[0].props});
                                        }}
                                        loadData={treeNode => {

                                            const {props} = treeNode;
                                            console.log({treeNode}, 'Org -> ');
                                            const record = props;

                                            return store.organization.getChilds(props.id)
                                                .then(res => {
                                                    const selectedIndex = data.findIndex(d => d.id === record.id);
                                                    data[selectedIndex] = {
                                                        ...record,
                                                        children: res.data.map(d => ({
                                                            ...d,
                                                            title: d.name,
                                                            key: d.id,
                                                            // children: []
                                                        }))
                                                    };
                                                    console.log({data}, 'Org -> ');
                                                    setData({data: [...data]});
                                                    return res;
                                                });
                                        }}
                                        treeData={data} />
                                </Spin>
                                {/*<Table className='table mb-0 thead-border-top-0 table-nowrap'*/}
                                {/*       pagination={false}*/}
                                {/*       columns={columns}*/}
                                {/*       rowKey={record => record.key}*/}
                                {/*       loading={store.organization.isLoading}*/}
                                {/*       onRow={(record, rowIndex) => {*/}
                                {/*           return {*/}
                                {/*               onClick: (event) => {*/}
                                {/*                   store.organization.getChilds(record.id)*/}
                                {/*                       .then(res => {*/}
                                {/*                           const {data} = this.state;*/}
                                {/*                           const selectedIndex = data.findIndex(d => d.id === record.id);*/}
                                {/*                           data[selectedIndex] = {*/}
                                {/*                               ...record,*/}
                                {/*                               children: res.data*/}
                                {/*                           };*/}
                                {/*                           console.log({data}, 'Org -> ')*/}
                                {/*                           this.setState({data});*/}
                                {/*                       });*/}
                                {/*               },// click row*/}
                                {/*               onDoubleClick: event => {}, // double click row*/}
                                {/*               onContextMenu: event => {}, // right button click row*/}
                                {/*               onMouseEnter: event => {}, // mouse enter row*/}
                                {/*               onMouseLeave: event => {}, // mouse leave row*/}
                                {/*           };*/}
                                {/*       }}*/}
                                {/*       dataSource={data}/>*/}
                                {/*<div className="Tables"><Division data={this.data}></Division>*/}
                         </Card>

                        </div>
                    </Col>
                </Row>
            </div>
            // </div>
        );

}
)

//export default Organizations;