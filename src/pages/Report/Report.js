import React, { useEffect, useState } from "react";
import {
    Row,
    Col,
    Avatar,
    Button,
    Typography,
    Statistic,
    Card,
    Empty,
    PageHeader,
    List,
    Divider,
    Dropdown,
    Table,
    Menu
} from "antd";
import {
    FilterOutlined,
    PlusOutlined
} from "@ant-design/icons";
import { Link, useHistory, useRouteMatch, BrowserRouter as Router, Route } from "react-router-dom";
import { LINKS } from "../../routes";
import { observer } from "mobx-react-lite";
import { ReportRoute } from "./route";

const { Title } = Typography;
const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    margin: '-1px -1px 0 0',
    height: 184,
    transition: 'transform .4s'
};

const optionFilter = [
    {
        key: 1,
        value: 'Enabled'
    }, {
        key: 2,
        value: 'Disabled'
    },
];

function handleMenuClick(e) {
    console.log('click', e);
}

const dataHeader = [{ name: 'Invoices', link: '/app/reports/invoices' }, { name: 'Billings' }, { name: 'Deposits' }];
const dataBalances = [{ name: 'Accounts', link: '/app/balances/accounts' }];
const dataTransaction = [{ name: 'Operation Transaction' }, { name: 'Subscriptions' }, { name: 'frauds', }];
const dataList = [{ name: 'Users' }, { name: 'Roles', }, { name: 'Partners', }, { name: 'Commissions' }];

const data = [{ menu: 'Invoice', link: '' }]

export const Report = observer(() => {
    console.log(data);
    const menu = [{ menu: 'Invoice', link: '' }];
    return <ReportRoute />;
});
