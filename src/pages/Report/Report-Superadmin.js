import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Avatar,
  Button,
  Typography,
  Statistic,
  Card,
  Empty,
  PageHeader,
  List,
  Divider,
  Dropdown,
  Table,
  Menu,
} from "antd";
import { FilterOutlined, PlusOutlined } from "@ant-design/icons";
import {
  Link,
  useHistory,
  useRouteMatch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import { observer } from "mobx-react-lite";

const { Title } = Typography;
const DemoBox = (props) => (
  <p className={`height-${props.value}`}>{props.children}</p>
);
const gridStyle = {
  overflow: "hidden",
  position: "relative",
  padding: "16px 15px 8px",
  margin: "-1px -1px 0 0",
  height: 184,
  transition: "transform .4s",
};

const optionFilter = [
  {
    key: 1,
    value: "Enabled",
  },
  {
    key: 2,
    value: "Disabled",
  },
];

function handleMenuClick(e) {
  console.log("click", e);
}

const dataHeader = [
  { name: "Invoices", link: "/app/reports/invoices" },
  { name: "Billings", link: "/app/reports/bills" },
  { name: "Deposits" },
];
const dataBalances = [{ name: "Accounts", link: "/app/balances/accounts" }];
const dataTransaction = [
  { name: "Operation Transaction" },
  { name: "Subscriptions" },
  { name: "frauds" },
];
const dataList = [
  { name: "Users" },
  { name: "Roles" },
  { name: "Organizations" },
];
const keyMetric = [
  {
    name: "Executive Summary",
      urlName: "executive-summary",
    url: "/app/reports/Key_Metrics/executive-summary",
    icon: "",
  },
  {
    name: "Daily Revenue",
      urlName: "daily-revenue",
      url: "/app/reports/Key_Metrics/daily-revenue",
    icon: "",
  },
  {
    name: "Daily Traffic Volume",
      urlName: "daily-traffic-volume",
      url: "/app/reports/Key_Metrics/daily-traffic-volume",
    icon: "",
  },
  {
    name: "Average Vehicle KM",
      urlName: "average-vehicle-km",
      url: "/app/reports/Key_Metrics/average-vehicle-km",
    icon: "",
  },
  {
    name: "Full Route Equivalent",
      urlName: "full-route-equivalente",
      url: "/app/reports/Key_Metrics/full-route-equivalente",
    icon: "",
  },
  {
    name: "Accident",
      urlName: "accident",
      url: "/app/reports/Key_Metrics/accident",
    icon: "",
  },
  {
    name: "Traffic Classification",
      urlName: "traffic-classification",
      url: "/app/reports/Key_Metrics/traffic-classification",
    icon: "",
  },
  {
    name: "Financial Summary",
      urlName: "financial-summary",
      url: "/app/reports/Key_Metrics/financial-summary",
    icon: "",
  },
  {
    name: "Cashflow",
      urlName: "cashflow",
      url: "/app/reports/Key_Metrics/cashflow",
      icon: "",
  },
  {
    name: "Balance Sheet",
      urlName: "balance-sheet",
      url: "/app/reports/Key_Metrics/balance-sheet",
    icon: "",
  },
];
const dataKpi = [
  { name: "Corporate" },
  { name: "Divisions" },
  { name: "Departments" },
  // { name: "Commissions" },
];

const data = [{ menu: "Invoice", link: "" }];

export const ReportSuperadmin = observer(() => {
  console.log(data);
  return (
    <div className={["cariparkir-container"].join(" ")}>
      <Card
        bordered={false}
        style={{ overflow: "hidden" }}
        className={"shadow"}
        bodyStyle={{ background: "#f7fafc", marginRight: "-1px", padding: 0 }}
      >
        <PageHeader
          className={"card-page-header"}
          ghost={false}
          title={<span style={{ fontSize: 20 }}>Reports</span>}
          subTitle=""
        />
        <div
          style={{
            minHeight: "calc(100vh - 450px)",
            paddingTop: 10,
            paddingLeft: 24,
            paddingRight: 24,
          }}
        >
          <Row gutter={[16, 16]}>
            <Col xs={20} sm={16} md={12} lg={8} xl={6}>
              <List
                size="small"
                dataSource={keyMetric}
                header={
                  <Title
                    level={4}
                    style={{
                      fontSize: 13,
                      textTransform: "uppercase",
                      fontWeight: 400,
                      marginBottom: 0,
                    }}
                  >
                    Key metrics
                  </Title>
                }
                bordered={false}
                renderItem={(item) => (
                  <List.Item style={{ paddingLeft: 0 }}>
                    <Link
                      to={"/app/reports/Key_Metrics/" + item.urlName}
                      style={{ fontWeight: 400 }}
                    >
                      {item.name}
                    </Link>
                  </List.Item>
                )}
              />
            </Col>
            <Col xs={20} sm={16} md={12} lg={8} xl={6}>
              <List
                size="small"
                dataSource={dataKpi}
                header={
                  <Title
                    level={4}
                    style={{
                      fontSize: 13,
                      textTransform: "uppercase",
                      fontWeight: 400,
                      marginBottom: 0,
                    }}
                  >
                    Kpi
                  </Title>
                }
                bordered={false}
                renderItem={(item) => (
                  <List.Item style={{ paddingLeft: 0 }}>
                    <Link
                      to={"/app/reports/Kpi/" + item.name}
                      style={{ fontWeight: 400 }}
                    >
                      {item.name}
                    </Link>
                  </List.Item>
                )}
              />
            </Col>
            <Col xs={20} sm={16} md={12} lg={8} xl={6}>
              <List
                size="small"
                dataSource={dataTransaction}
                header={
                  <Title
                    level={4}
                    type={"uppercase"}
                    style={{
                      textTransform: "uppercase",
                      fontSize: 13,
                      fontWeight: 400,
                      marginBottom: 0,
                    }}
                  >
                    Operational
                  </Title>
                }
                bordered={false}
                renderItem={(item) => (
                  <List.Item style={{ paddingLeft: 0 }}>
                    <Link to={"/"} style={{ fontWeight: 400 }}>
                      {item.name}
                    </Link>
                  </List.Item>
                )}
              />
            </Col>{" "}
            <Col xs={20} sm={16} md={12} lg={8} xl={6}>
              <List
                size="small"
                dataSource={dataList}
                header={
                  <Title
                    level={4}
                    style={{
                      fontSize: 13,
                      textTransform: "uppercase",
                      fontWeight: 400,
                      marginBottom: 0,
                    }}
                  >
                    List
                  </Title>
                }
                bordered={false}
                renderItem={(item) => (
                  <List.Item style={{ paddingLeft: 0 }}>
                    <Link to={"/app/reports/list/" + item.name} style={{ fontWeight: 400 }}>
                      {item.name}
                    </Link>
                  </List.Item>
                )}
              />
            </Col>
          </Row>
        </div>
      </Card>
      ;
    </div>
  );
});
