import React, {useEffect, useState} from "react";
import {useStore} from "../../../utils/useStores";
import {Table, PageHeader, Button, Menu, Dropdown, Card, Typography, Statistic, Input, Select, Tag} from 'antd'
import {observer} from 'mobx-react-lite';
import {FilterOutlined, PlusOutlined} from '@ant-design/icons';
import {Filter} from '../../../component/Filter'
import moment from 'moment';

const {Text, Title} = Typography;
const {Search} = Input;
const {Option} = Select;

const optionFilter = [
  {
    key: 1,
    value: 'Enabled'
  }, {
    key: 2,
    value: 'Disabled'
  },
];

function handleMenuClick(e) {
  console.log('click', e);
}

const menu = (
  <Menu onClick={handleMenuClick} style={{background: '#fff'}}>
    {optionFilter.map((i => {
      return <Menu.Item key={i.key}>{i.value}</Menu.Item>
    }))
    }
  </Menu>
);
export const ReportSuperAdminBill = observer((props) => {
  const store = useStore();
  const [calculatedData, setcalculatedData] = useState([]);


  useEffect(() => {

    async function init() {
      await store.mitra.getAll();
      await store.transaction.getAll();
      await store.commision.getAll();
    }

    init(calculatedData).then(res => console.log(res, calculatedData));


  }, [calculatedData]);


  const columnFin = [
    {
      title: 'Order ID',
      dataIndex: 'order_id',
      key: 'order_id',
      width: 135,
      fixed: 'left',

    },
    {
      title: 'Transaction ID',
      dataIndex: 'id',
      key: 'id',
      width: 310
    },
    {
      title: 'Va Number',
      dataIndex: 'va_number',
      key: 'va_number',
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: 'Customer',
      dataIndex: 'username',
      key: 'username',

    },
    {
      title: 'Partner Name',
      dataIndex: 'partner_name',
      key: 'partner_name',
    },
    {
      title: 'Parking Name',
      dataIndex: 'parking_name',
      key: 'parking_name',
    },
    {
      title: 'Booking Date',
      dataIndex: 'booking_date',
      key: 'booking_date',
    },
    {
      title: 'Transaction Date',
      dataIndex: 'transaction_date',
      key: 'transaction_date',
    },
    {
      title: 'Deposit Fee',
      dataIndex: 'deposit_fee',
      key: 'deposit_fee',

    },
    {
      title: 'Additional Fee 1',
      dataIndex: 'additional_fee_1',
      key: 'additional_fee_1',

    }, {
      title: 'Additional Fee 2',
      dataIndex: 'additional_fee_2',
      key: 'additional_fee_2',

    },

    {
      title: 'Insurance Fee',
      dataIndex: 'insurance_fee',
      key: 'insurance_fee',


    }, {
      title: 'Order Price',
      dataIndex: 'price',
      key: 'price',

    }, {
      title: 'Order Price',
      dataIndex: 'price',
      key: 'price',

    },

    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',

    },
    {
      title: 'Checkin',
      dataIndex: 'check_in',
      key: 'check_in',
    },
    {
      title: 'Checkout',
      dataIndex: 'check_out',
      key: 'check_out',
    },
    {
      title: 'Status',
      dataIndex: 'payment_status',
      key: 'payment_status',

    },
    {
      title: 'Method',
      dataIndex: 'payment_method',
      key: 'payment_method',

    },
    {
      title: 'Vehicle',
      dataIndex: 'vehicle_type',
      key: 'vehicle_type',

    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',

    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
    }
  ];
  const column = [
    {
      title: '',
      dataIndex: 'id',
      key: 'id',
      render: text => <a>{}</a>,
      width: 20,
      fixed: 'left',

    },
    {
      title: 'Order ID',
      dataIndex: 'order_id',
      key: 'order_id',
      ellipsis: true,
      fixed: 'left',

    },
    {
      title: 'Transaction ID',
      dataIndex: 'id',
      key: 'id',
      ellipsis: true,
    },
    {
      title: 'Code',
      dataIndex: 'code',
      key: 'code',
      width: 80
    },
    {
      title: 'Customer',
      dataIndex: 'username',
      key: 'username',
      ellipsis: true,
    },
    {
      title: 'Location',
      dataIndex: 'parking_name',
      key: 'parking_name',
      ellipsis: true,
    },
    {
      title: 'Checkin',
      dataIndex: 'check_in',
      key: 'check_in',
      ellipsis: true,
      render: (record) => moment(record).format("DD/MM/YY, H:mm:ss")
    },
    {
      title: 'Checkout',
      dataIndex: 'check_out',
      key: 'check_out',
      ellipsis: true,
      render: (record) => moment(record).format("DD/MM/YY, H:mm:ss")

    },
    {
      title: 'Status',
      dataIndex: 'payment_status',
      key: 'payment_status',
      ellipsis: true,
      width: 80
    },
    {
      title: 'Method',
      dataIndex: 'payment_method',
      key: 'payment_method',
      ellipsis: true,
      width: 80
    },
    {
      title: 'Vehicle',
      dataIndex: 'vehicle_type',
      key: 'vehicle_type',
      ellipsis: true,
      width: 80
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      width: 80,
      ellipsis: true,
    },
    {
      title: 'Total',
      dataIndex: 'total',
      key: 'total',
      ellipsis: true,
    },
    {
      title: 'Action',
      key: 'operation',
      width: 65,
      fixed: "right",
      render: () => <Button size="small" style={{fontSize: 10, color: '#5469d4'}} onClick={() => {
      }}>Detail</Button>,
      className: 'align-center'
    },
  ];
  const columnInv = [
    {
      title: '',
      dataIndex: 'id',
      key: 'id',
      render: text => <a>{}</a>,
      width: 20,


    },
    {
      title: 'Date',
      dataIndex: 'check_in',
      key: 'check_in',
      width: 100,
      render: (record) => moment(record).format("DD/MM/YYYY")
    },
    {
      title: 'Biils Number',
      dataIndex: 'order_id',
      key: 'order_id',
      width:350,
      render: (text, record, index) => {
        return <span>{text} - {(record.payment_status === 'S') ?
            <Tag style={{fontSize: 10, border: 0, borderRadius: 50, color: "#3d4eac", lineHeight: '18px'}} color="#d6ecff">Paid</Tag>
            : <Tag style={{fontSize: 10, border: 0, borderRadius: 50, color: "#3d4eac", lineHeight: '18px'}} color="#d6ecff">Outstanding</Tag>
        }</span>
    }},
    {
      title: 'Amount',
      dataIndex: 'total',
      key: 'total',
      width:100
    },

    {
      title: 'Partner',
      dataIndex: 'parking_name',
      key: 'parking_name',
      ellipsis: true,
      align:'left'
    },



    {
      title: 'Action',
      key: 'operation',
      width: 65,
      fixed: "right",
      render: () => <Button size="small" style={{fontSize: 10, color: '#5469d4'}} onClick={() => {
      }}>Detail</Button>,
      className: 'align-center'
    },
  ];


  return <div>
    <Card bordered={false} style={{}} className={'shadow'} bodyStyle={{padding: '0'}}>
      <PageHeader
        className={'card-page-header'}
        title={<div style={{}}>
          <Search
            placeholder="FIlter by name or email"
            onSearch={value => console.log(value)}
            style={{width: 200, padding: '2.5px 11px', marginRight: 12}}
          />
          <Dropdown overlay={menu} trigger={['click']}>
            <Button type="dashed" icon={<FilterOutlined/>} size={'small'} style={{margin: 3}}>
              Filter
            </Button>
          </Dropdown>
        </div>}
        subTitle=" "
        extra={<Button>Download</Button>}
      />
      {store.transaction.calculatedData.forEach(({commision, u}) =>{
        let test = 0;
       test += commision

        console.log(test)
      })}
      <Table size={"small"} tableLayout={'fixed'} pagination={{pageSize: 10}}
             loading={store.transaction.calculatedData.length === 0}
             summary={pageData=> {
               let aa= 0;
               pageData.forEach(({commision}) =>{
                 aa += commision
               })

             }}
             dataSource={store.transaction.calculatedData} columns={columnInv}/>
    </Card>
  </div>
});
