import React, {useEffect, useState} from 'react';
import { Button, Modal, Form, Input, Radio,message } from 'antd';
import {observer} from "mobx-react-lite";

const CollectionCreateForm = ({ visible, onCreate, onCancel, confirmLoading, loading, footer  }) => {
  const [form] = Form.useForm();


  return (
    <Modal
      title="Create bill"
      width={'100%'}
      wrapClassName={'modal-full'}
      style={{top: 0, borderRadius: 0,}}
      className={'modal-draft'}
      bodyStyle={{
        borderRadius: 0,
        margin: '0px auto',
        paddingTop: 92,
        maxWidth: '1024px',
        minWidth: '760px'
      }}
      closable={false}
      maskClosable={false}
      visible={visible}
      confirmLoading={confirmLoading}
      onOk={() => {
      form
        .validateFields()
        .then(values => {
          form.resetFields();
          onCreate(values);
        })
        .catch(info => {
          console.log('Validate Failed:', info);
        });

    }}
      footer={[footer]}
      okText="Create"
      cancelText="Cancel"
      onCancel={onCancel}

    >
      <Form
        form={form}
        layout="vertical"
        name="form_in_modal"
        initialValues={{
          modifier: 'public',
        }}
      >
        <Form.Item
          name="title"
          label="Title"
          rules={[
            {
              required: true,
              message: 'Please input the title of collection!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item name="description" label="Description">
          <Input type="textarea" />
        </Form.Item>
        <Form.Item name="modifier" className="collection-create-form_last-form-item">
          <Radio.Group>
            <Radio value="public">Public</Radio>
            <Radio value="private">Private</Radio>
          </Radio.Group>
        </Form.Item>
      </Form>
    </Modal>
  );
};



export const AddBillModal = observer((props) => {
  const [visible, setVisible] = useState(false);
  const [loading, setLoading] = useState(false);
  const [text, setText] = useState('');
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState("");


  useEffect(() => {
    setModalText(modalText)
  }, []);

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false)
  };

  const handleOk = () => {
    setModalText('The modal will be closed after two seconds');

  };
   const onCreate = values => {
    console.log('Received values of form: ', values);
    setConfirmLoading(true);
     setTimeout(()=>{
       setConfirmLoading(false);
       setVisible(false)
       message.info('This is a normal message', values);


     },2000)

  };
  return (
    <div>
      <Button
        type="link"
         style={{padding:0, margin:0}}
         onClick={() => {
          setVisible(true);
        }}
      >
     KPI entry
      </Button>
      <CollectionCreateForm
        visible={visible}
        onCreate={onCreate}
        confirmLoading={confirmLoading}
        onCancel={ () => {
          setVisible(false);
        }}

        footer={[
          <div className={'ant-modal-footer-div'} style={{position: "fixed", zIndex: 2, right: 0, top: 0}}>
            <Button key="back" disabled={confirmLoading} onClick={() => setVisible(false)}>
              Cancel
            </Button>
            <Button key="submit" type="primary" loading={confirmLoading} onClick={onCreate}>
              Submit
            </Button>
          </div>
        ]}
      />
    </div>
  );
});
