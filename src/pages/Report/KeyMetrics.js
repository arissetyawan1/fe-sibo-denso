import React, {useEffect, useState} from "react";
import {
    Row,
    Col,
    Button,
    Avatar,
    Typography,
    Statistic,
    Card,
    Empty,
    PageHeader,
    List,
    Divider,
    Dropdown,
    Table,
    Menu,
} from "antd";
import {UploadOutlined} from "@ant-design/icons";
import {
    Link,
    useHistory,
    useRouteMatch,
    BrowserRouter as Router,
    Route,
} from "react-router-dom";
import {LINKS} from "../../routes";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStores";
import { createUseStyles } from "react-jss";
import * as _ from "lodash";

const {Meta} = Card;
const useStyle = createUseStyles({
    cardBodyKeyMetric: {
        background: "#fff",
        paddingTop: 30,
        paddingBottom: 20,
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
        boxShadow: '0 2px 5px 0 rgba(60, 66, 87, 0.12), 0 1px 1px 0 rgba(0, 0, 0, 0.12)'
    }, cardBodyKeyMetricHovered: {"&:hover": `background: #000;`},

})
export const KeyMetrics = observer((props) => {
    const store = useStore();
    useEffect(() => { loadData(); }, [])

    async function loadData() {
        await store.user.getUserID(store.userData.id);
    }

    const {Text} = Typography;
    const classes = useStyle()
    const keyMetrics = [
        // {
        //     name: "Executive Summary",
        //     link: `EXECUTIVE SUMMARY`,
        //     icon: "",
        //     type:"\u00A0"

        // },
        {
            name: "Daily Revenue",
            link: "DAILY REVENUE",
            icon: "",
            type: "operation",
            dept: "all",
        },
        {
            name: "Daily Traffic Volume",
            link: "DAILY TRAFFIC VOLUME",
            icon: "",
            type: "operation",
            dept: "all",

        },
        {
            name: "Average Vehicle KM",
            link: "AVERAGE VEHICLE KM TRAVELLED",
            icon: "",
            type: "operation",
            dept: "all",
        },
        {
            name: "Full Route Equivalent",
            link: "FULL ROUTE EQUIVALENTE",
            icon: "",
            type: "operation",
            dept: "all",
        },
        {
            name: "Accident",
            link: "ACCIDENT",
            icon: "",
            type: "operation",
            dept: "all",
        },
        {
            name: "Traffic Classification",
            link: "TRAFFIC CLASSIFICATION",
            icon: "",
            type: "operation",
            dept: "all",
        },
        {
            name: "Financial Summary",
            link: "FINANCIAL SUMMARY",
            icon: "",
            type: "finance",
            dept: "finance",
        },
        {
            name: "Cashflow",
            link: "CASHFLOW",
            icon: "",
            type: "finance",
            dept: "finance",
        },
        {
            name: "Balance Sheet",
            link: "BALANCE SHEET",
            icon: "",
            type: "finance",
            dept: "finance",
        },
    ];

    const cardKey = () => {
        let checkFinance = _.get(store.user.dataProfil, `position_user.position.organization.organization.name`);
        if ((store.userData.role === "super_admin") || (store.userData.role === "BOD") || (checkFinance === "FINANCE")) {
            return keyMetrics.map((i => {
                return  <Col span={store.ui.mediaQuery.isMobile? 24: 8} >
                    <Link to={LINKS[`${i.link}`]}>
                    <Card className={['small-shadow', 'no-padding', classes.cardBodyKeyMetricHovered]}
                            hoverable
                            bordered={false}
                            bodyStyle={{padding: 0}}
                            style={{textAlign: "center", padding: "30px 22px 4px 22px", background: i.type === 'finance' ? '#f04e23' : i.type === 'operation' ? '#58585b' : '#f04e23'}}>
                        <Meta
                            className={[classes.cardBodyKeyMetric,]}
                            // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<Text strong style={{margin: 0}}>{i.name}</Text>}
                            description={i.type}
                        />
                    </Card>
                </Link>
                </Col>
                }
            ))   
        } else {
            return _.filter(keyMetrics, i => i.dept === "all")?.map((i => {
                return  <Col span={store.ui.mediaQuery.isMobile? 24: 8} >
                    <Link to={LINKS[`${i.link}`]}>
                    <Card className={['small-shadow', 'no-padding', classes.cardBodyKeyMetricHovered]}
                            hoverable
                            bordered={false}
                            bodyStyle={{padding: 0}}
                            style={{textAlign: "center", padding: "30px 22px 4px 22px", background: i.type === 'finance' ? '#f04e23' : i.type === 'operation' ? '#58585b' : '#f04e23'}}>
                        <Meta
                            className={[classes.cardBodyKeyMetric,]}
                            // avatar={<Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />}
                            title={<Text strong style={{margin: 0}}>{i.name}</Text>}
                            description={i.type}
                        />
                    </Card>
                </Link>
                </Col>
                }
            ))
        }
    }

    return <div className="site-card-wrapper">
        <PageHeader
            className={'card-page-header'}
            title={"Report Key Metrics"}
            extra={store.userData.role !== "BOD" && <Link to={LINKS["KEY METRICS DATA"]}>
                <Button type={'primary'} icon={<UploadOutlined/>}>
                    Upload Data
                </Button>
            </Link>}
        />
        <div className={'cariparkir-container'}>
            <Row gutter={[24, 24]}>
                {cardKey()}
            </Row>
        </div>
    </div>
});