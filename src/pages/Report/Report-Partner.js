import React, {useEffect, useState} from "react";
import {
    Row,
    Col,
    Avatar,
    Button,
    Typography,
    Statistic,
    Card,
    Empty,
    PageHeader,
    List,
    Divider,
    Dropdown,
    Table,
    Menu,
    Tag
} from "antd";
import {
    FilterOutlined,
    PlusOutlined
} from "@ant-design/icons";
import {Link, useHistory, useRouteMatch, BrowserRouter as Router, Route} from "react-router-dom";
import {LINKS} from "../../routes";
import {observer} from "mobx-react-lite";
import {Partner2WRoute} from "../Partner/Partner2w/Partner2WRoute";
import {ReportRoute} from "./route";
import {TransactionRoute} from "../Transaction/route";
import {Transaction} from "../Transaction/Transaction";
import {TransactionPartnerDetail} from "../Transaction/Transaction-Partner-Detail";

const {Title} = Typography;
const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    margin: '-1px -1px 0 0',
    height: 184,
    transition: 'transform .4s'
};

const optionFilter = [
    {
        key: 1,
        value: 'Enabled'
    }, {
        key: 2,
        value: 'Disabled'
    },
];

function handleMenuClick(e) {
    console.log('click', e);
}

const dataHeader = [{name: 'Invoices'}, {name: 'Deposits'}];
const dataBalances = [{name: 'Accounts', link: '/app/balances/accounts'}];
const dataTransaction = [{name: 'Operation',}];
const dataList = [{name: 'Users'}, {name: 'Roles',}, {name: 'Partners',}, {name: 'Commissions'}];

const data = [{menu: 'Invoice', link: ''}]

export const ReportPartner = observer(() => {
    let reportRouteMatch = useRouteMatch(LINKS.REPORT);
    let reportRouteNotMatch = useRouteMatch(LINKS.TRANSACTION_OPERATION);
    console.log({reportRouteMatch, reportRouteNotMatch}, "match");

    return <div className={['cariparkir-container'].join(' ')}>
        <Card bordered={false}
              style={{overflow: 'hidden'}}
              className={"shadow"}
              bodyStyle={{background: "#f7fafc", marginRight: '-1px', padding: 0}}>
            <PageHeader
              className={'card-page-header'}
              ghost={false}
              title={<span style={{fontSize: 20}}>Reports</span>}
              subTitle=""
            />
            <div
              style={{
                  minHeight: 'calc(100vh - 450px)',
                  paddingTop: 10,
                  paddingLeft: 24,
                  paddingRight: 24
              }}>
                <Row gutter={[16, 16]}>
                    <Col xs={20} sm={16} md={12} lg={8} xl={6}>
                        <List
                          size="small"
                          dataSource={dataBalances}
                          header={<Title level={4} style={{
                              fontSize: 13,
                              textTransform: 'uppercase',
                              fontWeight: 400,
                              marginBottom: 0
                          }}>Balances</Title>}
                          bordered={false}
                          renderItem={item => (
                            <List.Item style={{paddingLeft: 0}}>
                                <Link to={item.link} style={{fontWeight: 400}}>{item.name}</Link>
                            </List.Item>
                          )}
                        />
                    </Col>
                    <Col xs={20} sm={16} md={12} lg={8} xl={6}>
                        <List
                          size="small"
                          dataSource={dataHeader}
                          header={<Title level={4} style={{
                              fontSize: 13,
                              textTransform: 'uppercase',
                              fontWeight: 400,
                              marginBottom: 0
                          }}>Cash in-out</Title>}
                          bordered={false}
                          renderItem={item => (
                            <List.Item style={{paddingLeft: 0}}>
                                <Link to={'/'} style={{fontWeight: 400}}>{item.name}</Link>
                            </List.Item>
                          )}
                        />
                    </Col>

                    <Col xs={20} sm={16} md={12} lg={8} xl={6}>
                        <List
                          size="small"
                          dataSource={dataTransaction}
                          header={<Title level={4} type={'uppercase'} style={{
                              textTransform: 'uppercase',
                              fontSize: 13,
                              fontWeight: 400,
                              marginBottom: 0
                          }}>Transactions</Title>}
                          bordered={false}
                          renderItem={item => (
                            <List.Item style={{paddingLeft: 0}}>
                                <Link to="/app/reports/transaction_operation"
                                      style={{fontWeight: 400}}>{item.name}</Link>
                            </List.Item>
                          )}
                        />
                    </Col>
                </Row>
            </div>
        </Card>
    </div>;

    // return <div>
    //     {(reportRouteMatch && reportRouteMatch.isExact) && }
    //     {(reportRouteNotMatch && reportRouteNotMatch.isExact) && <Transaction/>}
    // </div>
});
