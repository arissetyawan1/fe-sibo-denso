import React, {useEffect, useState} from "react";
import {
    Button, Form, Input, List, Space, Statistic, Table, Tag, Modal,
    Select, DatePicker, Card, Dropdown, PageHeader, Affix, Divider
} from "antd";
import {CreditCardTwoTone, FilterOutlined, PlusOutlined, SearchOutlined, WalletTwoTone} from "@ant-design/icons";
import {Link} from "react-router-dom";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStores";
import Highlighter from "react-highlight-words";
import ClockCircleOutlined from "@ant-design/icons/lib/icons/ClockCircleOutlined";
import moment from 'moment';

const {Option} = Select;
const {RangePicker} = DatePicker;

export const TransactionPartner = observer(() => {
    const store = useStore();
    const [form] = Form.useForm();
    const [visible, setVisible] = useState(false);
    const [calculatedData, setcalculatedData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [disabled, setDisabled] = useState(false);
    const [checkPartner, setCheckPartner] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const [filterDialog, setFilterDialog] = useState(false);
    const [formFilter, setFormFilter] = useState(null)
    const [top, setTop] = useState(70);

    useEffect(() => {
        form.validateFields(["partner_name"]);
    }, [checkPartner, form]);

    const onCheckboxChange = e => {
        setCheckPartner(e.target.checked);
    };

    const onCheck = async () => {
        try {
            const values = await form.validateFields();
            console.log('Success:', values);
        } catch (errorInfo) {
            console.log('Failed:', errorInfo);
        }
    };
    const onGenderChange = value => {
        switch (value) {
            case 'male':
                form.setFieldsValue({
                    note: 'Hi, man!',
                });
                return;

            case 'female':
                form.setFieldsValue({
                    note: 'Hi, lady!',
                });
                return;

            case 'other':
                form.setFieldsValue({
                    note: 'Hi there!',
                });
        }
    };
    useEffect(() => {

        async function init() {
            await store.mitra.getAll();
            await store.transaction.getAll();
            await store.commision.getAll();
        }

        if (visible) {
            form.setFieldsValue({name: "form_in_modal"});
        }

        init(calculatedData).then(
          res =>
          console.log(store.transaction.calculatedData, "store.transaction.calculatedData"));
    }, [visible, form]);


    const onPartnerChange = value => {
        if (value) {
            console.log(value);
            form.setFieldsValue({
                    parking_name: `${value}`
                }
            )
        }
    };

    function onChange(value) {
        console.log(`selected ${value}`);
    }

    function onBlur() {
        console.log('blur');
    }

    function onFocus() {
        console.log('focus');
    }

    function onSearch(val) {
        console.log('search:', val);
    }

    function handleChange(value) {
        console.log(`selected ${value}`);
    }

    const onFinish = values => {
        console.log(values);
    };

    const onReset = () => {
        form.resetFields();
    };

    const onFill = () => {
        form.setFieldsValue({
            note: 'Hello world!',
            gender: 'male',
        });
    };


    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
            <div style={{padding: 8}}>
                <Input
                    ref={node => {
                        const searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{width: 188, marginBottom: 8, display: 'block'}}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined/>}
                        size="small"
                        style={{width: 90}}
                    >
                        Search
                    </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{width: 90}}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
        onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            // if (visible) {
            //   setTimeout(() =>  {
            //
            //   this.searchInput.select()
            //   });
            // }
        },
        render: text =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : (
                text
            ),
    });
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);

    };
    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('');
    };

    const columns = [
        {
            title: 'Code',
            dataIndex: 'code',
            key: 'code',
            ellipsis: true,
            fixed: 'left',
        },
        {
            title: 'Order ID',
            dataIndex: 'order_id',
            key: 'order_id',

            ellipsis: true,
        }, {
            title: 'Transaction ID',
            dataIndex: 'id',
            key: 'id',
            ellipsis: true,
        },
        {
            title: 'Code',
            dataIndex: 'code',
            key: 'code',
            // width: 80
        },
        {
            title: 'Customer',
            dataIndex: 'username',
            key: 'username',
            ellipsis: true,
            ...getColumnSearchProps('username')
        },
        {
            title: 'Location',
            dataIndex: 'parking_name',
            key: 'parking_name',
            ellipsis: true,
        },
        {
            title: 'Checkin',
            dataIndex: 'check_in',
            key: 'check_in',
            ellipsis: true,
            render: text => moment(text, 'x').format('DD/MM/YY HH:mm')
        },
        {
            title: 'Checkout',
            dataIndex: 'check_out',
            key: 'check_out',
            ellipsis: true,
            render: text => moment(text, 'x').format('DD/MM/YY HH:mm')
        },
        {
            title: 'Status',
            dataIndex: 'payment_status',
            key: 'payment_status',
            ellipsis: true,
            // width: 80
        },
        {
            title: 'Method',
            dataIndex: 'payment_method',
            key: 'payment_method',
            ellipsis: true,
            // width: 80
        },
        {
            title: 'Vehicle',
            dataIndex: 'vehicle_type',
            key: 'vehicle_type',
            ellipsis: true,
            // width: 80
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
            // width: 80,
            ellipsis: true,
        },
        {
            title: 'Total',
            dataIndex: 'total',
            key: 'total',
            ellipsis: true,
        },
        {
            title: 'Commision',
            dataIndex: 'commision',
            key: 'commision',
            ellipsis: true,
        },
        {
            title: 'Commision Tax',
            dataIndex: 'commisiontax',
            key: 'commisiontax',
            ellipsis: true,
        }
    ];

    const parking_type = [
        {parking_type: 'M', name: 'Motorcycle'},
        {parking_type: 'C', name: 'Car'},
        {parking_type: 'B', name: 'Bicycle'},
    ]

    const handleOk = () => {
        setLoading(false);
        setTimeout(() => {
            setVisible(false)
        }, 3000);
    };


    const onCreate = value => {
        console.log('Received values of form: ', value);
        setVisible(false);
    };

    // let reportRouteMatch = useRouteMatch(LINKS.TRANSACTION_OPERATION);
    // let reportRouteNotMatch = useRouteMatch(LINKS.TRANSACTION_OPERATION_DETAIL);
    //
    // console.log(reportRouteMatch, reportRouteNotMatch, "yoho");
    //
    // if (reportRouteNotMatch && reportRouteNotMatch.isExact) {
    //     return <TransactionPartnerDetail/>
    // }

    return <div className={store.ui.mediaQuery.isDesktop ? 'cariparkir-container' : null}>
        <Modal
            onCancel={() => setFilterDialog(false)}
            onOk={() => {
                formFilter.submit()
            }}
            afterClose={() => setFilterDialog(false)}
            title={"Filter"}
            visible={filterDialog}>

            <Form
                ref={elem => setFormFilter(elem)}
                onFinish={values => {
                    console.log({values}, ' -> ')
                    setFilterDialog(false);
                    // store.transaction.query = {
                    //     ...store.transaction.query,
                    //     vehicle_type: values.vehicle_type,
                    // };

                    if (values.vehicle_type) {
                        store.transaction.query.vehicle_type = values.vehicle_type
                    }

                    if (values.date_created) {
                        store.transaction.query.filter = 'range'
                        store.transaction.query.date_created = values.date_created.map(d => d.format('YYYY-MM-DD')).join(',')
                    }

                    store.transaction.getAll();
                }}>
                <Form.Item
                    label="Vehicle Type"
                    name="vehicle_type"
                >
                    <Select>
                        <Option value={"C"}>Car</Option>
                        <Option value={"M"}>Motorcycle</Option>
                        <Option value={"V"}>Valet</Option>
                    </Select>
                </Form.Item>

                <Form.Item
                    label="Date"
                    name="date_created"
                >
                    <RangePicker/>
                </Form.Item>
            </Form>

        </Modal>

        {store.ui.mediaQuery.isMobile && <div>
            <Affix offsetTop={top}>
                <Card bordered={true}
                      bodyStyle={{padding: '0'}}
                      style={{
                          borderRadius: store.ui.mediaQuery.isDesktop ? '6px 0px' : 0
                      }}>
                    <PageHeader
                        className={'card-page-header'}
                        style={{
                            padding: '6px 8px',
                        }}
                        title={<Button
                            icon={<FilterOutlined/>}
                            size={'small'}
                            style={{margin: 3}}
                            onClick={() => setFilterDialog(true)}>
                            Filter
                        </Button>}
                        subTitle=""
                    />
                </Card>
            </Affix>
            {store.ui.mediaQuery.isDesktop && (
                <React.Fragment>
                    <Table size={"small"}
                           loading={(store.transaction.calculatedData.length === 0)}
                           scroll={{x: 1500, y: 300}}
                           dataSource={store.transaction.calculatedData}
                           columns={columns}/>
                </React.Fragment>
            )}

            {store.ui.mediaQuery.isMobile &&
            <List
                itemLayout="horizontal"
                loading={(store.transaction.calculatedData.length === 0)}
                position={'top'}
                dataSource={store.transaction.calculatedData}
                style={{padding: 0}}
                renderItem={item => (
                    <Link to={`/app/reports/transaction_operation/${item.id}`}>
                        <List.Item key={item.id} style={{
                            backgroundColor: '#f6f6f6',
                            paddingTop: 0,
                            paddingBottom: 0,
                            display: 'flex',
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <List.Item.Meta
                                className={['cariparkir-container'].join(' ')}
                                title={<h3 style={{marginBottom: 0}}>{item.code}</h3>}
                                description={<div style={{
                                }}>
                                    <p>{item.parking_name} {item.parking_type} {item.reg_code}</p>
                                    {/*<Tag style={{fontSize: 9, marginRight: 10, fontWeight: 500}}*/}
                                    {/*     icon={<ClockCircleOutlined style={{fontSize: 9}}/>}*/}
                                    {/*     color="default">*/}
                                    {/*    {item.parking_name} {item.parking_type} {item.reg_code}*/}
                                    {/*</Tag>*/}

                                    {/*{item.payment_method.toLowerCase() === 'gopay' && <img*/}
                                    {/*    height={13}*/}
                                    {/*    alt="logo"*/}
                                    {/*    src={gopayLogo}*/}
                                    {/*/>}*/}

                                    {/*<Tag style={{fontSize: 9}} icon={<ClockCircleOutlined style={{fontSize: 9}}/>}*/}
                                    {/*     color="processing">*/}
                                    {/*    {item.payment_method} {item.payment_status} {item.price}*/}
                                    {/*</Tag>*/}
                                </div>}
                            />
                            <div style={{marginRight: 16,}}>
                                <Statistic
                                    title={<p style={{
                                        fontSize: 9,
                                        margin: 0
                                    }}>{moment(item.date_create).format("DD MMM YY, H:mm:ss")}</p>}
                                    precision={2}
                                    prefix={'Rp'}
                                    // suffix={item.incentive.incentive_type === 'percentage' ? '%' : ''}
                                    style={{fontSize: 12, fontWeight: 300}}
                                    valueStyle={{color: "#5469d4", fontSize: 12, fontWeight: 600, textAlign: 'right'}}
                                    value={item.total}/>
                                <div style={{textAlign: 'right'}}>
                                    {item.payment_method.toLowerCase() === 'gopay' &&
                                    <WalletTwoTone type="message" theme="outlined" style={{fontSize: 13}}/>}
                                    {item.payment_method.toLowerCase() !== 'gopay' &&
                                    <CreditCardTwoTone type="message" theme="outlined" style={{fontSize: 13}}/>}
                                </div>
                            </div>
                        </List.Item>
                        <Divider plain style={{margin: 0}}/>
                    </Link>
                )}
            />}
        </div>}

        {store.ui.mediaQuery.isDesktop && <Card bordered={true} bodyStyle={{padding: '0'}}>
            <PageHeader
                className={'card-page-header'}
                style={{
                    padding: '6px 8px',
                }}
                title={<Button
                    icon={<FilterOutlined/>}
                    size={'small'}
                    style={{margin: 3}}
                    onClick={() => setFilterDialog(true)}>
                    Filter
                </Button>}
                subTitle=""
            />

            {store.ui.mediaQuery.isDesktop && (
                <React.Fragment>
                    <Table size={"small"}
                           loading={(store.transaction.calculatedData.length === 0)}
                           scroll={{x: 1500, y: 300}}
                           dataSource={store.transaction.calculatedData}
                           columns={columns}/>
                </React.Fragment>
            )}

            {store.ui.mediaQuery.isMobile &&
            <List
                itemLayout="horizontal"
                loading={(store.transaction.calculatedData.length === 0)}
                position={'top'}
                dataSource={store.transaction.calculatedData}
                style={{padding: 0}}
                renderItem={item => (
                    <List.Item key={item.id} style={{backgroundColor: '#f6f6f6', paddingTop: 0}}>
                        <List.Item.Meta
                            className={['cariparkir-container'].join(' ')}
                            title={<Link to={`/app/reports/transaction_operation/${item.id}`}>{item.code}</Link>}
                            description={<div style={{
                                display: 'flex',
                                flexDirection: 'row',
                                alignItems: 'stretch',
                                justifyContent: 'flex-start'
                            }}>
                                <p>{item.parking_name} {item.parking_type} {item.reg_code}</p>
                                {/*<Tag style={{fontSize: 9, marginRight: 10, fontWeight: 500}}*/}
                                {/*     icon={<ClockCircleOutlined style={{fontSize: 9}}/>}*/}
                                {/*     color="default">*/}
                                {/*    {item.parking_name} {item.parking_type} {item.reg_code}*/}
                                {/*</Tag>*/}

                                {/*{item.payment_method.toLowerCase() === 'gopay' && <img*/}
                                {/*    height={13}*/}
                                {/*    alt="logo"*/}
                                {/*    src={gopayLogo}*/}
                                {/*/>}*/}

                                {/*<Tag style={{fontSize: 9}} icon={<ClockCircleOutlined style={{fontSize: 9}}/>}*/}
                                {/*     color="processing">*/}
                                {/*    {item.payment_method} {item.payment_status} {item.price}*/}
                                {/*</Tag>*/}
                            </div>}
                        />
                        <div style={{marginRight: 16, alignSelf: 'flex-end'}}>
                            <Statistic
                                title={<p style={{
                                    fontSize: 9,
                                    margin: 0
                                }}>{moment(item.date_create).format("DD MMM YY, H:mm:ss")}</p>}
                                precision={2}
                                prefix={'Rp'}
                                // suffix={item.incentive.incentive_type === 'percentage' ? '%' : ''}
                                style={{fontSize: 12, fontWeight: 300}}
                                valueStyle={{color: "#5469d4", fontSize: 12, fontWeight: 600, textAlign: 'right'}}
                                value={item.total}/>
                            <div style={{textAlign: 'right'}}>
                                {item.payment_method.toLowerCase() === 'gopay' &&
                                <WalletTwoTone type="message" theme="outlined" style={{fontSize: 13}}/>}
                                {item.payment_method.toLowerCase() !== 'gopay' &&
                                <CreditCardTwoTone type="message" theme="outlined" style={{fontSize: 13}}/>}
                            </div>
                        </div>
                    </List.Item>
                )}
            />}
        </Card>}
    </div>
});
