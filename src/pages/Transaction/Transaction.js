import React from "react";
import {
  Menu,
  Dropdown,
  Input,
  Col,
  Row,
  Select,
  Button,
  PageHeader,
  InputNumber,
  AutoComplete,
  DatePicker,
  Cascader,
  Statistic,
  Descriptions,
  Typography,
  Space,
  Card,
  Avatar
} from 'antd';
import {DownOutlined, FilterOutlined} from '@ant-design/icons';
import {createUseStyles} from "react-jss";
// import ParticlesBg from 'particles-bg';
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStores";
import {TransactionRoute} from "./route";
const {Title, Paragraph} = Typography;
const {Meta} = Card;


const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const useStyles = createUseStyles({
  gridStyle : {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    height:200,
    transition: 'transform .4s',
    boxShadow :'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'

  },
  gridStyleHeaderRight : {
    overflow: 'hidden',
    height:200/2,
    position: 'relative',
    padding: '16px 15px 8px',
    transition: 'transform .4s',
    boxShadow :'inset 0 -1px #e3e8ee'
  },
});

const gridStyle = {
  overflow: 'hidden',
  position: 'relative',
  padding: '16px 15px 8px',
  height: 84,
  transition: 'transform .4s',
  boxShadow :'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'
};

const {Option} = Select;

const options = [
  {
    value: 'All Time',
    label: 'Zhejiang',
    children: [
      {
        value: 'hangzhou',
        label: 'Hangzhou',
        children: [
          {
            value: 'xihu',
            label: 'West Lake',
          },
        ],
      },
    ],
  },
  {
    value: 'jiangsu',
    label: 'Jiangsu',
    children: [
      {
        value: 'nanjing',
        label: 'Nanjing',
        children: [
          {
            value: 'zhonghuamen',
            label: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    value: 'Today'
  }
];

const optionFilter = [
  {
    key: 1,
    value: 'Today'
  }, {
    key: 2,
    value: 'Last 3 days'
  }, {
    key: 3,
    value: 'Last 6 months'
  }, {
    key: 4,
    value: 'Last 12 months'
  }, {
    key: 5,
    value: 'Month to date'
  }, {
    key: 6,
    value: 'Quarter to date'
  }, {
    key: 7,
    value: 'Year to date'
  }, {
    key: 8,
    value: 'All time'
  },

];


function handleMenuClick(e) {
  console.log('click', e);
}

export const Transaction = observer(() => {
  return <TransactionRoute/>
});
