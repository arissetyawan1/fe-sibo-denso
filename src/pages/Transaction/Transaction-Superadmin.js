import React, {useEffect, useState} from "react";
import {
    Row,
    Col,
    Avatar,
    Button,
    Typography,
    Statistic,
    Card,
    Empty,
    PageHeader,
    List,
    Divider,
    Dropdown,
    Table,
    Menu, Space, Select
} from "antd";
import {
    FilterOutlined,
    PlusOutlined
} from "@ant-design/icons";
import {Link, useHistory, useRouteMatch, BrowserRouter as Router, Route} from "react-router-dom";
import {LINKS} from "../../routes";
import {observer} from "mobx-react-lite";
import {createUseStyles} from "react-jss";
import {useStore} from "../../utils/useStores";

const {Title, Paragraph} = Typography;
const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const useStyles = createUseStyles({
    gridStyle: {
        overflow: 'hidden',
        position: 'relative',
        padding: '16px 15px 8px',
        height: 200,
        transition: 'transform .4s',
        boxShadow: 'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'

    },
    gridStyleHeaderRight: {
        overflow: 'hidden',
        height: 200 / 2,
        position: 'relative',
        padding: '16px 15px 8px',
        transition: 'transform .4s',
        boxShadow: 'inset 0 -1px #e3e8ee'
    },
});

const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    height: 84,
    transition: 'transform .4s',
    boxShadow: 'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'
};

const {Option} = Select;

const options = [
    {
        value: 'All Time',
        label: 'Zhejiang',
        children: [
            {
                value: 'hangzhou',
                label: 'Hangzhou',
                children: [
                    {
                        value: 'xihu',
                        label: 'West Lake',
                    },
                ],
            },
        ],
    },
    {
        value: 'jiangsu',
        label: 'Jiangsu',
        children: [
            {
                value: 'nanjing',
                label: 'Nanjing',
                children: [
                    {
                        value: 'zhonghuamen',
                        label: 'Zhong Hua Men',
                    },
                ],
            },
        ],
    },
    {
        value: 'Today'
    }
];

const optionFilter = [
    {
        key: 1,
        value: 'Today'
    }, {
        key: 2,
        value: 'Last 3 days'
    }, {
        key: 3,
        value: 'Last 6 months'
    }, {
        key: 4,
        value: 'Last 12 months'
    }, {
        key: 5,
        value: 'Month to date'
    }, {
        key: 6,
        value: 'Quarter to date'
    }, {
        key: 7,
        value: 'Year to date'
    }, {
        key: 8,
        value: 'All time'
    },
];

function handleMenuClick(e) {
    console.log('click', e);
}

function handleChange(value) {
    console.log(`selected ${value}`);
}

const menu = (
    <Menu onClick={handleMenuClick} style={{background: '#fff'}}>
        {optionFilter.map((i => {
            return <Menu.Item key={i.key}>{i.value}</Menu.Item>
        }))
        }
    </Menu>
);
const optionList = (
    optionFilter.map(((i, index) => {
        return <Option key={index} value={i.value}>{i.value}</Option>
    }))

);

export const TransactionSuperadmin = observer(() => {
    const store = useStore();
    const classes = useStyles();
    return <div>
      <p>test</p>
    </div>
});
