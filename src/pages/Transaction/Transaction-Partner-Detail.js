import React, {useEffect, useState} from "react";
import {
    Row,
    Col,
    Avatar,
    Button,
    Typography,
    Statistic,
    Card,
    Empty,
    PageHeader,
    Divider,
    Dropdown,
    Table,
    Menu,
    Space,
    Select,
    Form,
    Input,
  Descriptions,
    List,
    Tag
} from "antd";
import {
    CreditCardTwoTone,
    FilterOutlined,
    PlusOutlined,
    SearchOutlined,
    WalletTwoTone
} from "@ant-design/icons";
import {Link, useHistory, useParams, useRouteMatch, BrowserRouter as Router, Route} from "react-router-dom";
import {LINKS} from "../../routes";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStores";
import {createUseStyles} from "react-jss";
import Highlighter from "react-highlight-words";
import ClockCircleOutlined from "@ant-design/icons/lib/icons/ClockCircleOutlined";
import gopayLogo from '../../assets/images/gopay.png';
import WalletOutlined from "@ant-design/icons/lib/icons/WalletOutlined";
import moment from 'moment';
import TagsTwoTone from "@ant-design/icons/lib/icons/TagsTwoTone";
const {Paragraph} = Typography;
const useStyles = createUseStyles({
    gridStyle: {
        overflow: 'hidden',
        position: 'relative',
        padding: '16px 15px 8px',
        height: 200,
        transition: 'transform .4s',
        boxShadow: 'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'

    },
    gridStyleHeaderRight: {
        overflow: 'hidden',
        height: 200 / 2,
        position: 'relative',
        padding: '16px 15px 8px',
        transition: 'transform .4s',
        boxShadow: 'inset 0 -1px #e3e8ee'
    },
});

const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    height: 84,
    transition: 'transform .4s',
    boxShadow: 'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'
};

export const TransactionPartnerDetail = observer(() => {
    const store = useStore();
    const {id} = useParams();
    const [form] = Form.useForm();
    const [visible, setVisible] = useState(false);
    const [calculatedData, setcalculatedData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [disabled, setDisabled] = useState(false);
    const [checkPartner, setCheckPartner] = useState(false);
    const [searchText, setSearchText] = useState('');
    const [searchedColumn, setSearchedColumn] = useState('');
    const classes = useStyles();
    useEffect(() => {

        async function init() {
            await store.transaction.getAll();
            await store.commision.getAll();
        }

        if (visible) {
            form.setFieldsValue({name: "form_in_modal"});
        }

        init(calculatedData).then(res => console.log(res, calculatedData));

    }, [visible, form]);
    useEffect(() => {
        console.log(id, "paramssss");
        if (id) {
            store.ui.setParams({id});

        }
        form.validateFields(["partner_name"]);
    }, [checkPartner, form]);

    const onCheckboxChange = e => {
        setCheckPartner(e.target.checked);
    };

    const onCheck = async () => {
        try {
            const values = await form.validateFields();
            console.log('Success:', values);
        } catch (errorInfo) {
            console.log('Failed:', errorInfo);
        }
    };
    const onGenderChange = value => {
        switch (value) {
            case 'male':
                form.setFieldsValue({
                    note: 'Hi, man!',
                });
                return;

            case 'female':
                form.setFieldsValue({
                    note: 'Hi, lady!',
                });
                return;

            case 'other':
                form.setFieldsValue({
                    note: 'Hi there!',
                });
        }
    };



    const onPartnerChange = value => {
        if (value) {
            console.log(value);
            form.setFieldsValue({
                    parking_name: `${value}`
                }
            )
        }
    };

    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({setSelectedKeys, selectedKeys, confirm, clearFilters}) => (
            <div style={{padding: 8}}>
                <Input
                    ref={node => {
                        const searchInput = node;
                    }}
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
                    style={{width: 188, marginBottom: 8, display: 'block'}}
                />
                <Space>
                    <Button
                        type="primary"
                        onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
                        icon={<SearchOutlined/>}
                        size="small"
                        style={{width: 90}}
                    >
                        Search
                    </Button>
                    <Button onClick={() => handleReset(clearFilters)} size="small" style={{width: 90}}>
                        Reset
                    </Button>
                </Space>
            </div>
        ),
        filterIcon: filtered => <SearchOutlined style={{color: filtered ? '#1890ff' : undefined}}/>,
        onFilter: (value, record) =>
            record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
        onFilterDropdownVisibleChange: visible => {
            // if (visible) {
            //   setTimeout(() =>  {
            //
            //   this.searchInput.select()
            //   });
            // }
        },
        render: text =>
            searchedColumn === dataIndex ? (
                <Highlighter
                    highlightStyle={{backgroundColor: '#ffc069', padding: 0}}
                    searchWords={[searchText]}
                    autoEscape
                    textToHighlight={text.toString()}
                />
            ) : (
                text
            ),
    });
    const handleSearch = (selectedKeys, confirm, dataIndex) => {
        confirm();
        setSearchText(selectedKeys[0]);
        setSearchedColumn(dataIndex);

    };
    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('');
    };

    const columns = [
        {
            title: 'Order ID',
            dataIndex: 'order_id',
            key: 'order_id',
            ellipsis: true,
            fixed: 'left',

        },
        {
            title: 'Transaction ID',
            dataIndex: 'id',
            key: 'id',
            ellipsis: true,
        },
        {
            title: 'Code',
            dataIndex: 'code',
            key: 'code',
            // width: 80
        },
        {
            title: 'Customer',
            dataIndex: 'username',
            key: 'username',
            ellipsis: true,
            ...getColumnSearchProps('username')
        },
        {
            title: 'Location',
            dataIndex: 'parking_name',
            key: 'parking_name',
            ellipsis: true,
        },
        {
            title: 'Checkin',
            dataIndex: 'check_in',
            key: 'check_in',
            ellipsis: true,
        },
        {
            title: 'Checkout',
            dataIndex: 'check_out',
            key: 'check_out',
            ellipsis: true,
        },
        {
            title: 'Status',
            dataIndex: 'payment_status',
            key: 'payment_status',
            ellipsis: true,
            // width: 80
        },
        {
            title: 'Method',
            dataIndex: 'payment_method',
            key: 'payment_method',
            ellipsis: true,
            // width: 80
        },
        {
            title: 'Vehicle',
            dataIndex: 'vehicle_type',
            key: 'vehicle_type',
            ellipsis: true,
            // width: 80
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
            // width: 80,
            ellipsis: true,
        },
        {
            title: 'Total',
            dataIndex: 'total',
            key: 'total',
            ellipsis: true,
        },
        {
            title: 'Commision',
            dataIndex: 'commision',
            key: 'commision',
            ellipsis: true,
        },
        {
            title: 'Commision Tax',
            dataIndex: 'commisiontax',
            key: 'commisiontax',
            ellipsis: true,
        }
    ];
    const parking_type = [
        {parking_type: 'M', name: 'Motorcycle'},
        {parking_type: 'C', name: 'Car'},
        {parking_type: 'B', name: 'Bicycle'},
    ]

    const handleOk = () => {
        setLoading(false);
        setTimeout(() => {
            setVisible(false)
        }, 3000);
    };


    const onCreate = value => {
        console.log('Received values of form: ', value);
        setVisible(false);
    };

    const card = id => {
   store.transaction.calculatedData.map((i, index) => {
       console.log(i)
   });
        return(<Card
        bordered={false}
        style={{overflow: 'hidden'}}
        className={"shadow"}
        title={"Parkiran Astra"}
        bodyStyle={{background: "#f7fafc", marginRight: '-1px', padding: 0}}
      >
              <Descriptions
                bordered
                column={{ xxl: 4, xl: 3, lg: 3, md: 3, sm: 2, xs: 1 }}
              >

                  <Descriptions.Item label="Order Id">CP-20200424-8v6K</Descriptions.Item>
                  <Descriptions.Item label="Type">Motorcycle</Descriptions.Item>
                  <Descriptions.Item label="Billing"><WalletTwoTone type="message" theme="outlined" style={{fontSize: 21}}/> <span>Gopay</span></Descriptions.Item>
                  <Descriptions.Item label="Promo">Promo Roda Dua</Descriptions.Item>
                  <Descriptions.Item label="Amount">Rp.2000,00</Descriptions.Item>
                  <Descriptions.Item label="Discount">Rp.1000,00</Descriptions.Item>
                  <Descriptions.Item label="Total">Rp.1000,00</Descriptions.Item>
                  <Descriptions.Item label="Config Info">
                      Data disk type: MongoDB
                      <br />
                      Database version: 3.4
                      <br />
                      Package: dds.mongo.mid
                      <br />
                      Storage space: 10 GB
                      <br />
                      Replication factor: 3
                      <br />
                      Region: East China 1
                  </Descriptions.Item>
              </Descriptions>

                </Card>
    )};


    return <div>
        {store.ui.mediaQuery.isDesktop &&
        <Table size={"small"} loading={(store.transaction.calculatedData.length === 0)} scroll={{x: 1500, y: 300}}
               dataSource={store.transaction.calculatedData} columns={columns}/>}

        {store.ui.mediaQuery.isMobile && <div className={['cariparkir-container'].join(' ')}>
            { card()}
        </div>

        }
    </div>
});
