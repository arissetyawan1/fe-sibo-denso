import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../routes";
import {TransactionSuperadmin} from "./Transaction-Superadmin";
import {TransactionPartner} from "./Transaction-Partner";
import {useStore} from "../../utils/useStores";
import {observer} from "mobx-react-lite";
import {AnimatedSwitch} from "react-router-transition";

export const TransactionRoute =  observer(() => {
    const store = useStore();
    return (
        <AnimatedSwitch
            atEnter={{opacity: 0}}
            atLeave={{opacity: 0}}
            atActive={{opacity: 1}}
            className="switch-wrapper-app">
            {store.authentication.userData.role === 'Superadmin' && <Route path={LINKS.TRANSACTION_OPERATION}>
                <TransactionSuperadmin/>
            </Route>}
            {store.authentication.userData.role === 'Partner' && <Route path={LINKS.TRANSACTION_OPERATION}>
                <TransactionPartner/>
            </Route>}

        </AnimatedSwitch>
    )
});
