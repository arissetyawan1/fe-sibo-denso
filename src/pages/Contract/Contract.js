import React, { useEffect, Fragment, useState } from "react";
import "antd/dist/antd.css";
import {
  Table,
  Input,
  Button,
  Tooltip,
  Icon,
  Typography,
  Modal,
  Breadcrumb,
  Space,
  Upload,
  Row,
  Col,
  PageHeader,
  Form,
  Select,
  DatePicker,
  Layout,
} from "antd";
import {
  PlusSquareOutlined,
  SearchOutlined,
  HomeOutlined,
  FilterOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import { observer } from "mobx-react";
import { Link, useHistory } from "react-router-dom";
import { LINKS } from "../../routes";
import { useStore } from "../../utils/useStores";
import { get } from "lodash";
import { ContractStore } from "../../store/contracts";
import moment from "moment";
import { TableView } from "../../component/Table/TableView";
import { ModalFilter } from "../../component/Modal/ModalFilter";
export const Contract = observer((initialData) => {
  const store = useStore();
  const {
    contracts: contractStore,
    division: divisionStore,
    position: positionStore,
  } = useStore();
  const [dataSource, setDataSource] = useState(null);
  const history = useHistory();
  const { Option } = Select;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const defaultModel = "contract";
  const defaultColumn = ".created_at";
  const [order, setOrder] = useState(defaultModel + defaultColumn);
  const [searchText, setSearchText] = useState(undefined);

  useEffect(() => {
    loadInitialData();
  }, []);

  const loadInitialData = async () => {
    await contractStore.getAll();
    await divisionStore.getAll();
    await positionStore.getAll();
  };

  // go to detail contract
  const handleClickRow = (record, index) => {
    history.push(LINKS.DETAIL_CONTRACT.replace(":id", record.id));
  };

  // bread crumb section
  const breadCrumb = [
    {
      link: "",
      name: "Contract",
      styles: { fontWeight: "bold" },
    },
  ];

  // go to add contract function
  const goToAdd = () => {
    history.push(LINKS.FORM_CONTRACT);
  };

  // column data
  const columns = [
    {
      title: "No",
      dataIndex: "no",
      width: 10,
      render: (t, r, index) => `${index + 1}`,
    },
    {
      title: "Name",
      dataIndex: "name",
      render: (t, record) => (
        <Tooltip title="Klik untuk lihat detail">
          <span>
            <Link style={{ color: "#42a5f5" }}>{record.employee?.name}</Link>
          </span>
        </Tooltip>
      ),
    },
    {
      title: "Contract Type",
      dataIndex: "type_contract",
      render: (text, record) => {
        return record.type_contract || "-";
      },
    },
    {
      title: "Division",
      dataIndex: "division",
      render: (t, record) => {
        return record.position?.division?.name;
      },
    },
    {
      title: "Position",
      dataIndex: "position",
      render: (t, record) => {
        return record.position?.name;
      },
    },
    {
      title: "Salary",
      dataIndex: "salary",
      render: (text, record) => {
        return `Rp.${record.salary}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      },
    },

    {
      title: "Latest Contract",
      dataIndex: "latest_contract",
      render: (t, record) =>
        moment(record.start_contract).format("Do MMMM YYYY"),
    },

    {
      title: "Expired Contract",
      dataIndex: "expired_contract",
      render: (t, record) => moment(record.end_contract).format("Do MMMM YYYY"),
    },
  ];

  // table data contract
  const tableData = contractStore.data;
  const divisionData = divisionStore.listData.map((it) => it);
  const positionData = positionStore.listData.map((it) => it);

  // submit modal
  const submit = async (data) => {
    console.log(data.order, "here");
    setIsModalVisible(false);
    let rqlData = {
      month: data.order,
    };
    console.log(rqlData);
    try {
      await contractStore.getAll(rqlData);
    } catch (e) {
      console.log(e);
    }
  };

  // rendering contract table from table view
  return (
    <Layout className="site-layout">
      <div
        className="site-card-wrapper"
        style={{
          padding:
            store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
              ? "10px"
              : "",
          marginTop: 10,
        }}
      >
        {/* table view section*/}
        <TableView
          title={<h1 style={{ fontWeight: 500, marginTop: 10 }}>Contracts</h1>}
          breadCrumb={breadCrumb}
          columns={columns}
          dataSource={tableData}
          showToolbar={true}
          showAddBtn={false}
          showFilterBtn={true}
          bordered
          // onClickAdd={goToAdd}
          placeholder={"Search by Name or Type Contract"}
          searchBarFull={true}
          searchKeys={["employee.name", "type_contract"]}
          onRowClick={handleClickRow}
          key={"table_contract"}
          onClickFilter={() => {
            setIsModalVisible(true);
          }}
        />
        {/* modal filter section */}
        <ModalFilter
          visible={isModalVisible}
          title={"Filter Contracts"}
          onCancel={() => setIsModalVisible(false)}
          model={"contract"}
          onOk={(value) => submit(value)}
          visibleFilterOrder={true}
          visibleFilterDivision={false}
          visibleFilterPosition={false}
          divisionOption={divisionData}
          positionOption={positionData}
        />
      </div>
    </Layout>
  );
});
