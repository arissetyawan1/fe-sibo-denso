import React, { useEffect, Fragment, useState } from "react";
import "antd/dist/antd.css";
import {
  Descriptions,
  Badge,
  List,
  Card,
  Table,
  Input,
  Button,
  Icon,
  Typography,
  Modal,
  message,
  Space,
  Upload,
  Row,
  Col,
  PageHeader,
  Form,
  Select,
  Breadcrumb,
} from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  LeftOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import {
  Link,
  useHistory,
  useRouteMatch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import Highlighter from "react-highlight-words";
import { inject, observer } from "mobx-react";
import moment from "moment";
import { useParams } from "react-router-dom";
import { useStore } from "../../utils/useStores";
import { appConfig } from "../../config/app";
import * as download from "downloadjs";
import FileViewer from "react-file-viewer";
import * as _ from "lodash";
import { http } from "../../utils/http";

export const DetailContract = observer((props) => {
  const { contracts: contractStore } = useStore();
  const history = useHistory();
  const { id: paramId } = useParams();
  // const [data, setData] = useState("");
  const [page, setPage] = useState(1);

  useEffect(() => {
    getInitialData();
  }, []);

  const getInitialData = async () => {
    await contractStore.getDetailById(paramId);
  };
  // console.log("detail contract", data);

  const deleteData = async (id) => {
    try {
      await contractStore.deleteContract(id);
      message.success("Berhasil menghapus");
      history.push(LINKS.CONTRACT);
    } catch (err) {
      console.log(err);
      message.error("Gagal menghapus");
    }
  };

  const confirmDelete = (id) => {
    Modal.confirm({
      title: "Apakah yakin ingin menghapus?",
      content: "Data yang dihapus tidak bisa kembali",
      onOk: () => {
        return deleteData(id);
      },
      onCancel() {},
    });
  };

  const backToContract = () => {
    history.push("/app/contract");
  };

  const editContract = (id) => {
    history.push(LINKS.EDIT_CONTRACT.replace(":id", id));
  };

  const actionButton = (
    <div style={{ margin: "12px 2px" }}>
      <Button
        className={"buttonText"}
        onClick={backToContract}
        icon={<LeftOutlined />}
      >
        Back
      </Button>
    </div>
  );

  const columns = [
    {
      title: "Pengambilan Ke-",
      dataIndex: "pengambilan",
      render: (t, r, index) => `${(page - 1) * 10 + index + 1}`,

      width: "30%",
      align: "center",
    },
    {
      title: "Tanggal",
      width: "50%",
      dataIndex: "date",
      render: (t, record) => {
        return moment(record.date).format("dddd, DD-MM-YYYY");
      },
    },
  ];

  const data = contractStore.detailData;
  return (
    <div className="site-card-less-wrapper" style={{ marginTop: 10 }}>
      <Breadcrumb style={{ marginLeft: 12, marginBottom: 15 }}>
        <Breadcrumb.Item>
          <Link to={LINKS.DASHBOARD}>
            <HomeOutlined />
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to={LINKS.CONTRACT}>Contract</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item style={{ fontWeight: "bold" }}>
          Contract Detail
        </Breadcrumb.Item>
      </Breadcrumb>

      <Row>
        <Col>{actionButton}</Col>
      </Row>
      <Card title="Contract Detail">
        <Descriptions layout="horizontal" bordered column={1}>
          <Descriptions.Item label="Type Contract">
            {data.type_contract}
          </Descriptions.Item>
          <Descriptions.Item label="Employee Name">
            {data.employee?.name}
          </Descriptions.Item>
          <Descriptions.Item label="Position">
            {data.position?.name}
          </Descriptions.Item>
          <Descriptions.Item label="Division">
            {data.position?.division?.name}
          </Descriptions.Item>
          <Descriptions.Item label="Salary">
            {`Rp. ${data.salary}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
          </Descriptions.Item>
          <Descriptions.Item label="Start Contract">
            {moment(data.start_contract).format("Do MMMM YYYY")}
          </Descriptions.Item>
          <Descriptions.Item label="End Contract">
            {moment(data.end_contract).format("Do MMMM YYYY")}
          </Descriptions.Item>
          {/* <Descriptions.Item label="File Contract">
            {data.path?.file?.name}
          </Descriptions.Item> */}
          {/* <Descriptions.Item label="No. Kontrak">
            {data.contract_number}
          </Descriptions.Item>
          <Descriptions.Item label="Transporter">
            {data.company?.name}
          </Descriptions.Item>
          <Descriptions.Item label="Kode Area">
            {data.area_code}
          </Descriptions.Item>
          <Descriptions.Item label="Nama Marketing">
            {data.employee?.name}
          </Descriptions.Item>
          <Descriptions.Item label="PIC">{data.pic_name}</Descriptions.Item>
          <Descriptions.Item label="No. Telepon (PIC)">
            {data.pic_phone}
          </Descriptions.Item>
          <Descriptions.Item label="Kode Pelanggan">
            {data.customer_code}
          </Descriptions.Item>
          <Descriptions.Item label="Nama Yankes">
            {data.hospital?.site?.name}
          </Descriptions.Item>
          <Descriptions.Item label="Alamat Yankes">
            {data.hospital?.site?.address}
          </Descriptions.Item>
          <Descriptions.Item label="Durasi Kontrak">
            {data.contract_duration} Bulan
          </Descriptions.Item>
          <Descriptions.Item label="Mulai Kontrak">
            {moment(data.start_date).format("DD-MM-YYYY")}
          </Descriptions.Item>
          <Descriptions.Item label="Akhir Kontrak">
            {moment(data.end_date).format("DD-MM-YYYY")}
          </Descriptions.Item>
          <Descriptions.Item label="Perkiraan Limbah per bulan">
            {parseInt(data.estimate_load)} kg
          </Descriptions.Item>
          <Descriptions.Item label="Alamat Pengambilan">
            {data.pickup_address}
          </Descriptions.Item>
          <Descriptions.Item label="Koordinat Pengambilan">
            Lat : {data.pickup_lat} <br />
            Lng : {data.pickup_lng}
          </Descriptions.Item>
          <Descriptions.Item label="Frekuensi">
            {parseInt(data.frequency_times)} Kali/
            {data.frequency_type}
          </Descriptions.Item>

          <Descriptions.Item label="Jadwal">
            <Table
              bordered
              columns={columns}
              // dataSource={tableData}
              style={{ marginTop: "20px" }}
              rowKey={"id"}
              pagination={{
                onChange(current) {
                  setPage(current);
                },
              }}
            />
          </Descriptions.Item>
          */}
          <Descriptions.Item label="Action">
            <Button
              style={{
                background: "#0077b6",
                color: "white",
                boxShadow: "none",
              }}
              onClick={() => editContract(data.id)}
              className={"buttonStyle2"}
            >
              <EditOutlined /> Edit
            </Button>
            <Button
              style={{
                background: "#03045e",
                color: "white",
                boxShadow: "none",
                marginLeft: "8px",
              }}
              onClick={() => confirmDelete(data.id)}
            >
              <DeleteOutlined /> Delete
            </Button>
          </Descriptions.Item>
        </Descriptions>
      </Card>
    </div>
  );
});
