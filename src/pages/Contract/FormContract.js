import React, { useEffect, useState } from "react";
import {
  Input,
  InputNumber,
  Form,
  DatePicker,
  Select,
  Button,
  Card,
  Table,
  Modal,
  Breadcrumb,
  Image,
  Row,
  Col,
  Tooltip,
  message,
  Upload,
} from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  DeleteOutlined,
  SwapRightOutlined,
  EditOutlined,
  UploadOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { Link, useHistory, useParams } from "react-router-dom";
import { LINKS } from "../../routes";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import { debounce } from "lodash";
import moment from "moment";
import { appConfig } from "../../config/app";
import * as uuid from "uuid";
import { constants } from "../../config/constants";

export const FormContract = observer((props) => {
  const {
    clients: clientStore,
    contracts: contractStore,
    human_resource: humanResourceStore,
    // companies: companiesStore,
    division: divisionStore,
    position: positionStore,
    userData,
  } = useStore();
  const { mode } = props;
  const [loading, setLoading] = useState(false);
  const [fileList, setFileList] = useState([]);
  const [previewPdf, setPreviewPdf] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileUrl, setFileUrl] = useState("");
  const firstIndexFileList = fileList[0];
  const { id: paramId } = useParams();

  const history = useHistory();

  const [form] = Form.useForm();

  const [page, setPage] = useState(1);
  const [price, setPrice] = useState(0);
  const [dataForm, setDataForm] = useState([]);
  // const [selectSearch, setSelectSearch] = useState([]);

  const [disableInput, setDisableInput] = useState(true);

  useEffect(() => {
    loadInitialData();
  }, []);

  const { TextArea } = Input;

  const { Option } = Select;

  const layout = {
    labelCol: { span: 5 },
    wrapperCol: { span: 14 },
  };

  const btnLayout = {
    wrapperCol: { offset: 17 },
  };

  const filter = (input, option) =>
    option.children.toString().toLowerCase().indexOf(input.toLowerCase()) >= 0;

  const loadInitialData = async () => {
    // await clientStore.getAll();
    await humanResourceStore.getData();
    // await companiesStore.getAll();
    await divisionStore.getAll();
    await positionStore.getAll();
    if (mode === "edit" && paramId) {
      console.log(contractStore.getDetailById(paramId), "apa");
      await contractStore.getDetailById(paramId).then((res) => {
        setDataForm(res.body.results);
        const formData = res.body.results;
        console.log(res.body.results, " ini result");

        form.setFieldsValue({
          id_employee: formData.employee?.id,
          employee_name: formData.employee?.name,
          start_contract: moment(formData.start_contract),
          end_contract: moment(formData.end_contract),
          type_contract: formData.type_contract,
          id_division: formData.position?.division?.id,
          id_position: formData.position?.id,
          salary: formData.salary,
          // id_company: formData.company?.id,
        });
      });
    }
  };

  // Companies Combo Box
  // const dataCompanies = companiesStore.listData;

  // const companiesContract = [...new Set(dataCompanies)].filter(
  //   (prop) => prop !== undefined
  // );

  // const optionCompanies = companiesContract.map((companies) => {
  //   return (
  //     <Option value={companies.id} key={companies.id}>
  //       {companies.name}
  //     </Option>
  //   );
  // });
  // const selectType = (
  //   <Select
  //     showSearch
  //     placeholder="Choose your input"
  //     style={{ width: 300 }}
  //     optionFilterProp="children"
  //     filterOption={(input, option) =>
  //       option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
  //       option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
  //     }
  //   >
  //     {optionCompanies}
  //   </Select>
  // );

  // Employee Combo box
  const dataEmployee = humanResourceStore.listData;

  const employeeContract = [...new Set(dataEmployee)].filter(
    (prop) => prop !== undefined
  );

  const optionEmployee = employeeContract.map((employee) => {
    return (
      <Option value={employee.id} key={employee.id}>
        {employee.name}
      </Option>
    );
  });

  const selectEmployees = (
    <Select
      showSearch
      placeholder="Employee Name"
      style={{ width: 300 }}
      disabled={mode === "edit" ? true : false}
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
        option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      {optionEmployee}
    </Select>
  );

  // Position Combo Box
  const dataPosition = positionStore.listData;
  const positionsContract = [...new Set(dataPosition)].filter(
    (prop) => prop != undefined
  );

  const optionPositions = positionsContract.map((positions) => {
    return (
      <Option value={positions.id} key={positions.id}>
        {positions.name}
      </Option>
    );
  });

  const selectPositions = (
    <Select
      showSearch
      placeholder="Positions"
      style={{ width: 300 }}
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
        option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      {optionPositions}
    </Select>
  );

  const saveContract = async (values) => {
    const bodyData = {
      ...values,
    };
    try {
      if (mode === "edit") {
        await contractStore.updateData(bodyData, dataForm.id);
        message.success("Berhasil mengubah kontrak");
        history.push(`${LINKS.CONTRACT}/` + dataForm.id);
      } else {
        history.push(LINKS.CONTRACT);
        await contractStore.createData(bodyData);
        message.success("Berhasil membuat kontrak");
      }
    } catch (e) {
      Modal.error({
        title: "Gagal",
        content:
          e.response?.body?.message ||
          "Gagal menyimpan data kontrak karena suatu kesalahan",
      });
    }
  };

  const handlerPrice = (value) => {
    // if (!value || value === null) {
    //   setPrice(0);
    // }
    // setPrice(value);
    console.log("changed", value);
  };

  const positiveNumberValidation = (_, value) => {
    if (value !== "" && +value < +0) {
      return Promise.reject("Angka harus lebih besar dari 0");
    }
    return Promise.resolve();
  };

  // upload setup
  const previewUpload = fileUrl?.includes(".pdf") ? (
    <iframe
      src={`${appConfig.imageUrl}${fileUrl ?? ""}`}
      width={"100%"}
      style={{ height: 500 }}
    />
  ) : (
    fileUrl && (
      <Image
        style={{ width: "100%", height: 500 }}
        preview={true}
        src={!fileUrl ? null : `${appConfig.imageUrl}${fileUrl ?? null}`}
      />
    )
  );

  const uploadButton = (
    <div>
      {loading ? (
        <LoadingOutlined />
      ) : (
        <Button icon={<UploadOutlined />}>Click to Upload</Button>
      )}
    </div>
  );

  const beforeUpload = (file) => {
    let isLt2M;
    let allowedFile = ["application/pdf"];
    let isValid = allowedFile.includes(file.type);
    if (!isValid) {
      message.error("Format File Hanya PDF!");
    }
    // isLt2M = file.size / 1024 / 1024 < 2;
    // if (!isLt2M) {
    //     message.error("File must smaller than 2MB!");
    // }
    return isValid ? true : Upload.LIST_IGNORE;
  };

  const handlePreview = async (file) => {
    const fileUrl = appConfig.imageUrl + file.response.path;
    setPreviewTitle(file.url?.substring(file.url?.lastIndexOf("/") + 1));
  };

  const handleChange = ({ fileList }) => {
    setFileList(fileList);
    if (fileList.length && fileList[0].status === "done") {
      form.setFieldsValue({
        file_url: fileList[0].response.path,
      });
      console.log(fileList, "list file");
      setFileUrl(fileList[0].response.path);
      setPreviewPdf(fileList[0].response.path);
      setPreviewTitle(fileList[0].name);
    }
  };

  const submitForm = () => {
    form
      .validateFields()
      .then(async (res) => {
        await saveContract(res);
        console.log(res, "asdasdasdsa");
      })
      .catch((errorInfo) => {
        console.log(errorInfo);
      });
  };

  const dateFormat = "YYYY-MM-DD";
  return (
    <div className="client-card-wrapper" style={{ marginTop: 10 }}>
      <Breadcrumb style={{ marginLeft: 12, marginBottom: 15 }}>
        <Breadcrumb.Item>
          <Link to={LINKS.DASHBOARD}>
            <HomeOutlined />
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to={LINKS.CONTRACT}>Contract</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item style={{ fontWeight: "bold" }}>
          {mode === "edit" ? "Edit" : "Add"} Contract
        </Breadcrumb.Item>
      </Breadcrumb>

      <Card title={"Contract Form"}>
        <Form
          {...layout}
          form={form}
          initialValues={{
            start_date: dataForm.start_date,
          }}
          scrollToFirstError={true}
        >
          <Form.Item
            label="Employee Name"
            name="id_employee"
            rules={[{ required: true, message: "Select employee is required" }]}
          >
            {selectEmployees}
          </Form.Item>

          <Form.Item
            label="Contract Period"
            style={{ marginBottom: 0 }}
            rules={[{ required: true, message: "Contract Period is required" }]}
          >
            <Input.Group compact>
              <Form.Item
                name="start_contract"
                rules={[
                  {
                    required: true,
                    message: "Input start contract!",
                  },
                ]}
              >
                <DatePicker
                  allowClear={false}
                  style={{ width: 200, marginRight: 5 }}
                  placeholder="Start Contract"
                />
              </Form.Item>
              <Form.Item>
                <SwapRightOutlined />
              </Form.Item>
              <Form.Item
                name="end_contract"
                rules={[
                  {
                    required: true,
                    message: "Input expaired contract!",
                  },
                ]}
              >
                <DatePicker
                  style={{ width: 200, marginLeft: 5 }}
                  placeholder="End Contract"
                />
              </Form.Item>
            </Input.Group>
          </Form.Item>

          {/* Type of contract */}
          <Form.Item
            label="Type Contract"
            name="type_contract"
            rules={[
              { required: true, message: "Select type contract  is required" },
            ]}
          >
            <Select
              showSearch
              placeholder="Select type contract"
              style={{ width: 400 }}
            >
              <Option value="Perjanjian Kerja Waktu Tertentu (PKWT)">
                Perjanjian Kerja Waktu Tertentu (PKWT)
              </Option>
              <Option value="Amandemen Perjanjian Kerja (APK)">
                Amandemen Perjanjian Kerja (APK)
              </Option>
              <Option value="Perjanjian Kerja Magang (PKM)">
                Perjanjian Kerja Magang (PKM)
              </Option>
              <Option value="Surat Pengangkatan Karyawan Tetap(SPKT)">
                Surat Pengangkatan Karyawan Tetap (SPKT)
              </Option>
            </Select>
          </Form.Item>

          <Form.Item
            label="Position"
            name="id_position"
            rules={[
              {
                required: true,
                messagemessage: "Select position  is required",
              },
            ]}
          >
            {selectPositions}
          </Form.Item>

          <Form.Item
            label="Salary"
            name="salary"
            rules={[
              { required: true, message: "Input salary is required" },
              { validator: positiveNumberValidation },
            ]}
          >
            <InputNumber
              style={{ width: 400 }}
              // value={price}
              // initialValhandues={0}
              // min={0}
              // max={10000000000000000000000000000000}
              formatter={(value) =>
                `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
              }
              parser={(value) => value.replace(/\Rp.\s?|([.]*)/g, "")}
              onChange={handlerPrice}
              // step={500}
            />
          </Form.Item>

          {/* <Form.Item
            label="Company"
            name="id_company"
            rules={[{ required: true, message: "Select company is required!" }]}
          >
            {selectType}
          </Form.Item> */}

          {/* <Form.Item label="Upload" name="path" rules={[{ required: false }]}> */}
          <Form.Item label="Upload" name="file_url">
            <Upload
              name="file"
              maxCount={1}
              className="avatar-uploader"
              action={`${appConfig.imageUrl}/files`}
              fileList={fileList}
              beforeUpload={beforeUpload}
              onPreview={handlePreview}
              onChange={handleChange}
              onRemove={() => {
                setFileUrl(null);
                setPreviewTitle(null);
              }}
            >
              {!firstIndexFileList ? uploadButton : null}
            </Upload>
          </Form.Item>

          <Form.Item {...btnLayout}>
            <Button
              type="primary"
              htmlType="submit"
              onClick={submitForm}
              size="large"
              className={"buttonStyle"}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </Card>
    </div>
  );
});
