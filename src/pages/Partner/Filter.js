import React, {useState} from "react";
import {Button, Card, Dropdown, Menu, PageHeader} from "antd";
import {FilterOutlined} from "@ant-design/icons";

const optionFilter = [
  {
    key: 1,
    value: 'Today'
  }, {
    key: 2,
    value: 'Last 3 days'
  }, {
    key: 3,
    value: 'Last 6 months'
  }, {
    key: 4,
    value: 'Last 12 months'
  }, {
    key: 5,
    value: 'Month to date'
  }, {
    key: 6,
    value: 'Quarter to date'
  }, {
    key: 7,
    value: 'Year to date'
  }, {
    key: 8,
    value: 'All time'
  },
];



export const Filter = () => {
  const [selected, setSelected] = useState('All Time');

  function handleMenuClick(e) {
    console.log('click', e.item);
    console.log('click', e);
    {optionFilter.map((i => {
      const key = i.key;
    }))}
  }
  const menu = (
    <Menu selectable selectedKeys={selected} onSelect={handleMenuClick} onClick={handleMenuClick} style={{background: '#fff'}}>
      {optionFilter.map((i => {
        return <Menu.Item key={i.key} >{i.value}</Menu.Item>
      }))
      }
    </Menu>
  );
  return <div>
    <Dropdown overlay={menu} trigger={['click']} >
      <Button icon={<FilterOutlined />} size={'default'} style={{margin: 3}}>
        {selected}
      </Button>
    </Dropdown>
  </div>
};
