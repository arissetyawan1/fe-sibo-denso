import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../routes";
import {Partner2w} from "./Partner2w/Partner2w";

export const PartnerRoute = () => {
    return (
        <Switch>
            <Route path={LINKS.PARTNER + "/2w"}>
                <Partner2w/>
            </Route>
            <Route path={LINKS.PARTNER + "/4w"}>
                4w
            </Route>
            <Route path={LINKS.PARTNER + "/cariservice"}>
                cari service
            </Route>
        </Switch>
    )
};
