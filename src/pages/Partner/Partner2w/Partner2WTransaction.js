import React, {useEffect} from "react";
import {useStore} from "../../../utils/useStores";
import {Table} from 'antd'
import {observer} from 'mobx-react-lite';

export const Partner2WTransaction = observer((props) => {
    const store = useStore();

    useEffect(() => {
        store.transaction.getAll();
    }, []);

    const column = [
        {
            title: 'Order ID',
            dataIndex: 'order_id',
            key: 'order_id',
        },
        {
            title: 'Transaction ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Customer',
            dataIndex: 'username',
            key: 'username',
        },
        {
            title: 'Partner Name',
            dataIndex: 'parking_name',
            key: 'parking_name',
        },
        {
            title: 'Checkin',
            dataIndex: 'check_in',
            key: 'check_in',
        },
        {
            title: 'Checkout',
            dataIndex: 'check_out',
            key: 'check_out',
        }
    ];

    console.log({data: store.transaction.data}, ' -> ')

    return <div>
        <div>
            <Table dataSource={store.transaction.data} columns={column} />
        </div>
    </div>
});
