import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../../routes";
import {Partner2WTransaction} from "./Partner2WTransaction";
import {Partner2WDashboard} from "./Partner2WDashboard";
import {Partner2WFraud} from "./Partner2WFraud";
import {Partner2WHistory} from "./Partner2WHistory";
import {Partner2WReconcile} from "./Partner2WReconcile";
import {Partner2WPayment} from "./Partner2WPayment";

export const Partner2WRoute = () => {
    return (
        <Switch>
            <Route path={LINKS.PARTNER_2W_DASHBOARD}>
                <Partner2WDashboard/>
            </Route>
            <Route path={LINKS.PARTNER_2W_TRANSACTION}>
                <Partner2WTransaction/>
            </Route>
            <Route path={LINKS.PARTNER_2W_RECONCILE}>
                <Partner2WReconcile/>
            </Route>
            <Route path={LINKS.PARTNER_2W_PAYMENT}>
                <Partner2WPayment/>
            </Route>
            <Route path={LINKS.PARTNER_2W_HISTORY}>
                <Partner2WHistory/>
            </Route>
            <Route path={LINKS.PARTNER_2W_FRAUD}>
                <Partner2WFraud/>
            </Route>
        </Switch>
    )
};
