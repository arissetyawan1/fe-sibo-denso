import React, {useEffect} from "react";
import {useStore} from "../../../utils/useStores";
import {Button, Table, Form, Radio} from 'antd'
import {observer} from 'mobx-react-lite';
import {Link} from "react-router-dom";
import {LINKS} from "../../../routes";
import {Partner2WRoute} from './Partner2WRoute'

export const Partner2w = observer(() => {

    return <div>
        <div>
            <Form.Item>
                <Radio.Group
                  value={'none'}
                  onChange={() => {

                  }}
                >
                    <Radio.Button value="dashboard">
                        <Link to={LINKS.PARTNER_2W_DASHBOARD}>
                            Dashboard
                        </Link>
                    </Radio.Button>
                    <Radio.Button value="transaction">
                        <Link to={LINKS.PARTNER_2W_TRANSACTION}>
                            Transaction
                        </Link>
                    </Radio.Button>
                    <Radio.Button value="reconcile">
                        <Link to={LINKS.PARTNER_2W_RECONCILE}>
                            Reconcile
                        </Link>
                    </Radio.Button>
                    <Radio.Button value="payment">
                        <Link to={LINKS.PARTNER_2W_PAYMENT}>
                            Payment
                        </Link>
                    </Radio.Button>
                    <Radio.Button value="history">
                        <Link to={LINKS.PARTNER_2W_HISTORY}>
                            History
                        </Link>
                    </Radio.Button>
                    <Radio.Button value="fraud">
                        <Link to={LINKS.PARTNER_2W_FRAUD}>
                            Fraud
                        </Link>
                    </Radio.Button>
                </Radio.Group>
            </Form.Item>
        </div>
        <div>
            <Partner2WRoute/>
        </div>
    </div>
});
