import React, {useEffect, useState, useRef} from "react";
import {useStore} from "../../utils/useStores";
import {
  Table,
  Button,
  Tabs,
  Modal,
  PageHeader,
  Dropdown,
  Menu,
  Card,
  Tag,
  Typography,
  Select,
  Space,
  Input,
  Form,
  Radio
} from 'antd'
import {observer} from 'mobx-react-lite';
import {PartnerRoute} from './PartnerRoute'
import {Link, useHistory} from 'react-router-dom'
import {LINKS} from "../../routes";
import {FilterOutlined, PlusOutlined} from "@ant-design/icons";

const {Text, Title} = Typography;
const {Search} = Input;
const {Option} = Select;

const optionFilter = [
  {
    key: 1,
    value: 'Enabled'
  }, {
    key: 2,
    value: 'Disabled'
  },
];

function handleMenuClick(e) {
  console.log('click', e);
}

const menu = (
  <Menu onClick={handleMenuClick} style={{background: '#fff'}}>
    {optionFilter.map((i => {
      return <Menu.Item key={i.key}>{i.value}</Menu.Item>
    }))
    }
  </Menu>
);


export const Partner = observer(() => {
  const [filteredInfo, setFilteredInfo] = useState(true);
  const [loading, setLoading] = useState(true);
  const [visible, setVisible] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const [modalText, setModalText] = useState('Content of the modal');
  const [fields, setFields] = useState([
    {
      name: ['name'],
      value: 'Aira 2',
    }
  ]);

  const history = useHistory();
  const store = useStore();
  useEffect(() => {
    store.mitra.getAll();
  }, []);


  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {

    setModalText('The modal will be closed after two seconds');
    setConfirmLoading(true);
    setTimeout(() => {
      setVisible(false);
      setConfirmLoading(false);
    }, 2000);
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setVisible(false)
  };


  const handleChange = (pagination, filters, sorter, selectedRowKeys) => {
    console.log('Various parameters', pagination, filters, sorter, selectedRowKeys);
    setFilteredInfo(filters)
  };

  const clearFilters = () => {
    setFilteredInfo(null)
  };

  const clearAll = () => {
    setFilteredInfo(null)
  };


  const columnDraft = [
    {
      title: '',
      dataIndex: 'id',
      key: 'id',
      render: text => <a>{}</a>,
      width: 20

    },
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      width: 200,
      ellipsis: true
    },
    // {
    //   title: 'Email',
    //   dataIndex: 'email',
    //   key: 'email',
    //   width: 250,
    //   ellipsis: true
    // },
    {
      title: 'Phone',
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: 'Status',
      dataIndex: 'enabled',
      render: (record, text) => record ?
        <Tag style={{fontSize: 10, border: 0, borderRadius: 50, color: "#3d4eac", lineHeight: '18px'}}
             color="#d6ecff">Enabled</Tag> :
        <Tag style={{fontSize: 10, border: 0, borderRadius: 50, lineHeight: '18px'}} color={"#e3e8ee"}><Text
          type="secondary">Disabled</Text></Tag>,
      width: 100,
    },
    {
      title: 'Bank',
      dataIndex: 'bank',
      render: (record, text) => !record ? <div>-</div> : <span>text</span>,
      className: 'align-right'
    },
    {
      title: 'Bank account',
      dataIndex: 'bank_account',
      render: (record, text) => !record ? <div>-</div> : <span>{record.text}</span>
    },
    {
      title: 'Action',
      key: 'operation',
      width: 100,
      render: () => <Button size="small"style={{fontSize: 11, color:'#5469d4'}}  onClick={() => {
        setVisible(true);
      }}>Edit</Button>,
    },

  ];

  const dataPartners = [{
    id: '',
    name: 'Aira 2',
    email: 'test@gmail',
    phone: '0812123123',
    period: '',
    status: 'Active',
    bank: 'BNI',
    bank_account: 305932932932
  }];


  const onChangeSelect = (value) => {
    console.log(`selected ${value}`);
  };

  const onBlur = () => {
    console.log('blur');
  };

  const onFocus = () => {
    console.log('focus');
  };

  const onSearch = (val) => {
    console.log('search:', val);
  };

  const selectBank = (
    <Select
      showSearch
      style={{width: 200}}
      placeholder="Select Bank"
      optionFilterProp="children"
      onChange={onChangeSelect}
      onFocus={onFocus}
      onBlur={onBlur}
      onSearch={onSearch}
      filterOption={(input, option) =>
        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
    >
      <Option value="bni">BNI</Option>
      <Option value="mandiri">Mandiri</Option>
      <Option value="btn">BTN</Option>
      <Option value="bca">BCA</Option>
    </Select>
  );

  const useResetFormOnCloseModal = ({form, visible}) => {
    const prevVisibleRef = useRef();
    useEffect(() => {
      prevVisibleRef.current = visible;
    }, [visible]);
    const prevVisible = prevVisibleRef.current;
    useEffect(() => {
      if (!visible && prevVisible) {
        form.resetFields();
      }
    }, [visible]);
  };

  const CollectionCreateForm = ({visible, onCreate, onCancel, onChange, fields}) => {
    const [form] = Form.useForm();
    useResetFormOnCloseModal({
      form,
      visible,
    });
    return (
      <Modal
        maskClosable={false}
        visible={visible}
        width={'448px'}
        closable={false}
        bodyStyle={{background: '#f7fafc'}}
        confirmLoading={confirmLoading}
        title="Edit partner"
        okText="Update"
        cancelText="Cancel"
        onCancel={onCancel}
        onOk={() => {
          form
            .validateFields()
            .then(values => {
              form.resetFields();
              onCreate(values);
            })
            .catch(info => {
              console.log('Validate Failed:', info);
            });
        }}
      >

        <Form
          form={form}
          layout="vertical"
          className={'custom-form'}
          name="form_in_modal"

        >
          <Form.Item
            name='name'
            label="Name"
            rules={[
              {
                required: true,
                message: 'Please input the title of collection!',
              },
            ]}
          >
            <Input/>
          </Form.Item>

          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                required: true,
                message: 'Please input email!',
              },
            ]}
          >
            <Input/>
          </Form.Item>
          <Form.Item
            name="phone"
            label="Phone"
            rules={[
              {
                required: true,
                message: 'Please input phone!',
              },
            ]}
          >
            <Input/>
          </Form.Item>
          <Form.Item
            name="status"
            label="Status"
            rules={[
              {
                required: true,
                message: 'Please input phone!',
              },
            ]}
          >
            <Input/>
          </Form.Item>
          <Form.Item
            style={{marginBottom: 12}}
          >
            <Title level={4} style={{fontSize: 16}}>Billing info</Title>
          </Form.Item>

          <Form.Item
            name="bank"
            label="Bank"
            rules={[
              {
                required: true,
                message: 'Please input Bank!',
              },
            ]}
          >
            {selectBank}
          </Form.Item>

          <Form.Item
            name="bank_account"
            label="Account number"
            rules={[
              {
                required: true,
                message: 'Please input Account Bank!',
              },
            ]}
          >
            <Input/>
          </Form.Item>
        </Form>
      </Modal>
    );
  };

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      {
        store.mitra.data.map((i, index) => {
          console.log(`selectedRowKeys: ${selectedRowKeys = i.id}`, 'selectedRows: ', selectedRows,);
          return {selectedRows, selectedRowKeys}
        });


      }
    }
  };

  const onCreate = values => {
    console.log('Received values of form: ', values);
    setVisible(false);
  };

  return (
    <div className={['cariparkir-container'].join(' ')}>
      <Card bordered={false} style={{}} className={'shadow'} bodyStyle={{padding: 0}}>
        <PageHeader
          className={'card-page-header'}
          title={<div style={{}}>
            <Search
              placeholder="FIlter by name or email"
              onSearch={value => console.log(value)}
              style={{width: 200, padding: '2.5px 11px', marginRight: 12}}
            />
            <Dropdown overlay={menu} trigger={['click']}>
              <Button type="dashed" icon={<FilterOutlined/>} size={'small'} style={{margin: 3}}>
                Filter
              </Button>
            </Dropdown>
          </div>}
          subTitle=""
          extra={[
            <div>
              <Button onClick={() => {
                setVisible(true);
              }}>
                <PlusOutlined/>New
              </Button>

            </div>
          ]}
        />
        <Table size={"small"}
               rowKey={record => record.id}
               onChange={handleChange} tableLayout={'fixed'} dataSource={store.mitra.data}
               columns={columnDraft}/>
      </Card>
      <CollectionCreateForm fields={fields} onChange={newFields => {
        setFields(newFields);
      }} visible={visible} onCreate={onCreate} onCancel={() => {
        setVisible(false);
      }}/>
      {/*<Modal*/}
      {/*  title="Title"*/}
      {/*  maskClosable={false}*/}
      {/*  visible={visible}*/}
      {/*  closable={false}*/}
      {/*  onOk={handleOk}*/}
      {/*  confirmLoading={confirmLoading}*/}
      {/*  onCancel={handleCancel}*/}
      {/*>*/}
      {/*  <p>{modalText}</p>*/}
      {/*</Modal>*/}
    </div>
  )
});
