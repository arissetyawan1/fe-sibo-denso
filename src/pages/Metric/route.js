import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../routes";
import {MetricSuperAdmin} from "./Report-Superadmin";
import {ReportSuperAdminMetricDailyRevenue} from "./Metric-Daily-Revenue"

import {ReportPartner} from "./Report-Partner";
import {useStore} from "../../utils/useStores";
import {observer} from "mobx-react-lite";
import {AnimatedSwitch} from "react-router-transition";
import {TransactionRoute} from "../Transaction/route";
import {Transaction} from "../Transaction/Transaction";
import {Partner2w} from "../Services/Partner2w/Partner2w";
import {ReportSuperAdminInvoice} from "./Invoice/Invoice";
import {ReportSuperAdminBill} from "./Bill/Bill";
import {TransactionPartnerDetail} from "../Transaction/Transaction-Partner-Detail";
import {MenuSuperAdministrator} from "../App/Menu-SuperAdministrator";


export const MetricRoute = observer(() => {
  const store = useStore();
  return (
    <AnimatedSwitch
      atEnter={{opacity: 0}}
      atLeave={{opacity: 0}}
      atActive={{opacity: 1}}
      >

      {store.userData.role === 'super_admin' && <div>
        <Route path={LINKS.REPORT} exact>
          <MetricSuperAdmin/>
        </Route>
        <Route path={LINKS.DAILY_REVENUE} exact>
          <ReportSuperAdminMetricDailyRevenue/>
        </Route>
        <Route path={LINKS.INVOICES} exact>
          <ReportSuperAdminInvoice/>
        </Route>
        <Route path={LINKS.BILLS} exact>
          <ReportSuperAdminBill/>
        </Route>
      </div>}

      {store.authentication.userData.role === 'Partner' && <div>
          <Route path={LINKS.REPORT} exact>
              <ReportPartner/>
          </Route>
        <Route path={LINKS.TRANSACTION_OPERATION} exact>
          <Transaction/>
        </Route>
        <Route path={LINKS.TRANSACTION_OPERATION_DETAIL} exact>
          <TransactionPartnerDetail/>
        </Route>

      </div>}

    </AnimatedSwitch>
  )
});
