import React, { useState, useEffect } from "react";
import Highlighter from "react-highlight-words";
import "antd/dist/antd.css";
import { Table, Input, Button, Icon } from "antd";
import { Card, CardBody, CardColumns, CardHeader, Row, Col } from "reactstrap";
import { Doughnut, Pie } from "react-chartjs-2";
import { inject, observer } from "mobx-react";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";

const pie = {
  labels: ["I", "II", "III", "IV", "V"],
  datasets: [
    {
      data: [80.4, 14, 4.5, 0.5, 0.5],
      backgroundColor: ["#D6EAF8", "#85C1E9", "#3498DB", "#2874A6", "#1B4F72"],
      hoverBackgroundColor: [
        "#D6EAF8",
        "#85C1E9",
        "#3498DB",
        "#2874A6",
        "#1B4F72",
      ],
    },
  ],
};
const pie1 = {
  labels: ["I", "II", "III", "IV", "V"],
  datasets: [
    {
      data: [80.4, 14, 4.5, 0.5, 0.5],
      backgroundColor: ["#D6EAF8", "#85C1E9", "#3498DB", "#2874A6", "#1B4F72"],
      hoverBackgroundColor: [
        "#D6EAF8",
        "#85C1E9",
        "#3498DB",
        "#2874A6",
        "#1B4F72",
      ],
    },
  ],
};
const pie2 = {
  labels: ["I", "II", "III", "IV", "V"],
  datasets: [
    {
      data: [80.4, 14, 4.5, 0.5, 0.5],
      backgroundColor: ["#D6EAF8", "#85C1E9", "#3498DB", "#2874A6", "#1B4F72"],
      hoverBackgroundColor: [
        "#D6EAF8",
        "#85C1E9",
        "#3498DB",
        "#2874A6",
        "#1B4F72",
      ],
    },
  ],
};
const pie3 = {
  labels: ["I", "II", "III", "IV", "V"],
  datasets: [
    {
      data: [80.4, 14, 4.5, 0.5, 0.5],
      backgroundColor: ["#D6EAF8", "#85C1E9", "#3498DB", "#2874A6", "#1B4F72"],
      hoverBackgroundColor: [
        "#D6EAF8",
        "#85C1E9",
        "#3498DB",
        "#2874A6",
        "#1B4F72",
      ],
    },
  ],
};
const TrafficClassification2 = observer((props) => {
  const store = useStore();
  // useState({
  //   searchText: "",
  //   isLoaded: false,
  //   report: {},
  // });

  const [searchText, setSearchText] = useState("");
  const [isLoaded, setIsLoaded] = useState(false);
  const [report, setReport] = useState({});

  useEffect(() => {
    store.keymetric.getClassification().then((res) => {
      this.setState({
        setReport: res,
        setIsLoaded: true,
      });
    });
    // props.store.keymetric.getClassification();
    // console.log(this.props.store.keymetric.getClassification())
  });

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button
          onClick={() => this.handleReset(clearFilters)}
          size="small"
          style={{ width: 90 }}
        >
          Reset
        </Button>
      </div>
    ),
    filterIcon: (filtered) => (
      <Icon type="search" style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex].toString().toLowerCase().includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: (text) => (
      <Highlighter
        highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
        searchWords={[this.state.searchText]}
        autoEscape
        textToHighlight={text.toString()}
      />
    ),
  });

  const handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  function generateDataPie(key, year) {
    return {
      labels: ["I", "II", "III", "IV", "V"],
      datasets: [
        {
          data: _.get(useState.report, `data.${key}.${year}`),
          backgroundColor: [
            "#e91e63",
            "#9c27b0",
            "#3f51b5",
            "#2196f3",
            "#00bcd4",
            "#009688",
          ],
          hoverBackgroundColor: [
            "#e91e63",
            "#9c27b0",
            "#3f51b5",
            "#2196f3",
            "#00bcd4",
            "#009688",
          ],
          // render,
        },
      ],
    };
  }
  {
    const year1 = _.get(setReport, "data.year1");
    const year2 = _.get(setReport, "data.year2");

    const columns = [
      {
        title: "Year",
        dataIndex: "year",
        key: "year",
        // ...this.getColumnSearchProps('name'),
      },
      {
        title: "I",
        dataIndex: "gol1",
        key: "gol1",
        // ...this.getColumnSearchProps('name'),
      },
      {
        title: "II",
        dataIndex: "gol2",
        key: "gol2",
        // ...this.getColumnSearchProps('name'),
      },
      {
        title: "III",
        dataIndex: "gol3",
        key: "gol3",
        // ...this.getColumnSearchProps('name'),
      },
      {
        title: "IV",
        dataIndex: "gol4",
        key: "gol4",
        // ...this.getColumnSearchProps('name'),
      },
      {
        title: "V",
        dataIndex: "gol5",
        key: "gol5",
        // ...this.getColumnSearchProps('name'),
      },
    ];

    const mtdYear1 = generateDataPie("mtd", "year1");
    const mtdYear2 = generateDataPie("mtd", "year2");
    const ytdYear1 = generateDataPie("ytd", "year1");
    const ytdYear2 = generateDataPie("ytd", "year2");

    const dataMTD = new Array(2).fill(1).map((n, i) => {
      const mtd = i === 0 ? mtdYear1 : mtdYear2;
      return {
        year:
          i === 0
            ? _.get(setReport, "data.year1")
            : _.get(setReport, "data.year2"),
        gol1: _.get(mtd, "datasets[0].data[0]"),
        gol2: _.get(mtd, "datasets[0].data[1]"),
        gol3: _.get(mtd, "datasets[0].data[2]"),
        gol4: _.get(mtd, "datasets[0].data[3]"),
        gol5: _.get(mtd, "datasets[0].data[4]"),
      };
    });

    const dataYTD = new Array(2).fill(1).map((n, i) => {
      const mtd = i === 0 ? ytdYear1 : ytdYear2;
      return {
        year:
          i === 0
            ? _.get(setReport, "data.year1")
            : _.get(setReport, "data.year2"),
        gol1: _.get(mtd, "datasets[0].data[0]"),
        gol2: _.get(mtd, "datasets[0].data[1]"),
        gol3: _.get(mtd, "datasets[0].data[2]"),
        gol4: _.get(mtd, "datasets[0].data[3]"),
        gol5: _.get(mtd, "datasets[0].data[4]"),
      };
    });

    return (
      <div className={"page-section " + setReport.id}>
        <div className={"page-separator"}>
          <div className={"page-separator__text"}>Overview</div>
        </div>

        <Table
          title={() => "MTD"}
          style={{ marginBottom: 20 }}
          columns={columns}
          dataSource={dataMTD}
        />

        <Table
          title={() => "YTD"}
          style={{ marginBottom: 20 }}
          columns={columns}
          dataSource={dataYTD}
        />

        <Row>
          <div className="col-md-6">
            <Card>
              <CardHeader>
                MTD {_.get(setReport, "data.month")}{" "}
                {_.get(setReport, "data.year2")}
                {/*<div className="card-header-actions">*/}
                {/*    <a href="" className="card-header-action">*/}
                {/*        <button className="primary"><small className="text-muted">Show Table</small></button>*/}
                {/*    </a>*/}
                {/*</div>*/}
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Pie data={mtdYear2} />
                </div>
              </CardBody>
            </Card>
          </div>
          <div className="col-md-6">
            <Card>
              <CardHeader>
                MTD {_.get(setReport, "data.month")}{" "}
                {_.get(setReport, "data.year1")}
                {/*<div className="card-header-actions">*/}
                {/*//     <a href="" className="card-header-action">*/}
                {/*//         <button className="primary"><small className="text-muted">Show Table</small></button>*/}
                {/*//     </a>*/}
                {/*</div>*/}
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Pie data={mtdYear1} />
                </div>
              </CardBody>
            </Card>
          </div>
        </Row>
        <Row>
          <div className="col-md-6">
            <Card>
              <CardHeader>
                YTD {_.get(setReport, "data.month")}{" "}
                {_.get(setReport, "data.year2")}
                {/*<div className="card-header-actions">*/}
                {/*    <a href="" className="card-header-action">*/}
                {/*        <button className="primary"><small className="text-muted">Show Table</small></button>*/}
                {/*    </a>*/}
                {/*</div>*/}
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Pie data={ytdYear2} />
                </div>
              </CardBody>
            </Card>
          </div>
          <div className="col-md-6">
            <Card>
              <CardHeader>
                YTD {_.get(setReport, "data.month")}{" "}
                {_.get(setReport, "data.year1")}
                {/*<div className="card-header-actions">*/}
                {/*    <a href="" className="card-header-action">*/}
                {/*        <button className="primary"><small className="text-muted">Show Table</small></button>*/}
                {/*    </a>*/}
                {/*</div>*/}
              </CardHeader>
              <CardBody>
                <div className="chart-wrapper">
                  <Pie data={ytdYear1} />
                </div>
              </CardBody>
            </Card>
          </div>
        </Row>
      </div>
    );
  }
});

export default TrafficClassification2;
