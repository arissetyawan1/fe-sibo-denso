import React, { useEffect, Fragment, useState } from "react";
import "antd/dist/antd.css";
import {
  Table,
  Input,
  Button,
  Tooltip,
  message,
  Modal,
  Select,
  Col,
  PageHeader,
  Row,
  Breadcrumb,
  Radio,
} from "antd";
import {
  DeleteOutlined,
  EditOutlined,
  SearchOutlined,
  PlusSquareOutlined,
  HomeOutlined,
  FilterOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import { inject, observer } from "mobx-react";
import { useHistory, Link, BrowserRouter as Router } from "react-router-dom";
import { LINKS } from "../../routes";
import { useStore } from "../../utils/useStores";
import { PageHeaderComponent } from "../../component/DefaultPage/PageHeader";
import { ModalForm } from "../../component/Form/ModalForm";
import { get } from "lodash";
import { ModalFilter } from "../../component/Modal/ModalFilter";
import { TableView } from "../../component/Table/TableView";

export const Employee = observer((initialData) => {
  const {
    human_resource: humanResourceStore,
    ui: uiStore,
    contracts: contractStore,
    division: divisionStore,
    position: positionStore,
  } = useStore();

  const [searchText, setSearchText] = useState(undefined);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const defaultModel = "employee";
  const defaultColumn = ".created_at";
  const [order, setOrder] = useState(defaultModel + defaultColumn);
  const [dataSource, setDataSource] = useState(null);
  const [divisionId, setDivisionId] = useState(undefined);
  const [positionId, setPositionId] = useState(undefined);
  const history = useHistory();

  useEffect(() => {
    loadInitialData();
  }, []);

  const loadInitialData = async () => {
    try {
      await divisionStore.getAll();
      await positionStore.getAll();
      await humanResourceStore.getData();
    } catch (e) {
      console.log(e, "errr get");
    }
  };

  const dataDivisionContract = humanResourceStore.listData.map((item) => {
    return item.position?.division?.name;
  });

  const itemDivision = [...new Set(dataDivisionContract)].filter(
    (prop) => prop !== undefined
  );
  const columns = [
    {
      title: "No",
      dataIndex: "no",
      render: (t, r, index) => `${index + 1}`,
      width: "5%",
    },
    {
      title: "Name",
      dataIndex: "name",
      width: "15%",
      render: (text, record) => (
        <Tooltip title="Klik untuk lihat detail">
          <span>
            <Link style={{ color: "#42a5f5" }}>{record?.name}</Link>
          </span>
        </Tooltip>
      ),
    },
    {
      title: "NIP",
      dataIndex: "nip",
      width: "15%",
      render: (t, record) => {
        return record?.nip || "-";
      },
    },
    {
      title: "Division",
      dataIndex: "division",
      width: "15%",
      render: (t, record) => {
        return record?.contract[0]?.position?.division?.name || "-";
      },
    },
    {
      title: "Position",
      dataIndex: "position",
      width: "15%",
      render: (t, record) => {
        return record?.contract[0]?.position?.name || "-";
      },
    },
    {
      title: "Salary",
      dataIndex: "salary",
      width: "15%",
      // ...getColumnSearchProps("code"),
      render: (t, record) => {
        return `Rp. ${record?.contract[0]?.salary || "-"}`.replace(
          /\B(?=(\d{3})+(?!\d))/g,
          "."
        );
      },
    },
  ];

  const breadCrumb = [
    {
      link: "",
      name: "Employee",
      styles: { fontWeight: "bold" },
    },
  ];

  const handleClickRow = (record, index) => {
    history.push(LINKS.DETAIL_EMPLOYEE.replace(":id", record.id));
  };

  const goToAdd = () => {
    history.push(LINKS.FORM_EMPLOYEE);
  };

  const tableData = humanResourceStore.getFilteredData;

  const divisionData = divisionStore.listData.map((it) => it);

  const positionData = positionStore.listData.map((it) => it);

  const submit = async (data) => {
    console.log(data, "hasil data");
    setIsModalVisible(false);
    let rqlData = {
      filter: {},
      sort: [data.order],
    };

    if (data.divisionId && data.positionId) {
      rqlData.filter["contract:position.id_division"] = {
        in: [data.divisionId],
      };
      rqlData.filter["contract.id_position"] = {
        in: data.positionId,
      };
    } else {
      rqlData.filter = {};
    }

    console.log(rqlData, "bruh123123");

    try {
      await humanResourceStore.getFiltered(rqlData);
    } catch (e) {
      console.log(e);
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <div
      className="site-card-wrapper"
      style={{
        padding:
          uiStore.mediaQuery.isMobile || uiStore.mediaQuery.isTablet
            ? "10px"
            : "",
        marginTop: 10,
      }}
    >
      <TableView
        title={<h1 style={{ fontWeight: 500, marginTop: 10 }}>Employees</h1>}
        breadCrumb={breadCrumb}
        columns={columns}
        dataSource={tableData}
        onClickAdd={goToAdd}
        placeholder={"Search by Name or NIP"}
        searchKeys={["name", "nip"]}
        // onClickFilter={() => {
        //   setIsModalVisible(true);
        // }}
        onRowClick={handleClickRow}
      />

      <ModalFilter
        visible={isModalVisible}
        title={"Filter Employee"}
        onCancel={() => setIsModalVisible(false)}
        model={"employee"}
        onOk={submit}
        visibleFilterOrder={true}
        visibleFilterDivision={true}
        visibleFilterPosition={true}
        divisionOption={divisionData}
        positionOption={positionData}
      />
    </div>
  );
});
