import React, { useEffect, useState } from "react";
import "antd/dist/antd.css";
import {
  Descriptions,
  Card,
  Button,
  Modal,
  message,
  Row,
  Col,
  Breadcrumb,
} from "antd";
import {
  HomeOutlined,
  LeftOutlined,
  DeleteOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Link, useHistory } from "react-router-dom";
import { LINKS } from "../../routes";
import { observer } from "mobx-react";
import { useParams } from "react-router-dom";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import moment from "moment";
export const DetailEmployee = observer((props) => {
  const { human_resource: humanResourceStore } = useStore();
  const history = useHistory();
  const { id } = useParams();

  useEffect(() => {
    getInitialData();
  }, []);

  const getInitialData = async () => {
    await humanResourceStore.getDetail(id);
  };

  const deleteData = async (id) => {
    try {
      await humanResourceStore.delData(id);
      message.success("Data Berhasil Dihapus");
      history.push(LINKS.EMPLOYEE);
    } catch (err) {
      console.log("error", err);
      message.error("Gagal menghapus");
    }
  };

  const confirmDelete = (id) => {
    Modal.confirm({
      title: "Apakah yakin ingin menghapus?",
      content: "Data yang dihapus tidak bisa kembali",
      onOk: () => {
        return deleteData(id);
      },
      onCancel() {},
    });
  };

  const backToEmployee = () => {
    history.push("/app/employee");
  };

  const editSDM = (id) => {
    history.push("/app/employee/update/" + id);
  };

  const actionButton = (
    <div style={{ margin: "12px 2px" }}>
      <Button onClick={backToEmployee} icon={<LeftOutlined />}>
        Back
      </Button>
    </div>
  );

  const data = humanResourceStore.detailData;
  const companyName = data?.contract?.map((it) => {
    return it.company?.name;
  });
  const positionName = data?.contract?.map((it) => {
    return it.position?.name;
  });

  return (
    <div className="site-card-less-wrapper" style={{ marginTop: 10 }}>
      <Breadcrumb style={{ marginLeft: 12, marginBottom: 15 }}>
        <Breadcrumb.Item>
          <Link to={LINKS.DASHBOARD}>
            <HomeOutlined />
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to={LINKS.EMPLOYEE}>Employee</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item style={{ fontWeight: "bold" }}>
          Detail Employee
        </Breadcrumb.Item>
      </Breadcrumb>
      <Row>
        <Col>{actionButton}</Col>
      </Row>
      <Card title="Detail Employee">
        <Descriptions layout="horizontal" bordered column={1}>
          <Descriptions.Item label="Name">{data.name || "-"}</Descriptions.Item>
          <Descriptions.Item label="NIP">{data.nip || "-"}</Descriptions.Item>
          <Descriptions.Item label="Birth Place">
            {(data.birth_place || "-").replace(/(^\w|\s\w)/g, (m) =>
              m.toUpperCase()
            )}
          </Descriptions.Item>
          <Descriptions.Item label="Date of Birth">
            {moment(data.date_of_birth).format("Do MMMM YYYY") || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Email">
            {data.email_company || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Phone Number">
            +62 {data.phone_number || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Whatsapp Number">
            +62 {data.whatsapp_number || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Address">
            {data.address || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Education">
            {data.education || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Marital Status">
            {data.status || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Blood Type">
            {data.blood || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Religion">
            {data.religion || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Sex">{data.sex || "-"}</Descriptions.Item>
          <Descriptions.Item label="NPWP Number">
            {data.npwp || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Relatives Name">
            {data.family || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Relatives Number">
            +62 {data.family_number || "-"}
          </Descriptions.Item>
          <Descriptions.Item label="Relatives Status">
            {data.family_status || "-"}
          </Descriptions.Item>

          <Descriptions.Item label="Action">
            <Button
              style={{
                background: "#0077b6",
                color: "white",
                boxShadow: "none",
              }}
              onClick={() => editSDM(data.id)}
            >
              <EditOutlined /> Edit
            </Button>
            <Button
              style={{
                background: "#03045e",
                color: "white",
                boxShadow: "none",
                marginLeft: "8px",
              }}
              onClick={() => confirmDelete(data.id)}
            >
              <DeleteOutlined /> Delete
            </Button>
          </Descriptions.Item>
        </Descriptions>
      </Card>
    </div>
  );
});
