import React, { useEffect, useState } from "react";
import {
  Divider,
  Form,
  DatePicker,
  Button,
  Card,
  Breadcrumb,
  Upload,
  InputNumber,
  Select,
  Input,
  message,
  Modal,
  Row,
  Col,
  Image,
} from "antd";
import {
  HomeOutlined,
  PlusOutlined,
  SwapRightOutlined,
  UploadOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import { Link, useHistory, useParams } from "react-router-dom";
import { LINKS } from "../../routes";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import moment from "moment";
import { appConfig } from "../../config/app";

export const FormEmployee = observer((props) => {
  const { mode } = props;
  const {
    human_resource: humanResourceStore,
    contracts: contractStore,
    division: divisionStore,
    position: positionStore,
  } = useStore();
  const [divisionState, setDivisionState] = useState("");
  const [gender, setGender] = useState("male");
  const [bloodType, setBloodType] = useState("Choose BloodType");
  const [religionType, setReligionType] = useState("Choose Religion");
  const [num, setNum] = useState("");
  const [txt, setTxt] = useState("");
  const [birthOfDate, setbirthOfDate] = useState(null);
  const [dataForm, setDataForm] = useState([]);
  const [items, setItems] = useState([
    "Islam",
    "Kristen Katolik",
    "Kristen Protestan",
    "Hindu",
    "Budha",
    "Kong-Hucu",
  ]);
  const [type, setType] = useState(["Married", "Unmarried"]);
  const [name, setName] = useState([]);
  const [mariage, setMariage] = useState([]);
  const [code, setCode] = useState();
  const history = useHistory();
  const [form] = Form.useForm();
  const { id: paramId } = useParams();
  const [fileList, setFileList] = useState([]);
  const [previewPdf, setPreviewPdf] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [loading, setLoading] = useState(false);
  const [fileUrl, setFileUrl] = useState("");
  const firstIndexFileList = fileList[0];

  const formItemLayout = {
    labelCol: { sm: { span: 6 } },
    wrapperCol: { sm: { span: 14 } },
  };

  const btnLayout = {
    wrapperCol: { offset: 21 },
  };

  const { Option } = Select;

  useEffect(() => {
    loadInitialData();
  }, []);

  const loadInitialData = async () => {
    await humanResourceStore.getData();
    await divisionStore.getAll();
    await positionStore.getAll();
    if (mode === "edit" && paramId) {
      await humanResourceStore.getDetail(paramId).then((res) => {
        setDataForm(res.body.results);
        const formData = res.body.results;

        form.setFieldsValue({
          name: formData.name,
          nip: formData.nip,
          email_company: formData.email_company,
          email_1: formData.email_1,
          email_2: formData.email_2,
          email_3: formData.email_3,
          phone_number: formData.phone_number,
          whatsapp_number: formData.whatsapp_number,
          address: formData.address,
          education: formData.education,
          status: formData.status,
          blood: formData.blood,
          religion: formData.religion,
          sex: formData.sex,
          birth_place: formData.birth_place,
          date_of_birth: moment(formData.date_of_birth),
          npwp: formData.npwp,
          family: formData.family,
          family_status: formData.family_status,
          family_number: formData.family_number,
        });
      });
    }
  };

  const saveData = async (values) => {
    let bodyData = {
      ...values,
    };

    console.log(bodyData, "ini body");
    try {
      if (mode === "edit" && paramId) {
        await humanResourceStore.editData(dataForm.id, bodyData);
        message.success("Updated Employee Success");
        history.push(`${LINKS.EMPLOYEE}/` + dataForm.id);
      } else {
        await humanResourceStore.addData(bodyData);
        message.success("Created Employee Success");
        history.push(LINKS.EMPLOYEE);
      }
    } catch (e) {
      console.log(e);
      Modal.error({
        title: "Gagal",
        content:
          e.response?.body?.message ||
          "Gagal menyimpan data SDM karena suatu kesalahan",
      });
    }
  };

  const submitForm = () => {
    form
      .validateFields()
      .then(async (res) => {
        await saveData(res);
      })
      .catch((e) => {
        console.log(e, "gagal");
      });
  };
  const handleChangeName = (e) => {
    e.persist();
    const value = e.target.value.replace(/\d/g, "");
    setTxt(value);
    return value;
  };

  const handleChangePhone = (e) => {
    e.persist();
    const value = e.target.value.replace(/\D/g, "");
    setNum(value);
    return value;
  };

  const onNameChange = (e) => {
    e.persist();
    setName(e.target.value.replace(/\d/g, ""));
  };

  const addItem = () => {
    let index = 0;
    console.log("addItem");
    setItems([...items, name] || `New item ${index++}`);
  };

  // Position Select

  const dataPosition = positionStore.listData;
  const positionsContract = [...new Set(dataPosition)].filter(
    (prop) => prop != undefined
  );

  const optionPositions = positionsContract.map((positions) => {
    return (
      <Option value={positions.id} key={positions.id}>
        {positions.name}
      </Option>
    );
  });

  const selectPositions = (
    <Select
      showSearch
      placeholder="Positions"
      style={{ width: 300 }}
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
        option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      {optionPositions}
    </Select>
  );

  // Positive Number
  const positiveNumberValidation = (_, value) => {
    if (value !== "" && +value < +0) {
      return Promise.reject("Angka harus lebih besar dari 0");
    }
    return Promise.resolve();
  };

  const handlerPrice = (value) => {
    console.log("changed", value);
  };

  const alpReg = new RegExp(/^([A-z\s])+$/);
  const numReg = new RegExp(/^[0-9]+$/i);

  const previewUpload = fileUrl?.includes(".pdf") ? (
    <iframe
      src={`${appConfig.imageUrl}${fileUrl ?? ""}`}
      width={"100%"}
      style={{ height: 500 }}
    />
  ) : (
    fileUrl && (
      <Image
        style={{ width: "100%", height: 500 }}
        preview={true}
        src={!fileUrl ? null : `${appConfig.imageUrl}${fileUrl ?? null}`}
      />
    )
  );

  const uploadButton = (
    <div>
      {loading ? (
        <LoadingOutlined />
      ) : (
        <Button icon={<UploadOutlined />}>Click to Upload</Button>
      )}
    </div>
  );

  const beforeUpload = (file) => {
    let isLt2M;
    let allowedFile = ["application/pdf"];
    let isValid = allowedFile.includes(file.type);
    if (!isValid) {
      message.error("Format File Hanya PDF!");
    }
    // isLt2M = file.size / 1024 / 1024 < 2;
    // if (!isLt2M) {
    //     message.error("File must smaller than 2MB!");
    // }
    return isValid ? true : Upload.LIST_IGNORE;
  };

  const handlePreview = async (file) => {
    const fileUrl = appConfig.imageUrl + file.response.path;
    setPreviewTitle(file.url?.substring(file.url?.lastIndexOf("/") + 1));
  };

  const handleChange = ({ fileList }) => {
    setFileList(fileList);
    if (fileList.length && fileList[0].status === "done") {
      form.setFieldsValue({
        file_url: fileList[0].response.path,
      });
      console.log(fileList, "list file");
      setFileUrl(fileList[0].response.path);
      setPreviewPdf(fileList[0].response.path);
      setPreviewTitle(fileList[0].name);
    }
  };

  return (
    <div className="client-card-wrapper" style={{ marginTop: 10 }}>
      <Breadcrumb style={{ marginLeft: 12, marginBottom: 15 }}>
        <Breadcrumb.Item>
          <Link to={LINKS.DASHBOARD}>
            <HomeOutlined />
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <Link to={LINKS.EMPLOYEE}>Employee</Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item style={{ fontWeight: "bold" }}>
          {mode === "edit" ? "Edit" : "Add"} Employee
        </Breadcrumb.Item>
      </Breadcrumb>
      <Card title={"Form Employee"}>
        <Row justify="center" gutter={16}>
          <Col>
            <Form
              {...formItemLayout}
              scrollToFirstError={true}
              form={form}
              initialValues={{
                birth_of_date: birthOfDate,
              }}
            >
              <Form.Item
                label="Name"
                name="name"
                getValueFromEvent={handleChangeName}
                rules={[
                  {
                    required: true,
                    // validator: aplhabetValidation,
                    pattern: alpReg,
                    message: "Please enter a valid name",
                  },
                ]}
              >
                <Input
                  value={txt}
                  onChange={handleChangeName}
                  autoComplete="on"
                  placeholder="Input your name"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                label="NIP"
                name="nip"
                rules={[
                  {
                    required: true,
                    pattern: numReg,
                    message: "Please enter a valid NIP",
                  },
                ]}
              >
                <Input
                  value={num}
                  maxLength={4}
                  onChange={handleChangePhone}
                  autoComplete="off"
                  placeholder="Input NIP"
                  autoFocus
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                label="Birth Place"
                name="birth_place"
                getValueFromEvent={handleChangeName}
                rules={[
                  {
                    required: true,
                    pattern: alpReg,
                    message: "Please enter a valid Birth Place",
                  },
                  { whitespace: true },
                  { type: "regexp" },
                ]}
              >
                <Input
                  value={txt}
                  onChange={handleChangeName}
                  autoComplete="off"
                  placeholder="Input Birth Place"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                label="Birth of Date"
                name="date_of_birth"
                rules={[
                  {
                    required: true,
                    message: "Please enter a Birth of Date",
                  },
                ]}
              >
                {/* <Input type="date" style={{ width: 200 }} /> */}
                <DatePicker />
              </Form.Item>

              <Form.Item
                label="Personal Email"
                name="email_1"
                rules={[
                  {
                    required: true,
                    message: "Please enter a Email",
                  },
                  { type: "email", message: "Input a Valid Email" },
                ]}
              >
                <Input
                  autoComplete="off"
                  placeholder="Input Email address"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                label="Company Email"
                name="email_company"
                rules={[
                  {
                    required: false,
                  },
                  { type: "email", message: "Please enter a Email Company" },
                ]}
              >
                <Input
                  autoComplete="off"
                  placeholder="Input email address"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                getValueFromEvent={handleChangePhone}
                label="Phone Number"
                name="phone_number"
              >
                <Input
                  prefix={"+62"}
                  value={num}
                  maxLength={20}
                  onChange={handleChangePhone}
                  autoComplete="off"
                  placeholder="Input Phone number"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                getValueFromEvent={handleChangePhone}
                suffix={"+62"}
                label="Whatsapp Number"
                name="whatsapp_number"
                rules={[
                  {
                    pattern: numReg,
                    required: true,
                    message: "Please enter a Whatsapp number",
                  },
                  {
                    type: "regexp",
                  },
                ]}
              >
                <Input
                  prefix={"+62"}
                  value={num}
                  onChange={handleChangePhone}
                  maxLength={20}
                  autoComplete="off"
                  placeholder="Input Whatsapp number"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                label="Address"
                name="address"
                rules={[
                  {
                    required: true,
                    message: "Address is required",
                  },
                ]}
              >
                <Input
                  autoComplete="off"
                  placeholder="Input Address"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                label="Last Education"
                name="education"
                rules={[
                  {
                    required: true,
                    message: "Last Education is required",
                  },
                ]}
              >
                <Select
                  placeholder={"Choose Last Education"}
                  style={{ width: 400 }}
                  value={"status"}
                >
                  <Option value="SD">SD</Option>
                  <Option value="SMP/MTS">SMP/MTS</Option>
                  <Option value="SMA/SMK/MA">SMA/SMK/MA</Option>
                  <Option value="D1">D1</Option>
                  <Option value="D2">D2</Option>
                  <Option value="D3">D3</Option>
                  <Option value="S1">S1</Option>
                  <Option value="S2">S2</Option>
                  <Option value="S3">S3</Option>
                </Select>
              </Form.Item>

              <Form.Item
                label="Marital Status"
                name="status"
                rules={[
                  {
                    required: true,
                    message: "Marital Status is required",
                  },
                ]}
              >
                <Select
                  placeholder={"Choose Marital Status"}
                  style={{ width: 400 }}
                  value={"status"}
                >
                  <Option value="married">Married</Option>
                  <Option value="unmarried">Unmarried</Option>
                  <Option value="others">Others</Option>
                </Select>
              </Form.Item>

              <Form.Item label="Blood Type" name="blood">
                <Select
                  placeholder={"Choose Blood Type"}
                  style={{ width: 400 }}
                  value={"blood"}
                >
                  <Option value="O">O</Option>
                  <Option value="A">A</Option>
                  <Option value="B">B</Option>
                  <Option value="AB">AB</Option>
                  <Option value="O+">O+</Option>
                  <Option value="A+">A+</Option>
                  <Option value="B+">B+</Option>
                  <Option value="AB+">AB+</Option>
                  <Option value="O-">O-</Option>
                  <Option value="A-">A-</Option>
                  <Option value="B-">B-</Option>
                  <Option value="AB-">AB-</Option>
                  <Option value="RH-NULL">Rh-Null</Option>
                </Select>
              </Form.Item>

              <Form.Item
                label="Religion"
                name="religion"
                rules={[
                  {
                    required: true,
                    message: "Religion is required",
                  },
                ]}
              >
                <Select
                  placeholder={"Choose Religion"}
                  style={{ width: 400 }}
                  value={"religion"}
                >
                  <Option value="islam">Islam</Option>
                  <Option value="katolik">Kristen Katolik</Option>
                  <Option value="protestan">Kristen Protestan</Option>
                  <Option value="hindu">Hindu</Option>
                  <Option value="budha">Budha</Option>
                  <Option value="konghucu">Kong Hu cu</Option>
                  <Option value="others">Others</Option>
                </Select>
              </Form.Item>

              <Form.Item
                label="Sex"
                name="sex"
                rules={[
                  {
                    required: true,
                    message: "Gender is required",
                  },
                ]}
              >
                <Select
                  placeholder={"Choose Gender"}
                  style={{ width: 400 }}
                  value={"sex"}
                >
                  <Option value="male">Male</Option>
                  <Option value="female">Female</Option>
                  <Option value="others">Others</Option>
                </Select>
              </Form.Item>

              <Form.Item
                getValueFromEvent={handleChangePhone}
                label="NPWP Number"
                name="npwp"
                rules={[
                  {
                    pattern: numReg,
                    required: true,
                    message: "Please enter a NPWP number",
                  },
                  {
                    type: "regexp",
                  },
                ]}
              >
                <Input
                  value={num}
                  onChange={handleChangePhone}
                  maxLength={15}
                  autoComplete="off"
                  placeholder="Input NPWP number"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                label="Relatives name"
                getValueFromEvent={handleChangeName}
                name="family"
                rules={[
                  {
                    pattern: alpReg,
                    required: true,
                    message: "Please enter a valid Relatives name",
                  },
                  {
                    type: "regexp",
                  },
                ]}
              >
                <Input
                  onChange={handleChangeName}
                  placeholder="Input Relatives name"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                getValueFromEvent={handleChangePhone}
                label="Relatives Number"
                name="family_number"
                rules={[
                  {
                    pattern: numReg,
                    required: true,
                    message: "Please enter a Relatives number",
                  },
                  {
                    type: "regexp",
                  },
                ]}
              >
                <Input
                  value={num}
                  prefix={"+62"}
                  maxLength={20}
                  onChange={handleChangePhone}
                  autoComplete="off"
                  placeholder="Input Relatives number"
                  style={{ width: 400 }}
                />
              </Form.Item>

              <Form.Item
                getValueFromEvent={handleChangeName}
                label="Relatives Status"
                name="family_status"
                rules={[
                  {
                    pattern: alpReg,
                    required: true,
                    message: "Please enter a valid Relatives status",
                  },
                  {
                    type: "regexp",
                  },
                ]}
              >
                <Input
                  onChange={handleChangeName}
                  placeholder="Input Relatives status"
                  style={{ width: 400 }}
                />
              </Form.Item>
              {mode !== "edit" ? (
                <>
                  <Divider orientation="left">Contract Employee </Divider>
                  <Form.Item
                    label="Type Contract"
                    name="type_contract"
                    rules={[
                      {
                        required: true,
                        message: "Select type contract  is required",
                      },
                    ]}
                  >
                    <Select
                      showSearch
                      placeholder="Select type contract"
                      style={{ width: 400 }}
                    >
                      <Option value="Perjanjian Kerja Waktu Tertentu (PKWT)">
                        Perjanjian Kerja Waktu Tertentu (PKWT)
                      </Option>
                      <Option value="Amandemen Perjanjian Kerja (APK)">
                        Amandemen Perjanjian Kerja (APK)
                      </Option>
                      <Option value="Perjanjian Kerja Magang (PKM)">
                        Perjanjian Kerja Magang (PKM)
                      </Option>
                      <Option value="Surat Pengangkatan Karyawan Tetap(SPKT)">
                        Surat Pengangkatan Karyawan Tetap (SPKT)
                      </Option>
                    </Select>
                  </Form.Item>
                  <Form.Item
                    label="Contract Period"
                    style={{ marginBottom: 0 }}
                    rules={[
                      {
                        required: true,
                        message: "Contract Period is required",
                      },
                    ]}
                  >
                    <Input.Group compact>
                      <Form.Item
                        name="start_contract"
                        rules={[
                          {
                            required: true,
                            message: "Input start contract!",
                          },
                        ]}
                      >
                        <DatePicker
                          allowClear={false}
                          style={{ width: 400, marginRight: 5 }}
                          placeholder="Start Contract"
                        />
                      </Form.Item>
                      <Form.Item
                        name="end_contract"
                        rules={[
                          {
                            required: true,
                            message: "Input expaired contract!",
                          },
                        ]}
                      >
                        <DatePicker
                          style={{ width: 400, marginLeft: 5 }}
                          placeholder="End Contract"
                        />
                      </Form.Item>
                    </Input.Group>
                  </Form.Item>
                  <Form.Item
                    label="Position"
                    name="id_position"
                    rules={[
                      {
                        required: true,
                        messagemessage: "Select position  is required",
                      },
                    ]}
                  >
                    {selectPositions}
                  </Form.Item>
                  <Form.Item
                    label="Salary"
                    name="salary"
                    rules={[
                      { required: true, message: "Input salary is required" },
                      { validator: positiveNumberValidation },
                    ]}
                  >
                    <InputNumber
                      style={{ width: 400 }}
                      formatter={(value) =>
                        `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                      }
                      parser={(value) => value.replace(/\Rp.\s?|([.]*)/g, "")}
                      onChange={handlerPrice}
                    />
                  </Form.Item>
                  <Form.Item label="Upload" name="file_url">
                    <Upload
                      name="file"
                      maxCount={1}
                      className="avatar-uploader"
                      action={`${appConfig.imageUrl}/files`}
                      fileList={fileList}
                      beforeUpload={beforeUpload}
                      onPreview={handlePreview}
                      onChange={handleChange}
                      onRemove={() => {
                        setFileUrl(null);
                        setPreviewTitle(null);
                      }}
                    >
                      {!firstIndexFileList ? uploadButton : null}
                    </Upload>
                  </Form.Item>
                </>
              ) : (
                ""
              )}

              <Form.Item {...btnLayout}>
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={submitForm}
                  size="large"
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
        <div style={{ width: "100%", marginTop: 20, textAlign: "center" }}>
          {previewUpload}
          {<span>{`${previewTitle ?? ""}`}</span>}
        </div>
      </Card>
    </div>
  );
});
