import React, { useState, useEffect } from "react";
import {
	Alert,
	Row,
	Col,
	Button,
	Statistic,
	Card,
	Empty,
	PageHeader,
	Dropdown,
	Table,
	Menu,
	Input,
	Select,
	Space,
	Typography,
	Tag,
    message,
    Modal,
    Form,
    DatePicker,
    Spin
} from "antd";
import {
    FilterOutlined,
    PlusOutlined,
    DeleteOutlined,
    EditOutlined
} from "@ant-design/icons";
import moment from "moment";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../../utils/useStores";
import * as _ from 'lodash';


export const AdjustTime = observer((initialData)=>{
    const store = useStore();
    const [form] = Form.useForm();
    const [modalForm, setModalForm] = useState(false);

    useEffect(()=>{
       fetchData()
    },[])

    async function fetchData() {
        await store.adjust.getAllAdjust();
    }


    let user = store.userData.id
    let role = store.userData.role
    async function onSubmit(e) {
        let data = {
            menu_name: e.menu_name,
            year: moment(e.year).format('YYYY')
        }

        await store.adjust.updateDate(e.id, data).then((res) => {
            console.log(res);
            setModalForm(false);
            message.success("Successfully Change Time!")
        })
        
    }

    function setEdit(value) {
        form.setFieldsValue({
            id: value.id,
            year: moment(`${value.year}`),
            menu_name: value.menu_name
        });
        setModalForm(true);
    }

    function modalEdit() {
        return <Modal title="Change Time Adjustment for Key Metrics or KPI" visible={modalForm}
            onOk={() => {
                form.validateFields().then((value) => {
                    onSubmit(value);
                    form.resetFields();
                })
            }}
            onCancel={() => { form.resetFields(); setModalForm(false) }}>
            <Form form={form} layout={"vertical"}>
                <Form.Item hidden={true} name="id">
                    <Input />
                </Form.Item>
                <Row justify="space-between">
                    <Col span={11}>
                        <Form.Item name="menu_name" label="Menu Name">
                            <Input disabled={true} />
                        </Form.Item>
                    </Col>
                    <Col span={11}>
                        <Form.Item name="year" label="Year Display">
                            <DatePicker picker="year" />
                        </Form.Item>
                    </Col>
                </Row>
            </Form>
        </Modal>
    }

       
   {
    const columns = [
        {
            title: 'Menu Adjustment',
            dataIndex: 'menu_name',
        },
        {
            title: 'Year Adjustment Start',
            dataIndex: 'year',
        },
        {
            title: 'Last Modified',
            width: '20%',
            dataIndex: 'updated_at',
            render: (text, record) => {
                return text ? moment(text).format("LLLL") : moment(record.created_at).format("LLLL");
            }
        },
        {
            title: "Action",
            dataIndex: "operation",
            render: (text, record) => {
                return <Button onClick={() => {
                    setEdit(record);
                    setModalForm(true);
                }}>
                    Edit
                    </Button>
            }
        }
    ];

        return <div style={{ padding: 10 }}>
            {modalEdit()}
            <Spin spinning={store.adjust.isLoading}>
                <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                    <PageHeader
                        className={'card-page-header'}
                        title={<div> Adjust Time </div>}
                    />
                    <Table columns={columns} bordered
                        dataSource={store?.adjust?.data}
                        scroll={{x:store.ui.mediaQuery.isMobile ? 500 : 0 }}
                    />
                </Card>
           </Spin>
            </div>
   }
})
export default AdjustTime;
