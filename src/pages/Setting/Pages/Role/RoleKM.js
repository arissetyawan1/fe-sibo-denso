import React, { useState, useEffect } from 'react';
import { Select, Table, Form, Input, Card, PageHeader, Modal, Button } from 'antd';
import { PlusOutlined } from "@ant-design/icons";
import * as _ from "lodash";
import {observer} from "mobx-react"; 
import { useStore } from "../../../../utils/useStores";

let SelectedRole
const RoleKM = observer((initialData) => {    
    const store = useStore();
    const [form] = Form.useForm();
    const Option = Select;
    const [state, setState] = useState({
        visible : false
    })

    useEffect(() => {
        fetchData()
    }, [])

    async function fetchData() {
        await store.role.getAll();
    }

    const showModal = (() => {
        setState({
            visible: !state.visible
        })
    })

    const columns =  [
        {
            title:'Role',
            dataIndex:'name', 

        },
        {
            title:'Key Metrics',
            dataIndex:'keymetrics_raw.name'

        }
    ]

    {
        return <div> 
        <Card bordered={false} className={'shadow'} bodyStyle={{padding:0}}>
            <PageHeader title="Key Metrics Role Access" extra={store.userData.role === "super_admin" && [
                <Button color="secondary" onClick={showModal}>
                    <PlusOutlined /> Add Access
                </Button>
            ]} />
        {renderModal()}
            <Table columns={columns} bordered pagination={false}/>
        </Card> </div>
    }


    function renderModal() {
        return <Modal destroyOnClose={true}
            visible={state.visible}
            title="Role Access on Key Metrics"
            closable={true}
            okText="Save"
            onOk={showModal}
            onCancel={() => { 
                form
                .validateFields()
                .then(values => {
                    form.resetFields();
                })
                showModal();
            }}
        > 
            <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
                <Form.Item name="role" label="Role" rules={[{required:true}]}>
                <Select
                    placeholder="Please select role"
                    allowClear
                    onChange={async (value) => {
                        SelectedRole = value;
                        fetchData();    
                        }} 
                >
                    {(store.role.data || ["-"]).map(d => <Option value={d.id}>{d.name}</Option>)}    
                </Select>
                </Form.Item>
                <Form.Item name="keymetrics" label="Key Metrics">
                    <Select mode="multiple" placeholder="Please select key metrics" >
                        <Option value="-"></Option>
                        <Option value="1">gg</Option>
                        <Option value="2">hh</Option>
                    </Select> 
                </Form.Item>
            </Form>
        </Modal>
    }
})

export default RoleKM;