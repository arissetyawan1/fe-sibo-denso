import React, {useEffect, useState} from "react";
import {
    Row,
    Col,
    Button,
    Statistic,
    Card,
    Empty,
    PageHeader,
    Dropdown,
    Table,
    Menu,
    Input,
    Select,
    Space,
    Typography,
    Tag,
    Checkbox
} from "antd";
import {FilterOutlined, PlusOutlined} from "@ant-design/icons";
import {observer} from "mobx-react-lite";
import {useStore} from "../../../utils/useStores";

const {Search} = Input;
const {Option} = Select;
const {Text} = Typography;
const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    margin: '-1px -1px 0 0',
    height: 184,
    transition: 'transform .4s'
};

const optionFilter = [
    {
        key: 1,
        value: 'All Roles'
    }, {
        key: 2,
        value: 'Administrator'
    }, {
        key: 3,
        value: 'Operation'
    },
    {
        key: 4,
        value: 'Finance'
    },
    {
        key: 5,
        value: 'Partner'
    },

];

function handleMenuClick(e) {
    console.log('click', e);
}

const menu = (
    <Menu onClick={handleMenuClick} style={{background: '#fff'}}>
        {optionFilter.map((i => {
            return <Menu.Item key={i.key}>{i.value}</Menu.Item>
        }))
        }
    </Menu>
);

function handleChange(value) {
    console.log(`selected ${value}`);
}

const optionList = (
    optionFilter.map((i => {
        console.log({userRole: i}, "User Role Types")
        return <Option value={i.key}>{i.value}</Option>
    }))
);

const columnDraft = [
    // {
    //     title: 'Id',
    //     dataIndex: 'id',
    //     render: (record, text) => record ?  <a>{text.id}</a> :  <p>none</p>,
    //     width: 100,
    //     ellipsis: true
    //
    // },
    // {
    //     title: 'Name',
    //     dataIndex: 'name',
    //     render: (record, text) => record ?
    //         <Tag style={{fontSize: 10, border: 0, borderRadius: 50, color: "#3d4eac", lineHeight: '18px'}}
    //              color="#d6ecff">Active</Tag> :
    //         <Tag style={{fontSize: 10, border: 0, borderRadius: 50, lineHeight: '18px'}} color={"#e3e8ee"}><Text
    //             type="secondary">{text.data.name}</Text></Tag>,
    //     width: '93%',
    //     ellipsis: true
    // },
    {
        title: 'Name',
        dataIndex: 'name',
        render: (text, record) => <span>{text}</span>,
        // width: '93%',
        ellipsis: true
    },
    {
        title: 'Description',
        dataIndex: 'description',
        render: (text, record) => <span>{text || '-'}</span>,
        // width: '93%',
        ellipsis: true
    },
    // {
    //     title: 'Active',
    //     dataIndex: 'active',
    //     render: (record, text) => <Checkbox disabled={false} checked={text.data.status}/>
    // },


];

const dataUser = [{
    id: 1,
    name: 'Aira 2',
    user: 'test@gmail',
    role: 'Administrator',
    role_desc: '',
    last_login: '',
    status: 'Active',
}];

export const Roles = observer((props) => {
    const store = useStore();
    // let [roleData] = useState([]);
    useEffect(() => {
        async function fetchData() {
            // You can await here
            // await store.user.initUser();
            // await store.role.initRole();
            await store.role.getAll();
            console.log({roleData: store.role.data}, "roleData taken from the firebase");
        }

        fetchData();
    }, []);
    return <div>
        <Card bordered={false} className={'shadow'} bodyStyle={{padding: 0}}>
            <PageHeader
                className={'card-page-header'}
                title={<div>
                    <Search
                        placeholder="FIlter by name or email"
                        onSearch={value => console.log(value)}
                        style={{width: 200, padding: '2.5px 11px', marginRight: 12}}
                    />

                </div>}
                subTitle=""
            />
            {store.role.data.length > 0 ?
                <Table key="1"
                       hasEmpty
                       size={"small"}
                       dataSource={store.role.data.length === 0 ? [] : store.role.data}
                       loading={store.role.data.length === 0}
                       columns={columnDraft}
                       style={{paddingLeft: 10}}
                /> :
                <div
                    style={{
                        minHeight: 'calc(100vh - 450px)',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                    <Empty/>

                </div>}

        </Card>
    </div>
});
