import React, { useState, useEffect } from "react";
import {
	Alert,
	Row,
	Col,
	Button,
	Statistic,
	Card,
	Empty,
	PageHeader,
	Dropdown,
	Table,
	Menu,
	Input,
	Select,
	Space,
	Typography,
	Tag,
    message,
    Modal,
    Form,
    Spin
} from "antd";
import {
    FilterOutlined,
    PlusOutlined,
    DeleteOutlined,
    EditOutlined
} from "@ant-design/icons";
import moment from "moment";
import { observer } from "mobx-react-lite";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { useStore } from "../../../../utils/useStores";
import * as _ from 'lodash';


export const Formula = observer((initialData)=>{
    const store = useStore();
    const [form] = Form.useForm();
    let selectedType

    const[state,setState] = useState({
        success : false
    })
    const [filterMode, setFilterMode] = useState(false);
    const [filterData, setFilterData] = useState([]);

    useEffect(()=>{
       fetchData()
    },[])

    async function fetchData(){
        await store.formula.getAllFormula();
    }



    const toggleSuccess = (() =>{
        setState({
            success: !state.success,
        });
        console.log(state.success)
    })

    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        // e.preventDefault()
        const data = {
            formula_key: e.formula_key,
            name: e.name,
            formula_value: e.formula_value,
        };
        const log = {
            user_id : user,
            event_name: "Update Formula",
            data: {
                location:{
                    pathname:"/app/settings/formula",
                    search: "",
                    hash: "",
                    key: role,
                },
                action:"PUSH",
            },
        }

        if (e.isEdit) {
            // console.log(data, "is edit")
            // console.log(log, "Oke?")
            store.log.createData(log)
            store.formula.updateFormula(e.isEdit, data)
                .then(res => {
                    message.success('Formula Updated!');
                    toggleSuccess();
                    fetchData();
                })
                .catch(err => {
                    message.error(`Error on Updating Formula, ${err.message}`);
                    message.error(err.message);
                });
        }
         else {

            store.formula.create(data)
                .then(res => {
                    message.success('Formula Created!');
                    toggleSuccess();
                    fetchData();
                })
                .catch(err => {
                    message.error(`Error on creating Formula, ${err.message}`);
                    message.error(err.message);
                });
                }
        }

        const setEditMode =  (record) => {
            setState(prevState =>({
                ...prevState,
                success :true,
                // isEdit :true
            }))
            form.setFieldsValue({
                isEdit: record.id,
                success: true,
                formula_key: record.formula_key,
                name: record.name,
                formula_value : record.formula_value
            })
        }



   {
    const columns = [
        {
            title: 'Title',
            dataIndex: 'name',
            width :'60%',
            render: (text, record) => <span>{record.name}</span>,
        },
        {
            title: 'Type',
            dataIndex: 'formula_key',
            render: (text, record) => <span>{record.formula_key}</span>,
        },
        {
            title: 'Value',
            dataIndex: 'formula_value',
            render: (text, record) => <span>{record.formula_value}</span>,
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            margin : 5,
            render: (text, record) => (
                <span>
                   <Button
                   onClick={()=>{setEditMode(record)}}
                   >
                        Edit
                    </Button>
                </span>
            )
        },
    ];
        
        function filterFunction(value) {
            if (value !== "All Type") {
                let dataFind = _.filter(store.formula.data, i => i.formula_key === value)
                setFilterData(dataFind);
                setFilterMode(true);
            } else {
                setFilterData([]);
                setFilterMode(false);
            }
        }

        return <div style={{padding:10}}>
        {renderModal()}
            <Spin spinning={store.formula.isLoading}>
                <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                    <PageHeader
                        className={'card-page-header'}
                        title={<div> Formula <br style={{ display: store.ui.mediaQuery.isMobile ? '' : 'none'}} />
                            <Select
                            style={{width:store.ui.mediaQuery.isMobile ? 240 : 270, marginLeft: store.ui.mediaQuery.isMobile ? 0 : 10,
                                marginTop: store.ui.mediaQuery.isMobile ? 10 : 0,}}
                            size={'small'}
                            dropdownClassName={'select-dropdown-custom'}
                            className={'select-'}
                            bordered={true}
                            onChange={(value) =>{
                                filterFunction(value);
                            }} defaultValue="All Type">

                                <Select.Option key={'All Type'} value={'All Type'}>All Type</Select.Option>
                                <Select.Option key={'month'} value={'month'}>Month</Select.Option>
                                <Select.Option key={'grade'} value={'grade'}>Grade</Select.Option>
                                <Select.Option key={'actvstarget'} value={'actvstarget'}>Actual vs Target</Select.Option>
                                <Select.Option key={'targetvsact'} value={'targetvsact'}>Target vs Actual</Select.Option>

                            </Select>
                        </div>}
                        subTitle=""
                        // extra={[
                        //     <Button color="secondary" onClick={toggleSuccess} >
                        //         <PlusOutlined /> New
                        //     </Button>
                        // ]}

                    />
                    <Table columns={columns} bordered
                        dataSource={filterMode ? filterData : store.formula.data}
                        pagination={{ pageSize: 6 }}
                        scroll={{x:store.ui.mediaQuery.isMobile ? 500 : 0 }}
                    />
                </Card>
           </Spin>
            </div>
   }
   function renderModal() {
    return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
    title="Update Formula"
    okText="Save"
    cancelText="Cancel"
    bodyStyle={{ background: '#f7fafc' }}
    onCancel={() => {
        form.setFieldsValue({});
        toggleSuccess();
    }}
    onOk={() => {
        form
            .validateFields()
            .then(values => {
                form.resetFields();
                onSubmit(values);
                form.setFieldsValue({});
            })
            .catch(info => {
                console.log('Validate Failed:', info);
            });
    }}
    >
        <div className="animated fadeIn">
            <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
                <Form.Item  name="isEdit" hidden={true}>
                    <Input  />
                </Form.Item>
                <Form.Item name="formula_key" label="Type" rules={[{ required: true }]} >
                    <Select
                    allowClear
                    >
                    <Select.Option>Select Type</Select.Option>
                    <Select.Option value={'month'}>Month</Select.Option>
                    <Select.Option value={'grade'}>Grade</Select.Option>
                    </Select>

                </Form.Item>
                <Form.Item name="name" label="Name" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
                <Form.Item name="formula_value" label="Value" rules={[{ required: true }]}>
                    <Input />
                </Form.Item>
            </Form>
        </div>
    </Modal>
}
})
export default Formula;
