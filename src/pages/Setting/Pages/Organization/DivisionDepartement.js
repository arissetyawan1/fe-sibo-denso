import React, { useEffect, useState } from "react";
import {
	Tree, TreeSelect, Spin,
	Row,
	Col,
	Button,
	Statistic,
	Card,
	Empty,
	PageHeader,
	Dropdown,
	Table,
	Menu,
	Input,
	Select,
	Space,
	Typography,
	Tag,
	message, Modal, Form, Popconfirm
} from "antd";
import { FilterOutlined, PlusOutlined } from "@ant-design/icons";
import {inject, observer } from "mobx-react-lite";
import upperFirst from "lodash.upperfirst";
import { useStore } from "../../../../utils/useStores";
import { OrganizationForm } from "./organizationForm";
import * as _ from "lodash";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { filter } from "bluebird";

const { Search } = Input;
const { Option } = Select;
const { Text } = Typography;

export const DivisionDepartement = observer((initialDataForm) => {
	const store = useStore();
	const[state, setState] = useState({
		data :[]
	})
	const [form] = Form.useForm();

	const [showUserFormModal, setShowUserFormModal] = useState(false);
	const [success, setSuccess] = useState(false);
	const[idEdit, setIdEdit] =useState('')
	const [confirmLoading, setConfirmLoading] = useState(false);
	const [initialData, setInitialData] = useState({});
	const [getAllNew, setGetAllNew] = useState('');
	const [manipulate, setManipulate] = useState(false)
	const [manipulated, setManipulated] = useState([])
	const [activeTab, setActiveTab] = useState(new Array(4).fill('1'))
	// const [data, setData] = useState([])
	const [type, setType] = useState(['Org', 'Dept/Div', 'Pos'])
	const [loading, setLoading] = useState(false)
	const [visible, setVisible] = useState(false)
	const [selectedKeys, setSelectedKeys] = useState({})

	// let filterData = store.user.data
	
	useEffect(() => {
		fetchData();
	}, []);
		
	function fetchData() {
		store.organizations.getAll()
		.then(res => {
			store.organizations.data = store.organizations.data.map((x,i) =>{
				return {
					...store.organizations.data[i],
					key : i +1
				}
			})
			// setState({
			// 	data : res.body.data.map((d,i) => ({
			// 	...d,
			// 	title: d.name,
			// 	key: i+1,
			// 	// children: []
			// }))
			// })
		});
		store.organizations.getAllOrganization()
	}

	const setEditMode = (record) => {
        console.log(record, "ini ya")
		setSuccess(true)
		setIdEdit(record.id)
		console.log(success)
        form.setFieldsValue({
            isEdit: record.id,
            name: record.name,
        })
        
	}
	
	const saveEdit = (e) =>{
		const dataEdit = {
			name : e.name
		}
		console.log(e.isEdit, dataEdit)
		store.organizations.update(e.isEdit, dataEdit)
		.then(res => {
			message.success('Organizations Saved Successfully!');
			setSuccess(false); 
			fetchData();
		})
		.catch(err => {
			message.error(`Error on creating Organizations, ${err.message}`);
			message.error(err.message);
		});
	}

	const rowSelection = {
		onChange: (selectedRowKeys, selectedRows) => {
			console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
		},
		onSelect: (record, selected, selectedRows) => {
			console.log(record, selected, selectedRows);
		},
		onSelectAll: (selected, selectedRows, changeRows) => {
			console.log(selected, selectedRows, changeRows);
		},
	};

	const columnsDivision =[
		{
			title: 'Division',
			dataIndex: 'name',
			key: 'name',
			className: 'pl-16pt',
		},
		{
			title: 'Action',
			dataIndex: 'action',
			key: 'action',
			className: 'pl-16pt',
			render: (text, record) => {
				return record.name == "NO DIVISION" ? null : <span>
			   <Button onClick={() => { setEditMode(record) }} >
					Edit
				</Button>
				<Button onClick={() => {
					Modal.confirm({
						title: 'Do you want to delete these Organization?',
						icon: <ExclamationCircleOutlined />,
						onOk() {
							return store.organizations.delete(record.id).then(res => {
								message.success('Deleted Successfully!');
								setSuccess(false); 
								fetchData();
							})
							.catch(err => {
								message.error(`Error on deleting Organizations, ${err.message}`);
								setSuccess(false); 
								message.error(err.message);
							});
						},
						onCancel() { },
					});
				}} style={{marginLeft: 8}}>Delete</Button>
			</span>
			}
		}
	]
	const expandedRowRenderPosition = (record, index, indent, expanded) => {
		
	
		const columns = [
		  {
			title: 'Position',
			dataIndex: 'name',
		  },
		  {
			title: 'Action',
			dataIndex: 'action',
			key: 'action',
			className: 'pl-16pt',
			render: (text, record) =>
			<span>
			   <Button onClick={() => { setEditMode(record) }} >
					Edit
				</Button>
				<Button onClick={() => {
					Modal.confirm({
						title: 'Do you want to delete these Organization?',
						icon: <ExclamationCircleOutlined />,
						onOk() {
							return store.organizations.delete(record.id).then(res => {
								message.success('Deleted Successfully!');
								setSuccess(false); 
								fetchData();
							})
							.catch(err => {
								message.error(`Error on deleting Organizations, ${err.message}`);
								setSuccess(false); 
								message.error(err.message);
							});
						},
						onCancel() { },
					});
				}} style={{marginLeft: 8}}>Delete</Button>
			</span>
		}
		  
		];
	
	
		const translatedData = _.filter(store.organizations.organizationAll, i=>i.organization_tree_id === record.id).map(p => {
		  return {
			id : p.id,
			name: p.name,
		  }
		});
	
		return <Table columns={columns} dataSource={translatedData} pagination={false} />
	  };
	const expandedRowRenderDepartment = (record, index, indent, expanded) => {
		
	
		const columns = [
		  {
			title: 'Department',
			dataIndex: 'name',
		  },
		  {
			title: 'Action',
			dataIndex: 'action',
			key: 'action',
			className: 'pl-16pt',
			  render: (text, record) => {
				return record.name === "NO DEPARTMENT" ? null :
			<span>
			   <Button onClick={() => { setEditMode(record) }} >
					Edit
				</Button>
				<Button onClick={() => {
					Modal.confirm({
						title: 'Do you want to delete these Organization?',
						icon: <ExclamationCircleOutlined />,
						onOk() {
							return store.organizations.delete(record.id).then(res => {
								message.success('Deleted Successfully!');
								setSuccess(false); 
								fetchData();
							})
							.catch(err => {
								message.error(`Error on deleting Organizations, ${err.message}`);
								setSuccess(false); 
								message.error(err.message);
							});
						},
						onCancel() { },
					});
				}} style={{marginLeft: 8}}>Delete</Button>
			</span>
			}
		}
		];
	
	
		const translatedData = _.filter(store.organizations.organizationAll, i=>i.organization_tree_id === record.id).map(p => {
		  return {
			id : p.id,
			name: p.name,
			key : p.id
		  }
		});
	
		return <Table columns={columns} expandedRowRender={expandedRowRenderPosition} dataSource={translatedData} pagination={false} />;
	  };
	
	let data = state.data;
	let selectedData
	const rowNew = () => {
		return <table className=' mb-0 thead-border-top-0 table-nowrap'/>
	};
	let user = store.userData.id
	let role = store.userData.role
	
	const components = {
		table: rowNew
	};
	
	{
	return (
		<div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
			<Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
				<PageHeader
					className={'card-page-header'}
					title={<div>Organizations
						
					</div>}
					subTitle=""
					extra={[
						<Button key="1" onClick={() => {
							setInitialData({});
							setShowUserFormModal(true);
						}}>
							<PlusOutlined /> New
						</Button>
					]}
				/>
				{renderModalEdit()}
				{store.organizations.data.length > 0 ?
					<Card>
						<Table  scroll={{ x: store.ui.mediaQuery.isMobile?500:'' }} columns={columnsDivision} expandedRowRender={expandedRowRenderDepartment} dataSource={store.organizations.data} pagination={false} />;
					
					</Card>
					:
					<div
						style={{
							minHeight: 'calc(100vh - 450px)',
							display: 'flex',
							justifyContent: 'center',
							alignItems: 'center'
						}}>
						<Empty />

					</div>}
			</Card>
			{showUserFormModal && <OrganizationForm visible={showUserFormModal} onSubmit={async (data) => {
				const log = {
					user_id : user,
					event_name: "Add Division/Department/Position",
					data: {
						location:{
							pathname:"/app/settings/division-departement",
						search: "",
						hash: "",
						key: role,
					},
					action:"PUSH",
					},
				}
				// data = { ...data }
				// console.log(log, "oke?")
				console.log( data.organization , 'data -> ')

				setConfirmLoading(true);
				if (initialData.id) {
					await store.user.update(initialData.id, data);
				} else {
					await store.log.createData(log)
					await store.organizations.create(data).then(() => {
						store.organizations.getAll().then(res => {
							store.organizations.data = res.body.data
							store.organizations.filterData = res.body.data
							fetchData()
						});
					});

				}
				setConfirmLoading(false);
				setShowUserFormModal(false);
			}} onCancel={() => {
				setShowUserFormModal(false);
			}} confirmLoading={confirmLoading} initialData={initialData} />} 
		</div>
	)

	function renderModalEdit() {
        return <Modal visible={success} closable={false} confirmLoading={false} destroyOnClose={true}
		title="Organizations"
		okText="Update"
        cancelText="Cancel"
        bodyStyle={{ background: '#f7fafc' }}
        
		
		footer={[
			
			<Button key="back" onClick={() => {
				setSuccess(false)
				form.setFieldsValue({});
				
			}}>
              Cancel
            </Button>,
            <Button key="submit" type="primary" loading={loading} onClick={() => {
				form
					.validateFields()
					.then(values => {
						form.resetFields();
						saveEdit(values);
						form.setFieldsValue({});
					})
					.catch(info => {
						console.log('Validate Failed:', info);
					});
			}}>
              Update
            </Button>,
          ]}
        >
			<div className="animated fadeIn">
				<Button></Button>
				<Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialDataForm} >
				<Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
				<Form.Item
					name='name'
					label="Name"
					rules={[
						{
							required: true,
							message: 'Please input the title of departement or division !',
						},
					]}
				>
					<Input />
				</Form.Item>
				</Form>
			</div>
        </Modal>
    }
	}
});
