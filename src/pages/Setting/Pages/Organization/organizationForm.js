import { Form, Input, Modal, Select } from "antd";
import React, { Component, useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import * as _ from "lodash";
import { useStore } from "../../../../utils/useStores";

let selectedDiv
export const OrganizationForm = observer(({ visible, onSubmit, onCancel, confirmLoading, initialData }) => {
	const [form] = Form.useForm();
	const [showPartnerSelection, setShowPartnerSelection] = useState(false);
	const store = useStore();
	useEffect(() => {
		fetchData();
	}, []);
	
	async function fetchData() {
		
		await setShowPartnerSelection(initialData.role_id === 'a0c395ff-a7b6-4668-95ad-63bbcb31485f');
		await store.division.getAll();
		await store.department.getAll();
		await store.organizations.getChilds(selectedDiv);
		await store.organizations.getAll();
		await store.kpiMaster.getDepartments({level: 'department-4-5'});
		await store.kpiMaster.getCorporate();
		store.organizations.filterData = _.filter(store.organizations.data, i => i.type === "department");
	}

	return <Modal
		maskClosable={false}
		visible={visible}
		width={'448px'}
		closable={false}
		bodyStyle={{ background: '#f7fafc' }}
		title="Organizations"
		okText="Save"
		cancelText="Cancel"
		onCancel={() => {
			form.setFieldsValue({});
			onCancel();
		}}
		confirmLoading={confirmLoading}
		destroyOnClose={true}
		onOk={() => {
			form
				.validateFields()
				.then(values => {
					form.resetFields();
					onSubmit(values);
					form.setFieldsValue({});
				})
				.catch(info => {
					console.log('Validate Failed:', info);
				});
		}}
	>


                <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
                    <Form.Item
						name="type"
						label="Type"
						rules={[
							{
								required: true,
								message: 'Please choose type',
							},
						]}
					>
						<Select
							showSearch
							style={{ width: 200 }}
							placeholder="Select Type"
							optionFilterProp="children"
							filterOption={(input, option) =>
								option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						>
							<Select.Option value={'division'}>division</Select.Option>
							<Select.Option value={'department'}>departement</Select.Option>
							<Select.Option value={'position'}>position</Select.Option>
							{/* {store.role.data.map(it => {
								return <Select.Option value={it.id}>{it.name}</Select.Option>;
							})} */}
						</Select>
					</Form.Item>
					<Form.Item
						noStyle
						shouldUpdate={(prevValues, currentValues) => prevValues.type !== currentValues.type}
						>
						{({ getFieldValue }) => {
							return getFieldValue('type') === 'department' ? (
							<Form.Item name="organization_tree_id" label="Divisi"  rules={[{ required: true }]}>
								<Select
								placeholder="Pilih Divisi"
								allowClear
								onChange={async (value) => {
									selectedDiv = value;
									fetchData();    
								
								}} 
								>
								<Select.Option>Divisi</Select.Option>
								{(store.division.data || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
								</Select>
							</Form.Item>
							) : null;
						}}
					</Form.Item>
					<Form.Item
						noStyle
						shouldUpdate={(prevValues, currentValues) => prevValues.type !== currentValues.type}
						>
						{({ getFieldValue }) => {
							return getFieldValue('type') === 'position' ? (
							<Form.Item  label="Divisi"  rules={[{ required: true }]}>
								<Select
								placeholder="Pilih Divisi"
								allowClear
								onChange={async (value) => {
									selectedDiv = value;
									fetchData();    
								
								}} 
								>
								<Select.Option>Divisi</Select.Option>
								{(store.division.data || ["-"]).map(d => <Select.Option value={d.id}>{d.name}</Select.Option>)}
								</Select>
							</Form.Item>
							) : null;
						}}
					</Form.Item>
	
                   
                    <Form.Item
                            noStyle
							shouldUpdate={(prevValues, currentValues) => prevValues.type !== currentValues.type}
							>
							{({ getFieldValue }) => {
                                return getFieldValue('type') === 'position' ? (
                                    <Form.Item 
                                    name="organization_tree_id"
                                    label="Department" rules={[{ required: true }]}>
                                            <Select
                                        allowClear>
                                        <Select.Option>-</Select.Option>
                                        {store.organizations.dataDepart.map(c => <Select.Option value={c.id}>{c.name}</Select.Option>)}
                                        </Select>
                                    </Form.Item>
                                ) : null;
                            }}
                    </Form.Item>
					<Form.Item
						name='name'
						label="Name"
						rules={[
							{
								required: true,
								message: 'Please input the title of departement or division !',
							},
						]}
					>
						<Input />
					</Form.Item>

                    
                    
                    
                </Form>
	</Modal>
});
