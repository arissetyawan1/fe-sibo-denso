import React, { useState, useEffect } from "react";
import {
	Alert,
	Row,
	Col,
	Button,
	Statistic,
	Card,
	Empty,
	PageHeader,
	Dropdown,
	Table,
	Menu,
	Input,
	Select,
	Space,
	Typography,
	Tag,
    message,
    Modal,
    Form
} from "antd";
import {
    FilterOutlined,
    PlusOutlined,
    DeleteOutlined,
    EditOutlined 
} from "@ant-design/icons";
import moment from "moment";
import { observer } from "mobx-react-lite";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { useStore } from "../../../../utils/useStores";
import * as _ from 'lodash';


export const VisionMission = observer((initialData)=>{
    const store = useStore();
    const [form] = Form.useForm();
    const [filterMode, setFilterMode] = useState(false);
    const [filterData, setFilterData] = useState([]);
    let selectedType

    const[state,setState] = useState({
        success : false
    })

    useEffect(()=>{
       fetchData()
    },[])

    async function fetchData(){
        await store.visionMission.getAll();
    }


    const toggleSuccess = (() =>{
        setState({
            success: !state.success,
        });
        console.log(state.success)
    })

    let user = store.userData.id
    let role = store.userData.role
    function onSubmit(e) {
        // e.preventDefault()
        const data = {
            type: e.type,
            title: e.title,
            
        };
        const log = {
            user_id : user,
            event_name: "Update Visi, Misi & Culture",
            data: {
                location:{
                    pathname:"/app/settings/vision_mission",
                    search: "",
                    hash: "",
                    key: role,
                },
                action:"PUSH",
            },
        }
    
        if (e.isEdit) {
            // console.log(data, "is edit")
            // console.log(log, "Oke?")
            store.log.createData(log)
            store.visionMission.update(e.isEdit, data)
                .then(res => {
                    message.success('Vision, Mision & Culture Updated!');
                    toggleSuccess();
                    fetchData();
                })
                .catch(err => {
                    message.error(`Error on Updating Vision, Mision & Culture, ${err.message}`);
                    message.error(err.message);
                });
        } else {
    
            store.visionMission.create(data)
                .then(res => {
                    message.success('Vision, Mision & Culture Created!');
                    toggleSuccess();
                    fetchData();
                })
                .catch(err => {
                    message.error(`Error on creating Vision, Mision & Culture, ${err.message}`);
                    message.error(err.message);
                });
                }
        }

        const setEditMode =  (record) => {
            setState(prevState =>({
                ...prevState,
                success :true,
                // isEdit :true
            }))
            form.setFieldsValue({
                isEdit: record.id,
                success: true,
                type: record.type,
                title: record.title
            })
        }
    


   {
    const columns = [
        {
            title: 'Title',
            dataIndex: 'title',
            width :'60%',
            render: (text, record) => <span>{record.title}</span>,
        },
        {
            title: 'Type',
            dataIndex: 'type',
            render: (text, record) => <span>{record.type}</span>,
        },
       
        
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            margin: 5,
            fixed:'right',
            render: (text, record) => (
                <span>
                   <Button 
                   onClick={()=>{setEditMode(record)}} style={{margin:10}}
                   >
                        Edit
                    </Button>
                    <Button onClick={() => {
                        Modal.confirm({
                            title: 'Do you want to delete?',
                            icon: <ExclamationCircleOutlined />,
                            onOk() {
                                message.success('successfully delete data!')
                                return store.visionMission.delete(record.id);
                            },
                            onCancel() { },
                        });
                    }}>Delete</Button>
                </span>
            )
        },
        ];
        
        function filterFunction(value) {
            if (value !== "All Type") {
                let dataFind = _.filter(store.visionMission.data, i => i.type === value)
                setFilterData(dataFind);
                setFilterMode(true);
            } else {
                setFilterMode(false)
                setFilterData([])
            }
        }

       return <div style={{padding:10}}>
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding:0 }}>
                <PageHeader
                    className={'card-page-header'}
                   title={<div> Vision, Mission & culture
                       <br style={{ display: store.ui.mediaQuery.isMobile ? '' : 'none'}} />
                   <Select
                           style={{
                               width: 160, marginLeft: store.ui.mediaQuery.isMobile ? 0 : 10,
                               marginTop: store.ui.mediaQuery.isMobile ? 10 : 0,
                               marginBottom: store.ui.mediaQuery.isMobile ? 10 : 0
                           }}
						size={'small'}
						dropdownClassName={'select-dropdown-custom'}
						className={'select-'}
						bordered={true}
						onChange={(value) =>{
                            filterFunction(value)
                        }} defaultValue="All Type">
                      
							<Select.Option key={'All'} value={'All Type'}>All</Select.Option>
                            <Select.Option key={'vision'} value={'vision'}>Vision</Select.Option>
                            <Select.Option key={'mission'} value={'mission'}>Mission</Select.Option>
                            <Select.Option key={'culture'} value={'culture'}>Culture</Select.Option>
                       </Select>
                    </div>}
                    subTitle=""
                    extra={[
                        <Button color="secondary" onClick={toggleSuccess}>
                            <PlusOutlined /> New
                        </Button>
                    ]}
                   
                />          
                {renderModal()}       
                <Table columns={columns} bordered
                   dataSource={filterMode ? filterData: store.visionMission.data }
                   pagination={{ pageSize: 50 }}
                   scroll={{x:store.ui.mediaQuery.isDesktop ? 0 : 900 }}
                />
            </Card>
            </div>
   }
   function renderModal() {
    return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
    title="Add Vision, Mission & Culture"
    okText="Save"
    cancelText="Cancel"
    bodyStyle={{ background: '#f7fafc' }}
    onCancel={() => {
        form.setFieldsValue({});
        toggleSuccess();
    }}
    onOk={() => {
        form
            .validateFields()
            .then(values => {
                form.resetFields();
                onSubmit(values);
                form.setFieldsValue({});
            })
            .catch(info => {
                console.log('Validate Failed:', info);
            });
    }}
    >
        <div className="animated fadeIn">
            <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData}>
                <Form.Item  name="isEdit" hidden={true}>
                    <Input  />
                </Form.Item>
                <Form.Item name="type" label="Type" rules={[{ required: true }]} >
                    <Select
                    allowClear
                    >
                    <Select.Option>Select Type</Select.Option>
                    <Select.Option value={'vision'}>Vision</Select.Option>
                    <Select.Option value={'mission'}>Mission</Select.Option>
                    <Select.Option value={'culture'}>Culture</Select.Option>
                    </Select>
                        
                </Form.Item>
                <Form.Item name="title" label="Title" rules={[{ required: true }]}>
                    <Input.TextArea/>
                </Form.Item>
            </Form>
        </div>
    </Modal>
}
})

 