import React, {Component, useState, useEffect} from 'react'; 
import { Row, Col, Button, Statistic, Card, Empty, Dropdown, Table, Menu, Input, Select, Space, Typography,	Tag, message, Modal, PageHeader } from "antd";
import { ExclamationCircleOutlined, SearchOutlined } from "@ant-design/icons";
import { observer } from "mobx-react-lite"; 
import { useStore } from "../../../../utils/useStores";
import moment from "moment";
import * as _ from "lodash";

const AuditLog = observer((initialData) => { 
  const store = useStore();
  const Option = Select;
  const [filterData, setFilterData] = useState([]);
  const [filterMode, setFilterMode] = useState(false);
  const [selectedActivity, setSelectedActivity] = useState('')
  let selectedEvent

  useEffect(() => {
    fetchData();
  }, []);
  
  async function fetchData() {
    await store.user.getAll();
    await store.log.getAllLog();
  }
  const columns = [
    {
      title: "Data Change",
      dataIndex: "created_at",
      key: "created_at",
      render: (text, record) => { 
        return moment(record.created_at).format("DD MMMM YYYY, HH:mm")
      }
    }, 
    {
      title: "User",
      dataIndex: "user",
      key: "user",
      render: (text) => {
        if (text) {
          return <span>{(_.get(text, 'full_name') || _.get(text, 'email') || _.get(text, 'username'))}</span>
        } else {
          return "-"
        }
      }
    }, 
    {
      title: "Event",
      dataIndex: "event_name",
      key: "event_name",
      render: (text, record) => <span>{record.event_name}</span>,
    },
  ];
  
  function filterFunction(value) {
    if (value === "All Event") {
      delete store.log.query.activity
      fetchData();
    } else {
      store.log.query.activity = `${value}`
      fetchData();
    }
  }
  
  { 
    return (<div style={{ margin: 10 }}>
      <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
        <PageHeader 
        title={<div>Audit Log</div>} 
        extra={[<Select
          style={{width:270, marginLeft: store.ui.mediaQuery.isMobile ? 0 : 10,
            marginTop: store.ui.mediaQuery.isMobile ? 10 : 0,
            marginBottom: store.ui.mediaQuery.isMobile ? 10 : 0}}
          className={'select-'}
          bordered={true}
          onChange={(value) =>{
                  filterFunction(value)
              }} defaultValue="All Event">
                    
            <Option value={'All Event'}>All</Option>
            <Option value={'Add User'}>Add User</Option>
            <Option value={'Add Division/Department/Position'}>Add Division/Department/Position</Option>
            <Option value={'Add / Edit Corporate'}>Add / Edit Corporate</Option>
            <Option value={'Add / Edit Department Gol. 1-3'}>Add / Edit Department Gol. 1-3</Option>
            <Option value={'Add / Edit Department Gol. 4-5'}>Add / Edit Department Gol. 4-5</Option>
            <Option value={'Add / Edit Divisi Gol. 1-3'}>Add / Edit Divisi Gol. 1-3</Option>
            <Option value={'Add / Edit Divisi Gol. 4-5'}>Add / Edit Divisi Gol. 4-5</Option>
            <Option value={'Download Operational Data'}>Download Operational Data</Option>
            <Option value={'Submit Event Calendar'}>Submit Event Calendar</Option>
            <Option value={'Update Key Metrics'}>Update Key Metrics</Option>
            <Option value={'Update KPI - Corporate'}>Update KPI - Corporate</Option>
            <Option value={'Update KPI Department'}>Update KPI Department</Option>
            <Option value={'Update KPI Divisi'}>Update KPI Divisi</Option>
            <Option value={'Update Visi dan Misi'}>Update Visi dan Misi</Option>
            <Option value={'Update Formula'}>Update Formula</Option>
            <Option value={'Upload Operational Data'}>Upload Operational Data</Option>
        </Select>]}
        />
        <Table columns={columns} pagination={{ total: store.log.maxPageLog, current: store.log.page, pageSize: 30 }} dataSource={filterMode ? filterData : store.log.data}
          scroll={{ x: store.ui.mediaQuery.isDesktop ? 0 : 600, y: 320 }}
          onChange={(page) => {
            store.log.page = page.current;
            store.log.pageSizeLog = page.pageSize
            fetchData();
          }} current={store.log.page}
          />
      </Card>
    </div>
      );
  }
}) 

export default AuditLog;