import React, { useEffect, useState } from "react";
import {
	Alert,
	Row,
	Col,
	Button,
	Statistic,
	Card,
	Empty,
	PageHeader,
	Dropdown,
	Table,
	Menu,
	Input,
	Select,
	Space,
	Typography,
	Tag,
	message, Modal,Form
} from "antd";
import { FilterOutlined, PlusOutlined } from "@ant-design/icons";
import { observer } from "mobx-react-lite";
import upperFirst from "lodash.upperfirst";
import { useStore } from "../../../../utils/useStores";
import { UserForm } from "./UserForm";
import * as _ from "lodash";
import moment from "moment";
import ExclamationCircleOutlined from "@ant-design/icons/lib/icons/ExclamationCircleOutlined";
import { filter } from "bluebird";

const { Search } = Input;
const { Option } = Select;
const { Text } = Typography;

const optionFilter = [
	{
		key: 1,
		value: 'All Roles'
	}, {
		key: 2,
		value: 'Administrator'
	}, {
		key: 3,
		value: 'Operation'
	},
	{
		key: 4,
		value: 'Finance'
	},
	{
		key: 5,
		value: 'Partner'
	},

];

function handleMenuClick(e) {
	console.log('click', e);
}

const menu = (
	<Menu onClick={handleMenuClick} style={{ background: '#fff' }}>
		{optionFilter.map((i => {
			return <Menu.Item key={i.key}>{i.value}</Menu.Item>
		}))
		}
	</Menu>
);


const optionList = (
	optionFilter.map((i => {
		console.log({ userRole: i }, "User Role Types")
		return <Option value={i.value}>{i.value}</Option>
	}))
);

const dataUser = [{
	id: 1,
	name: 'Aira 2',
	user: 'test@gmail',
	role: 'Administrator',
	role_desc: '',
	last_login: '',
	status: 'Active',
}];

let selectedDiv
let selectedDept
export const Users = observer((initialData) => {
	const [form] = Form.useForm();
	const store = useStore();
	const [filterMode, setFilterMode] = useState(false);
	const [filterData, setFilterData] = useState([]);
	const [selectedRole, setSelectedRole] = useState();

    const [state, setState] = useState({
        modal: false,
        success: false,
        department: '',
        isEdit: false,
    })

    useEffect(() => {
        fetchData();
    }, []);
    async function fetchData() {
		await store.user.getAll({role_id: selectedRole});
		await store.role.getAll();
        await store.division.getAll();
		await store.organizations.getChilds(selectedDiv);
		await store.organizations.getChildsDepart(selectedDept);
        await store.organizations.getAll();
    }

	let user = store.userData.id
	let role = store.userData.role
	async function onSubmit(e) {
        const data = {
				full_name : e.full_name,
				username: e.username,
				email: e.email,
				password:e.password,
				role_id :e.role_id,
				// division_id: e.division_id,
				// department_id: e.department_id,
				position_id: e.position_id,
			}
		if(data.position_id == null && data.role_id === "ef195ddc-1c59-4748-b416-a08785718ea5") {
			data.position_id = e.division_id
		}
		const log = {
			user_id : user,
            event_name: "Add User",
            data: {
                location:{
                    pathname:"/app/settings/users",
                    search: "",
                    hash: "",
                    key: role,
                },
                action:"PUSH",
            },
		}
		
		if(e.isEdit){
			if (data.position_id == '-') {
				data.position_id = null;
			}else if(data.position_id == null && data.role_id === "ef195ddc-1c59-4748-b416-a08785718ea5") {
				data.position_id = e.division_id
			}
			await store.user.update(e.isEdit, data)
				.then(res => {
					form.resetFields();
					message.success('User Updated!');
					setState({
						succes :false
					})
				})
				.catch(err => {
					message.error('Error on updating User!');
					setState({
						succes :false
					})
				})
		}else{
			await store.user.checkUser({'email' : e.email,'username': e.username})
			if(store.user.check == 'OK'){
				await store.log.createData(log)
				await store.user.createData(data)
				.then(res => {
					form.resetFields();
					message.success('User Created!');
						setState({
							succes :false
						})
					})
					.catch(err => {
					message.error('Error on creating User!');
						setState({
							succes :false
						})
					})
			}
			else{
				message.error(store.user.check);
			}
		}
	}
        

    const toggleSuccess = (() => {
        setState({
            success: !state.success,
        });
    });

    const setEditMode = (record) => {
		if(record.role_id === "ef195ddc-1c59-4748-b416-a08785718ea5") {
			message.error("Can't edit head division")
		}else {
			store.department.divisionId = record.division_id;
			store.department.getAll();
			setState(prevState => ({
				...prevState,
				success:true
			}))
			selectedDiv = record.position_user ==null?'' :record.position.organization.organization.id;
			selectedDept = record.position_user ==null?'' : record.position.organization.id;
			fetchData()
			form.setFieldsValue({
			    isEdit: record.id,
			    success: true,
			    full_name : record.full_name,
				email: record.email,
				username: record.username,
				role_id :record.role_id,
			    division_id: record.position_user ==null?'-' : record.position.organization.organization.name,
				department_id: record.position_user ==null?'-' :record.position.organization.name,
				position_id: record.position_user ==null?'-' :record.position.id,
			})
		}

    }
	
	async function filterRole(role) {
		if (role === 'All Role') {
			delete store.user.query.role_id
		}else {
			store.user.query.role_id = role
		}
		setSelectedRole(role)
		fetchData()

	}

    {
		
        const {warning} = state;
        const columns = [
			{
				title: "Username",
				dataIndex: 'username'
			},
			{
                title: 'Email',
                dataIndex: 'email',
                render: (text, record) => <span>{text  || '-'}</span>,
            },
			{
                title: 'Name',
                dataIndex: 'name',
                render: (text, record) => <span>{record.full_name  || '-'}</span>,
			},
			{
				title: 'Role',
				dataIndex: 'role',
				render: (text, record) => {
					const dataRole = store.role.data
					// <div key={text}>{text || '-'}</div>
					// console.log(dataRole)
					for (let index = 0; index < dataRole.length; index++) {
						if (dataRole[index].id === record.role_id) {

							return <div key={record.username}>{dataRole[index].name ?? '-'}</div>
							// console.log(dataRole[index].name)
						}
						// console.log(record.role_id)
					}
					// const data = store.role
					// for (let index = 0; index < data.length; index++) {
					// 	console.log(data, "haha")

					// }
					// console.log(store.role.data[1].id)
					// return <div>{store.role.data[1].name}</div>
				},
				ellipsis: true
			},
		
			{
                title: 'Division',
                dataIndex: 'position',
				   render: (text, record) => {
					if (record.role_id === "ef195ddc-1c59-4748-b416-a08785718ea5"){
						return <span>{text.name}</span>
					}else if(!text) {
						return <span>-</span>
					}else {
						return <span>{text?.organization.organization.name}</span>
					}
				},
			},
			{
                title: 'Department',
                dataIndex: 'position',
                render: (text, record) => {
					if ((record.role_id === "ef195ddc-1c59-4748-b416-a08785718ea5") || (!text)){
						return <span>-</span>
					}else {
						return <span>{text?.organization.name}</span>
					}
				},
			},
			{
                title: 'Position',
                dataIndex: 'position',
				render: (text, record) => {
					if (record.role_id === "ef195ddc-1c59-4748-b416-a08785718ea5"){
						return <span>Head of Division</span>
					}else if(!text) {
						return <span>-</span>
					}else {
						return <span>{text?.name}</span>
					}
				},
            },
            {
                title: 'Action',
                dataIndex: 'action',
                key: 'action',
				margin: 5,
				fixed:'right',
				width : store.ui.mediaQuery.isMobile?150:'',
                render: (text, record) => (
                    <span>
                       <Button onClick={() => { setEditMode(record) }} style={{marginRight:10}}>
                            Edit
                        </Button>
                        <Button onClick={() => {
                            Modal.confirm({
                                title: 'Do you want to delete?',
                                icon: <ExclamationCircleOutlined />,
                                onOk() {
                                    return store.user.delete(record.id);
                                },
                                onCancel() { },
                            });
                        }} style={{ marginLeft: 8 }}>Delete</Button>
                    </span>
                )
            },
        ];
        return <div style={{padding: store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet?'10px':''}}>
            <Card bordered={false} className={'shadow'} bodyStyle={{ padding: 0 }}>
                <PageHeader
                    className={'card-page-header'}
                    title={<div> User </div>}
                    subTitle=""
                    extra={[ <div>
						<Select
							style={{width:store.ui.mediaQuery.isMobile?150 :270, marginLeft:store.ui.mediaQuery.isMobile?0:10}}
							className={'select-'}
							bordered={true}
							onChange={(value) => {
								filterRole(value)
							}} defaultValue="All Role">
							<Option value={'All Role'}>All Role</Option>
							{store.role.data.map(r => {
								return <Option value={r.id}>{r.name}</Option> })}
						</Select>
                        <Button color="secondary" onClick={toggleSuccess} style={{marginLeft:20}}>
                            <PlusOutlined /> Add User
                        </Button> </div>
                    ]}
                /> 
                    {warning === 1 ? <Alert Color="success">Data berhasil disimpan</Alert> : null}
                      {renderModal()}
                        <Table columns={columns} bordered
							dataSource={store.user.data}
							pagination={{pageSize: 50}}
							scroll={{x: 1500, y: 350}}
							pagination={{total:store.user.maxLength, pageSize : store.user.query.per_page, current:store.user.query.page }} 
							onChange={(page) => {
							store.user.query.page = page.current;
							store.user.query.per_page = page.pageSize;
							fetchData()
						  }} 
						  current={store.user.query.page} 
						/>
                </Card>
            </div>
    }

    function renderModal() {
        return <Modal visible={state.success} closable={false} confirmLoading={false} destroyOnClose={true}
		title="Add User"
		okText="Save"
        cancelText="Cancel"
        bodyStyle={{ background: '#f7fafc' }}
        onCancel={() => {
			form.setFieldsValue({});
			toggleSuccess();
        }}
        onOk={() => {
			form
				.validateFields()
				.then(values => {
					// form.resetFields();
					onSubmit(values);
					form.setFieldsValue({});
				})
				.catch(info => {
					console.log('Validate Failed:', info);
				});
		}}
        >
            <div className="animated fadeIn">
                <Form layout="vertical" form={form} className={'custom-form'} name="form_in_modal" initialValues={initialData} >
				<	Form.Item name="isEdit" hidden={true}>
                        <Input />
                    </Form.Item>
					<Form.Item
						name='full_name'
						label="Name"
						rules={[
							{
								required: true,
								message: 'Please input Name!',
							},
						]}
					>
						<Input />
					</Form.Item>
					<Form.Item
						name="email"
						label="Email"
						rules={[
							{
								required: true,
								message: 'Please input email!',
							},
						]}
					>
						<Input />
					</Form.Item>
					<Form.Item
						name="username"
						label="Username"
						rules={[
							{
								required: true,
								message: 'Please input username!',
							},
						]}
					>
						<Input />
					</Form.Item>
					<Form.Item
						name="password"
						label="Password"
						rules={[
							// {
							// 	required: true,
							// 	message: 'Please input password!',
							// },
						]}
					><Input.Password />
					</Form.Item>
					<Form.Item
						name="role_id"
						label="Role"
						rules={[
							{
								required: true,
								message: 'Please input Role!',
							},
						]}
					>
						<Select
							showSearch
							placeholder="Select User Type"
							optionFilterProp="children"
							onSelect={(value) => {
								if (value === "ef195ddc-1c59-4748-b416-a08785718ea5") {
									form.setFieldsValue({
										department_id: '-',
										position_id: '-'
									})
								}
							}}
							filterOption={(input, option) =>
								option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						>
							{store.role.data.map(it => {
								return <Select.Option value={it.id}>{it.name}</Select.Option>;
							})}
						</Select>
					</Form.Item>
                    <Form.Item
						name="division_id"
						label="Division"
						rules={[
							{
								required: true,
								message: 'Please select division!',
							},
						]}
					>
						<Select
							showSearch
							
							placeholder="Select Division"
							onChange={async (value) => {
								selectedDiv = value;
								fetchData();    
							}} 
						>
							{store.division.data.map(it => {
								return <Select.Option value={it.id}>{it.name}</Select.Option>;
							})}
						</Select>
					</Form.Item>

						<Form.Item
							name="department_id"
							label="Department"
							rules={[
								{
									required: true,
									message: 'Please select Department!',
								},
							]}
						>
							<Select
								showSearch
								
								placeholder="Select Department"
								onChange={async (value) => {
									selectedDept = value;
									fetchData();    
								}} 
							>
								{store.organizations.dataDepart.map(it => {
									return <Select.Option value={it.id}>{it.name}</Select.Option>;
								})}
							</Select>
						</Form.Item>
						<Form.Item
							name="position_id"
							label="Positions"
							rules={[
								{
									required: true,
									message: 'Please select Positions!',
								},
							]}
							filterOption={(input, option) =>
								option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
						>
							<Select
								showSearch
								
								placeholder="Select Positons"
							>
								{store.organizations.dataPositions.map(it => {
									return <Select.Option value={it.id}>{it.name}</Select.Option>;
								})}
							</Select>
						</Form.Item>
                    </Form>
                </div>
        </Modal>
    }
});
