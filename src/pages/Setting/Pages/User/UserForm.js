import { Form, Input, Modal, Select } from "antd";
import React, { useEffect, useState } from "react";
import { observer } from "mobx-react-lite";
import { useStore } from "../../../../utils/useStores";

let selectedDiv 
export const UserForm = observer(({ visible, onSubmit, onCancel, confirmLoading, initialData }) => {
	const [form] = Form.useForm();
	const [showPartnerSelection, setShowPartnerSelection] = useState(false);
	const store = useStore();

	useEffect(() => {
		 fetchData()
	}, [])
	async function fetchData() {
		store.division.getAll();
		store.organizations.getChilds(selectedDiv)
		setShowPartnerSelection(initialData.role_id === 'a0c395ff-a7b6-4668-95ad-63bbcb31485f');
	}

	

	return <Modal
		maskClosable={false}
		visible={visible}
		width={'448px'}
		closable={false}
		bodyStyle={{ background: '#f7fafc' }}
		title="User"
		okText="Save"
		cancelText="Cancel"
		onCancel={() => {
			form.setFieldsValue({});
			onCancel();
		}}
		confirmLoading={confirmLoading}
		destroyOnClose={true}
		onOk={() => {
			form
				.validateFields()
				.then(values => {
					form.resetFields();
					onSubmit(values);
					form.setFieldsValue({});
				})
				.catch(info => {
					console.log('Validate Failed:', info);
				});
		}}
	>

		<Form
			form={form}
			layout="vertical"
			className={'custom-form'}
			name="form_in_modal"
			initialValues={initialData}

			onValuesChange={(values) => {
				console.log({ values }, 'values -> ')
				if (values.role_id) {
					if (values.role_id === 'a0c395ff-a7b6-4668-95ad-63bbcb31485f') {
						setShowPartnerSelection(true);
					} else {
						setShowPartnerSelection(false);
					}
				}
			}}
		>
			<Form.Item
				name='full_name'
				label="Name"
				rules={[
					{
						required: true,
						message: 'Please input Name!',
					},
				]}
			>
				<Input />
			</Form.Item>

			<Form.Item
				name="email"
				label="Email"
				rules={[
					{
						required: true,
						message: 'Please input email!',
					},
				]}
			>
				<Input />
			</Form.Item>
			<Form.Item
				name="password"
				label="Password"
				rules={[
					// {
					// 	required: true,
					// 	message: 'Please input password!',
					// },
				]}
			>
				<Input.Password />
			</Form.Item>
			<Form.Item
				name="role_id"
				label="Role"
				rules={[
					{
						required: true,
						message: 'Please input Role!',
					},
				]}
			>
				<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select User Type"
					optionFilterProp="children"
					filterOption={(input, option) =>
						option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				>
					{store.role.data.map(it => {
						return <Select.Option value={it.id}>{it.name}</Select.Option>;
					})}
				</Select>
			</Form.Item>
			
			<Form.Item
				name="division_id"
				label="Division"
				rules={[
					{
						required: true,
						message: 'Please select division!',
					},
				]}
			>
				<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select Division"
					onChange={async (value) => {
						selectedDiv = value;
						fetchData();    
					  }} 
				>
					{store.division.data.map(it => {
						return <Select.Option value={it.id}>{it.name}</Select.Option>;
					})}
				</Select>
			</Form.Item>
			<Form.Item
				name="department_id"
				label="Department"
				rules={[
					{
						required: true,
						message: 'Please select Department!',
					},
				]}
			>
				<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select Department"
					onChange={async (value) => {
						selectedDiv = value;
						fetchData();    
					  }} 
				>
					{store.organizations.dataDepart.map(it => {
						return <Select.Option value={it.id}>{it.name}</Select.Option>;
					})}
				</Select>
			</Form.Item>
			<Form.Item
				name="position_id"
				label="Positions"
				rules={[
					{
						required: true,
						message: 'Please select Positions!',
					},
				]}
			>
				<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select Positons"
				>
					{store.organizations.dataDepart.map(it => {
						return <Select.Option value={it.id}>{it.name}</Select.Option>;
					})}
				</Select>
			</Form.Item>
			

			

			{showPartnerSelection && <Form.Item
				name="partner_id"
				label="Partner"
				rules={[
					{
						required: true,
						message: 'Please input Bank!',
					},
				]}
			>
				<Select
					showSearch
					style={{ width: 200 }}
					placeholder="Select Partner"
					optionFilterProp="children"
					filterOption={(input, option) =>
						option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
				>
					{store.mitra.data.map(it => {
						return <Select.Option value={it.id}>{it.name}</Select.Option>;
					})}
				</Select>
			</Form.Item>}

		</Form>
	</Modal>
});
