import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../routes";
import {Users} from "./Pages/User/User";
import {Roles} from "./Pages/Role";

export const UsersRoute = () => {
    return (
      <Switch>
          <Route path={LINKS.SETTING + "/users"}>
              <Users/>
          </Route>
          <Route path={LINKS.SETTING + "/roles"}>
              <Roles/>
          </Route>
      </Switch>
    )
};
