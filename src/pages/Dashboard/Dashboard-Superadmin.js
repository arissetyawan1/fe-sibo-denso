import React, { useEffect, useState } from "react";
import {
  Row,
  Col,
  Avatar,
  Button,
  Typography,
  Statistic,
  Card,
  Empty,
  PageHeader,
  Progress,
  Skeleton,
  List,
  Divider,
  Dropdown,
  Table,
  Menu,
  Space,
  Select,
  Collapse,
  DatePicker,
  Spin,
  Modal,
  Breadcrumb,
  Tooltip,
} from "antd";
import { Card as CardReact, CardBody } from "reactstrap";
import eye from "../../assets/images/eye.png";
import {
  UsergroupAddOutlined,
  DollarOutlined,
  GoldOutlined,
  HomeOutlined,
  PlusSquareOutlined,
} from "@ant-design/icons";
import {
  Link,
  useHistory,
  useRouteMatch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import moment from "moment";
import { observer } from "mobx-react-lite";
import { createUseStyles } from "react-jss";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import { TableView } from "../../component/Table/TableView";
import { DashboardStore } from "../../store/dashboard";

const { Title, Paragraph } = Typography;
const useStyles = createUseStyles({
  gridStyle: {
    overflow: "hidden",
    position: "relative",
    padding: "14px 13px 6px",
    height: 200,
    transition: "transform .4s",
    boxShadow: "inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee",
  },
  gridStyleHeaderRight: {
    overflow: "hidden",
    height: 200 / 2,
    position: "relative",
    padding: "14px 13px 6px",
    transition: "transform .4s",
    boxShadow: "inset 0 -1px #e3e8ee",
  },
});

const gridStyle = {
  overflow: "hidden",
  position: "relative",
  padding: "14px 13px 6px",
  height: 84,
  transition: "transform .4s",
  boxShadow: "inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee",
};

const { Option } = Select;

const optionFilter = [
  {
    key: 1,
    value: "Today",
  },
  {
    key: 2,
    value: "Last 3 days",
  },
  {
    key: 3,
    value: "Last 6 months",
  },
  {
    key: 4,
    value: "Last 12 months",
  },
  {
    key: 5,
    value: "Month to date",
  },
  {
    key: 6,
    value: "Quarter to date",
  },
  {
    key: 7,
    value: "Year to date",
  },
  {
    key: 8,
    value: "All time",
  },
];

const labelMapping = {
  OL3: "Target",
  actVsOl: "Actual vs Target",
  actVsYL: "Actual vs LY",
};

export const DashboardSuperadmin = observer(() => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const store = useStore();
  const classes = useStyles();
  const { Panel } = Collapse;
  const { Option } = Select;
  const history = useHistory();
  const [state, setState] = useState({
    isLoadingRevenue: true,
    isLoadingTraffic: true,
    isLoadingAVKT: true,
    isLoadingFRE: true,
    isLoadingAccident: true,
    isLoadingTrafficClass: true,
    isLoadingFinancial: true,
    isLoadingCashflow: true,
    isLoadingBalance: true,
    isLoadingVision: true,
    dataRevenue: "",
    dataTraffic: "",
    dataAVKT: "",
    dataFRE: "",
    dataAccident: "",
    dataTrafficClass: "",
    dataFinancial: "",
    dataCashflow: "",
    dataBalance: "",
    reffRevenue: "",
    reffDT: "",
  });
  const [yearKey, setYearKey] = useState("");

  useEffect(() => {
    // fetchData();
    // store.keymetric.getFinancialSummary(yearKey).then((res) => {
    //   setState((prevState) => ({
    //     ...prevState,
    //     reffRevenue: res.body,
    //   }));
    // });
    // store.keymetric.getDailyTraffic(yearKey).then((res) => {
    //   setState((prevState) => ({
    //     ...prevState,
    //     reffDT: res.body,
    //   }));
    // });
    getInitialData();
  }, []);

  const getInitialData = async () => {
    await store.dashboard.getAllEmployee();
    await store.dashboard.getAllSalary();
    await store.human_resource.getData();
    // alert(JSON.stringify((store.dashboard.salary)))
  };

  const monthRevenue = _.get(state.reffRevenue, `data.month`);
  const yearRevenue = _.get(state.reffRevenue, `data.year`);

  const dataDT = _.filter(_.get(state.reffDT, `data.rawExcel`), {
    A: `Average up to December`,
  });
  const dataOpex1 =
    _.get(
      state.dataFinancial,
      `data.overlayProvision.ytd.${monthRevenue}'${yearRevenue}`
    ) +
    _.get(
      state.dataFinancial,
      `data.directExpense.ytd.${monthRevenue}'${yearRevenue}`
    );
  const dataOpex2 =
    _.get(state.dataFinancial, `data.overlayProvision.ytd.OL3`) +
    _.get(state.dataFinancial, `data.directExpense.ytd.OL3`);

  const datasAccident =
    _.sum(_.get(state.dataAccident, `data.kecelakaanTunggalYear1`)) +
    _.sum(_.get(state.dataAccident, `data.kecelakaanKrLainYear1`));
  const datasAccident2 =
    _.sum(_.get(state.dataAccident, `data.kecelakaanTunggalYear2`)) +
    _.sum(_.get(state.dataAccident, `data.kecelakaanKrLainYear2`));

  async function fetchData() {
    await store.keymetric.getDailyRevenue(yearKey).then(setData("Revenue"));
    await store.keymetric.getAVKT(yearKey).then(setData("AVKT"));
    await store.keymetric.getFRE(yearKey).then(setData("FRE"));
    await store.keymetric.getAccident(yearKey).then(setData("Accident"));
    await store.keymetric
      .getClassification(yearKey)
      .then(setData("TrafficClass"));
    await store.keymetric
      .getFinancialSummary(yearKey)
      .then(setData("Financial"));
    await store.keymetric.getCashflow(yearKey).then(setData("Cashflow"));
    await store.keymetric.getBalanceSheet(yearKey).then(setData("Balance"));
    await store.keymetric.getDailyTraffic(yearKey).then(setData("Traffic"));
  }

  const getLabel = (k) => (labelMapping[k] ? labelMapping[k] : _.startCase(k));

  const setData = (key) => (res) => {
    setState((prevState) => ({
      ...prevState,
      ["isLoading" + key]: false,
      ["data" + key]: res.body,
    }));
  };

  const generateRevenueChart = (props) => {
    if (!state.dataRevenue) {
      return [];
    }

    return new Array(12).fill(1).map((n, i) => {
      return {
        name: moment().month(i).format("MMM"),
        ...Object.keys(state.dataRevenue.data || {}).reduce((all, cur) => {
          if (cur === "rawExcel") {
            return all;
          }
          const label = getLabel(cur);
          let value = state.dataRevenue.data[cur];
          const data = Array.isArray(value) ? value : value.data;
          all[label] = data[i];
          return all;
        }, {}),
      };
    });
  };
  const generateChartData = (report) => {
    if (!report) {
      return [];
    }

    return new Array(12).fill(1).map((n, i) => {
      return {
        name: moment().month(i).format("MMM"),
        ...Object.keys(report.data).reduce((all, cur) => {
          if (cur === "rawExcel") {
            return all;
          }
          const label = getLabel(cur);
          let value = report.data[cur];
          const data = Array.isArray(value) ? value : value.data;
          all[label] = data[i];
          return all;
        }, {}),
      };
    });
  };

  const emptyChart = (title) => {
    return (
      <div
        className={"col-lg-4 col-md-6 card-group-row__col"}
        style={{
          marginBottom:
            store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
              ? "10px"
              : "",
        }}
      >
        <Card className={"shadow border-0"} style={{ height: 256 }}>
          <CardBody className={"d-flex flex-row align-items-center flex-0"}>
            <div className="flex">
              <div className="d-flex flex-row align-items-start flex-1 w-100 justify-content-between">
                <div className="d-flex flex-1 ">
                  <p className="mb-0">
                    <strong>{title}</strong>
                  </p>
                </div>
              </div>
              <span className="h2 m-0" style={{ fontSize: "1.7rem" }}>
                0<small className="text-muted"> / 0</small>
              </span>
              <p className={"text-50 mt-2 mb-0 d-flex"}>
                <Empty />
              </p>
            </div>
          </CardBody>
        </Card>
      </div>
    );
  };

  const renderRadialChart = (
    title,
    desc,
    actual,
    budget,
    ket,
    reverse = null
  ) => {
    let percent = ((Math.abs(actual) / Math.abs(budget)) * 100).toFixed(2);
    let status = "";
    if (reverse) {
      if (percent >= 100) {
        status = "exception";
      } else {
        status = "success";
      }
    } else {
      if (percent < 100) {
        status = "exception";
      }
    }

    return (
      <div
        className={"col-lg-4 col-md-4 card-group-row__col"}
        style={{
          marginBottom:
            store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
              ? "10px"
              : "",
        }}
      >
        <CardReact className={"shadow  border-0"} style={{ height: 256 }}>
          <CardBody className={"d-flex flex-row align-items-center flex-0"}>
            <div className="flex">
              <div className="d-flex flex-row align-items-start flex-1 w-100 justify-content-between">
                <div className="d-flex flex-1 ">
                  <p className="mb-0">
                    <strong>{title}</strong>
                    <small style={{ marginLeft: 10 }}>{ket}</small>
                  </p>
                </div>
              </div>
              <span className="h2 m-0" style={{ fontSize: "1.5rem" }}>
                {Number.isFinite(actual)
                  ? (actual || 0).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,")
                  : actual}
                <small className="text-muted">
                  {" "}
                  /{" "}
                  {Number.isFinite(budget)
                    ? (budget || 0)
                        .toFixed(2)
                        .replace(/\d(?=(\d{3})+\.)/g, "$&,")
                    : budget}
                </small>
              </span>
              <p className={"text-50 mt-2 mb-0 d-flex"}>
                <small className="text-black">{desc}</small>
              </p>
            </div>
          </CardBody>
          <CardBody style={{ display: "flex", justifyContent: "center" }}>
            <Progress
              type={"dashboard"}
              status={status}
              format={(p) => `${percent}%`}
              percent={percent}
            />
          </CardBody>
        </CardReact>
      </div>
    );
  };

  // let revenueChart = generateRevenueChart();
  // let trafficChart = generateChartData(state.dataTraffic);
  // let avktChart = generateChartData(state.dataAVKT);
  // let freChart = generateChartData(state.dataFRE);

  const refreshNewKey = async (val) => {
    await fetchData();
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      width: 10,
      render: (t, r, index) => `${index + 1}`,
    },
    {
      title: "Name",
      dataIndex: "name",
      render: (t, record) => (
        <Tooltip title="Klik untuk lihat detail">
          <span>
            <Link style={{ color: "#42a5f5" }}>{record.employee?.name}</Link>
          </span>
        </Tooltip>
      ),
    },
    {
      title: "Expired Contract",
      dataIndex: "name",
      render: (t, record) => (
       moment(record.end_contract).format("Do MMMM YYYY")
      ),
    },
  ];

  // go to detail contract
  const handleClickRow = (record, index) => {
    history.push(LINKS.DETAIL_CONTRACT.replace(":id", record.id));
  };

  store.dashboard.getOneMonthDuration();
  store.dashboard.getThreeMonthDuration();
  return (
    <div className="site-card-wrapper" style={{ marginTop: 10 }}>
      <Breadcrumb style={{ marginLeft: 12 }}>
        <Breadcrumb.Item>
          <Link to={LINKS.DASHBOARD}>
            <HomeOutlined style={{ marginRight: 4 }} />
            <span>Dashboard</span>
          </Link>
        </Breadcrumb.Item>
      </Breadcrumb>
      <PageHeader
        // className={"card-page-header"}
        title={<h1 style={{ fontWeight: 500, marginTop: 10 }}>Dashboard</h1>}
      />
      <Spin spinning={store.keymetric.isLoading}>
        <div className={["cariparkir-container"].join(" ")}>
          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            <Col
              className="gutter-row"
              xs={24}
              sm={24}
              md={8}
              xl={8}
              xxl={8}
              style={{ marginBottom: 20 }}
            >
              <Link to={LINKS.EMPLOYEE}>
                <Card title="Total Employees" bordered={false} hoverable>
                  <UsergroupAddOutlined style={{ fontSize: 40 }} />
                  <span className="h2 m-0" style={{ fontSize: "1.7rem" }}>
                    {store.dashboard.employee}
                  </span>{" "}
                </Card>
              </Link>
            </Col>
            <Col
              className="gutter-row"
              xs={24}
              sm={24}
              md={8}
              xl={8}
              xxl={8}
              style={{ marginBottom: 20 }}
            >
              <Card title="Total Salary" bordered={false} hoverable>
                <DollarOutlined style={{ fontSize: 40 }} />
                <Tooltip
                  title={`${store.dashboard.salary}`.replace(
                    /\B(?=(\d{3})+(?!\d))/g,
                    "."
                  )}
                >
                  <span className="h2 m-0" style={{ fontSize: "1.7rem" }}>
                    {`${store.dashboard.salary}`.replace(
                      /\B(?=(\d{3})+(?!\d))/g,
                      "."
                    )}
                  </span>
                </Tooltip>
              </Card>
            </Col>
            <Col
              className="gutter-row"
              xs={24}
              sm={24}
              md={8}
              xl={8}
              xxl={8}
              style={{ marginBottom: 20 }}
            >
              <Card title="Pipeline Project" bordered={false} hoverable>
                <GoldOutlined style={{ fontSize: 40 }} />
                <span className="h2 m-0" style={{ fontSize: "1.7rem" }}>
                  10<small className="text-muted"> / 1000 </small>
                </span>
              </Card>
            </Col>
          </Row>
          <Row
            gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
            style={{ marginTop: 20 }}
          >
            <Col
              className="gutter-row"
              xs={24}
              sm={24}
              md={12}
              xl={12}
              xxl={12}
            >
              <TableView
                title={<h6>Contract expired in 3 month</h6>}
                showBreadCrumb={false}
                columns={columns}
                dataSource={store.dashboard.threeMonthData}
                showToolbar={false}
                showAddBtn={false}
                showFilterBtn={true}
                bordered
                placeholder={"Search by Name or Type Contract"}
                searchBarFull={false}
                searchKeys={["employee.name", "type_contract"]}
                onRowClick={handleClickRow}
                key={"table_contract"}
                onClickFilter={() => {
                  setIsModalVisible(false);
                }}
              />
            </Col>
            <Col
              className="gutter-row"
              xs={24}
              sm={24}
              md={12}
              xl={12}
              xxl={12}
            >
              <TableView
                title={<h6>Contract expired in a month</h6>}
                columns={columns}
                dataSource={store.dashboard.oneMonthData}
                showToolbar={false}
                showBreadCrumb={false}
                showAddBtn={false}
                showFilterBtn={true}
                bordered
                placeholder={"Search by Name or Type Contract"}
                searchBarFull={false}
                searchKeys={["employee.name", "type_contract"]}
                onRowClick={handleClickRow}
                key={"table_contract"}
                onClickFilter={() => {
                  setIsModalVisible(false);
                }}
              />
            </Col>
          </Row>
        </div>
      </Spin>
    </div>
  );
});
