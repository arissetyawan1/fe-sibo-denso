import React, {useEffect, useState} from "react";
import {
    Row,
    Col,
    Avatar,
    Button,
    Typography,
    Statistic,
    Card,
    Empty,
    PageHeader,
    List,
    Divider,
    Dropdown,
    Table,
    Menu, Space, Select, Tag
} from "antd";
import {
    CreditCardTwoTone,
    FilterOutlined,
    PlusOutlined, WalletTwoTone
} from "@ant-design/icons";
import {Link, useHistory, useRouteMatch, BrowserRouter as Router, Route} from "react-router-dom";
import {LINKS} from "../../routes";
import {observer} from "mobx-react-lite";
import {useStore} from "../../utils/useStores";
import {createUseStyles} from "react-jss";
import ClockCircleOutlined from "@ant-design/icons/lib/icons/ClockCircleOutlined";
import moment from "moment";

const {Title, Paragraph} = Typography;
const DemoBox = props => <p className={`height-${props.value}`}>{props.children}</p>;
const useStyles = createUseStyles({
    gridStyle: {
        overflow: 'hidden',
        position: 'relative',
        padding: '16px 15px 8px',
        height: 200,
        transition: 'transform .4s',
        boxShadow: 'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'

    },
    gridStyleHeaderRight: {
        overflow: 'hidden',
        height: 200 / 2,
        position: 'relative',
        padding: '16px 15px 8px',
        transition: 'transform .4s',
        boxShadow: 'inset 0 -1px #e3e8ee'
    },
});

const gridStyle = {
    overflow: 'hidden',
    position: 'relative',
    padding: '16px 15px 8px',
    height: 84,
    transition: 'transform .4s',
    boxShadow: 'inset -1px 0 #e3e8ee, inset 0 -1px #e3e8ee'
};

const {Option} = Select;

const options = [
    {
        value: 'All Time',
        label: 'Zhejiang',
        children: [
            {
                value: 'hangzhou',
                label: 'Hangzhou',
                children: [
                    {
                        value: 'xihu',
                        label: 'West Lake',
                    },
                ],
            },
        ],
    },
    {
        value: 'jiangsu',
        label: 'Jiangsu',
        children: [
            {
                value: 'nanjing',
                label: 'Nanjing',
                children: [
                    {
                        value: 'zhonghuamen',
                        label: 'Zhong Hua Men',
                    },
                ],
            },
        ],
    },
    {
        value: 'Today'
    }
];

const optionFilter = [
    {
        key: 'today',
        value: 'Today'
    }, {
        key: 'last_3_days',
        value: 'Last 3 days'
    }, {
        key: 'last_6_month',
        value: 'Last 6 months'
    }, {
        key: 'last_12_month',
        value: 'Last 12 months'
    }, {
        //     key: 'last_12_month',
        //     value: 'Month to date'
        // }, {
        //     key: 6,
        //     value: 'Quarter to date'
        // }, {
        //     key: 7,
        //     value: 'Year to date'
        // }, {
        key: 'all_time',
        value: 'All time'
    },
];

function handleMenuClick(e) {
    console.log('click', e);
}

const menu = (
    <Menu onClick={handleMenuClick} style={{background: '#fff'}}>
        {optionFilter.map((i => {
            return <Menu.Item key={i.key}>{i.value}</Menu.Item>
        }))
        }
    </Menu>
);
const optionList = (
    optionFilter.map(((i, index) => {
        return <Option key={index} value={i.key}>{i.value}</Option>
    }))

);

export const DashboardPartner = observer(() => {
    const store = useStore();
    const classes = useStyles();
    useEffect(() => {
        async function init() {
            await store.news.getAll();
        }

        init().then(res => console.log(res));
    }, []);

    return <div className={'cariparkir-container'}>
        <Card
            bordered={false}
            style={{overflow: 'hidden'}}
            className={"shadow"}
            bodyStyle={{background: "#f7fafc", marginRight: '-1px', padding: 0}}
        >
            <PageHeader
                className={'card-page-header'}
                ghost={false}
                // onBack={() => window.history.back()}
                title={<span style={{}}>Today</span>}
                // subTitle="This is a subtitle"

            >
            </PageHeader>
            <Card.Grid hoverable={false} className={classes.gridStyle} style={{width: '50%'}}>
                <Statistic title="Gross Volume" precision={2} prefix={'Rp'} value={store.dashboard.data.grossVolume}/>
            </Card.Grid>
            <Card.Grid hoverable={false} className={classes.gridStyleHeaderRight} style={{width: '50%'}}>
                <Statistic title="Income" precision={2} prefix={'Rp'} valueStyle={{color: "#5469d4"}}
                           value={store.dashboard.data.income}/>
                <Paragraph style={{fontSize: 9, marginTop: 3, marginBottom: 0}}>Estimated future income</Paragraph>
            </Card.Grid>
            <Card.Grid hoverable={false} className={classes.gridStyleHeaderRight} style={{width: '50%'}}>
                <Statistic title="Paid income" precision={2} prefix={'Rp'} value={store.dashboard.data.paidIncome}/>
            </Card.Grid>
        </Card>

        <Card
            bordered={false}
            style={{overflow: 'auto', marginTop: 20}}
            className={"shadow"}
            bodyStyle={{background: "#f7fafc", marginRight: '-1px', padding: 0}}
        >
            <PageHeader
                className={'card-page-header'}
                ghost={false}
                // onBack={() => window.history.back()}
                title="Reports overview"
                // subTitle="This is a subtitle"
            >
                <Space>
                    <Select size={'small'} onChange={(value) => {
                        store.dashboard.setPeriod(value);
                    }} style={{minWidth: 80, marginTop: 8}} value={store.dashboard.period}>
                        {optionList}
                    </Select>
                </Space>
            </PageHeader>
            <Card.Grid hoverable={false} style={gridStyle}>
                <Statistic
                    title="Gross Volume"
                    precision={2}
                    valueStyle={{color: "#5469d4"}}
                    prefix={'Rp'}
                    value={store.dashboard.dataPerPeriod.grossVolume}/>
            </Card.Grid>
            <Card.Grid hoverable={false} style={gridStyle}>
                <Statistic title="Income" value={store.dashboard.dataPerPeriod.income}
                           prefix={'Rp'} valueStyle={{color: "#5469d4"}}/>
            </Card.Grid>
            <Card.Grid hoverable={false} style={gridStyle}>
                <Statistic title="Paid Income" value={store.dashboard.dataPerPeriod.paidIncome}
                           prefix={'Rp'} valueStyle={{color: "#5469d4"}}/>
            </Card.Grid>
        </Card>

        <Card
            bordered={false}
            style={{overflow: 'auto', marginTop: 20}}
            className={"shadow"}
            bodyStyle={{background: "#f7fafc", marginRight: '-1px', padding: 0}}
        >
            <PageHeader
                className={'card-page-header'}
                ghost={false}
                title={<span style={{}}>News</span>}
                // subTitle="This is a subtitle"

            >
            </PageHeader>
            <List
                itemLayout="horizontal"
                loading={(store.news.calculatedData.length === 0)}
                position={'bottom'}
                dataSource={store.news.calculatedData}
                style={{
                    padding: 0,
                    // overflowY: 'auto',
                    // overflowX: 'hidden'
                }}
                renderItem={item => (
                    <List.Item key={item.id} style={{
                        backgroundColor: '#fff',
                        paddingTop: 0,
                        display: 'flex',
                        alignItems: 'stretch',
                        justifyContent: 'center'
                    }}>
                        <List.Item.Meta
                            avatar={<Avatar shape="square" size={30}
                                            style={{border: '0.5px solid rgb(156, 156, 156)'}}
                                            src={item?.detail?.image ? item.detail.image : ""}/>}
                            className={['cariparkir-container'].join(' ')}
                            style={{padding: '14px 14px 0px 14px', alignSelf: 'center'}}
                            // title={<Link to={`/app/reports/transaction_operation/${item.id}`}>{item.title}</Link>}
                            title={<a href={item?.detail?.url} target={'_blank'}
                                      style={{}}>{item.title}</a>}
                            // description={<div
                            //     // dangerouslySetInnerHTML={{ __html: item?.detail?.description }}
                            //     style={{
                            //         display: 'flex',
                            //         flexDirection: 'row',
                            //         alignItems: 'center',
                            //         justifyContent: 'center'
                            //     }}>
                            // </div>}
                        />
                        {/*<div style={{marginRight: 16, alignSelf: 'flex-end'}}>*/}
                        {/*    <Statistic*/}
                        {/*        title={<p style={{fontSize: 9, margin: 0}}>{moment(item.date_create).format("DD MMM YY, H:mm:ss")}</p>}*/}
                        {/*        precision={2}*/}
                        {/*        prefix={'Rp'}*/}
                        {/*        // suffix={item.incentive.incentive_type === 'percentage' ? '%' : ''}*/}
                        {/*        style={{fontSize: 12, fontWeight: 300}}*/}
                        {/*        valueStyle={{color: "#5469d4", fontSize: 12, fontWeight: 600, textAlign: 'right'}}*/}
                        {/*        value={item.total}/>*/}
                        {/*</div>*/}
                    </List.Item>
                )}
            />
        </Card>

    </div>
});
