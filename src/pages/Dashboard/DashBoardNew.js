import React, { useEffect, useState} from "react";
import {
  Row,
  Col,
  Avatar,
  Button,
  Typography,
  Statistic,
  Card,
  Empty,
  PageHeader,
  Progress,
  Skeleton,
  Badge,
  List,
  Divider,
  Dropdown,
  Table,
  Menu,
  Space,
  Select,
  Collapse,
  Modal,
} from "antd";
import {
  Link,
  useHistory,
  useRouteMatch,
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import { LINKS } from "../../routes";
import moment from "moment";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import background from "../../assets/images/kanci.jpeg"
import { CloseOutlined } from "@ant-design/icons";

export const DashBoardNew = observer(() => {
  const store = useStore();
  const history = useHistory();
  const [detail, setDetail] = useState(true);

  useEffect(() => {
    fetchData();
  }, []);

  async function fetchData() {
    await store.visionMission.getAll();
    store.visionMission.vision = _.filter(
      store.visionMission.data,
      (i) => i.type === "vision"
    );
    store.visionMission.mission = _.filter(
      store.visionMission.data,
      (i) => i.type === "mission"
    );
    store.visionMission.culture = _.filter(
      store.visionMission.data,
      (i) => i.type === "culture"
    );
  }

  return <Modal visible={detail} bodyStyle={{ minHeight: '98vh', maxHeight: '98vh', overflowY: 'auto', backgroundImage: "url(" + background + ")", backgroundSize: "cover", backgroundRepeat: 'no-repeat' }}
    destroyOnClose={true} closeIcon={<Avatar size="small" style={{background: "transparent", color: "#58585b", marginTop: "-30px", marginRight: "-28px"}} icon={<CloseOutlined />}/>}
        onCancel={() => {
            history.push(LINKS.DASHBOARD);
            setDetail(false);
        }}
        footer={null}
        className="modal-superadmin-vm"
    >
    {/* <div
      style={{
        minHeight:'85vh',
        padding: 10,
        color: "#fff",
        backgroundImage: "url(" + background + ")",
        backgroundSize: "cover",
        backgroundPosition:'fixed',
        height: 'auto'
      }}
      > */}
      <Row style={{marginTop: '-10px'}}>
        {/* <Col lg={14} style={{ backgroundColor: 'transaparent', marginTop: '-10px', height: '85vh' }}>
        </Col> */}
        <Col lg={24} style={{color: "#fff"}}>
          <Badge.Ribbon text="Vision" style={{fontSize:24, marginRight:5}}>
            <Row style={{marginBottom:10}}> <br></br>
              <Card bordered={false} style={{width:'100%', marginRight:5}}>
            {store.visionMission.isEmpty ? (
                      <Skeleton loading={true} active />
                    ) : (
                      <List
                        dataSource={store.visionMission.vision}
                      bordered={false}
                      // style={{fontSize:14, color:'black'} }
                        renderItem={(item) => {
                          return (
                            <Skeleton
                              loading={store.visionMission.vision ? false : true}
                              active
                            >
                              <List.Item
                                style={{
                                  lineHeight:'15px',
                                  paddingLeft: 0,
                                  borderBottom: "none",
                                  fontSize: 14,
                                  color: "black",
                                }}
                              >
                                {item.title}
                              </List.Item>
                            </Skeleton>
                          );
                        }}
                      />
                    )}

              </Card>
            </Row>
          </Badge.Ribbon>
          <Badge.Ribbon text="Mission" style={{fontSize:24}}>
            <Row style={{marginBottom:'10px'}} > <br></br>
              <Card bordered={false} style={{width:'100%'}}>
            {store.visionMission.isEmpty ? (
                <Skeleton loading={true} active />
              ) : (
                <List
                  dataSource={store.visionMission.mission}
                  bordered={false}
                  // style={{ borderBottom: "none",  color:'black',}}
                  renderItem={(item) => (
                    <List.Item style={{ lineHeight:'15px', fontSize:14, paddingLeft: 0, borderBottom: "none" ,
                    color: "black"}}>
                      {item.title}
                    </List.Item>
                  )}
                />
                  )} 
                </Card>
            </Row>
          </Badge.Ribbon>
          <Badge.Ribbon text="Culture" style={{fontSize:24}}>
            <Row> <br></br>
              <Card bordered={false} style={{width:'100%'}}>
          {store.visionMission.isEmpty ? (
                <Skeleton loading={true} active />
              ) : (
                <List
                  dataSource={store.visionMission.culture}
                  bordered={false}
                  // style={{borderBottom: "none" }}
                  renderItem={(item) => (
                    <List.Item style={{lineHeight:'15px', paddingLeft: 0,fontSize:14, color:'black', borderBottom: "none" }}>
                      {item.title}
                    </List.Item>
                  )}
                />
                  )}
                </Card>
            </Row>
          </Badge.Ribbon>
        </Col>
        {/* <Col lg={1}></Col> */}
      </Row>
    {/* </div> */}
  </Modal>
});
