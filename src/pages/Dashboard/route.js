import React, {useEffect} from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import {LINKS} from "../../routes";
import {DashboardSuperadmin} from "./Dashboard-Superadmin";
import {useStore} from "../../utils/useStores";
import {observer} from "mobx-react-lite";
import {AnimatedSwitch} from "react-router-transition";

export const DashboardRoute =  observer(() => {
    const store = useStore();
    useEffect(() => {
        async function init() {
            store.dashboard.getAll();
            store.dashboard.getDashboardByPeriod();
        }
        init();
    }, []);
    return (
        <AnimatedSwitch
            atEnter={{opacity: 0}}
            atLeave={{opacity: 0}}
            atActive={{opacity: 1}}
            className="switch-wrapper-app">
            {store.userData.role !== null && <Route path={LINKS.DASHBOARD}>
                <DashboardSuperadmin/>
            </Route>}
        </AnimatedSwitch>
    )
});
