import React, { useEffect, useState } from "react";
import {
  Input,
  InputNumber,
  Form,
  DatePicker,
  Select,
  Button,
  Card,
  Table,
  Modal,
  Breadcrumb,
  Row,
  Col,
  Tooltip,
  message,
  Upload,
} from "antd";
import {
  HomeOutlined,
  SearchOutlined,
  DeleteOutlined,
  SwapRightOutlined,
  EditOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { useParams, useHistory } from "react-router-dom";
import { LINKS } from "../../routes";
import { observer } from "mobx-react-lite";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import { debounce } from "lodash";
import moment from "moment";

import * as uuid from "uuid";
import { constants } from "../../config/constants";
import { findPositionOfBar } from "recharts/lib/util/ChartUtils";

export const FormPermit = observer((props) => {
  const {
    permit: PermitStore,
    human_resource: humanResourceStore,
    userData,
  } = useStore();

  const { mode } = props;
  const { id: paramId } = useParams();
  const history = useHistory();
  const [form] = Form.useForm();
  const [dataForm, setDataForm] = useState([]);
  // // const [startDate, setStartDate] = useState(null);
  // const [endDate, setEndDate] = useState(null);
  // const [typePermit, setPermit] = useState()
  // const [disableInput, setDisableInput] = useState(true);

  useEffect(() => {
    loadInitialData();
  }, []);

  const { TextArea } = Input;

  const { Option } = Select;

  const layout = {
    labelCol: { span: 8 },
  };

  const btnLayout = {
    wrapperCol: { offset: 5 },
  };

  const loadInitialData = async () => {
    // await humanResourceStore.getData();
    await PermitStore.getAllEmployee();
  };
  // Employee Combo box
  const dataEmployee = PermitStore.listData;
  console.log(dataEmployee, "mau liat ini dong");

  const employeeContract = [...new Set(dataEmployee)].filter(
    (prop) => prop !== undefined
  );

  const optionEmployee = employeeContract.map((employee) => {
    return (
      <Option value={employee.id} key={employee.name}>
        {employee.name}
      </Option>
    );
  });

  const selectEmployees = (
    <Select
      showSearch
      placeholder="Employee Name"
      style={{ width: 300 }}
      disabled={mode === "edit" ? true : false}
      optionFilterProp="children"
      filterOption={(input, option) =>
        option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0 ||
        option.props.value.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }
    >
      {optionEmployee}
    </Select>
  );

  const savePermit = async (values) => {
    const bodyData = {
      ...values,
    };
    try {
      await PermitStore.createData(bodyData);
      message.success("Permit Created successfully");
      history.push(LINKS.LOGIN);
    } catch (e) {
      Modal.error({
        title: "Failed",
        content: e.response?.body?.message,
      });
      console.log(e, " pesan error");
    }
  };

  const submitForm = () => {
    form
      .validateFields()
      .then(async (res) => {
        await savePermit(res);
        console.log(res);
      })
      .catch((errorInfo) => {
        console.log(errorInfo);
      });
  };
  // disable date
  const disabledDate = (current) => {
    return current && current < moment().endOf("day");
  };

  return (
    <div
      className="client-card-wrapper"
      style={{ marginTop: 10, textAlign: "center" }}
    >
      <Row align="middle">
        <Col span={12} offset={6}>
          <Card title={"Permit Form"}>
            <Form {...layout} form={form} scrollToFirstError={true}>
              <Form.Item
                label="Employee Name"
                name="id_employee"
                rules={[{ required: true }]}
              >
                {selectEmployees}
              </Form.Item>

              <Form.Item
                label="Type Permit"
                name="permit_type"
                rules={[{ required: true }]}
              >
                <Select style={{ width: 300 }}>
                  <option value={"Casual Leave"}>Casual Leave</option>
                  <option value={"Medical Leave"}>Medical Leave</option>
                  <option value={"Maternity Leave"}>Maternity Leave</option>
                  <option value={"WFH"}>Work From Home/ Online</option>
                  <option value={"Other"}>Other</option>
                </Select>
              </Form.Item>

              <Form.Item label="Permit Duration" style={{ marginBottom: 0 }}>
                <Input.Group compact>
                  <Form.Item
                    name="start_date"
                    rules={[
                      {
                        required: true,
                        message: "" + "Enter start date!",
                      },
                    ]}
                  >
                    <DatePicker
                      // defaultValue={hari.substr(0, 10).replace("-", "/")}
                      allowClear={false}
                      disabledDate={disabledDate}
                      style={{ width: 150, marginRight: 5 }}
                      placeholder="Start Date"
                    />
                  </Form.Item>
                  <Form.Item name="end_date">
                    <DatePicker
                      disabledDate={disabledDate}
                      style={{ width: 150, marginLeft: 5 }}
                      placeholder="End Date"
                    />
                  </Form.Item>
                </Input.Group>
              </Form.Item>

              <Form.Item label="Description" name="description">
                <TextArea
                  placeholder="add some description"
                  allowClear
                  style={{ width: 300, height: 100 }}
                />
              </Form.Item>

              <Form.Item>
                <Button
                  type="primary"
                  htmlType="submit"
                  onClick={submitForm}
                  size="default"
                  style={{ width: 300 }}
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Col>
      </Row>
    </div>
  );
});
