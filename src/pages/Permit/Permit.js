import React, { useEffect, Fragment, useState } from "react";
import dateFormat from "dateformat";
import "antd/dist/antd.css";
import {
  Table,
  Input,
  Button,
  Tooltip,
  Icon,
  Typography,
  Modal,
  Breadcrumb,
  Space,
  Upload,
  Row,
  Col,
  PageHeader,
  Form,
  Select,
  DatePicker,
} from "antd";
import {
  PlusSquareOutlined,
  SearchOutlined,
  HomeOutlined,
} from "@ant-design/icons";
import Highlighter from "react-highlight-words";
import { observer } from "mobx-react";
import { Link, useHistory } from "react-router-dom";
import { LINKS } from "../../routes";
import { useStore } from "../../utils/useStores";
import * as _ from "lodash";
import moment from "moment";
import { TableView } from "../../component/Table/TableView";
export const Permit = observer((initialData) => {
  const store = useStore();
  const { permit: PermitStore } = useStore();
  const [dataSource, setDataSource] = useState(null);
  const { Option } = Select;
  const history = useHistory();

  useEffect(() => {
    loadInitialData();
  }, []);

  const loadInitialData = async () => {
    await PermitStore.getAll();
  };

  //   const handleClickRow = (record, index) => ({
  //     history.push(LINKS.DETAIL_CONTRACT.replace(":id", record.id));
  //   });
  const handleClickRow = () => {
    alert("Detail permission dipencet");
  };

  const columns = [
    {
      title: "No",
      dataIndex: "no",
      width: "5%",
      render: (t, r, index) => `${index + 1}`,
    },
    {
      title: "Name",
      dataIndex: "name",
      width: "20%",
      render: (t, record) => (
        <Tooltip title="Klik untuk lihat detail">
          <span>
            <Link style={{ color: "#42a5f5" }}>{record?.employee?.name}</Link>
          </span>
        </Tooltip>
      ),
    },
    {
      title: "Permit Type",
      dataIndex: "permit_type",
      width: "20%",
      render: (text, record) => {
        return record?.permit_type || "-";
      },
    },
    {
      title: "Start date",
      dataIndex: "start_date",
      width: "15%",
      render: (t, record) => moment(record?.start_date).format("Do MMMM YYYY"),
    },
    {
      title: "End date",
      dataIndex: "end_date",
      width: "15%",
      render: (t, record) => moment(record?.end_date).format("Do MMMM YYYY"),
    },
    {
      title: "Description",
      dataIndex: "description",
      width: "25%",
      render: (t, record) => {
        return record?.description;
      },
    },
  ];

  const tableData = PermitStore.listData;
  console.log(tableData, "ini data table");
  return (
    <div
      className="site-card-wrapper"
      style={{
        padding:
          store.ui.mediaQuery.isMobile || store.ui.mediaQuery.isTablet
            ? "10px"
            : "",
        marginTop: 10,
      }}
    >
      <Breadcrumb style={{ marginLeft: 12 }}>
        <Breadcrumb.Item>
          <Link to={LINKS.DASHBOARD}>
            <HomeOutlined />
          </Link>
        </Breadcrumb.Item>
        <Breadcrumb.Item style={{ fontWeight: "bold" }}>
          Permission
        </Breadcrumb.Item>
      </Breadcrumb>

      <Row>
        <Col flex="auto">
          <PageHeader
            className={"card-page-header"}
            title={<h1>Permission List</h1>}
          />
        </Col>
      </Row>

      <Table
        scroll={{ x: store.ui.mediaQuery.isMobile ? 500 : "" }}
        columns={columns}
        dataSource={dataSource === null ? tableData : dataSource}
        bordered
        onRow={(r, i) => {
          return {
            onClick: (event) => {
              handleClickRow();
            }, 
          };
        }}
        className={"styleTable"}
      />
    </div>
  );
});
