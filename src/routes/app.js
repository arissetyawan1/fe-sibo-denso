import React, { Suspense, lazy } from "react";
import { Redirect, Route, Switch, useHistory } from "react-router-dom";
import { Inquiry } from "../pages/Inquiry/Inquiry";
import { Report } from "../pages/Report/Report";
import { Setting } from "../pages/Setting/Setting";
import { Task } from "../pages/Master/Master";
import Calendar from "../pages/Calendar/Calendar";
import { Dashboard } from "../pages/Dashboard/Dashboard";
import { Contract } from "../pages/Contract/Contract";
import { DetailContract } from "../pages/Contract/DetailContract";
import { FormContract } from "../pages/Contract/FormContract";
// import { Client } from "../pages/Client/Client";
// import { FormClient } from "../pages/Client/FormClient";
// import { DetailClient } from "../pages/Client/Detail";
import { Jadwal } from "../pages/Jadwal/Jadwal";
import { Profile } from "../pages/Profile/Profile";
import { Balance } from "../pages/Balance/Balance";
import { Partner } from "../pages/Partner/Partner";
import { Services } from "../pages/Services/Partner";
import { Kpi } from "../pages/KPI/Kpi";
import { KeyMetrics } from "../pages/Report/KeyMetrics";
import Corporate from "../pages/KPI/Corporate";
import TrafficClassification from "../pages/Report/KeyMetrics/TrafficClassification";
import BalanceSheet from "../pages/Report/KeyMetrics/BalanceSheet";
import { LINKS } from "./index";
// import ParticlesBg from "particles-bg";
import { AnimatedSwitch } from "react-router-transition";
import { Users } from "../pages/Setting/Pages/User/User";
import { VisionMission } from "../pages/Setting/Pages/VisionMission/Vision_Mission";
import { Roles } from "../pages/Setting/Pages/Role";
import { DailyTraffic } from "../pages/Report/DailyTraffic";
import Accident from "../pages/Report/KeyMetrics/Accident";
import { Employee } from "../pages/Employee";
import { FormEmployee } from "../pages/Employee/FormEmployee";
import { DetailEmployee } from "../pages/Employee/DetailEmployee";
import { DivisionSIBO } from "../pages/Division";
import { Cashflow } from "../pages/Report/KeyMetrics/Cashflow";
import { ExecutiveSummary } from "../pages/Report/KeyMetrics/ExecutiveSummary";
import { Divisions } from "../pages/KPI/Divisions";
import { Departments } from "../pages/KPI/Departments";
import { Staffs } from "../pages/KPI/Staff";
import FullRouteEquivalente from "../pages/Report/KeyMetrics/FullRouteEquivalente";
import AverageVehicleKM from "../pages/Report/KeyMetrics/AverageVechileKM";
import FinancialSummary from "../pages/Report/KeyMetrics/FinancialSummary";
import DailyRevenue from "../pages/Report/KeyMetrics/DailyRevenue";
import UploadKey from "../pages/Upload/UploadKey";
import UploadKPI from "../pages/Upload/UploadKPI";
import { DivisionDepartement } from "../pages/Setting/Pages/Organization/DivisionDepartement";
import EntryCorporate from "../pages/KPI/EntryKPI/EntryKPICorporate";
import EntryDepartment from "../pages/KPI/EntryKPI/EntryKPIDepartment";
import { DivisiEntryKPI } from "../pages/KPI/DivisiEntryKPI";
import { DepartEntryKPI } from "../pages/KPI/DepartEntryKPI";
import { StaffEntryKPI } from "../pages/KPI/ST/StaffEntryKPI";
import EntryStaff from "../pages/KPI/ST/EntryKPIStaff";
import DepartGolongan13 from "../pages/KPI/EntryKPI/Gol1-3Depart";
// import DepartGol13 from "../pages/KPI/EntryKPI/Gol1-3Depart";
import DepartGol45 from "../pages/KPI/EntryKPI/Gol4-5Depart";
// import DetailDepartment from "../pages/KPI/DetailDepartment";
import DivisiGol13 from "../pages/KPI/EntryKPI/Gol1-3Divisi";
import DivisiGol45 from "../pages/KPI/EntryKPI/Gol4-5Divisi";
import StaffGol13 from "../pages/KPI/EntryKPI/Gol1-3Staff";
import StaffGol45 from "../pages/KPI/EntryKPI/Gol4-5Staff";
import DivisionDetailKPI from "../pages/KPI/DI/Detail";
import DepartmentDetailKPI from "../pages/KPI/DE/Detail";
import StaffDetailKPI from "../pages/KPI/ST/Detail";
import OperationalData from "../pages/OperationalData/Operational";
import OperationalDataDetail from "../pages/OperationalData/OperationalDetail";
import AuditLog from "../pages/Setting/Pages/AuditLog/AuditLogSetting";
import Formula from "../pages/Setting/Pages/Formula/FormulaSetting";
import { StaffDetail13 } from "../pages/KPI/ST/Detail13";

// const Inquiry = lazy(() => import('./app-reimport'));
// const Report = lazy(() => import('./app-reimport'));
// const Setting = lazy(() => import('./app-reimport'));
// const Task = lazy(() => import('./app-reimport'));
// const Dashboard = lazy(() => import('./app-reimport'));
// const Profile = lazy(() => import('./app-reimport'));
// const Balance = lazy(() => import('./app-reimport'));
// const Partner = lazy(() => import('./app-reimport'));
// const Services = lazy(() => import('./app-reimport'));
import { Organizations } from "../pages/Report/List/Organization/Organizations";
import { Commissions } from "../pages/Services/Commissions/Commissions";
import { useStore } from "../utils/useStores";
import { DashBoardNew } from "../pages/Dashboard/DashBoardNew";
import { AdjustTime } from "../pages/Setting/Pages/AdjustTime/AdjustTime";
import { EntryDetailStaff } from "../pages/KPI/ST/EntryDetail";
import { Permit } from "../pages/Permit/Permit";
import { FormPermit } from "../pages/Permit/FormPermit";
import { DetailPermit } from "../pages/Permit/DetailPermit";

// event menu
import Event from "../pages/Event/Event.js";
// import { FormEvent } from "../pages/Event/FormEvent.js";

export const AppRoutes = () => {
  const store = useStore();
  const role = store.userData.role;
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <AnimatedSwitch
        atEnter={{ opacity: 0 }}
        atLeave={{ opacity: 0 }}
        atActive={{ opacity: 1 }}
        className="switch-wrapper-app"
      >
        {/* <Route path="/app" exact>
          <DashBoardNew />
        </Route> */}
        <Route path="/app" key={"Dashboard"} exact>
          <Dashboard />
        </Route>
        {/* <Route path={LINKS.CLIENT} key={"Client"} exact>
          <Client />
        </Route>
        <Route path={LINKS.DETAIL_CLIENT} key={"DetailClient"} exact>
          <DetailClient />
        </Route>
        <Route path={LINKS.EDIT_CLIENT} key={"update-client"} exact>
          <FormClient mode="edit"/>
        </Route> */}
        <Route path="/app/jadwal" key={"Jadwal"} exact>
          <Jadwal />
        </Route>
        <Route path={LINKS.CONTRACT} exact>
          <Contract />
        </Route>
        <Route path={LINKS.FORM_CONTRACT} exact>
          <FormContract />
        </Route>
        <Route path={LINKS.EDIT_CONTRACT} exact>
          <FormContract mode="edit" />
        </Route>
        <Route path={LINKS.DETAIL_CONTRACT} key={"DetailContract"} exact>
          <DetailContract />
        </Route>
        <Route path="/app/tasks" exact>
          <Task />
        </Route>
        <Route path="/app/calendar" exact>
          <Calendar />
        </Route>
        <Route path="/app/profile" exact>
          <Profile />
        </Route>
        <Route path="/app/inquiry" exact>
          <Inquiry />
        </Route>
        <Route path={LINKS.PARTNER}>
          <Partner />
        </Route>
        <Route path={LINKS.KPI} exact>
          <Kpi />
        </Route>
        <Route path={LINKS["DAILY TRAFFIC VOLUME"]}>
          <DailyTraffic />
        </Route>
        <Route path={LINKS.CASHFLOW} exact>
          <Cashflow />
        </Route>
        <Route path={LINKS["FINANCIAL SUMMARY"]} exact>
          <FinancialSummary />
        </Route>
        <Route path="/app/reports/list/Organizations" exact>
          <DivisionDepartement />
        </Route>
        <Route path="/app/reports/list/Users" exact>
          <Users />
        </Route>
        <Route path={LINKS.CORPORATE} exact>
          <Corporate />
        </Route>
        {/* {role !== "analyst" && role !== "staff" && (
          <Route path={LINKS.DIVISIONS} exact>
            <Divisions />
          </Route>
        )} */}
        <Route path={LINKS.DEPARTMENTS} exact>
          <Departments />
        </Route>
        <Route path={LINKS.STAFF} exact>
          <Staffs />
        </Route>
        <Route path={LINKS["KEY METRICS"]} exact>
          <KeyMetrics />
        </Route>
        <Route path="/app/Kpi/commissions" exact>
          <Commissions />
        </Route>
        <Route path={LINKS.ACCIDENT} exact>
          <Accident />
        </Route>
        <Route path={LINKS["FULL ROUTE EQUIVALENTE"]} exact>
          <FullRouteEquivalente />
        </Route>
        <Route path={LINKS["AVERAGE VEHICLE KM TRAVELLED"]} exact>
          <AverageVehicleKM />
        </Route>
        <Route path={LINKS["TRAFFIC CLASSIFICATION"]} exact>
          <TrafficClassification />
        </Route>

        <Route path={LINKS["EXECUTIVE SUMMARY"]} exact>
          <ExecutiveSummary />
        </Route>
        <Route path={LINKS["DAILY REVENUE"]} exact>
          <DailyRevenue />
        </Route>
        <Route path={LINKS["BALANCE SHEET"]} exact>
          <BalanceSheet />
        </Route>
        <Route path={LINKS["DIVISION & DEPARTEMENT"]} exact>
          <DivisionDepartement />
        </Route>
        {/* service route */}
        {/* <Route path={LINKS.SERVICE}>
          <Services />
        </Route> */}
        <Route path={LINKS.SETTING} exact>
          <Setting />
        </Route>
        <Route path={LINKS.BALANCE} exact>
          <Balance />
        </Route>
        <Route path={LINKS.REPORT} exact>
          <Report />
        </Route>
        <Route path={LINKS["KEY METRICS DATA"]} exact>
          <UploadKey />
        </Route>
        <Route path={LINKS["OPERATIONAL DATA UPLOAD"]} exact>
          <UploadKPI />
        </Route>
        <Route path={LINKS["ENTRY CORPORATE"]} exact>
          <EntryCorporate />
        </Route>
        <Route path={LINKS["ENTRY DEPARTMENT"]} exact>
          <DepartEntryKPI />
        </Route>
        <Route path={LINKS["ENTRY DEPARTMENT GOLONGAN 1-3"]} exact>
          <DepartGolongan13 />
        </Route>
        <Route
          path={LINKS["ENTRY DEPARTMENT"] + "/:id_divisi" + "/:id_depart"}
          exact
        >
          <DepartGol45 />
        </Route>
        {/* { role !== "analyst" && role !== "staff" &&  */}
        <Route path={LINKS["ENTRY DIVISI"]} exact>
          <DivisiEntryKPI />
        </Route>

        {role !== "analyst" && role !== "staff" && (
          <Route path={LINKS["ENTRY DIVISI GOLONGAN 1-3"]} exact>
            <DivisiGol13 />
          </Route>
        )}
        {role !== "analyst" && role !== "staff" && (
          <Route path={LINKS["ENTRY DIVISI"] + "/:id"} exact>
            <DivisiGol45 />
          </Route>
        )}
        <Route
          path={LINKS["ENTRY STAFF"] + "/:id_divisi" + "/:id_depart"}
          exact
        >
          <EntryStaff />
        </Route>
        <Route path={LINKS["STAFF ENTRY KPI"]} exact>
          <StaffEntryKPI />
        </Route>
        {/* <Route path={LINKS["ENTRY STAFF"]} exact>

        </Route> */}
        <Route
          path={
            LINKS["ENTRY STAFF GOLONGAN 1-3"] + "/:id_divisi" + "/:id_depart"
          }
          exact
        >
          <StaffGol13 />
        </Route>
        <Route
          path={
            LINKS["ENTRY STAFF GOLONGAN 4-5"] + "/:id_divisi" + "/:id_depart"
          }
          exact
        >
          <StaffGol45 />
        </Route>
        <Route path={LINKS["DETAIL DIVISI"] + "/:id"} exact>
          <DivisionDetailKPI />
        </Route>
        <Route path={LINKS["DETAIL DEPARTMENT"] + "/:id"} exact>
          <DepartmentDetailKPI />
        </Route>
        <Route path={LINKS["ENTRY DETAIL STAFF"] + "/:id"} exact>
          <EntryDetailStaff />
        </Route>
        <Route path={LINKS["DETAIL STAFF GOL 4-5"] + "/:id"} exact>
          <StaffDetailKPI />
        </Route>
        <Route path={LINKS["DETAIL STAFF GOL 1-3"] + "/:id"} exact>
          <StaffDetail13 />
        </Route>
        <Route path={LINKS["OPERATIONAL DATA"]} exact>
          <OperationalData />
        </Route>
        <Route path={LINKS["OPERATIONAL DATA"] + "/:id"} exact>
          <OperationalDataDetail />
        </Route>
        <Route path={LINKS.SETTING + "/users"} exact>
          <Users />
        </Route>
        <Route path={LINKS.SETTING + "/vision_mission"} exact>
          <VisionMission />
        </Route>
        <Route path={LINKS.SETTING + "/audit-log"} exact>
          <AuditLog />
        </Route>
        <Route path={LINKS.SETTING + "/formula"} exact>
          <Formula />
        </Route>
        <Route path={LINKS.SETTING + "/roles"} exact>
          <Roles />
        </Route>
        {role === "super_admin" && (
          <Route path={LINKS["ADJUST TIME"]} exact>
            <AdjustTime />
          </Route>
        )}

        {/* Employee Routes */}
        <Route path={LINKS.EMPLOYEE} exact>
          <Employee />
        </Route>
        <Route path={LINKS.FORM_EMPLOYEE} exact>
          <FormEmployee />
        </Route>
        <Route path={LINKS.FORM_EDIT_EMPLOYEE} key={"Update Employee"} exact>
          <FormEmployee mode="edit" />
        </Route>
        <Route path={LINKS.DETAIL_EMPLOYEE} key={"DetailEMPLOYEE"} exact>
          <DetailEmployee />
        </Route>

        {/* Event Routes */}
        <Route path={LINKS.EVENT} exact>
          <Event />
        </Route>
        {/* <Route path={LINKS.FORM_CONTRACT} exact>
          <FormEvent />
        </Route> */}

        {/* Permission Routes */}
        <Route path={LINKS.PERMIT} exact>
          <Permit />
        </Route>
        <Route path={LINKS.FORM_PERMIT} exact>
          <FormPermit />
        </Route>
        <Route path={LINKS.DETAIL_PERMIT} key={"DetailPermit"} exact>
          <DetailPermit />
        </Route>

        <Route path={LINKS.DIVISIONS} exact>
          <DivisionSIBO />
        </Route>
        <Route path={LINKS.ACCIDENT} exact>
          <Accident />
        </Route>
      </AnimatedSwitch>
    </Suspense>
  );
};
