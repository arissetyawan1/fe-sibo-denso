import { HomeOutlined, PlusSquareOutlined } from '@ant-design/icons';
import React from 'react';
import { LINKS } from '../../routes';
import {
    Link,
  } from "react-router-dom";
import {
  Button,
  Breadcrumb,
  Row,
  Col,
  PageHeader
} from "antd";
export const PageHeaderComponent = (props) => {
    const {
        title,
        breadCrumb=[],
        actions=[]
    } = props;

    const renderBreadCrumb = (bc) => {
        if(bc.link){
            return (
                <Link to={bc.link}>
                    {bc.icon}{bc.name}
                </Link>
            )
        } else {
            return <React.Fragment>{bc.icon}{bc.name}</React.Fragment>
        }
    }

    const breadCrumbProp = breadCrumb.map(bc => {
        return (
            <Breadcrumb.Item style={bc.styles}>
                {renderBreadCrumb(bc)}
            </Breadcrumb.Item>
        )
    })

    const breadCrumbComponent = [
        <Breadcrumb.Item>
            <Link to={LINKS.DASHBOARD}>
                <HomeOutlined />
            </Link>
        </Breadcrumb.Item>
    ].concat(breadCrumbProp)

    return (
        <React.Fragment>
             <Breadcrumb style={{ marginLeft: 12 }}>
                 {breadCrumbComponent}
            </Breadcrumb>
            <Row>
                <Col flex="auto">
                <PageHeader
                    className={"card-page-header"}
                    title={title}
                    subTitle=""
                />
                </Col>

                <Col flex="100px">
                <PageHeader
                    className={"card-page-header"}
                    title={
                        actions
                    }
                    subTitle=""
                />
                </Col>
            </Row>
        </React.Fragment>
    )
}