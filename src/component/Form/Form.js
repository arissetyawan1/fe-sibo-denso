import React, { useEffect, useState } from "react";
import { TreeSelectComp } from '../TreeSelect';
import { Modal, Form, Input, message, DatePicker, Divider } from "antd";

export const FormMarketingComponent = (props) => {
    const {
        defaultData,
        mode = "add",
        title,
        visible = false,
        onClose = () => { },
        onSave = () => { },
        forms = [],
    } = props;

    useEffect(() => {
        if (mode === "add") {
            form.resetFields();
        }
    }, [mode]);

    useEffect(() => {
        form.setFieldsValue(defaultData);
    }, [defaultData]);

    const [isLoading, setLoading] = useState(false);

    const [form] = Form.useForm();

    const submit = () => {
        setLoading(true);
        form
            .validateFields()
            .then(async (res) => {
                const savedData = mode === "edit" ? { ...defaultData } : {};
                try {
                    await onSave({
                        ...savedData,
                        ...res,
                    });
                    form.resetFields();
                    close();
                    message.success("Berhasil menyimpan");
                    setLoading(false);
                } catch (err) {
                    setLoading(false);
                    message.error("Gagal menyimpan");
                    console.log(err);
                }
            })
            .catch((err) => {
                setLoading(false);
            });
    };

    const close = () => {
        onClose();
    };

    const renderFormInput = (formItem) => {
        switch (formItem.type) {
            case "input":
                return <Input autoComplete="off" />;
            case "datepicker":
                return <DatePicker />;
            // case "treeselect":
            //   return <TreeSelectComp />;
            default:
                return <Input />;
        }
    };

    const formItems = forms.map((item) => {
        const rules = [{ required: true, ...(item.rules || {}) }];
        return (
            <Form.Item label={item.label} name={item.name} rules={rules}>
                {renderFormInput(item)}
            </Form.Item>
        );
    });

    return (
        <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{ remember: true }}
            onFinish={submit}
            // onFinishFailed={onFinishFailed}
            autoComplete="off"
        >
            <Form form={form}>{formItems}</Form>
        </Form>
    )
}