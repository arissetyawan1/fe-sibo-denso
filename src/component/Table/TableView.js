import React, { useCallback, useState } from "react";
import { observer } from "mobx-react";
import {
  Button,
  Col,
  Input,
  Row,
  Table,
  Typography,
  message,
  Breadcrumb,
} from "antd";
import SearchOutlined from "@ant-design/icons/SearchOutlined";
import { debounce, get as getLodash } from "lodash";
import { appConfig } from "../../config/app";
import { Link } from "react-router-dom";
import { LINKS } from "../../routes";
import { HomeOutlined } from "@ant-design/icons";
const { Title } = Typography;
export const TableView = (props) => {
  const {
    placeholder,
    title,
    history,
    columns = [],
    dataSource = [],
    onSearch = () => {},
    onClickAdd = () => {},
    onClickFilter = () => {},
    onRowClick = (r, i) => {},
    showToolbar = true,
    showTitle = true,
    showAddBtn = true,
    showFilterBtn = false,
    showBreadCrumb = true,
    searchKeys = ["name"],
    searchBarFull = false,
    pagination,
    breadCrumb = [],
    bordered = true,
  } = props;

  const [searchText, setSearchText] = useState(undefined);

  const onTypingStart = (e) => {
    setSearchText(e.target.value);
  };

  const filteredData = dataSource.filter((prop) => {
    if (!searchText) {
      return true;
    }
    let isFounds = false;
    searchKeys.forEach((key) => {
      const value = getLodash(prop, key, "dd")?.toLowerCase();
      const index = value?.indexOf(searchText.toLowerCase());
      const validity = index > -1;
      if (validity) {
        isFounds = true;
      }
    });
    return isFounds;
  });

  const getSpan = () => {
    if (searchBarFull) {
      return 24;
    } else if (showFilterBtn && showAddBtn) {
      return 16;
    }
    return 18;
  };

  const onRowProps = (record, index) => {
    return {
      onClick: (event) => {
        onRowClick(record, index);
      },
    };
  };

  const activeButtonCount = [showAddBtn, showFilterBtn].filter(
    (prop) => !!prop
  ).length;
  let searchBarSpan, buttonSpan;

  switch (activeButtonCount) {
    case 0: {
      searchBarSpan = 24;
      buttonSpan = 0;
      break;
    }
    case 1: {
      searchBarSpan = 18;
      buttonSpan = 6;
      break;
    }
    case 2: {
      buttonSpan = 4;
      searchBarSpan = 16;
      break;
    }
    case 3: {
      buttonSpan = 4;
      searchBarSpan = 12;
      break;
    }
    default: {
      buttonSpan = 4;
      searchBarSpan = 10;
      break;
    }
  }

  const renderBreadCrumb = (bc) => {
    if (bc.link) {
      return (
        <Link to={bc.link}>
          {bc.icon}
          {bc.name}
        </Link>
      );
    } else {
      return (
        <React.Fragment>
          {bc.icon}
          {bc.name}
        </React.Fragment>
      );
    }
  };

  const breadCrumbProp = breadCrumb.map((bc) => {
    return (
      <Breadcrumb.Item style={bc.styles}>
        {renderBreadCrumb(bc)}
      </Breadcrumb.Item>
    );
  });

  const breadCrumbComponent = [
    <Breadcrumb.Item>
      <Link to={LINKS.DASHBOARD}>
        <HomeOutlined />
      </Link>
    </Breadcrumb.Item>,
  ].concat(breadCrumbProp);

  return (
    <div>
      {showBreadCrumb && (
        <Breadcrumb style={{ marginLeft: 12 }}>
          {breadCrumbComponent}
        </Breadcrumb>
      )}
      {showTitle && <Row style={{ marginBottom: 16 }}>{title}</Row>}
      {showToolbar && (
        <Row gutter={16} style={{ marginBottom: 20 }}>
          <Col span={searchBarSpan}>
            <Input
              placeholder={placeholder || "Search Here..."}
              prefix={
                <SearchOutlined
                  style={{ marginRight: 10, color: "rgba(0, 0, 0, 0.25)" }}
                />
              }
              onChange={onTypingStart}
              value={searchText}
            />
          </Col>
          {showFilterBtn && (
            <Col span={buttonSpan}>
              <Button
                htmlType="submit"
                type={"primary"}
                style={{ width: "100%", borderRadius: 10, color: "#fff" }}
                onClick={onClickFilter}
              >
                Filter
              </Button>
            </Col>
          )}
          {showAddBtn && (
            <Col span={buttonSpan}>
              <Button className={"btnAdd"} onClick={onClickAdd}>
                Add
              </Button>
            </Col>
          )}
        </Row>
      )}
      <Row gutter={16}>
        <Col span={24}>
          <Table
            onRow={onRowProps}
            dataSource={filteredData}
            columns={columns}
            scroll={{ x: "max-content" }}
            bordered={bordered}
            pagination={pagination}
            className={"styleTable"}
          />
        </Col>
      </Row>
    </div>
  );
};
