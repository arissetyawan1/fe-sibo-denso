import React from 'react'
import { TreeSelect } from "antd";
import { inject, observer } from "mobx-react";
// import { useStore } from "../../utils/useStores";

const { TreeNode } = TreeSelect;

export const TreeSelectComp = observer((initialData) => {
    // const { division_sibo: divisionSIBOStore, ui: uiStore } = useStore();

    // const data = divisionSIBOStore.data
    //     .map((prop) => prop)
    //     .sort((a, b) => parseFloat(a.code) - parseFloat(b.code));

    // const tableData = [];
    // const treeParent = data.map((it) => {
    //     const newCode = it.code || "";
    //     if (newCode.substr(3, 4) === "0") {
    //         const data = {
    //             title: it.name,
    //             key: newCode.substr(0, 2),
    //             children: [],
    //         };

    //         tableData.push(data);
    //     }
    // });

    // const treeChildren = data.map((it) => {
    //     const newCode = it.code || "";
    //     if (newCode.substr(3, 4) > 0) {
    //         for (let i = 0; i < tableData.length; i++) {
    //             if (tableData[i].key === newCode.substr(0, 2)) {
    //                 const data = {
    //                     title: it.name,
    //                     key: newCode.substr(0, 2).concat(newCode.substr(3, 4)),
    //                 };

    //                 tableData[i].children.push(data);
    //             }
    //         }
    //     }
    // });

    return (
        <TreeSelect
            showSearch
            style={{ width: '100%' }}
            // value={value}
            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }
            }
            placeholder="Please select"
            allowClear
            treeDefaultExpandAll
        // onChange={onChange}
        >
            <TreeNode value="parent 1" title="parent 1">
                <TreeNode value="parent 1-0" title="parent 1-0">
                    <TreeNode value="leaf1" title="leaf1" />
                    <TreeNode value="leaf2" title="leaf2" />
                </TreeNode>
                <TreeNode value="parent 1-1" title="parent 1-1">
                    <TreeNode value="leaf3" title={<b style={{ color: '#08c' }}>leaf3</b>} />
                </TreeNode>
            </TreeNode>
        </TreeSelect >
    )
});