import React, { useEffect, useState } from "react";
import { Button, DatePicker, Modal, Radio, Select } from "antd";

export const ModalFilter = (props) => {
  const { RangePicker } = DatePicker;
  const { Option } = Select;
  const [dateRangeFilter, setDateRangeFilter] = useState(undefined);
  const [date, setDateString] = useState(undefined);
  const defaultModel = props.model;
  const defaultColumn = ".created_at";
  const [order, setOrder] = useState("-" + defaultModel + defaultColumn);
  const [divisionId, setDivisionId] = useState(undefined);
  const [positionId, setPositionId] = useState(undefined);
  const [loading, setLoading] = useState(false);

  const {
    title = "",
    visible = false,
    visibleFilterOrder = true,
    visibleFilterDivision = false,
    visibleFilterPosition = false,
    divisionOption,
    positionOption,
    onCancel = () => {},
    onOk = () => {},
    handleFilterSort = () => {},
    handleFilterDate = (value) => {},
    handleFilterCompanies = () => {},
  } = props;

  const submit = () => {
    onOk({
      order,
      divisionId,
      positionId,
    });
  };

  const reset = () => {
    const defOrder = defaultModel + defaultColumn;
    const divisionId = undefined;
    const positionId = undefined;
    setOrder(defOrder);
    setDivisionId(divisionId);
    setPositionId(positionId);
    onOk({
      order: defOrder,
      id_division: divisionId,
      id_position: positionId,
    });
  };

  console.log(divisionOption, "oxoxoxxoxo");

  const divisionList = divisionOption?.map((prop) => {
    return (
      <Select.Option key={prop.id} value={prop.id}>
        {prop.name}
      </Select.Option>
    );
  });

  const filterPosition = positionOption.filter(
    (it) => it.id_division === divisionId
  );

  console.log(filterPosition, "lklaasa");

  const positionList = filterPosition?.map((prop) => {
    return (
      <Select.Option key={prop.id} value={prop.id}>
        {prop.name}
      </Select.Option>
    );
  });

  return (
    <Modal
      visible={visible}
      closable={false}
      bodyStyle={{ backgroundColor: "#fff" }}
      onCancel={onCancel}
      onOk={submit}
      footer={[
        <Button key="cancel" onClick={onCancel}>
          Cancel
        </Button>,
        <Button key="reset" onClick={reset}>
          Reset
        </Button>,
        <Button key="submit" loading={loading} type="primary" onClick={submit}>
          Submit
        </Button>,
      ]}
      style={{ width: "20rem" }}
    >
      <div>
        <h5>{title}</h5>
        <hr />
        {visibleFilterOrder && (
          <>
            <h6 style={{ marginBottom: 8 }}>Order By</h6>
            <div style={{ marginBottom: 8 }}>
              <Radio.Group
                name="month"
                onChange={(e) => setOrder(e.target.value)}
                value={order}
              >
                <Radio value={"3"}>Expired in 3 months</Radio>
                <Radio value={"1"}>Expired in 1 month</Radio>
              </Radio.Group>
            </div>
          </>
        )}
        {visibleFilterDivision && (
          <>
            <h6 style={{ marginBottom: 8 }}>Division</h6>
            <Select
              placeholder={"Select Division..."}
              onChange={(val) => setDivisionId(val)}
              style={{ width: "100%", marginBottom: 8 }}
            >
              {divisionList}
            </Select>
          </>
        )}
        {visibleFilterPosition && (
          <>
            <h6 style={{ marginBottom: 8 }}>Position</h6>
            <Select
              mode={"multiple"}
              allowClear
              placeholder={"Select Division..."}
              onChange={(val) => setPositionId(val)}
              style={{ width: "100%" }}
            >
              {positionList}
            </Select>
          </>
        )}
      </div>
    </Modal>
  );
};
