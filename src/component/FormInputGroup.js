import {Col, FormGroup, Input, Label, Row, } from "reactstrap";
import React from "react";

export const FormInputGroup = (props) => {

    const textInputComponent = <Input {...(props.inputProps || {})} type={props.type} name={props.name}/>
    const selectInputComponent = (<Input {...(props.inputProps || {})} type={props.type} name={props.name}>
        {props.type === 'select' && (props.options || []).map(o => <option value={o.key}>{o.label}</option>)}
    </Input>);

    const componentMapping = {
        text: textInputComponent,
        select: selectInputComponent,
    };

    return (
        <Row>
            <Col xs="12">
                <FormGroup>
                    <Label htmlFor="name">{props.title}</Label>
                    {props.inputComponent ? props.inputComponent : componentMapping[props.type || 'text']}
                </FormGroup>
            </Col>
        </Row>
    )
};
